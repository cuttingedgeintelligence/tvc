﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class CPTCodeDA : BaseDA<CPTCode>
    {

        #region Stored procedure names

        private const string STP_CPTCODES_GET_ALL = "dbo.stp_CPTCodes_GetAll";
        private const string STP_CPTCODES_INSERT = "dbo.stp_CPTCodes_Insert";
        private const string STP_CPTCODES_UPDATE = "dbo.stp_CPTCodes_Update";
        private const string STP_CPTCODES_DELETE = "dbo.stp_CPTCodes_Delete";
       
        
        #endregion //Stored procedure names

        #region DataTable

        public DataTable GetAllCPTCodes()
        {
            return base.GetDataTable(STP_CPTCODES_GET_ALL);
        }

        #endregion //DataTable

        #region DB CUD methods


        public bool InsertCPTCode(CPTCode cptCode)
        {

            SqlParameter[] parameters = 
                {
                  
                  new SqlParameter("@code", cptCode.Code)
                  ,new SqlParameter("@codeType", cptCode.CodeType)
                 ,new SqlParameter("@description", cptCode.Description)
                
                };

            return base.CMDExecuteNonQuery(STP_CPTCODES_INSERT, parameters) > 0;

        }



        public bool UpdateCPTCode(CPTCode cptCode)
        {

            SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",cptCode.Id)
                 ,new SqlParameter("@code", cptCode.Code)
                 ,new SqlParameter("@codeType", cptCode.CodeType)
                 ,new SqlParameter("@description", cptCode.Description)
                };

            return base.CMDExecuteNonQuery(STP_CPTCODES_UPDATE, parameters) > 0;

        }


        public bool DeleteCPTCode(int id)
        {

            SqlParameter[] parameters = {
                  new SqlParameter("@id",id) };

            return base.CMDExecuteNonQuery(STP_CPTCODES_DELETE, parameters) > 0;

        }




        #endregion  //DB CUD methods

        #region Mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            hashTable.Add("Id", "@id");
            hashTable.Add("Code", "@code");
            hashTable.Add("Description", "@description");
            return hashTable;
        }

        protected override CPTCode FillProperties(SqlDataReader dataReader)
        {
            return null;


        }

        #endregion //Mapping methods


    }
}
