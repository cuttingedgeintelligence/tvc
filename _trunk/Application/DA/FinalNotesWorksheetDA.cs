using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class FinalNotesWorksheetDA : BaseDA<FinalNotesWorksheet>
    {
        #region cache keys


        #endregion //cache keys

        #region Stored procedure names

        
        private const string STP_PATIENTLOGS_USERS_GET_FINAL_NOTES_WORKSHEETS_SEARCH = "dbo.stp_PatientLogs_Users_GetFinalNotesWorksheetsSearch";
        private const string STP_PATIENTLOGS_UPDATE_STATUS_BY_TECHNICIAN = "dbo.stp_PatientLogs_UpdateStatusByTechnician";
        private const string STP_PATIENTLOGS_PATIENTLOGSPATIENTS_GET_FOLLOW_UP_PATIENTS = "dbo.stp_PatientLogs_PatientLogsPatients_GetFollowUpPatients";

        private const string STP_FOLLOWUPS_INSERT = "dbo.stp_FollowUps_Insert";
        private const string STP_FOLLOWUPPATIENTS_INSERT = "dbo.stp_FollowUpPatients_Insert";

        private const string STP_FOLLOWUPS_USERS_GET_FOLLOW_UPS_SEARCH = "dbo.stp_FollowUps_Users_GetFollowUpsSearch";
        private const string STP_PATIENTS_FOLLOWUPPATIENTS_GET_FOLLOW_UP_PATIENTS_BY_FOLLOW_UP_ID = "dbo.stp_Patients_FollowUpPatients_GetFollowUpPatientsByFollowUpId";


        private const string STP_PATIENTLOGS_PATIENTLOGSPATIENTS_USERS_GET_MONTHLY_REPORT_BY_PRACTICE_ID_AND_YEAR = "dbo.stp_PatientLogs_PatientLogsPatients_Users_GetMonthlyReportByPracticeIdAndYear";
        private const string STP_PATIENTLOGS_PATIENTLOGSPATIENTS_USERS_GET_YEAR_TO_YEAR_REPORT = "dbo.stp_PatientLogs_PatientLogsPatients_Users_GetYearToYearReport";

        private const string STP_PATIENTLOGS_USERS_GET_COMPLETED_FINAL_NOTES_WORKSHEETS_SEARCH = "dbo.stp_PatientLogs_Users_GetCompletedFinalNotesWorksheetsSearch";


        private const string STP_PATIENTLOGS_PATIENTLOGSPATIENTS_PATIENTS_GET_PATIENTS_HISTORY_BY_PATIENT_LOG_ID = "dbo.stp_PatientLogs_PatientLogsPatients_Patients_GetPatientsHistoryByPatientLogId";

        private const string STP_PATIENTLOGS_PATIENTLOGSPATIENTS_INSURANCECOMPANIES_GET_PER_PRACTICE_REPORT = "dbo.stp_PatientLogs_PatientLogsPatients_InsuranceCompanies_GetPerPracticeReport";

        private const string STP_PATIENTLOGS_PATIENTLOGSPATIENTS_GET_PATIENT_HISTORY_BY_PATIENT_ID =  "dbo.stp_PatientLogs_PatientLogsPatients_GetPatientHistoryByPatientId";
		#endregion //Stored procedure names


        #region DB access methods

    
        #endregion //DB access methods

        #region DataTable


        public DataTable GetFinalNotesWorksheetsSearch(Guid technicianId,Guid practiceId,PatientLogStatusEnum status, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[6];


            if (technicianId.Equals(Guid.Empty))
                parameters[0] = new SqlParameter("@technicianId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@technicianId", technicianId);

            if (practiceId.Equals(Guid.Empty))
                parameters[1] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@practiceId", practiceId);

            parameters[2] = new SqlParameter("@status", status);

            parameters[3] = new SqlParameter("@pageSize", pageSize);
            parameters[4] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[5] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[5].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_PATIENTLOGS_USERS_GET_FINAL_NOTES_WORKSHEETS_SEARCH);

            totalCount = (int)parameters[5].Value;

            return table;
        }



        public DataTable GetCompletedFinalNotesWorksheetsSearch(Guid technicianId, Guid practiceId, PatientLogStatusEnum status, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[6];


            if (technicianId.Equals(Guid.Empty))
                parameters[0] = new SqlParameter("@technicianId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@technicianId", technicianId);

            if (practiceId.Equals(Guid.Empty))
                parameters[1] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@practiceId", practiceId);

            parameters[2] = new SqlParameter("@status", status);

            parameters[3] = new SqlParameter("@pageSize", pageSize);
            parameters[4] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[5] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[5].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_PATIENTLOGS_USERS_GET_COMPLETED_FINAL_NOTES_WORKSHEETS_SEARCH);

            totalCount = (int)parameters[5].Value;

            return table;
        }



        public DataTable GetFollowUpsSearch(Guid practiceId,int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[4];
         
            if (practiceId.Equals(Guid.Empty))
                parameters[0] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@practiceId", practiceId);


            parameters[1] = new SqlParameter("@pageSize", pageSize);
            parameters[2] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[3] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[3].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_FOLLOWUPS_USERS_GET_FOLLOW_UPS_SEARCH);

            totalCount = (int)parameters[3].Value;

            return table;
        }



        public DataTable GetFollowUpPatientsByFollowUpId(int followUpId)
        {
            SqlParameter[] parameters = {new SqlParameter("@followUpId", followUpId) };
            return base.GetDataTable(parameters, STP_PATIENTS_FOLLOWUPPATIENTS_GET_FOLLOW_UP_PATIENTS_BY_FOLLOW_UP_ID);
        }


        /// <summary>
        ///  get follow up patients for the next appointment date
        /// </summary>
        /// <param name="practiceId"></param>
        /// <param name="retinaExamDate"></param>
        /// <param name="glaucomaExamDate"></param>
        /// <returns></returns>
        public DataTable GetFollowUpPatientsForNextAppointmentDate(Guid practiceId,DateTime retinaExamDate,DateTime glaucomaExamDate)
        {
           
             SqlParameter[] parameters = 
             {
               new SqlParameter("@retinaExamDate",retinaExamDate) 
              ,new SqlParameter("@glaucomaExamDate",glaucomaExamDate)
              ,new SqlParameter("@practiceId",practiceId)
              ,new SqlParameter("@finalNotestWorksheetStatusId",PatientLogStatusEnum.Final__notes__worksheet)
              ,new SqlParameter("@examinedPatientStatusId ",PatientLogPatientStatusEnum.Examined)
             };
             return base.GetDataTable(parameters, STP_PATIENTLOGS_PATIENTLOGSPATIENTS_GET_FOLLOW_UP_PATIENTS);
        }



        public DataTable GetMonthlyReportByPracticeIdAndYear(Guid practiceId,DateTime year)
        {
            SqlParameter[] parameters = new SqlParameter[4];

            if (practiceId.Equals(Guid.Empty))
                parameters[0] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@practiceId", practiceId);


            parameters[1] = new SqlParameter("@year", year);
            parameters[2] = new SqlParameter("@status",PatientLogStatusEnum.Final__notes__worksheet);
            parameters[3] = new SqlParameter("@patientLogPatientStatus", PatientLogPatientStatusEnum.Examined);
            return base.GetDataTable(parameters, STP_PATIENTLOGS_PATIENTLOGSPATIENTS_USERS_GET_MONTHLY_REPORT_BY_PRACTICE_ID_AND_YEAR);
        }


        public DataTable GetYearToYearReport(PatientLogStatusEnum patientLogStatus,PatientLogPatientStatusEnum patientLogPatientStatus)
        {
            SqlParameter[] parameters = 
            {
                 new SqlParameter("@patientLogStatus", patientLogStatus)
                ,new SqlParameter("@patientLogPatientStatus", patientLogPatientStatus)
            };
            return base.GetDataTable(parameters, STP_PATIENTLOGS_PATIENTLOGSPATIENTS_USERS_GET_YEAR_TO_YEAR_REPORT);
        }


        /// <summary>
        /// Get patients history by patient log id
        /// </summary>
        /// <param name="patientLogId"></param>
        /// <param name="patientLogStatus"></param>
        /// <returns>Data table of patients history data [ patient name, service date... ]</returns>
        public DataTable GetPatientsHistoryByPatientLogId(int patientLogId, PatientLogStatusEnum patientLogStatus)
        {
            SqlParameter[] parameters = 
            {
                 new SqlParameter("@patientLogId",patientLogId)
                ,new SqlParameter("@patientLogStatus", patientLogStatus)
            };
            return base.GetDataTable(parameters, STP_PATIENTLOGS_PATIENTLOGSPATIENTS_PATIENTS_GET_PATIENTS_HISTORY_BY_PATIENT_LOG_ID);
        }



        /// <summary>
        /// Get per practice report
        /// </summary>
        /// <param name="practiceId"></param>
        /// <returns>data table </returns>
         public DataTable GetPerPracticeReport(Guid practiceId)
        {
            SqlParameter[] parameters = 
            {
                 new SqlParameter("@practiceId",practiceId)
                ,new SqlParameter("@patientLogStatus", PatientLogStatusEnum.Final__notes__worksheet)
                ,new SqlParameter("@patientLogPatientExaminedStatus", PatientLogPatientStatusEnum.Examined)
            };
            return base.GetDataTable(parameters, STP_PATIENTLOGS_PATIENTLOGSPATIENTS_INSURANCECOMPANIES_GET_PER_PRACTICE_REPORT);
        }


        

        /// <summary>
        /// Get patient history by patient  id
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="patientLogStatus"></param>
        /// <returns>Data table of patient history data [  service date,practice notes , exam results etc... ]</returns>
        public DataTable GetPatientHistoryByPatientId(int patientId, PatientLogStatusEnum patientLogStatus)
        {
            SqlParameter[] parameters = 
            {
                 new SqlParameter("@patientId",patientId)
                ,new SqlParameter("@patientLogStatus", patientLogStatus)
            };
            return base.GetDataTable(parameters, STP_PATIENTLOGS_PATIENTLOGSPATIENTS_GET_PATIENT_HISTORY_BY_PATIENT_ID);
        }
        

        #endregion //DataTable

        #region DB CUD methods






        public bool UpdateFinalNotesWorksheet(FinalNotesWorksheet worksheet)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {
                foreach (FinalNotesWorksheetPatient finalNotesWorksheetPatient in worksheet.FinalNotesWorksheetPatients)
                {
                    new FinalNotesWorksheetPatientDA().UpdateFinalNotesWorksheetPatient(finalNotesWorksheetPatient, ref transaction); 
                }

                transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                transaction.Rollback();
               
            }
            finally
            {
                if (transaction.Connection != null)
                    transaction.Connection.Close();
            }

            return returnValue;

           

        }


        /// <summary>
        /// set patient log status to "finalNotesWorksheet" ;update final note worksheet patients; get followUpPatiens for the next appointment date 
        /// and insert follow up in the database
        /// </summary>
        /// <param name="patientLogId"></param>
        /// <param name="patientsList"></param>
        /// <param name="status"></param>
        /// <param name="retinaExamDate"></param>
        /// <param name="glaucomaExamDate"></param>
        /// <param name="practiceId"></param>
        /// <returns></returns>
        public bool UpdateFinalNotesWorksheetStatus(int patientLogId,List<FinalNotesWorksheetPatient> patientsList, PatientLogStatusEnum status,bool createFollowUp,DateTime nextAppointmentDate, DateTime retinaExamDate,DateTime glaucomaExamDate,Guid practiceId,Guid createdBy, Guid completedBy, DateTime completedOn)
        {
           
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@id",patientLogId) 
                  ,new SqlParameter("@status",status)
                  ,new SqlParameter("@completedById", completedBy)
                  ,new SqlParameter("@completedOn", completedOn)
                 };

                base.CMDExecuteNonQuery(STP_PATIENTLOGS_UPDATE_STATUS_BY_TECHNICIAN, parameters,ref transaction);


                foreach (FinalNotesWorksheetPatient finalNotesWorksheetPatient in patientsList)
                {
                    new FinalNotesWorksheetPatientDA().UpdateFinalNotesWorksheetPatient(finalNotesWorksheetPatient, ref transaction);
                }


              
                if (createFollowUp)
                {
                    //get follow up patients for the next appointment date
                     SqlParameter[] parametersGetFollowUp = 
                     {
                       new SqlParameter("@retinaExamDate",retinaExamDate) 
                      ,new SqlParameter("@glaucomaExamDate",glaucomaExamDate)
                      ,new SqlParameter("@practiceId",practiceId)
                      ,new SqlParameter("@finalNotestWorksheetStatusId",PatientLogStatusEnum.Final__notes__worksheet)
                      ,new SqlParameter("@examinedPatientStatusId ",PatientLogPatientStatusEnum.Examined)
                     };
                     DataTable dtFollowUpPatients = base.GetDataTable(parametersGetFollowUp, STP_PATIENTLOGS_PATIENTLOGSPATIENTS_GET_FOLLOW_UP_PATIENTS,ref transaction);
                     if (dtFollowUpPatients.Rows.Count > 0)
                     {

                         SqlParameter[] parametersFollowUp = new SqlParameter[5];
                         parametersFollowUp[0] = new SqlParameter("@id", SqlDbType.Int);
                         parametersFollowUp[0].Direction = ParameterDirection.Output;
                         parametersFollowUp[1] = new SqlParameter("@practiceId", practiceId);
                         parametersFollowUp[2] = new SqlParameter("@serviceDate", nextAppointmentDate);
                         parametersFollowUp[3] = new SqlParameter("@createdBy", createdBy);
                         parametersFollowUp[4] = new SqlParameter("@numOfPatients", dtFollowUpPatients.Rows.Count);

                         base.CMDExecuteNonQuery(STP_FOLLOWUPS_INSERT, parametersFollowUp, ref transaction);

                         int followUpId = (int)parametersFollowUp[0].Value;

                        
                         foreach (DataRow drFollowUpPatient in dtFollowUpPatients.Rows)
                         {
                             this.InsertFollowUpPatient(drFollowUpPatient, followUpId, ref transaction);
                         }
                         
                     }
                }
                if(transaction!= null)
                    transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                transaction.Rollback();

            }
            finally
            {
                if (transaction != null && transaction.Connection != null)
                    transaction.Connection.Close();
            }

            return returnValue;

        }

       

        

		
        #endregion  //DB CUD methods

      


       

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override FinalNotesWorksheet FillProperties(SqlDataReader dataReader)
        {
            return null;
			
        }

        #endregion //mapping methods

        #region Private methods

        private void InsertFollowUpPatient(DataRow drFollowUpPatient,int followUpId,ref SqlTransaction transaction)
        {
             SqlParameter[] parameters = 
             {
               new SqlParameter("@followUpId",followUpId) 
              ,new SqlParameter("@patientLogPatientId",(int)drFollowUpPatient["patientLogPatientId"])
              ,new SqlParameter("@patientLogServiceDate",(DateTime)drFollowUpPatient["serviceDate"])
              ,new SqlParameter("@patientId",(int)drFollowUpPatient["patientId"])
              ,new SqlParameter("@glaucoma ",(bool)drFollowUpPatient["glaucoma"])
              ,new SqlParameter("@retina ",(bool)drFollowUpPatient["retina"])
              ,new SqlParameter("@status ",(short)drFollowUpPatient["status"])
             };

            base.CMDExecuteNonQuery(STP_FOLLOWUPPATIENTS_INSERT, parameters, ref transaction);
        }

        #endregion //Private methods

    }
}
