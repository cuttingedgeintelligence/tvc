﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using CEI.Common;

namespace CEI.DA
{
	public class NameValueGuidDA : BaseDA<NameValueGuid>
	{
		#region cache keys

		//private const string CACHE_KEY_NAME_VALUES_USERSINROLE = "CACHE_KEY_NAME_VALUES:USERSINROLE";

		#endregion //cache keys

		#region stored procedure names

		//private const string STP_USERS_GETALL_INROLE = "dbo.stp_Users_GetAllInRole";

		#endregion //stored procedure names

		#region DB access methods

        //public List<NameValueGuid> GetAllUsersInRole(int roleId)
        //{
        //    List<NameValueGuid> listOfUsersInRole = base.GetFromCache(CACHE_KEY_NAME_VALUES_USERSINROLE + "_" + roleId.ToString()) as List<NameValueGuid>;
        //    if (listOfUsersInRole == null)
        //    {
        //        SqlParameter[] parameters = { new SqlParameter("@permissionId", roleId) };
        //        listOfUsersInRole = base.GetMultipleObjects(parameters, STP_USERS_GETALL_INROLE);

        //        base.InsertInCache(CACHE_KEY_NAME_VALUES_USERSINROLE + "_" + roleId.ToString(), listOfUsersInRole);
        //        return listOfUsersInRole;
        //    }
        //    else
        //        return listOfUsersInRole;
        //}

		#endregion //DB access methods

		#region mapping methods

		protected override Hashtable MapTable()
		{
			Hashtable hashTable = new Hashtable();
			hashTable.Add("Name", "@name");
			hashTable.Add("Id", "@id");
			return hashTable;
		}

		protected override NameValueGuid FillProperties(SqlDataReader dataReader)
		{
			NameValueGuid nameValueGuid = new NameValueGuid();

			for (int i = 0; i < dataReader.FieldCount; i++)
			{
				switch (dataReader.GetName(i))
				{
					case "id":
						nameValueGuid.IdGuid = (Guid)dataReader[i];
						break;
					case "name":
						nameValueGuid.Name = (string)dataReader[i];
						break;
				}
			}
			return nameValueGuid;
		}

		#endregion //mapping methods
	}
}
