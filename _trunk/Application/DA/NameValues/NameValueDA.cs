using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using CEI.Common;



namespace CEI.DA
{
    public class NameValueDA : BaseDA<NameValue>
    {
        #region cache keys


        private const string CACHE_KEY_DATATABLE_STATES = "CACHE_KEY_DATATABLE:STATES";
        private const string CACHE_KEY_DATATABLE_COUNTRIES = "CACHE_KEY_DATATABLE:COUNTRIES";
        private const string CACHE_KEY_DATATABLE_COUNTIES = "CACHE_KEY_DATATABLE:COUNTIES";
        private const string CACHE_KEY_DATATABLE_INSURANCE_COMPANIES = "CACHE_KEY_DATATABLE:INSURANCE_COMPANIES";

        
        #endregion //cache keys

        #region stored procedure names

    
		private const string STP_STATES_GET_ALL = "dbo.stp_States_GetAll";
        private const string STP_COUNTRIES_GET_ALL = "dbo.stp_Countries_GetAll";
        private const string STP_COUNTIES_GET_ALL = "dbo.stp_Counties_GetAll";
        private const string STP_INSURANCE_COMPANIES_GET_ALL = "dbo.stp_InsuranceCompanies_GetAll";

        private const string STP_INSURANCE_COMPANIES_INSERT = "dbo.stp_InsuranceCompanies_Insert";
        private const string STP_INSURANCE_COMPANIES_UPDATE = "dbo.stp_InsuranceCompanies_Update";
        private const string STP_INSURANCE_COMPANIES_DELETE = "dbo.stp_InsuranceCompanies_Delete";

        
        

        private const string STP_COUNTIES_INSERT = "dbo.stp_Counties_Insert";
        private const string STP_COUNTIES_UPDATE = "dbo.stp_Counties_Update";
        private const string STP_COUNTIES_DELETE = "dbo.stp_Counties_Delete";

        #endregion //stored procedure names

        #region DataTable

		public DataTable GetAllStates()
		{
			DataTable dtStates = base.GetFromCache(CACHE_KEY_DATATABLE_STATES) as DataTable;
            if (dtStates == null)
			{

                dtStates = base.GetDataTable(STP_STATES_GET_ALL);
                base.InsertInCache(CACHE_KEY_DATATABLE_STATES, dtStates);
                return dtStates;
			}
			else
                return dtStates;
        }


        public DataTable GetAllCountries()
        {
            DataTable dtCountries = base.GetFromCache(CACHE_KEY_DATATABLE_COUNTRIES) as DataTable;
            if (dtCountries == null)
            {

                dtCountries = base.GetDataTable(STP_COUNTRIES_GET_ALL);
                base.InsertInCache(CACHE_KEY_DATATABLE_COUNTRIES, dtCountries);
                return dtCountries;
            }
            else
                return dtCountries;
        }


        public DataTable GetAllCounties()
        {
            DataTable dtCounties = base.GetFromCache(CACHE_KEY_DATATABLE_COUNTIES) as DataTable;
            if (dtCounties == null)
            {

                dtCounties = base.GetDataTable(STP_COUNTIES_GET_ALL);
                base.InsertInCache(CACHE_KEY_DATATABLE_COUNTIES, dtCounties);
                return dtCounties;
            }
            else
                return dtCounties;
        }


        public DataTable GetAllInsuranceCompanies()
        {
            DataTable dtInsuranceCompanies = base.GetFromCache(CACHE_KEY_DATATABLE_INSURANCE_COMPANIES) as DataTable;
            if (dtInsuranceCompanies == null)
            {

                dtInsuranceCompanies = base.GetDataTable(STP_INSURANCE_COMPANIES_GET_ALL);
                base.InsertInCache(CACHE_KEY_DATATABLE_INSURANCE_COMPANIES, dtInsuranceCompanies);
                return dtInsuranceCompanies;
            }
            else
                return dtInsuranceCompanies;
        }


        #endregion //DataTable

        #region DB CUD methods

        public bool InsertInsuranceCompany(NameValue insuranceCompany)
        {
            SqlParameter[] parameters = { new SqlParameter("@name", insuranceCompany.Name) };

            if (base.CMDExecuteNonQuery(STP_INSURANCE_COMPANIES_INSERT, parameters) > 0)
            {
                base.ClearCache(CACHE_KEY_DATATABLE_INSURANCE_COMPANIES);
                return true;
            }
            else
                return false;

        }

        public bool UpdateInsuranceCompany(NameValue insuranceCompany)
        {
            SqlParameter[] parameters = 
            { 
                 new SqlParameter("@id", insuranceCompany.Id) 
                ,new SqlParameter("@name", insuranceCompany.Name) 
            };

            if (base.CMDExecuteNonQuery(STP_INSURANCE_COMPANIES_UPDATE, parameters) > 0)
            {
                base.ClearCache(CACHE_KEY_DATATABLE_INSURANCE_COMPANIES);
                return true;
            }
            else
                return false;



        }

        public bool DeleteInsuranceCompany(int insuranceCompanyId)
        {
            SqlParameter[] parameters = { new SqlParameter("@id", insuranceCompanyId) };

            if (base.CMDExecuteNonQuery(STP_INSURANCE_COMPANIES_DELETE, parameters) > 0)
            {

                base.ClearCache(CACHE_KEY_DATATABLE_INSURANCE_COMPANIES);
                return true;
            }
            else
                return false;

        }


        public bool InsertCounty(NameValue county)
        {
            SqlParameter[] parameters = { new SqlParameter("@name", county.Name) };

            if (base.CMDExecuteNonQuery(STP_COUNTIES_INSERT, parameters) > 0)
            {
                base.ClearCache(CACHE_KEY_DATATABLE_COUNTIES);
                return true;
            }
            else
                return false;

        }

        public bool UpdateCounty(NameValue county)
        {
            SqlParameter[] parameters = 
            { 
                 new SqlParameter("@id", county.Id) 
                ,new SqlParameter("@name", county.Name) 
            };

            if (base.CMDExecuteNonQuery(STP_COUNTIES_UPDATE, parameters) > 0)
            {
                base.ClearCache(CACHE_KEY_DATATABLE_COUNTIES);
                return true;
            }
            else
                return false;



        }

        public bool DeleteCounty(int countyId)
        {
            SqlParameter[] parameters = { new SqlParameter("@id", countyId) };

            if (base.CMDExecuteNonQuery(STP_COUNTIES_DELETE, parameters) > 0)
            {

                base.ClearCache(CACHE_KEY_DATATABLE_COUNTIES);
                return true;
            }
            else
                return false;

        }



        #endregion //DB CUD methods

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            hashTable.Add("Name", "@name");
            hashTable.Add("Id", "@id");
            return hashTable;
        }

        protected override NameValue FillProperties(SqlDataReader dataReader)
        {
            NameValue nameValue = new NameValue();

            for (int i = 0; i < dataReader.FieldCount; i++)
            {
                switch (dataReader.GetName(i))
                {
                    case "id":
                        nameValue.Id = (int)dataReader[i];
                        break;
                    case "name":
                        nameValue.Name = (string)dataReader[i];
                        break;
                }
            }
            return nameValue;
        }

        #endregion //mapping methods
    }
}
