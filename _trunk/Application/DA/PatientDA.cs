using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class PatientDA : BaseDA<Patient>
    {
        #region cache keys


        #endregion //cache keys

        #region Stored procedure names

        private const string STP_PATIENTS_GET_BY_ID = "dbo.stp_Patients_GetById";
        private const string STP_PATIENTS_INSERT = "dbo.stp_Patients_Insert";
        private const string STP_PATIENTS_UPDATE = "dbo.stp_Patients_Update";
        private const string STP_PATIENTS_UPDATE_INSURANCE = "dbo.stp_Patients_Update_Insurance";
        private const string STP_PATIENTS_UPDATE_DOCTOR = "dbo.stp_Patients_Update_Doctor";
        private const string STP_PATIENTS_DELETE = "dbo.stp_Patients_Delete";
        private const string STP_PATIENTS_INSURANCECOMPANIES_GET_PATIENTS_MANAGE_SEARCH = "dbo.stp_Patients_InsuranceCompanies_GetPatientsManageSearch";

        private const string STP_PATIENTS_GET_BY_PRACTICE_ID = "dbo.stp_Patients_GetByPracticeId";


        private const string STP_PATIENTS_EXISTS_PATIENT_WITH_INSURANCE_COMPANY_ID = "dbo.stp_Patients_ExistsPatientWithInsuranceCompanyId";


        private const string STP_PATIENTS_EXISTS_PATIENT_WITH_PRACTICE_ID = "dbo.stp_Patients_ExistsPatientWithPracticeId";
        private const string STP_PATIENTS_EXISTS_PATIENT_WITH_DOCTOR_ID = "dbo.stp_Patients_ExistsPatientWithDoctorId";

        private const string STP_PATIENTS_USERS_DOCTORS_INSURANCECOMPANIES_GET_BY_ID_FOR_PROFILE_PAGE = "dbo.stp_Patients_Users_Doctors_InsuranceCompanies_GetByIdForProfilePage";

        #endregion //Stored procedure names


        #region DB access methods

        public bool ExistsPatientWithInsuranceCompanyId(int insuranceCompanyId)
        {
            SqlParameter[] parameters = { new SqlParameter("@insuranceCompanyId", insuranceCompanyId) };
            return base.Exists(STP_PATIENTS_EXISTS_PATIENT_WITH_INSURANCE_COMPANY_ID, parameters);
        }

        public bool ExistsPatientWithPracticeId(Guid practiceId)
        {
            SqlParameter[] parameters = { new SqlParameter("@practiceId", practiceId) };
            return base.Exists(STP_PATIENTS_EXISTS_PATIENT_WITH_PRACTICE_ID, parameters);
        }

        public bool ExistsPatientWithDoctorId(int doctorId)
        {
            SqlParameter[] parameters = { new SqlParameter("@doctorId", doctorId) };
            return base.Exists(STP_PATIENTS_EXISTS_PATIENT_WITH_DOCTOR_ID, parameters);
        }
            
        #endregion //DB access methods

        #region DataTable


        public DataTable GetPatientById(int id)
        {
            SqlParameter[] parameters = { new SqlParameter("@id", id) };
            return base.GetDataTable(parameters, STP_PATIENTS_GET_BY_ID);
        }

        public DataTable GetPatientsByPracticeId(Guid practiceId)
        {
            SqlParameter[] parameters = { new SqlParameter("@practiceId", practiceId) };
            return base.GetDataTable(parameters, STP_PATIENTS_GET_BY_PRACTICE_ID);
        }


        public DataTable GetPatientsManageSearch(Guid practiceId,string firstName,string lastName, string dateOfBirth, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[7];

           
            if (practiceId.Equals(Guid.Empty))
                parameters[0] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@practiceId", practiceId);


            if (firstName.Equals(string.Empty))
                parameters[1] = new SqlParameter("@firstName", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@firstName", firstName);

            if (lastName.Equals(string.Empty))
                parameters[2] = new SqlParameter("@lastName", DBNull.Value);
            else
                parameters[2] = new SqlParameter("@lastName", lastName);


            parameters[3] = new SqlParameter("@pageSize", pageSize);
            parameters[4] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[5] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[5].Direction = ParameterDirection.Output;

            if (dateOfBirth.Equals(string.Empty))
                parameters[6] = new SqlParameter("@dateOfBirth", DBNull.Value);
            else
                parameters[6] = new SqlParameter("@dateOfBirth", DateTime.Parse(dateOfBirth));

            DataTable table = base.GetDataTable(parameters, STP_PATIENTS_INSURANCECOMPANIES_GET_PATIENTS_MANAGE_SEARCH);

            totalCount = (int)parameters[5].Value;

            return table;
        }

        public DataTable GetPatientByIdForProfilePage(int id)
        {
            SqlParameter[] parameters = { new SqlParameter("@id", id) };
            return base.GetDataTable(parameters, STP_PATIENTS_USERS_DOCTORS_INSURANCECOMPANIES_GET_BY_ID_FOR_PROFILE_PAGE);
        }


        #endregion //DataTable

        #region DB CUD methods


        public bool InsertPatient(Patient patient)
        {
                SqlParameter[] parameters = new SqlParameter[20];

                parameters[0] = new SqlParameter("@id", SqlDbType.Int);
                parameters[0].Direction = ParameterDirection.Output;

                parameters[1] = new SqlParameter("@firstName", patient.FirstName);
                parameters[2] = new SqlParameter("@middleInitial", patient.MiddleInitial);
                parameters[3] = new SqlParameter("@lastName", patient.LastName);
                parameters[4] = new SqlParameter("@gender", patient.Gender);
                parameters[5] = new SqlParameter("@dateOfBirth", patient.DateOfBirth);
                parameters[6] = new SqlParameter("@insuranceCompanyId", patient.InsuranceCompanyId);
                parameters[7] = new SqlParameter("@OD_Sphere", patient.OD_Sphere);
                parameters[8] = new SqlParameter("@OS_Sphere", patient.OS_Sphere);
                parameters[9] = new SqlParameter("@OD_Cylinder", patient.OD_Cylinder);
                parameters[10] = new SqlParameter("@OS_Cylinder", patient.OS_Cylinder);
                parameters[11] = new SqlParameter("@OD_AxisOfAstigmatism", patient.OD_AxisOfAstigmatism);
                parameters[12] = new SqlParameter("@OS_AxisOfAstigmatism", patient.OS_AxisOfAstigmatism);
                parameters[13] = patient.OD_Pachymetry.HasValue ? new SqlParameter("@OD_Pachymetry", patient.OD_Pachymetry) : new SqlParameter("@OD_Pachymetry", DBNull.Value) ; 
                parameters[14] = patient.OS_Pachymetry.HasValue ?  new SqlParameter("@OS_Pachymetry", patient.OS_Pachymetry) : new SqlParameter("@OS_Pachymetry", DBNull.Value);
                parameters[15] = new SqlParameter("@TVC_IdCode", patient.TVC_IdCode);
                parameters[16] = new SqlParameter("@createdBy", patient.CreatedBy);
                parameters[17] = new SqlParameter("@practiceId", patient.PracticeId);
                parameters[18] = patient.DoctorId != -1 ? new SqlParameter("@doctorId", patient.DoctorId) : new SqlParameter("@doctorId", DBNull.Value);
                parameters[19] = new SqlParameter("@isActive", patient.IsActive);

                int affectedRows = base.CMDExecuteNonQuery(STP_PATIENTS_INSERT, parameters);

                patient.Id = (int)parameters[0].Value;

                return affectedRows > 0;

        }



        public bool UpdatePatient(Patient patient)
        {

                SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",patient.Id)
                 ,new SqlParameter("@firstName", patient.FirstName)
                 ,new SqlParameter("@middleInitial", patient.MiddleInitial)
                 ,new SqlParameter("@lastName", patient.LastName)
                 ,new SqlParameter("@gender", patient.Gender)
                 ,new SqlParameter("@dateOfBirth", patient.DateOfBirth)
                 ,new SqlParameter("@insuranceCompanyId", patient.InsuranceCompanyId)
                 ,new SqlParameter("@OD_Sphere", patient.OD_Sphere)
                 ,new SqlParameter("@OS_Sphere", patient.OS_Sphere)
                 ,new SqlParameter("@OD_Cylinder", patient.OD_Cylinder)
                 ,new SqlParameter("@OS_Cylinder", patient.OS_Cylinder)
                 ,new SqlParameter("@OD_AxisOfAstigmatism", patient.OD_AxisOfAstigmatism)
                 ,new SqlParameter("@OS_AxisOfAstigmatism", patient.OS_AxisOfAstigmatism)
                 ,patient.OD_Pachymetry.HasValue ? new SqlParameter("@OD_Pachymetry", patient.OD_Pachymetry) : new SqlParameter("@OD_Pachymetry", DBNull.Value) 
                 ,patient.OS_Pachymetry.HasValue ?  new SqlParameter("@OS_Pachymetry", patient.OS_Pachymetry) : new SqlParameter("@OS_Pachymetry", DBNull.Value)
                 ,new SqlParameter("@TVC_IdCode", patient.TVC_IdCode)
                 ,new SqlParameter("@practiceId", patient.PracticeId)
                 ,patient.DoctorId != -1 ?  new SqlParameter("@doctorId", patient.DoctorId) : new SqlParameter("@doctorId", DBNull.Value)
                 ,new SqlParameter("@isActive", patient.IsActive)
                };

              return   base.CMDExecuteNonQuery(STP_PATIENTS_UPDATE, parameters) > 0;

        }

        public bool UpdatePatientInsurance(int insuranceId,int patientId)
        {

            SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",patientId)
                 ,new SqlParameter("@insuranceCompanyId", insuranceId)
                
                };

            return base.CMDExecuteNonQuery(STP_PATIENTS_UPDATE_INSURANCE, parameters) > 0;

        }

        public bool UpdatePatientDoctor(int doctorId, int patientId)
        {

            SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",patientId)
                 ,patientId != -1 ?  new SqlParameter("@doctorId", doctorId) : new SqlParameter("@doctorId", DBNull.Value)
                };

            return base.CMDExecuteNonQuery(STP_PATIENTS_UPDATE_DOCTOR, parameters) > 0;

        }


       
        public bool DeletePatient(int id)
        {

            SqlParameter[] parameters = {
                  new SqlParameter("@id",id) };

            return base.CMDExecuteNonQuery(STP_PATIENTS_DELETE, parameters) > 0;

        }


        
		
        #endregion  //DB CUD methods

      


       

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override Patient FillProperties(SqlDataReader dataReader)
        {
            return null;

			
        }

        #endregion //mapping methods

    }
}
