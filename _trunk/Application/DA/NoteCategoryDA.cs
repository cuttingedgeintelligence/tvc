﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class NoteCategoryDA : BaseDA<NoteCategory>
    {
        #region Stored procedure names

        private const string STP_NOTE_CATEGORIES_INSERT = "dbo.stp_NoteCategories_Insert";
        private const string STP_NOTE_CATEGORIES_UPDATE = "dbo.stp_NoteCategories_Update";
        private const string STP_NOTE_CATEGORIES_DELETE = "dbo.stp_NoteCategories_Delete";
        private const string STP_NOTE_CATEGORIES_GET_ALL = "dbo.stp_NoteCategories_GetAll";
        private const string STP_NOTE_CATEGORY_GET_BY_ID_ = "stp_NoteCategories_ByCategoryId";

        #endregion //Stored procedure names

        #region cache keys

        private const string CACHE_KEY_DATATABLE_NOTE_CATEGORIES = "CACHE_KEY_DATATABLE:NOTECATEGORIES";

        #endregion // cache keys

        #region DB CUD methods


        public bool InsertNoteCategory(NoteCategory  noteCategory,int noteTypeId)
        {


            SqlParameter[] parameters = 
                {
                  
                  new SqlParameter("@category", noteCategory.Category ),
                  new SqlParameter("noteTypeId",noteTypeId)
                
                };

            return base.CMDExecuteNonQuery(STP_NOTE_CATEGORIES_INSERT, parameters) > 0;

        }



        public bool UpdateNoteCategory(NoteCategory noteCategory, int noteTypeId)
        {

            SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",noteCategory.Id)
                 ,new SqlParameter("@category", noteCategory.Category )
                 ,new SqlParameter("noteTypeId",noteTypeId)

                };

            return base.CMDExecuteNonQuery(STP_NOTE_CATEGORIES_UPDATE, parameters) > 0;

        }


        public bool DeleteNoteCategory(int id)
        {

            SqlParameter[] parameters = {
                  new SqlParameter("@id",id) };

            return base.CMDExecuteNonQuery(STP_NOTE_CATEGORIES_DELETE, parameters) > 0;

        }




        #endregion  //DB CUD methods

        #region DataTable

        public DataTable GetAllNoteCategories(int noteTypeId)
        {
            //DataTable dtNoteCategories = base.GetFromCache(CACHE_KEY_DATATABLE_NOTE_CATEGORIES) as DataTable;
            //if (dtNoteCategories == null)
            //{

            //    dtNoteCategories = base.GetDataTable(STP_NOTE_CATEGORIES_GET_ALL);
            //    base.InsertInCache(CACHE_KEY_DATATABLE_NOTE_CATEGORIES, dtNoteCategories);
            //    return dtNoteCategories;
            //}
            //else
            //    return dtNoteCategories;
            SqlParameter[] parameters = {
                  new SqlParameter("@noteTypeId",noteTypeId) };


            DataTable dtNoteCategories = base.GetDataTable(parameters,STP_NOTE_CATEGORIES_GET_ALL);
            return dtNoteCategories;
        }

         public DataTable GetNoteCategoriesById(int categoryId)
        {
           SqlParameter[] parameters = {
                  new SqlParameter("@category",categoryId) };

           DataTable dtNoteCategory = base.GetDataTable(parameters, STP_NOTE_CATEGORY_GET_BY_ID_);
            return dtNoteCategory;
        }

         


        

        #endregion //DataTable

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override NoteCategory FillProperties(SqlDataReader dataReader)
        {
            return null;


        }

        #endregion //mapping methods
    }
}
