using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class PracticeAppointmentsSettingsDA : BaseDA<PracticeAppointmentsSettings>
    {
        #region cache keys

       

        #endregion //cache keys

        #region Stored procedure names

        private const string STP_PRACTICESAPPOINTMENTSSETTINGS_GET_BY_PRACTICE_ID = "dbo.stp_PracticesAppointmentsSettings_GetByPracticeId";


        private const string STP_PRACTICESAPPOINTMENTSSETTINGS_INSERT = "dbo.stp_PracticesAppointmentsSettings_Insert";
        private const string STP_PRACTICESAPPOINTMENTSSETTINGS_UPDATE = "dbo.stp_PracticesAppointmentsSettings_Update";
        private const string STP_PRACTICESAPPOINTMENTSSETTINGS_DELETE = "dbo.stp_PracticesAppointmentsSettings_Delete";
       
         
		#endregion //Stored procedure names


        #region DB access methods

      
        #endregion //DB access methods

        #region DataTable

      

        public DataTable GetPracticeAppointmentsSettingsByPracticeId(Guid practiceId)
        {
            SqlParameter[] parameters = { new SqlParameter("@practiceId", practiceId) };
            return base.GetDataTable(parameters, STP_PRACTICESAPPOINTMENTSSETTINGS_GET_BY_PRACTICE_ID);
        }


        #endregion //DataTable

        #region DB CUD methods


        public bool InsertPracticeAppintmentsSettings(PracticeAppointmentsSettings practiceAppintmentsSettings)
        {
           
                SqlParameter[] parameters = 
                {
                  new SqlParameter("@practiceId",practiceAppintmentsSettings.PracticeId)
                 ,new SqlParameter("@recurrencePattern", practiceAppintmentsSettings.RecurrencePattern)
                 ,new SqlParameter("@createdBy", practiceAppintmentsSettings.CreatedBy)
                
                };

                return base.CMDExecuteNonQuery(STP_PRACTICESAPPOINTMENTSSETTINGS_INSERT, parameters) > 0;



        }



        public bool UpdatePracticeAppintmentsSettings(PracticeAppointmentsSettings practiceAppintmentsSettings)
        {

                SqlParameter[] parameters = 
                {
                  new SqlParameter("@practiceId",practiceAppintmentsSettings.PracticeId)
                 ,new SqlParameter("@recurrencePattern", practiceAppintmentsSettings.RecurrencePattern)
                
               
                };

                return base.CMDExecuteNonQuery(STP_PRACTICESAPPOINTMENTSSETTINGS_UPDATE, parameters) > 0;
              

        }




        public bool DeletePracticeAppintmentsSettings(Guid practiceId)
        {

            SqlParameter[] parameters = {
                  new SqlParameter("@practiceId",practiceId) };

            return base.CMDExecuteNonQuery(STP_PRACTICESAPPOINTMENTSSETTINGS_DELETE,parameters) > 0;

        }



        #endregion  //DB CUD methods

      


       

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override PracticeAppointmentsSettings FillProperties(SqlDataReader dataReader)
        {
            return null;

			
        }

        #endregion //mapping methods

    }
}
