using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using System.Data;

using CEI.Common;

namespace CEI.DA
{
    public class RoleDA : BaseDA<Role>
    {
        #region stored procedure names

     
      
       

        #endregion //stored procedure names

        #region DB access methods

        

        #endregion //DB access methods

        #region DataTable

     
       

        #endregion //DataTable

        #region DB CUD methods

       

       

       

       

        #endregion //DB CUD methods

        

        #region Mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            hashTable.Add("Id", "@id");
            hashTable.Add("Name", "@name");
			hashTable.Add("IsDefault", "@isDefault");
            return hashTable;
        }

        protected override Role FillProperties(SqlDataReader dataReader)
        {
            //return RoleEnum.Member;//(RoleEnum)Enum.Parse(typeof(RoleEnum), dataReader["id"].ToString());
            Role role = new Role();

            //for (int i = 0; i < dataReader.FieldCount; i++)
            //{
            //    switch (dataReader.GetName(i))
            //    {
            //        case "id":
            //            role.Id = (int)dataReader[i];
            //            break;
            //        case "userId":
            //            role.UserId = (int)dataReader[i];
            //            break;
            //        case "roleId":
            //            role.RoleId = (dataReader[i] != DBNull.Value) ? (RoleEnum)Enum.Parse(typeof(RoleEnum), dataReader[i].ToString()) : RoleEnum.Member;
            //            break;
            //        //(dataReader[i] != DBNull.Value) ? (string)dataReader[i] : null;
            //    }
            //}

            return role;
        }

        #endregion //Mapping methods

      
    }
}
