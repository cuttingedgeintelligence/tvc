using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class FinalNotesWorksheetPatientDA : BaseDA<FinalNotesWorksheetPatient>
    {
        #region cache keys


        #endregion //cache keys

        #region Stored procedure names

        private const string  STP_PATIENTS_PATIENTLOGPATIENTS_GET_FINAL_NOTES_WORKSHEET_PATIENTS_BY_PATIENT_LOG_ID   =  "dbo.stp_Patients_PatientLogPatients_GetFinalNotesWorksheetPatientsByPatientLogId";


        private const string STP_PATIENTLOGSPATIENTS_UPDATE_FINAL_NOTES_WORKSHEET_PATIENT = "dbo.stp_PatientLogsPatients_UpdateFinalNotesWorksheetPatient";
       

		#endregion //Stored procedure names


        #region DB access methods

       
        public List<FinalNotesWorksheetPatient> GetFinalNotesWorksheetPatientsByPatientLogId(int patientLogId)
        {
            SqlParameter[] parameters = new SqlParameter[1]
            {
                new SqlParameter("@patientLogId", patientLogId)
            };
            return base.GetMultipleObjects(parameters, STP_PATIENTS_PATIENTLOGPATIENTS_GET_FINAL_NOTES_WORKSHEET_PATIENTS_BY_PATIENT_LOG_ID);
        }

        #endregion //DB access methods

        #region DataTable


        #endregion //DataTable

        #region DB CUD methods





        public bool UpdateFinalNotesWorksheetPatient(FinalNotesWorksheetPatient finalNotesWorksheetPatient, ref SqlTransaction transaction)
        {


            SqlParameter[] parameters = 
                {
                  
                  new SqlParameter("@id", finalNotesWorksheetPatient.Id)
                 ,new SqlParameter("@status", finalNotesWorksheetPatient.Status)
                 ,new SqlParameter("@internalNotes", finalNotesWorksheetPatient.InternalNotes)
                 ,new SqlParameter("@practiceNotes", finalNotesWorksheetPatient.PracticeNotes)
                 ,new SqlParameter("@OD_IOP", finalNotesWorksheetPatient.OD_IOP)
                 //,new SqlParameter("@OD_BFA", finalNotesWorksheetPatient.OD_BFA)
                 ,finalNotesWorksheetPatient.OD_BFA.HasValue ? new SqlParameter("@OD_BFA", finalNotesWorksheetPatient.OD_BFA) : new SqlParameter("@OD_BFA", DBNull.Value)
                 ,new SqlParameter("@OS_IOP", finalNotesWorksheetPatient.OS_IOP)
                 //,new SqlParameter("@OS_BFA", finalNotesWorksheetPatient.OS_BFA)
                 ,finalNotesWorksheetPatient.OS_BFA.HasValue ? new SqlParameter("@OS_BFA", finalNotesWorksheetPatient.OS_BFA) : new SqlParameter("@OS_BFA", DBNull.Value)
                ,new SqlParameter("@BFA", finalNotesWorksheetPatient.BFA)
                ,new SqlParameter("@HRT_Glaucoma", finalNotesWorksheetPatient.HRT_Glaucoma)
                ,new SqlParameter("@HRT_Retina", finalNotesWorksheetPatient.HRT_Retina)
             //   ,new SqlParameter("@insuranceCompanyName", finalNotesWorksheetPatient.Insurance) Marina this not exists in stp
                };

            return base.CMDExecuteNonQuery(STP_PATIENTLOGSPATIENTS_UPDATE_FINAL_NOTES_WORKSHEET_PATIENT, parameters, ref transaction) > 0;

        }


       


        
		
        #endregion  //DB CUD methods

      


       

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }



        protected override FinalNotesWorksheetPatient FillProperties(SqlDataReader dataReader)
        {
            FinalNotesWorksheetPatient finalNotesWorksheetPatient = new FinalNotesWorksheetPatient();
			for (int i = 0; i < dataReader.FieldCount; i++)
			{
				switch (dataReader.GetName(i))
				{

					case "id":
                        finalNotesWorksheetPatient.Id = (int)dataReader[i];
						break;
					case "fullName":
                        finalNotesWorksheetPatient.FullName = dataReader[i] != DBNull.Value ? (string)dataReader[i] : string.Empty;
						break;
                    case "patientId":
                        finalNotesWorksheetPatient.PatientId = (int)dataReader[i];
						break;
                    case "glaucoma":
                        finalNotesWorksheetPatient.Glaucoma = (bool)dataReader[i];
						break;
					case "retina":
                        finalNotesWorksheetPatient.Retina = (bool)dataReader[i];
						break;
                    case "internalNotes":
                         finalNotesWorksheetPatient.InternalNotes = dataReader[i] != DBNull.Value ? (string)dataReader[i] : string.Empty;
                        break;
                    case "practiceNotes":
                        finalNotesWorksheetPatient.PracticeNotes = dataReader[i] != DBNull.Value ? (string)dataReader[i] : string.Empty;
                        break;

                    case "CPT_Codes":
                        finalNotesWorksheetPatient.CPT_Codes = dataReader[i] != DBNull.Value ? (string)dataReader[i] : string.Empty;
                        break;
                    case "status":
                        //if(dataReader[i] != DBNull.Value)
                            finalNotesWorksheetPatient.Status = (PatientLogPatientStatusEnum)Enum.Parse(typeof(PatientLogPatientStatusEnum), dataReader[i].ToString());
                        break;

                    case "OD_IOP":
                        finalNotesWorksheetPatient.OD_IOP = dataReader[i] != DBNull.Value ? (string)dataReader[i] : null;
                        break;

                    case "OS_IOP":
                        finalNotesWorksheetPatient.OS_IOP = dataReader[i] != DBNull.Value ? (string)dataReader[i] : null;
                        break;

                    case "OD_BFA":
                        if(dataReader[i] != DBNull.Value )
                            finalNotesWorksheetPatient.OD_BFA = (short)dataReader[i];
                        break;

                    case "OS_BFA":
                        if (dataReader[i] != DBNull.Value)
                            finalNotesWorksheetPatient.OS_BFA = (short)dataReader[i];
                        break;
                    case "BFA":
                        finalNotesWorksheetPatient.BFA = (decimal)dataReader[i];
                        break;
                    case "HRT_Glaucoma":
                        finalNotesWorksheetPatient.HRT_Glaucoma = (decimal)dataReader[i];
                        break;

                    case "HRT_Retina":
                        finalNotesWorksheetPatient.HRT_Retina = (decimal)dataReader[i];
                        break;
                    case "internalCustomNotes":
                        if (dataReader[i] != DBNull.Value)
                            finalNotesWorksheetPatient.InternalCustomNotes = dataReader[i] != DBNull.Value ? (string)dataReader[i] : string.Empty;
                        break;
                    case "practiceCustomNotes":
                        if (dataReader[i] != DBNull.Value)
                            finalNotesWorksheetPatient.PracticeCustomNotes = dataReader[i] != DBNull.Value ? (string)dataReader[i] : string.Empty;
                        break;
                    case "insuranceCompanyName":
                        finalNotesWorksheetPatient.Insurance = dataReader[i] != DBNull.Value ? (string)dataReader[i] : string.Empty;
                        break;
                    case "OS_Pachymetry":
                        if (dataReader[i] != DBNull.Value)
                            finalNotesWorksheetPatient.OS_Pachymetry= (short)dataReader[i];
                        break;
                    case "OD_Pachymetry":
                        if (dataReader[i] != DBNull.Value)
                            finalNotesWorksheetPatient.OD_Pachymetry = (short)dataReader[i];
                        break;






				    
				}

			}

            return finalNotesWorksheetPatient;

			
        }

        #endregion //mapping methods

    }
}
