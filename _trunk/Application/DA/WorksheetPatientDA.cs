using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class WorksheetPatientDA : BaseDA<WorksheetPatient>
    {
        #region cache keys


        #endregion //cache keys

        #region Stored procedure names

    
        

       
        
        private const string  STP_PATIENTS_PATIENTLOGPATIENTS_PATIENTLOGSPATIENTSCPTCODES_CPTCODES_GET_WORKSHEET_PATIENTS_BY_PATIENT_LOG_ID = "dbo.stp_Patients_PatientLogPatients_PatientLogsPatientsCPTCodes_CPTCodes_GetWorksheetPatientsByPatientLogId";

        private const string STP_PATIENTLOGSPATIENTSCPTCODES_INSERT = "dbo.stp_PatientLogsPatientsCPTCodes_Insert";
        private const string STP_PATIENTLOGSPATIENTSCPTCODES_DELETE = "dbo.stp_PatientLogsPatientsCPTCodes_Delete";
        private const string STP_PATIENTLOGSPATIENTS_EXISTS_WORKSHEETPATIENT_WITH_CPT_CODE_ID = "dbo.stp_PatientLogsPatients_ExistsWorksheetPatientWithCPTCodeId";
        private const string STP_PATIENTLOGSPATIENTS_UPDATE_COLORCODEID = "stp_PatientLogsPatients_Update_ColorCodeId";
        
		#endregion //Stored procedure names


        #region DB access methods

        
       
        /// <summary>
        /// Check if CPT Code is assigned to some patient in some worksheet document
        /// </summary>
        /// <param name="CPT_CodeId"></param>
        /// <returns></returns>
        public bool ExistsWorksheetPatientWithCPTCodeId(int CPT_CodeId)
        {
            SqlParameter[] parameters = { new SqlParameter("@CPT_CodeId", CPT_CodeId) };
            return base.Exists(STP_PATIENTLOGSPATIENTS_EXISTS_WORKSHEETPATIENT_WITH_CPT_CODE_ID, parameters);
        }


      

        #endregion //DB access methods

        #region DataTable

        public DataTable GetWorksheetPatientsByPatientLogId(int patientLogId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@patientLogId", patientLogId)
            };

            return base.GetDataTable(parameters, STP_PATIENTS_PATIENTLOGPATIENTS_PATIENTLOGSPATIENTSCPTCODES_CPTCODES_GET_WORKSHEET_PATIENTS_BY_PATIENT_LOG_ID);

        }


        #endregion //DataTable

        #region DB CUD methods





        public bool UpdateWorksheetPatients(List<WorksheetPatient> worksheetPatients)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {
                foreach (WorksheetPatient worksheetPatient in worksheetPatients)
                {
                    foreach (CPTCode CPT_Code in worksheetPatient.CPT_CodesList)
                    {
                        if(CPT_Code.ActionType.Equals(ActionTypeEnum.Insert))
                        {
                            this.InsertWorksheetPatientCPTCode(CPT_Code.Id, worksheetPatient.Id,ref transaction);
                        }
                        else if (CPT_Code.ActionType.Equals(ActionTypeEnum.Delete))
                        {
                            this.DeleteWorksheetPatientCPTCode(CPT_Code.PatientLogPatientCPTCodeId, ref transaction);
                        }

                    }

                    this.UpdateWorksheetPatientColorCodeId(worksheetPatient.Id, worksheetPatient.ColorCodeId, ref transaction);
                }

                if(transaction!= null)
                    transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                if(transaction!= null)
                    transaction.Rollback();
                
            }
            finally
            {
                if (transaction != null && transaction.Connection != null)
                    transaction.Connection.Close();
            }

            return returnValue;



        }

        /// <summary>
        /// Insert record into the intersection table tbl_PatientLogsPatientsCPTCodesPatients
        /// </summary>
        /// <param name="CPT_CodeId"></param>
        /// <param name="patientLogPatientId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        private bool InsertWorksheetPatientCPTCode(int CPT_CodeId, int patientLogPatientId,ref SqlTransaction transaction)
        {
            SqlParameter[] parameters =
            {
                 new SqlParameter("@patientLogPatientId", patientLogPatientId)
                ,new SqlParameter("@CPT_CodeId", CPT_CodeId)
            };
            return base.CMDExecuteNonQuery(STP_PATIENTLOGSPATIENTSCPTCODES_INSERT, parameters, ref transaction) > 0;
        }


        /// <summary>
        /// Delete recorn the intersection table tbl_PatientLogsPatientsCPTCodesPatients
        /// </summary>
        /// <param name="patientLogPatientCPTCodeId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        private bool DeleteWorksheetPatientCPTCode(int patientLogPatientCPTCodeId, ref SqlTransaction transaction)
        {
            SqlParameter[] parameters =
            {
                 new SqlParameter("@id", patientLogPatientCPTCodeId)
            };
            return base.CMDExecuteNonQuery(STP_PATIENTLOGSPATIENTSCPTCODES_DELETE, parameters, ref transaction) > 0;
        }

        private bool UpdateWorksheetPatientColorCodeId(int patientLogPatientId, string colorCodeId, ref SqlTransaction transaction)
        {
            SqlParameter[] parameters =
            {
                 new SqlParameter("@id", patientLogPatientId),
                 new SqlParameter("@colorCodeId", colorCodeId)
            };
            return base.CMDExecuteNonQuery(STP_PATIENTLOGSPATIENTS_UPDATE_COLORCODEID, parameters, ref transaction) > 0;
        }

        #endregion  //DB CUD methods

      


       

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }



        protected override WorksheetPatient FillProperties(SqlDataReader dataReader)
        {
            return null;
        }

        #endregion //mapping methods

    }
}
