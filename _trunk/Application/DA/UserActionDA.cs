﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CEI.Common;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

namespace CEI.DA
{
    public class UserActionDA:BaseDA<UserAction>
    {
        #region VARIABLE DECLARATIONS

        private int _totalCountManageAdminsSearch = 0;

        #endregion

        #region CONSTRUCTOR

        public UserActionDA()
            : base()
        {

        }

        #endregion

        #region STORED PROCEDURES NAMES

        private const string STP_USERACTIONS_INSERT = "dbo.stp_UserActions_Insert";
        private const string STP_USERACTIONS_UPDATE = "dbo.stp_UserActions_Update";
        private const string STP_USERACTIONS_GET_USERACTION_SEARCH = "dbo.stp_UserActions_GetUserActionSearch";


        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// Inserts new record in UserActions table and returns the last inserted identity of the data base
        /// </summary>
        /// <param name="userId">UserId that is loging in the application</param>
        /// <param name="loginDateTime">The date time of the login</param>
        /// <returns>The last indentity of database</returns>
        public int InsertLoginUser(Guid userId, DateTime loginDateTime)
        {
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@userId", SqlDbType.UniqueIdentifier);
            param[0].IsNullable = false;
            param[0].Direction = ParameterDirection.Input;
            param[0].Value = userId;

            param[1] = new SqlParameter("@LoginTime", SqlDbType.DateTime);
            param[1].IsNullable = false;
            param[1].Direction = ParameterDirection.Input;
            param[1].Value = loginDateTime;

            param[2] = new SqlParameter("@id", SqlDbType.Int);
            param[2].IsNullable = false;
            param[2].Direction = ParameterDirection.Output;

            this.CMDExecuteNonQuery(STP_USERACTIONS_INSERT, param);

            if (param[2].Value != null)
                return Convert.ToInt32(param[2].Value);
            else
                return 0;
        }

        /// <summary>
        /// Updates the logout time of the user in the UserActions
        /// </summary>
        /// <param name="id">Id of the user actions table</param>
        /// <param name="userId">The ID of the user that is logging out</param>
        /// <param name="logoutTime">The DateTime of the Logout</param>
        public void UpdateLogoutUser(int id, Guid userId, DateTime logoutTime)
        {
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@userId", SqlDbType.UniqueIdentifier);
            param[0].IsNullable = false;
            param[0].Direction = ParameterDirection.Input;
            param[0].Value = userId;

            param[1] = new SqlParameter("@LogoutTime", SqlDbType.DateTime);
            param[1].IsNullable = false;
            param[1].Direction = ParameterDirection.Input;
            param[1].Value = logoutTime;

            param[2] = new SqlParameter("@id", SqlDbType.Int);
            param[2].IsNullable = false;
            param[2].Direction = ParameterDirection.Input;
            param[2].Value = id;

            this.CMDExecuteNonQuery(STP_USERACTIONS_UPDATE, param);
        }

        /// <summary>
        /// Gets the users actions
        /// </summary>
        /// <param name="userId">UserID for which data should be retrived</param>
        /// <param name="dateFrom">Date from which data should be retrived</param>
        /// <param name="dateTo">Date to which data should be retrived</param>
        /// <param name="pageSize">Number of items per page</param>
        /// <param name="startRowIndex">Starting row index</param>
        /// <param name="totalCount">Total counts of items</param>
        /// <returns>Login logout data for user</returns>
        public DataTable GetUserActionsForUserIDSearch(Guid userId, DateTime dateFrom, DateTime dateTo, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[6];

            parameters[0] = new SqlParameter("@userId", SqlDbType.UniqueIdentifier);
            parameters[0].Direction = ParameterDirection.Input;
            parameters[0].IsNullable = false;
            parameters[0].Value = userId;

            parameters[1] = new SqlParameter("@dateFrom", SqlDbType.DateTime);
            parameters[1].Direction = ParameterDirection.Input;
            parameters[1].IsNullable = false;
            parameters[1].Value = dateFrom;

            parameters[2] = new SqlParameter("@dateTo", SqlDbType.DateTime);
            parameters[2].Direction = ParameterDirection.Input;
            parameters[2].IsNullable = false;
            parameters[2].Value = dateTo;

            parameters[3] = new SqlParameter("@pageSize", SqlDbType.Int);
            parameters[3].Direction = ParameterDirection.Input;
            parameters[3].IsNullable = false;
            parameters[3].Value = pageSize;

            parameters[4] = new SqlParameter("@startRowIndex", SqlDbType.Int);
            parameters[4].Direction = ParameterDirection.Input;
            parameters[4].IsNullable = false;
            parameters[4].Value = startRowIndex;

            parameters[5] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[5].Direction = ParameterDirection.Output;
            parameters[5].IsNullable = false;

            DataTable table = new DataTable();

            try
            {
                table = base.GetDataTable(parameters, STP_USERACTIONS_GET_USERACTION_SEARCH);
            }
            catch (Exception ex)
            {
                throw ex;
            }


            totalCount = (int)parameters[5].Value;
            
            _totalCountManageAdminsSearch = totalCount;

            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public int GetAdminsManageSearchTotalCount(Guid userId, DateTime dateFrom, DateTime dateTo)
        {
            return _totalCountManageAdminsSearch;
        }

        #endregion

        #region OVERRIDE METHODS

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override UserAction FillProperties(SqlDataReader reader)
        {
            return new UserAction();
        }

        #endregion
    }
}
