using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Reflection;
using System.Web;
using System.Web.Caching;

using CEI.Common;

namespace CEI.DA
{
    public abstract class BaseDA<T>
    {
        private static string _connectionString;
        private const string CACHE_KEY_SP_PARAMETERS = "STP_CACHED_PARAMS:";
        private const int SP_PARAMETERS_CACHE_SLIDING_EXPIRATION_MINUTES = 60;

        private T _retObject;

        #region abstract methods

        protected abstract Hashtable MapTable();
        protected abstract T FillProperties(SqlDataReader reader);

        #endregion //abstract methods

        #region Constructors

        static BaseDA()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["CEI.ConnectionString"].ConnectionString;
        }

        #endregion //Constructors


        #region Fill SQL parameters with object properties

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customObj"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        /// 
        private SqlParameter[] FillSQLParameters(T customObj, SqlParameter[] sqlParams)
        {
            PropertyInfo[] objectProperties = customObj.GetType().GetProperties();

            string sqlParamName = string.Empty;
            foreach (SqlParameter sqlParameter in sqlParams)
            {
                sqlParamName = sqlParameter.ParameterName;
                foreach (PropertyInfo propertyInfo in objectProperties)
                {

                    if (this.MapTable()[propertyInfo.Name] != null && this.MapTable()[propertyInfo.Name].ToString().Equals(sqlParamName))
                    {
                        if (propertyInfo.PropertyType.Equals(typeof(System.DateTime)))
                            sqlParameter.Value = ((DateTime)propertyInfo.GetValue(customObj, null) != DateTime.MinValue) ? propertyInfo.GetValue(customObj, null) : DBNull.Value;
                        else
                            sqlParameter.Value = (propertyInfo.GetValue(customObj, null) != null) ? propertyInfo.GetValue(customObj, null) : DBNull.Value;

                        break;
                    }

                }
            }

            return sqlParams;
        }

        #endregion //Fill SQL parameters with object properties


        #region Derive parameters methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stpName"></param>
        /// <returns></returns>
        private SqlParameter[] DeriveSpParameters(string stpName)
        {
            string cacheKey = CACHE_KEY_SP_PARAMETERS + stpName;
            SqlParameter[] cachedParameters = HttpRuntime.Cache[cacheKey] as SqlParameter[];

            if (cachedParameters == null)
            {


                SqlConnection conn = new SqlConnection(_connectionString);
                try
                {

                    SqlCommand cmd = new SqlCommand(stpName, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    SqlCommandBuilder.DeriveParameters(cmd);

                    cachedParameters = new SqlParameter[cmd.Parameters.Count];

                    cmd.Parameters.CopyTo(cachedParameters, 0);

                    cmd.Parameters.Clear();
                    cmd.Dispose();
                    this.InsertInCache(cacheKey, cachedParameters);
                }
                catch (Exception e)
                {
                    throw e;

                }
                finally
                {
                    conn.Close();
                }
                return cachedParameters;
            }

            return CloneParameters(cachedParameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="originalParameters"></param>
        /// <returns></returns>
        private SqlParameter[] CloneParameters(SqlParameter[] originalParameters)
        {
            SqlParameter[] clonedParameters = new SqlParameter[originalParameters.Length];

            for (int i = 0, j = originalParameters.Length; i < j; i++)
            {

                clonedParameters[i] = (SqlParameter)((ICloneable)originalParameters[i]).Clone();
                clonedParameters[i].Value = DBNull.Value;
            }
           
            return clonedParameters;
        }

        #endregion //Derive parameters methods


        #region Cache methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="item"></param>
        protected void InsertInCache(string key, object item)
        {
            HttpRuntime.Cache.Insert(key, item, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, SP_PARAMETERS_CACHE_SLIDING_EXPIRATION_MINUTES, 0), CacheItemPriority.Normal, null);
        }

        protected void ClearCache(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }

        protected object GetFromCache(string key)
        {
            return HttpRuntime.Cache[key];
        }

        //protected List<User> GetContactsFromCache(string key)
        //{
        //    return HttpRuntime.Cache[key];
        //}

        #endregion //Cache methods


        #region DB access methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stpName"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        private SqlDataReader CMDExecuteReader(string stpName, SqlParameter[] commandParameters)
        {
            SqlConnection conn = new SqlConnection(_connectionString);
            SqlCommand command = new SqlCommand(stpName, conn);
            command.CommandType = CommandType.StoredProcedure;

            // Add Parameters to stored procedure
            command.Parameters.AddRange(commandParameters);

            conn.Open();

            SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            //Return the data reader 

            return reader;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stpName"></param>
        /// <returns></returns>
        private SqlDataReader CMDExecuteReader(string stpName)
        {
            SqlConnection conn = new SqlConnection(_connectionString);
            SqlCommand command = new SqlCommand(stpName, conn);
            command.CommandType = CommandType.StoredProcedure;

            conn.Open();

            SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            //Return the data reader 

            return reader;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stpName"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        protected int CMDExecuteNonQuery(string stpName)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(stpName, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    //command.CommandTimeout = 100;
                    //Execute the command
                    int affectedRows = command.ExecuteNonQuery();
                    // Return the number of affected rows
                    return affectedRows;
                    //conn.Close();
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="stpName"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        protected int CMDExecuteNonQuery(string stpName, SqlParameter[] commandParameters)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {                
                using (SqlCommand command = new SqlCommand(stpName, conn))
                {
                    try
                    {
						command.CommandType = CommandType.StoredProcedure;
						//Add Parameters
						command.Parameters.AddRange(commandParameters);
						conn.Open();
						//command.CommandTimeout = 100;
						//Execute the command
						int affectedRows = command.ExecuteNonQuery();
						// Return the number of affected rows
						return affectedRows;
						//conn.Close();
                    }
                    catch (Exception ex)
                    {
                       
                        return 0;
                    }
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="stpName"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        protected int CMDExecuteNonQuery(string stpName, SqlParameter[] commandParameters, ref SqlTransaction transaction)
        {
            SqlCommand command = new SqlCommand(stpName);
            if (transaction == null)
            {
                SqlConnection conn = new SqlConnection(_connectionString);
                command.Connection = conn;
                conn.Open();

                transaction = conn.BeginTransaction();

                command.Transaction = transaction;
            }
            else
            {
                //TODO: check this row --> is this neccessery
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
            }
            //if (command.Connection.Open();
            command.CommandType = CommandType.StoredProcedure;
            //Add Parameters
            command.Parameters.AddRange(commandParameters);
            //Execute the command
            int affectedRows = command.ExecuteNonQuery();
            // Return the number of affected rows
            return affectedRows;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="stpName"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        protected int CMDExecuteNonQuery(string stpName,  ref SqlTransaction transaction)
        {
            SqlCommand command = new SqlCommand(stpName);
            if (transaction == null)
            {
                SqlConnection conn = new SqlConnection(_connectionString);
                command.Connection = conn;
                conn.Open();

                transaction = conn.BeginTransaction();

                command.Transaction = transaction;
            }
            else
            {
                //TODO: check this row --> is this neccessery
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
            }
            //if (command.Connection.Open();
            command.CommandType = CommandType.StoredProcedure;
           
            //Execute the command
            int affectedRows = command.ExecuteNonQuery();
            // Return the number of affected rows
            return affectedRows;
        }


        protected object CMDExecuteScalar(string stpName, SqlParameter[] commandParameters)
        {
            SqlConnection conn = new SqlConnection(_connectionString);
            SqlCommand command = new SqlCommand(stpName, conn);

            command.CommandType = CommandType.StoredProcedure;

            //Add Parameters
            command.Parameters.AddRange(commandParameters);
            conn.Open();
            //Execute the command
            object returnVal = command.ExecuteScalar();

            conn.Close();

            return returnVal;

        }

        protected object CMDExecuteScalar(string stpName)
        {
            SqlConnection conn = new SqlConnection(_connectionString);
            SqlCommand command = new SqlCommand(stpName, conn);

            command.CommandType = CommandType.StoredProcedure;

            //Add Parameters
            //command.Parameters.AddRange(commandParameters);
            conn.Open();
            //Execute the command
            object returnVal = command.ExecuteScalar();

            conn.Close();

            return returnVal;

        }

        #endregion //DB access methods


        #region CUD methods

        /// <summary>
        /// create update delete object
        /// </summary>
        /// <param name="myObject"></param>
        /// <param name="stpName"></param>
        /// <returns></returns>
        ///
        protected int CUDObject(T customObj, string stpName)
        {
            SqlParameter[] sqlParameters = this.DeriveSpParameters(stpName);

            sqlParameters = this.FillSQLParameters(customObj, sqlParameters);
            int affectedRows = this.CMDExecuteNonQuery(stpName, sqlParameters);

            if (affectedRows > 0)
            {
                if ((customObj as Common.BaseCommon).ActionType.Equals(ActionTypeEnum.Insert))
                {
                    (customObj as Common.BaseCommon).ActionType = ActionTypeEnum.Update;
                    foreach (SqlParameter param in sqlParameters)
                    {
                        if (param.Direction.Equals(ParameterDirection.InputOutput))
                        {
                            //fill property id with output parameter @id
                            (customObj as Common.BaseCommon).Id = (int)param.Value;
                            break;
                        }
                    }
                }


                //return true;
            }

            return affectedRows;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="myObject"></param>
        /// <param name="stpName"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        protected int CUDObject(T customObj, string stpName, ref SqlTransaction transaction)
        {
            SqlParameter[] sqlParameters = this.DeriveSpParameters(stpName);

            sqlParameters = FillSQLParameters(customObj, sqlParameters);
            int affectedRows = this.CMDExecuteNonQuery(stpName, sqlParameters, ref transaction);

            if (affectedRows > 0)
            {
                if ((customObj as Common.BaseCommon).ActionType.Equals(ActionTypeEnum.Insert))
                {
                    (customObj as Common.BaseCommon).ActionType = ActionTypeEnum.Update;
                    foreach (SqlParameter param in sqlParameters)
                    {
                        if (param.Direction.Equals(ParameterDirection.InputOutput))
                        {
                            (customObj as Common.BaseCommon).Id = (int)param.Value;
                            break;
                        }
                    }
                }
                //return true;
            }

            return affectedRows;
        }

        #endregion //CUD methods


        #region GetObject methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stpName"></param>
        /// <returns></returns>
        protected T GetSingleObject(string stpName)
        {
            // Create stored procedure parameters
            using (SqlDataReader dataReader = this.CMDExecuteReader(stpName))
            {

                if (dataReader.Read())
                {
                    _retObject = this.FillProperties(dataReader);
                    this.SetPropertiesOfGetObject(_retObject);

                }

                //dataReader.Close();
            }
            return _retObject;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="stpName"></param>
        /// <returns></returns>
        protected T GetSingleObject(SqlParameter[] parameters, string stpName)
        {
            // Create stored procedure parameters
            using (SqlDataReader dataReader = this.CMDExecuteReader(stpName, parameters))
            {

                if (dataReader.Read())
                {
                    _retObject = this.FillProperties(dataReader);
                    this.SetPropertiesOfGetObject(_retObject);

                }

                //dataReader.Close();
            }
            return _retObject;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="stpName"></param>
        /// <returns></returns>
        protected List<T> GetMultipleObjects(SqlParameter[] parameters, string stpName)
        {
            List<T> retObjects = new List<T>();
            // Create stored procedure parameters
            using (SqlDataReader dataReader = this.CMDExecuteReader(stpName, parameters))
            {
                while (dataReader.Read())
                {
                    T retObject = this.FillProperties(dataReader);
                    this.SetPropertiesOfGetObject(retObject);

                    retObjects.Add(retObject);
                }

                //dataReader.Close();
            }
            return retObjects;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stpName"></param>
        /// <returns></returns>
        protected List<T> GetMultipleObjects(string stpName)
        {
            List<T> retObjects = new List<T>();

            // Create stored procedure parameters
            using (SqlDataReader dataReader = this.CMDExecuteReader(stpName))
            {
                while (dataReader.Read())
                {
                    T retObject = this.FillProperties(dataReader);
                    this.SetPropertiesOfGetObject(retObject);


                    retObjects.Add(retObject);
                }

                //dataReader.Close();
            }

            return retObjects;
        }

        protected DataTable GetDataTable(SqlParameter[] parameters, string stpName)
        {
            DataTable returnTable = new DataTable();

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand(stpName, conn);

                command.CommandType = CommandType.StoredProcedure;

                //Add Parameters
                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);

                conn.Open();
                //Fill data table
                dataAdapter.Fill(returnTable);

            }

            return returnTable;
        }

        //New method 28.04.2009
        protected DataTable GetDataTable(SqlParameter[] parameters, string stpName, ref SqlTransaction transaction)
        {
            DataTable returnTable = new DataTable();

          
            SqlCommand command = new SqlCommand(stpName);

            if (transaction == null)
            {
                SqlConnection conn = new SqlConnection(_connectionString);
                command.Connection = conn;
                conn.Open();

                transaction = conn.BeginTransaction();

                command.Transaction = transaction;
            }
            else
            {
                //TODO: check this row --> is this neccessery
                command.Connection = transaction.Connection;
                command.Transaction = transaction;
            }


            command.CommandType = CommandType.StoredProcedure;
            //Add Parameters
            command.Parameters.AddRange(parameters);

            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);

           
            //Fill data table
            dataAdapter.Fill(returnTable);

            return returnTable;
        }
        

        protected DataTable GetDataTable(string stpName)
        {

            DataTable returnTable = new DataTable();

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand(stpName, conn);

                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);

                conn.Open();
                //Fill data table
                dataAdapter.Fill(returnTable);

            }

            return returnTable;
        }


        protected DataSet GetDataSet(string stpName)
        {

            DataSet returnDataSet = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand(stpName, conn);

                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);

                conn.Open();
                //Fill data table
                dataAdapter.Fill(returnDataSet);

            }

            return returnDataSet;
        }
        protected DataSet GetDataSet(SqlParameter[] parameters, string stpName)
        {

            DataSet returnDataSet = new DataSet();

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand command = new SqlCommand(stpName, conn);

                command.CommandType = CommandType.StoredProcedure;

                //Add Parameters
                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);

                conn.Open();
                //Fill data table
                dataAdapter.Fill(returnDataSet);

            }

            return returnDataSet;
        }

        //protected DataTable GetDataTableAndAddSQLCacheDependency(string stpName, string cacheKey)
        //{

        //    DataTable returnTable = new DataTable();
        //    using (SqlConnection conn = new SqlConnection(_connectionString))
        //    {
                
        //        SqlCommand command = new SqlCommand(stpName, conn);
        //        command.CommandType = CommandType.StoredProcedure;
                
        //        SqlCacheDependency cacheDependency = new SqlCacheDependency(command);
        //        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
        //        conn.Open();
        //        //Fill data table
        //        dataAdapter.Fill(returnTable);
        //        HttpRuntime.Cache.Insert(cacheKey, returnTable, cacheDependency, DateTime.MaxValue, Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
        //    }
            

        //    return returnTable;
        //}


        
        #endregion //GetObject methods

        #region Count methods

        protected int GetCount(string stpName, SqlParameter[] parameters)
        {
            object obj = this.CMDExecuteScalar(stpName, parameters);
            if ((obj != null) && (obj.ToString() != ""))
                return (int)obj;
            else
                return 0;
        }

        protected int GetCount(string stpName)
        {
            object obj = this.CMDExecuteScalar(stpName);
            
            if ((obj != null)&& (obj.ToString() != ""))
                return (int)obj;
            else
                return 0;
        }


        #endregion //Count methods

		#region String

		protected string ReturnString(string stpName, SqlParameter[] parameters)
		{
			object obj = this.CMDExecuteScalar(stpName, parameters);
			if ((obj != null) && (obj.ToString() != ""))
				return ((int)obj).ToString();
			else
				return null;
		}

		protected string ReturnString(string stpName)
		{
			object obj = this.CMDExecuteScalar(stpName);

			if ((obj != null) && (obj.ToString() != ""))
				return (string)obj;
			else
				return null;
		}

		#endregion //String

		#region Exists method

		protected bool Exists(string stpName, SqlParameter[] parameters)
        {
            return (bool)this.CMDExecuteScalar(stpName, parameters);
        }

        #endregion //Exists method

        #region Private methods

        private void SetPropertiesOfGetObject(T getObject)
        {
            if (getObject is BaseCommon)
                (getObject as BaseCommon).ActionType = ActionTypeEnum.DoNothing;
        }

        #endregion //Private methods
    }
}
