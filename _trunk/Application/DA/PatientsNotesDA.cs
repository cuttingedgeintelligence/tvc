﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CEI.Common.PatientsNotes;
using System.Data.SqlClient;
using System.Data;

namespace CEI.DA
{
    public class PatientsNotesDA<T>: BaseDA<T> where T: PatientsNotes
    {

        #region STORED PROCEDURES

        private const string SPT_PATIENTSLOGSPATIENTS_GETPATIENTSNOTES = "spt_PatientsLogsPatients_GetPatientsNotes";
        private const string SPT_PATIENTSLOGSPATIETS_INSERTPATIENTSNOTES = "spt_PatientsLogsPatients_InsertPatientsNotes";
        private const string SPT_PATIENTSLOGSPATIETS_DELETEPATIENTSNOTES = "spt_PatientsLogsPatients_DeletePatientsNotes";
        private const string SPT_PATIENTSLOGSPATIETS_UPDATECUSTOMNOTE = "spt_PatientsLogsPatients_UpdateCustomNote";

        #endregion

        #region CONSTRUCTOR

        public PatientsNotesDA()
        {

        }

        #endregion

        #region PUBLIC METHODS

        public List<T> GetPatientsNotes(int patientsLogId)
        {
            if (typeof(T).Equals(typeof(PracticePatientsNotes)))
            {
                return this.GetPatientsNotes(patientsLogId, false);
            }
            else if (typeof(T).Equals(typeof(InternalPatientsNotes)))
            {
                return this.GetPatientsNotes(patientsLogId, true);
            }
            else return new List<T>();

        }

        public bool InsertPatientsNotes(List<T> items, string customNote, string previewNote)
        {
            bool retVal = false;
            SqlTransaction tran = null;

            try
            {
                if (typeof(T).Equals(typeof(PracticePatientsNotes)))
                {
                    if (DeletePatientsNotes(items[0].PatientLogPatientId, false, ref tran))
                    {
                        retVal = InsertPatientsNotes(items, customNote, previewNote, false, ref tran);
                    }
                }
                else if (typeof(T).Equals(typeof(InternalPatientsNotes)))
                {
                    if (DeletePatientsNotes(items[0].PatientLogPatientId, true, ref tran))
                    {
                        retVal = InsertPatientsNotes(items, customNote, previewNote, true, ref tran);
                    }
                }

                tran.Commit();
               
            }
            catch
            {
                tran.Rollback();
            }

            return retVal;
        }

        #endregion

        #region PRIVATE METHODS

        private List<T> GetPatientsNotes(int id, bool isInternalNotes)
        {
            List<T> retVal = new List<T>();


            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@patientLogPatientId", SqlDbType.Int);
            param[0].Direction = ParameterDirection.Input;
            param[0].IsNullable = false;
            param[0].Value = id;

            param[1] = new SqlParameter("@isInternalNotes", SqlDbType.Bit);
            param[1].Direction = ParameterDirection.Input;
            param[1].IsNullable = false;
            param[1].Value = isInternalNotes;

            DataTable dt = this.GetDataTable(param, SPT_PATIENTSLOGSPATIENTS_GETPATIENTSNOTES);

            foreach (DataRow dr in dt.Rows)
            {
                T t = Activator.CreateInstance<T>();

                t.Id = Convert.ToInt32(dr["id"]);
                t.PatientLogPatientId = Convert.ToInt32(dr["patientLogPatientId"]);
                t.NoteId = Convert.ToInt32(dr["noteId"]);

                retVal.Add(t);
            }

            return retVal;
        }

        private bool InsertPatientsNotes(List<T> items,string customNote,string previewNote, bool isInternalNotes, ref SqlTransaction transaction)
        {
            int inserted = 0;
            int totalCount = items.Count;
            
            
            foreach (T item in items)
            {
                SqlParameter[] param = new SqlParameter[3];

                param[0] = new SqlParameter("@patientLogPatientId", SqlDbType.Int);
                param[0].Direction = ParameterDirection.Input;
                param[0].IsNullable = false;
                param[0].Value = item.PatientLogPatientId;

                param[1] = new SqlParameter("@noteId", SqlDbType.Int);
                param[1].Direction = ParameterDirection.Input;
                param[1].IsNullable = false;
                param[1].Value = item.NoteId;

                param[2] = new SqlParameter("@isInternalNotes", SqlDbType.Bit);
                param[2].Direction = ParameterDirection.Input;
                param[2].IsNullable = false;
                param[2].Value = isInternalNotes;

                inserted += this.CMDExecuteNonQuery(SPT_PATIENTSLOGSPATIETS_INSERTPATIENTSNOTES, param, ref transaction);
            }

            if (totalCount.Equals(inserted) && UpdateCustomNote(items[0].PatientLogPatientId, customNote, previewNote, isInternalNotes, ref transaction))
                return true;
            else
            {
                transaction.Rollback();
                return false;
            }
        }

        private bool UpdateCustomNote(int id, string customNote, string previewNote, bool isInternalNotes, ref SqlTransaction transaction)
        {
            SqlParameter[] param = new SqlParameter[4];

            param[0] = new SqlParameter("@id", SqlDbType.Int);
            param[0].Direction = ParameterDirection.Input;
            param[0].IsNullable = false;
            param[0].Value = id;

            param[1] = new SqlParameter("@customNote", SqlDbType.NVarChar);
            param[1].Direction = ParameterDirection.Input;
            param[1].IsNullable = false;
            param[1].Value = customNote;

            param[2] = new SqlParameter("@previewNote", SqlDbType.NVarChar);
            param[2].Direction = ParameterDirection.Input;
            param[2].IsNullable = false;
            param[2].Value = previewNote;

            param[3] = new SqlParameter("@isInternalNotes", SqlDbType.Bit);
            param[3].Direction = ParameterDirection.Input;
            param[3].IsNullable = false;
            param[3].Value = isInternalNotes;

            return Convert.ToBoolean(this.CMDExecuteNonQuery(SPT_PATIENTSLOGSPATIETS_UPDATECUSTOMNOTE, param, ref transaction));
        }

        private bool DeletePatientsNotes(int patientLogPatientId,bool isInternalNotes, ref SqlTransaction transaction)
        {
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@patientLogPatientId", SqlDbType.Int);
            param[0].Direction = ParameterDirection.Input;
            param[0].IsNullable = false;
            param[0].Value = patientLogPatientId;

            param[1] = new SqlParameter("@isInternalNotes", SqlDbType.Bit);
            param[1].Direction = ParameterDirection.Input;
            param[1].IsNullable = false;
            param[1].Value = isInternalNotes;

            param[2] = new SqlParameter("@isFreshInsert", SqlDbType.Bit);
            param[2].Direction = ParameterDirection.Output;
            param[2].IsNullable = false;
            

            

            bool resultSp = Convert.ToBoolean(this.CMDExecuteNonQuery(SPT_PATIENTSLOGSPATIETS_DELETEPATIENTSNOTES,param,ref transaction));

            if (Convert.ToBoolean(param[2].Value))
                return true;
            else
                return resultSp;
        }

        #endregion

        #region BASE DA IMPLEMENTATION

        protected override System.Collections.Hashtable MapTable()
        {
            throw new NotImplementedException();
        }

        protected override T FillProperties(System.Data.SqlClient.SqlDataReader reader)
        {
            throw new NotImplementedException();
        }

        #endregion

        
    }
}
