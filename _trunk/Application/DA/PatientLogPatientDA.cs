using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class PatientLogPatientDA : BaseDA<PatientLogPatient>
    {
        #region cache keys


        #endregion //cache keys

        #region Stored procedure names

        private const string STP_PATIENTLOGSPATIENTSPATIENTS_INSERT = "dbo.stp_PatientLogsPatients_Insert";
        private const string STP_PATIENTLOGSPATIENTSPATIENTS_INSERT_FROM_EXCEL = "stp_PatientLogsPatients_Insert_From_Excel";
        
        private const string STP_PATIENTLOGSPATIENTS_EXISTS_PATIENTLOGPATIENT_WITH_INSURANCE_COMPANY_ID = "dbo.stp_PatientLogsPatients_ExistsPatientLogPatientWithInsuranceCompanyId";

        private const string  STP_PATIENTLOGSPATIENTS_EXISTS_PATIENTLOGSPATIENT_WITH_PATIENT_ID = "dbo.stp_PatientLogsPatients_ExistsPatientLogsPatientWithPatientId";

        private const string  STP_PATIENTS_PATIENTLOGPATIENTS_INSURANCECOMPANIES_GET_PATIENT_LOG_PATIENTS_BY_PATIENT_LOG_ID   =  "dbo.stp_Patients_PatientLogPatients_InsuranceCompanies_GetPatientLogPatientsByPatientLogId";


        private const string STP_PATIENTLOGSPATIENTSPATIENTS_UPDATE = "dbo.stp_PatientLogsPatients_Update";
        private const string STP_PATIENTLOGSPATIENTSPATIENTS_DELETE = "dbo.stp_PatientLogsPatients_Delete";


       

		#endregion //Stored procedure names


        #region DB access methods

        public bool ExistsPatientLogPatientWithInsuranceCompanyId(int insuranceCompanyId)
        {
            SqlParameter[] parameters = { new SqlParameter("@insuranceCompanyId", insuranceCompanyId) };
            return base.Exists(STP_PATIENTLOGSPATIENTS_EXISTS_PATIENTLOGPATIENT_WITH_INSURANCE_COMPANY_ID, parameters);
        }


        public bool ExistsPatientLogPatientWithPatientId(int patientId)
        {
            SqlParameter[] parameters = { new SqlParameter("@patientId", patientId) };
            return base.Exists(STP_PATIENTLOGSPATIENTS_EXISTS_PATIENTLOGSPATIENT_WITH_PATIENT_ID, parameters);
        }




        public List<PatientLogPatient> GetPatientLogPatientsByPatientLogId(int patientLogId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@patientLogId", patientLogId)
            };
            return base.GetMultipleObjects(parameters, STP_PATIENTS_PATIENTLOGPATIENTS_INSURANCECOMPANIES_GET_PATIENT_LOG_PATIENTS_BY_PATIENT_LOG_ID);
        }

        #endregion //DB access methods

        #region DataTable

       

        #endregion //DataTable

        #region DB CUD methods


        public bool InsertPatientLogPatient(PatientLogPatient patientLogPatient,ref SqlTransaction transaction)
        {


            SqlParameter[] parameters = new SqlParameter[20];
                  
             parameters[0] = new SqlParameter("@id", SqlDbType.Int);
             parameters[0].Direction = ParameterDirection.Output;
             parameters[1] = new SqlParameter("@patientLogId", patientLogPatient.PatientLogId);
             parameters[2] = new SqlParameter("@patientId", patientLogPatient.PatientId);
             parameters[3] = new SqlParameter("@appointmentTime", patientLogPatient.AppointmentTime);
             parameters[4] = new SqlParameter("@glaucoma", patientLogPatient.Glaucoma);
             parameters[5] = new SqlParameter("@retina", patientLogPatient.Retina);
             parameters[6] = new SqlParameter("@insuranceCompanyId", patientLogPatient.InsuranceCompanyId);
             parameters[7] = new SqlParameter("@OD_Sphere", patientLogPatient.OD_Sphere);
             parameters[8] = new SqlParameter("@OS_Sphere", patientLogPatient.OS_Sphere);
             parameters[9] = new SqlParameter("@OD_Cylinder", patientLogPatient.OD_Cylinder);
             parameters[10] = new SqlParameter("@OS_Cylinder", patientLogPatient.OS_Cylinder);
             parameters[11] = new SqlParameter("@OD_AxisOfAstigmatism", patientLogPatient.OD_AxisOfAstigmatism);
             parameters[12] = new SqlParameter("@OS_AxisOfAstigmatism", patientLogPatient.OS_AxisOfAstigmatism);
             parameters[13] = patientLogPatient.OD_Pachymetry.HasValue ? new SqlParameter("@OD_Pachymetry", patientLogPatient.OD_Pachymetry) : new SqlParameter("@OD_Pachymetry", DBNull.Value);
             parameters[14] = patientLogPatient.OS_Pachymetry.HasValue ?  new SqlParameter("@OS_Pachymetry", patientLogPatient.OS_Pachymetry) : new SqlParameter("@OS_Pachymetry", DBNull.Value);
             parameters[15] = new SqlParameter("@patientHistory", patientLogPatient.PatientHistory);
             parameters[16] = new SqlParameter("@doctorId", patientLogPatient.DoctorId);
             parameters[17] = new SqlParameter("@BFA", patientLogPatient.BFA);
             parameters[18] = new SqlParameter("@HRT_Glaucoma", patientLogPatient.HRT_Glaucoma);
             parameters[19] = new SqlParameter("@HRT_Retina", patientLogPatient.HRT_Retina);
              
             int affectedRows = base.CMDExecuteNonQuery(STP_PATIENTLOGSPATIENTSPATIENTS_INSERT, parameters,ref transaction);

             patientLogPatient.Id = (int)parameters[0].Value;

             
             return affectedRows > 0;

        }

        public int InsertPatientLogPatientFromExcel(FinalNotesWorksheetPatient patientLogPatient, ref SqlTransaction transaction)
        {
            int patientLogPatientId = 0;

            SqlParameter[] parameters = new SqlParameter[23];

            parameters[0] = new SqlParameter("@id", SqlDbType.Int);
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1] = new SqlParameter("@patientLogId", patientLogPatient.PatientLogId);
            parameters[2] = new SqlParameter("@patientId", patientLogPatient.PatientId);//1
            parameters[3] = new SqlParameter("@appointmentTime", patientLogPatient.AppointmentTime);//1
           
            parameters[4] = new SqlParameter("@glaucoma", patientLogPatient.Glaucoma);
            parameters[5] = new SqlParameter("@retina", patientLogPatient.Retina);
          
            parameters[6] = new SqlParameter("@insuranceCompanyId", patientLogPatient.InsuranceCompanyId);//1
            parameters[7] = patientLogPatient.OD_Pachymetry.HasValue ? new SqlParameter("@OD_Pachymetry", patientLogPatient.OD_Pachymetry) : new SqlParameter("@OD_Pachymetry", DBNull.Value);//1
            parameters[8] = patientLogPatient.OS_Pachymetry.HasValue ? new SqlParameter("@OS_Pachymetry", patientLogPatient.OS_Pachymetry) : new SqlParameter("@OS_Pachymetry", DBNull.Value);//1
            parameters[9] = new SqlParameter("@patientHistory", patientLogPatient.PatientHistory);//1
            parameters[10] = new SqlParameter("@doctorId", patientLogPatient.DoctorId); //1
            
            parameters[11] = new SqlParameter("@BFA", patientLogPatient.BFA);
          
            if(patientLogPatient.OD_BFA != null)
            parameters[12] = new SqlParameter("@OD_BFA", patientLogPatient.OD_BFA);//1
            else
            parameters[12] = new SqlParameter("@OD_BFA", DBNull.Value);//1

            if (patientLogPatient.OS_IOP != null)
            parameters[13] = new SqlParameter("@OS_IOP", patientLogPatient.OS_IOP);//1
            else
            parameters[13] = new SqlParameter("@OS_IOP", DBNull.Value);//1

            if (patientLogPatient.OD_IOP != null)
            parameters[14] = new SqlParameter("@OD_IOP", patientLogPatient.OD_IOP);//1
            else
            parameters[14] = new SqlParameter("@OD_IOP", DBNull.Value);//1

            if (patientLogPatient.OS_BFA != null)
            parameters[15] = new SqlParameter("@OS_BFA", patientLogPatient.OS_BFA);//1
            else
                parameters[15] = new SqlParameter("@OS_BFA", DBNull.Value);//1

            parameters[16] = new SqlParameter("@internalNotes", patientLogPatient.internalNotes);//1
            parameters[17] = new SqlParameter("@practiceNotes", patientLogPatient.practiceNotes);//1


            


            parameters[18] = new SqlParameter("@status", patientLogPatient.Status);//1


            parameters[19] = new SqlParameter("@internalCustomNotes", patientLogPatient.InternalCustomNotes);//1
            parameters[20] = new SqlParameter("@practiceCustomNotes", patientLogPatient.PracticeCustomNotes);//1

            //     parameters[7] = new SqlParameter("@OD_Sphere", DBNull.Value);
            //     parameters[8] = new SqlParameter("@OS_Sphere", DBNull.Value);
            //     parameters[9] = new SqlParameter("@OD_Cylinder", DBNull.Value);
            //     parameters[10] = new SqlParameter("@OS_Cylinder", DBNull.Value);
            //     parameters[11] = new SqlParameter("@OD_AxisOfAstigmatism", DBNull.Value);
            //     parameters[12] = new SqlParameter("@OS_AxisOfAstigmatism", DBNull.Value);
            parameters[21] = new SqlParameter("@HRT_Glaucoma",patientLogPatient.HRT_Glaucoma);
            parameters[22] = new SqlParameter("@HRT_Retina", patientLogPatient.HRT_Retina);
        

            int affectedRows = base.CMDExecuteNonQuery(STP_PATIENTLOGSPATIENTSPATIENTS_INSERT_FROM_EXCEL, parameters, ref transaction);

            patientLogPatient.Id = (int)parameters[0].Value;
            patientLogPatientId = (int)parameters[0].Value;

            return patientLogPatientId;
            
        }


        public bool UpdatePatientLogPatient(PatientLogPatient patientLogPatient, ref SqlTransaction transaction)
        {


            SqlParameter[] parameters = 
                {
                  
                  new SqlParameter("@id", patientLogPatient.Id)
                 ,new SqlParameter("@appointmentTime", patientLogPatient.AppointmentTime)
                 ,new SqlParameter("@glaucoma", patientLogPatient.Glaucoma)
                 ,new SqlParameter("@retina", patientLogPatient.Retina)

                 ,new SqlParameter("@insuranceCompanyId", patientLogPatient.InsuranceCompanyId)
                 ,new SqlParameter("@OD_Sphere", patientLogPatient.OD_Sphere)
                 ,new SqlParameter("@OS_Sphere", patientLogPatient.OS_Sphere)
                 ,new SqlParameter("@OD_Cylinder", patientLogPatient.OD_Cylinder)
                 ,new SqlParameter("@OS_Cylinder", patientLogPatient.OS_Cylinder)
                 ,new SqlParameter("@OD_AxisOfAstigmatism", patientLogPatient.OD_AxisOfAstigmatism)
                 ,new SqlParameter("@OS_AxisOfAstigmatism", patientLogPatient.OS_AxisOfAstigmatism)
                 ,patientLogPatient.OD_Pachymetry.HasValue ? new SqlParameter("@OD_Pachymetry", patientLogPatient.OD_Pachymetry) : new SqlParameter("@OD_Pachymetry", DBNull.Value) 
                 ,patientLogPatient.OS_Pachymetry.HasValue ?  new SqlParameter("@OS_Pachymetry", patientLogPatient.OS_Pachymetry) : new SqlParameter("@OS_Pachymetry", DBNull.Value)
              
                

                };

            return base.CMDExecuteNonQuery(STP_PATIENTLOGSPATIENTSPATIENTS_UPDATE, parameters, ref transaction) > 0;

        }



        public bool UpdatePatientLogPatient(PatientLogPatient patientLogPatient)
        {


            SqlParameter[] parameters = 
                {
                  
                  new SqlParameter("@id", patientLogPatient.Id)
                 ,new SqlParameter("@appointmentTime", patientLogPatient.AppointmentTime)
                 ,new SqlParameter("@glaucoma", patientLogPatient.Glaucoma)
                 ,new SqlParameter("@retina", patientLogPatient.Retina)

                 ,new SqlParameter("@insuranceCompanyId", patientLogPatient.InsuranceCompanyId)
                 ,new SqlParameter("@OD_Sphere", patientLogPatient.OD_Sphere)
                 ,new SqlParameter("@OS_Sphere", patientLogPatient.OS_Sphere)
                 ,new SqlParameter("@OD_Cylinder", patientLogPatient.OD_Cylinder)
                 ,new SqlParameter("@OS_Cylinder", patientLogPatient.OS_Cylinder)
                 ,new SqlParameter("@OD_AxisOfAstigmatism", patientLogPatient.OD_AxisOfAstigmatism)
                 ,new SqlParameter("@OS_AxisOfAstigmatism", patientLogPatient.OS_AxisOfAstigmatism)
                 ,patientLogPatient.OD_Pachymetry.HasValue ? new SqlParameter("@OD_Pachymetry", patientLogPatient.OD_Pachymetry) : new SqlParameter("@OD_Pachymetry", DBNull.Value) 
                 ,patientLogPatient.OS_Pachymetry.HasValue ?  new SqlParameter("@OS_Pachymetry", patientLogPatient.OS_Pachymetry) : new SqlParameter("@OS_Pachymetry", DBNull.Value)
                 ,new SqlParameter("@BFA", patientLogPatient.BFA)
                 ,new SqlParameter("@HRT_Glaucoma", patientLogPatient.HRT_Glaucoma)
                 ,new SqlParameter("@HRT_Retina", patientLogPatient.HRT_Retina)
                

                };

            return base.CMDExecuteNonQuery(STP_PATIENTLOGSPATIENTSPATIENTS_UPDATE, parameters) > 0;

        }


        public bool DeletePatientLogPatient(PatientLogPatient patientLogPatient, ref SqlTransaction transaction)
        {
            SqlParameter[] parameters = 
                {
                  new SqlParameter("@id", patientLogPatient.Id)
                };
            return base.CMDExecuteNonQuery(STP_PATIENTLOGSPATIENTSPATIENTS_DELETE, parameters, ref transaction) > 0;
        }

        
		
        #endregion  //DB CUD methods

      


       

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }



        protected override PatientLogPatient FillProperties(SqlDataReader dataReader)
        {
            PatientLogPatient patientLogPatient = new PatientLogPatient();
            
			for (int i = 0; i < dataReader.FieldCount; i++)
			{
				switch (dataReader.GetName(i))
				{

					case "id":
						patientLogPatient.Id = (int)dataReader[i];
						break;
					case "fullName":
                        patientLogPatient.FullName = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
						break;
                    case "firstName":
                        patientLogPatient.FirstName = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
                        break;
                    case "lastName":
                        patientLogPatient.LastName = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
                        break;
                    case "dateOfBirth":
                        patientLogPatient.DateOfBirth = (DateTime)dataReader[i];
                        break;
                    case "gender":
                        patientLogPatient.Gender = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
                        break;
                    case "patientHistory":
                        patientLogPatient.PatientHistory = (PatientHistoryEnum)Enum.Parse(typeof(PatientHistoryEnum), dataReader[i].ToString()); 
                        break;
					case "insuranceCompanyId":
						patientLogPatient.InsuranceCompanyId = (int)dataReader[i];
						break;
					case "insuranceCompanyName":
                        patientLogPatient.InsuranceCompanyName = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
						break;
                    case "patientId":
						patientLogPatient.PatientId = (int)dataReader[i];
						break;
					case "OD_Sphere":
                        patientLogPatient.OD_Sphere = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
						break;
                    case "OS_Sphere":
                        patientLogPatient.OS_Sphere = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
						break;
                    case "OD_Cylinder":
                        patientLogPatient.OD_Cylinder = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
						break;
                    case "OS_Cylinder":
                        patientLogPatient.OS_Cylinder = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
						break;
                    case "OD_AxisOfAstigmatism":
                        patientLogPatient.OD_AxisOfAstigmatism = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
						break;
                    case "OS_AxisOfAstigmatism":
                        patientLogPatient.OS_AxisOfAstigmatism = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
						break;
                    case "OD_Pachymetry":
                        if (dataReader[i] != DBNull.Value)
						    patientLogPatient.OD_Pachymetry = (short)dataReader[i];
						break;
                    case "OS_Pachymetry":
                        if (dataReader[i] != DBNull.Value)
                            patientLogPatient.OS_Pachymetry = (short)dataReader[i];
						break;
                    case "appointmentTime":
                        patientLogPatient.AppointmentTime = (dataReader[i] == DBNull.Value) ? string.Empty : (string)dataReader[i];
						break;
                    case "glaucoma":
						patientLogPatient.Glaucoma = (bool)dataReader[i];
						break;
					case "retina":
						patientLogPatient.Retina = (bool)dataReader[i];
						break;
                    case "HRT_Glaucoma":
                        patientLogPatient.HRT_Glaucoma = (decimal)dataReader[i];
                        break;
                    case "HRT_Retina":
                        patientLogPatient.HRT_Retina = (decimal)dataReader[i];
                        break;
                    //case "status":
                    //    patientLogPatient.Status = (short)dataReader[i];
                    //    break;
				    
				}

			}

            return patientLogPatient;

			
        }

        #endregion //mapping methods

    }
}
