using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;
using System.Threading;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class PatientLogDA : BaseDA<PatientLog>
    {
        #region cache keys


        #endregion //cache keys

        #region Stored procedure names

        private const string STP_PATINETLOGSERVICEDATE_BYPRACTICEID = "stp_PatientLogServiceDate_byPracticeId";

        private const string STP_PATIENTLOGS_INSERT = "dbo.stp_PatientLogs_Insert";
        private const string STP_PATIENTLOGS_INSERT_FROM_EXCEL = "dbo.stp_PatientLogs_Insert_From_Excel";
        
        private const string STP_PATIENTLOGS_UPDATE = "dbo.stp_PatientLogs_Update";
       

        private const string STP_PATIENTLOGS_PATIENTLOGSPATIENTS_DELETE_PATIENT_LOG = "dbo.stp_PatientLogs_PatientLogsPatients_DeletePatientLog";

        private const string STP_PATIENTLOGS_USERS_GET_PATIENTLOGS_SEARCH = "dbo.stp_PatientLogs_Users_GetPatientLogsSearch";

   

         private const string STP_PATIENTLOGS_UPDATE_STATUS = "dbo.stp_PatientLogs_UpdateStatus";

         private const string STP_PATIENTLOGS_USERS_GET_COMPLETED_PATIENT_LOGS_SEARCH = "dbo.stp_PatientLogs_Users_GetCompletedPatientLogsSearch";

         private const string STP_PATIENTLOGS_GET_BY_PRACTICE_ID_AND_SERVICE_DATE = "dbo.stp_PatientLogs_GetByPracticeIdAndServiceDate";

         private const string STP_PATIENTLOGS_USERS_GET_PATIENT_LOGS_FOR_SUBMISSION = "dbo.stp_PatientLogs_Users_GetPatientLogsForSubmission";

         private const string STP_PATIENTLOGS_GET_PATIENT_LOGS_SERVICE_DATES_BY_PRACTICE_ID = "dbo.stp_PatientLogs_GetPatientLogsServiceDatesByPracticeId";

         private const string STP_PATIENTLOGS_UPDATE_DESKTOP_IMPORTED_DATA =  "dbo.stp_PatientLogs_UpdateDesktopImportedData";
         private const string STP_PATIENTLOGSPATIENTSCPTCODES_INSERT = "dbo.stp_PatientLogsPatientsCPTCodes_Insert";
         private const string STP_CPTCODES_FOR_IMPORT = "dbo.stp_CPTCodes_ForImport";


         private const string STP_GETDATATABLES_INSERT_FROM_EXCEL = "dbo.stp_GetTablesToInsertfromExcel";

        


		#endregion //Stored procedure names


        #region DB access methods

    
        #endregion //DB access methods

        #region DataTable


        public DataTable GetPatientLogsSearch(Guid practiceId,PatientLogStatusEnum status, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[5];


            if (practiceId.Equals(Guid.Empty))
                parameters[0] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@practiceId", practiceId);

            parameters[1] = new SqlParameter("@status", status);

            parameters[2] = new SqlParameter("@pageSize", pageSize);
            parameters[3] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[4] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[4].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_PATIENTLOGS_USERS_GET_PATIENTLOGS_SEARCH);

            totalCount = (int)parameters[4].Value;

            return table;
        }

        public DataTable GetCompletedPatientLogsSearch(Guid practiceId, PatientLogStatusEnum status, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[5];


            if (practiceId.Equals(Guid.Empty))
                parameters[0] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@practiceId", practiceId);

            parameters[1] = new SqlParameter("@status", status);

            parameters[2] = new SqlParameter("@pageSize", pageSize);
            parameters[3] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[4] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[4].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_PATIENTLOGS_USERS_GET_COMPLETED_PATIENT_LOGS_SEARCH);

            totalCount = (int)parameters[4].Value;

            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceDate"></param>
        /// <param name="practiceId"></param>
        /// <returns></returns>
        public DataTable GetPatientLogByPracticeIdAndServiceDate(DateTime serviceDate, Guid practiceId)
        {
            SqlParameter[] parameters = 
            {
                 new SqlParameter("@serviceDate", serviceDate)
                ,new SqlParameter("@practiceId", practiceId)
               
            };
            return base.GetDataTable(parameters, STP_PATIENTLOGS_GET_BY_PRACTICE_ID_AND_SERVICE_DATE);
           
            
        }

        /// <summary>
        /// Get all patient logs for submission; deadlinedate == today.AddDays(2)
        /// </summary> 
        /// <param name="deadlineDate"></param>
        /// <param name="patinentLogStatus"></param>
        /// <returns></returns>
        public DataTable GetPatientLogsForSubmission(DateTime deadlineDate, PatientLogStatusEnum patinentLogStatus)
        {
            SqlParameter[] parameters = 
            {
                 new SqlParameter("@deadlineDate", deadlineDate)
                ,new SqlParameter("@status", patinentLogStatus)
               
            };
            return base.GetDataTable(parameters, STP_PATIENTLOGS_USERS_GET_PATIENT_LOGS_FOR_SUBMISSION);
        }


        /// <summary>
        /// Get patient logs service dates(ordered desc) by practice Id 
        /// Used in: FTP functionality
        /// </summary>
        /// <param name="practiceId"></param>
        /// <returns>data table : serviceDate , patientLogId , desktopImportedDate , desktopImportedComment</returns>
        public DataTable GetPatientLogsServiceDatesByPracticeId(Guid practiceId)
        {
            SqlParameter[] parameters = 
            {
                 new SqlParameter("@practiceId", practiceId)
            };
            return base.GetDataTable(parameters, STP_PATIENTLOGS_GET_PATIENT_LOGS_SERVICE_DATES_BY_PRACTICE_ID);
        }


        /// <summary>
        /// Returns table names for insert fro mexcel
        /// </summary>
        /// <returns></returns>
        public DataTable GetTablesToInsertFromExcel()
        {
            //stp_GetTablesToInsertfromExcel
            return base.GetDataTable(STP_GETDATATABLES_INSERT_FROM_EXCEL);
        }


        #endregion //DataTable

        #region DB CUD methods

        

        public bool InsertPatientLog(PatientLog patientLog)
        {
           bool returnValue = false;
           SqlTransaction transaction = null;
           try
           {
               SqlParameter[] parameters = new SqlParameter[6];
               
                parameters[0] = new SqlParameter("@id", SqlDbType.Int);
                parameters[0].Direction = ParameterDirection.Output;

                parameters[1] = new SqlParameter("@practiceId", patientLog.PracticeId);
                parameters[2] = new SqlParameter("@serviceDate", patientLog.ServiceDate);
                parameters[3] = new SqlParameter("@createdBy", patientLog.CreatedBy);
                parameters[4] = new SqlParameter("@status", patientLog.Status);
                parameters[5] = new SqlParameter("@numOfPatients", patientLog.NumOfPatients);
                base.CMDExecuteNonQuery(STP_PATIENTLOGS_INSERT, parameters,ref transaction);

                patientLog.Id = (int)parameters[0].Value;

                foreach (PatientLogPatient patientLogPatient in patientLog.PatientLogPatients)
                {
                    patientLogPatient.PatientLogId = patientLog.Id;
                    new PatientLogPatientDA().InsertPatientLogPatient(patientLogPatient, ref transaction);
                }


               transaction.Commit();
               returnValue = true;
           }


           catch (Exception exc)
           {
               transaction.Rollback();

           }
           finally
           {
               if (transaction.Connection != null)
                   transaction.Connection.Close();
           }

           return returnValue;



        }


        //public bool InsertPatientLogFromExcel(PatientLog patientLog)
        //{
        //    bool returnValue = false;
        //    SqlTransaction transaction = null;
        //    try
        //    {
        //        SqlParameter[] parameters = new SqlParameter[12];

        //        parameters[0] = new SqlParameter("@id", SqlDbType.Int);
        //        parameters[0].Direction = ParameterDirection.Output;

        //        parameters[1] = new SqlParameter("@practiceId", patientLog.PracticeId);
        //        parameters[2] = new SqlParameter("@serviceDate", patientLog.ServiceDate);
        //        parameters[3] = new SqlParameter("@createdBy", patientLog.CreatedBy);
        //        parameters[4] = new SqlParameter("@status", patientLog.Status);
        //        parameters[5] = new SqlParameter("@numOfPatients", patientLog.PatientLogPatients.Count);
        //        ///
        //        if(patientLog.DesktopImportedDate.ToString() != "1/1/0001 12:00:00 AM")
        //        parameters[6] = new SqlParameter("@desktopImportedDate", patientLog.DesktopImportedDate);
        //        else
        //        parameters[6] = new SqlParameter("@desktopImportedDate", DBNull.Value);
        //        if (patientLog.DesktopImportedComment.ToString() != "")
        //        parameters[7] = new SqlParameter("@desktopImportedComment", patientLog.DesktopImportedComment);
        //        else
        //            parameters[7] = new SqlParameter("@desktopImportedComment", DBNull.Value);
        //        parameters[8] = new SqlParameter("@dateCreated", patientLog.DateCreated);
        //        parameters[9] = new SqlParameter("@completedById", patientLog.CompletedById);
        //        parameters[10] = new SqlParameter("@completedOn", patientLog.CompletedOn);
        //        parameters[11] = new SqlParameter("@technicianId", patientLog.TechnicianId);
        //        base.CMDExecuteNonQuery(STP_PATIENTLOGS_INSERT_FROM_EXCEL, parameters, ref transaction);

        //        patientLog.Id = (int)parameters[0].Value;

        //        foreach (PatientLogPatient patientLogPatient in patientLog.PatientLogPatients)
        //        {
        //            patientLogPatient.PatientLogId = patientLog.Id;
        //            new PatientLogPatientDA().InsertPatientLogPatientFromExcel(patientLogPatient, ref transaction);
        //        }


        //        transaction.Commit();
                
        //        returnValue = true;
        //    }


        //    catch (Exception exc)
        //    {
        //        transaction.Rollback();

        //    }
        //    finally
        //    {
        //        if (transaction.Connection != null)
        //        {
                    
        //            transaction.Connection.Close();
        //            transaction.Connection.Dispose();
        //         //   GC.SuppressFinalize(transaction.Connection);
        //        }
        //        if (transaction != null)
        //        {
                    
        //            transaction.Dispose();
        //            GC.SuppressFinalize(transaction);

        //            //System.Threading.Thread.CurrentThread.Sleep(5000);

        //            //Thread.CurrentThread.currentThread().sleep(5000)
                    
        //           // Thread.Sleep(5000);
                    
        //        }

                
        //    }

        //    return returnValue;



        //}

        private const string STP_PATIENTLOGS_PATIENTLOGSPATIENTS_GET_FOLLOW_UP_PATIENTS = "dbo.stp_PatientLogs_PatientLogsPatients_GetFollowUpPatients";
        private const string STP_FOLLOWUPS_INSERT = "dbo.stp_FollowUps_Insert";
        private const string STP_FOLLOWUPPATIENTS_INSERT = "dbo.stp_FollowUpPatients_Insert";
        private const string STP_PATIENTLOGS_PATIENTLOGSPATIENTS_GETPATIENTLOGPATIENTSTATUS_IMPORT_FROM_EXCEL = "dbo.stp_PatientLogs_PatientLogsPatients_GetPatientLogPatientStatus_ImportFromExcel";

        public bool InsertPatientLogFromExcel(FinalNotesWorksheet patientLog, DateTime nextAppointmentDate, DateTime retinaExamDate, DateTime glaucomaExamDate,int CPTCodeId)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {
                SqlParameter[] parameters = new SqlParameter[12];

                parameters[0] = new SqlParameter("@id", SqlDbType.Int);
                parameters[0].Direction = ParameterDirection.Output;

                parameters[1] = new SqlParameter("@practiceId", patientLog.PracticeId);
                parameters[2] = new SqlParameter("@serviceDate", patientLog.ServiceDate);
                parameters[3] = new SqlParameter("@createdBy", patientLog.CreatedBy);
                parameters[4] = new SqlParameter("@status", patientLog.Status);
                parameters[5] = new SqlParameter("@numOfPatients", patientLog.FinalNotesWorksheetPatients.Count);
                ///
                if (patientLog.DesktopImportedDate.ToString() != "1/1/0001 12:00:00 AM")
                    parameters[6] = new SqlParameter("@desktopImportedDate", patientLog.DesktopImportedDate);
                else
                    parameters[6] = new SqlParameter("@desktopImportedDate", DBNull.Value);
                if (patientLog.DesktopImportedComment.ToString() != "")
                    parameters[7] = new SqlParameter("@desktopImportedComment", patientLog.DesktopImportedComment);
                else
                    parameters[7] = new SqlParameter("@desktopImportedComment", DBNull.Value);
                parameters[8] = new SqlParameter("@dateCreated", patientLog.DateCreated);
                parameters[9] = new SqlParameter("@completedById", patientLog.CompletedById);
                parameters[10] = new SqlParameter("@completedOn", patientLog.CompletedOn);
                parameters[11] = new SqlParameter("@technicianId", patientLog.TechnicianId);
                base.CMDExecuteNonQuery(STP_PATIENTLOGS_INSERT_FROM_EXCEL, parameters, ref transaction);

                patientLog.Id = (int)parameters[0].Value;
                int  patientLogPatientId=0;
                foreach (FinalNotesWorksheetPatient patientLogPatient in patientLog.FinalNotesWorksheetPatients)
                {
                    patientLogPatient.PatientLogId = patientLog.Id;
                    //STP_PATIENTLOGS_PATIENTLOGSPATIENTS_GETPATIENTHISTORYSTATUS_IMPORT_FROM_EXCEL
                    //set patient history status
                    SqlParameter[] parametersPatientLogPatientStatus = 
                    {
                        new SqlParameter("@patientLog", patientLog.Id)
                       ,new SqlParameter("@practiceId", patientLog.PracticeId)
                       ,new SqlParameter("@finalNotesWorksheetStatusId", PatientLogStatusEnum.Final__notes__worksheet)
                       ,new SqlParameter("@patientId", patientLogPatient.PatientId)
                    };

                    DataTable dtPatientLogPatientStatus = base.GetDataTable(parametersPatientLogPatientStatus, STP_PATIENTLOGS_PATIENTLOGSPATIENTS_GETPATIENTLOGPATIENTSTATUS_IMPORT_FROM_EXCEL, ref transaction);
                    if (dtPatientLogPatientStatus.Rows.Count == 0)
                        patientLogPatient.PatientHistory = PatientHistoryEnum.New__Patient;
                    else
                    {
                        PatientLogPatientStatusEnum patientStatus = (PatientLogPatientStatusEnum)Enum.Parse(typeof(PatientLogPatientStatusEnum), dtPatientLogPatientStatus.Rows[0]["status"].ToString());
                        if (patientStatus.Equals(PatientLogPatientStatusEnum.Examined))
                            patientLogPatient.PatientHistory = PatientHistoryEnum.Follow__Up;
                        else if (patientStatus.Equals(PatientLogPatientStatusEnum.No__Show))
                            patientLogPatient.PatientHistory = PatientHistoryEnum.No__Show;
                           
                    }



                   patientLogPatientId=new PatientLogPatientDA().InsertPatientLogPatientFromExcel(patientLogPatient, ref transaction);
                   if (patientLogPatientId > 0)
                    {
                       
                    

                          this.InsertWorksheetPatientCPTCode(CPTCodeId, patientLogPatientId, ref transaction);

                    }
                   
                }


                if (nextAppointmentDate != DateTime.MinValue)
                {
                    //get follow up patients for the next appointment date
                    SqlParameter[] parametersGetFollowUp = 
                     {
                       new SqlParameter("@retinaExamDate",retinaExamDate) 
                      ,new SqlParameter("@glaucomaExamDate",glaucomaExamDate)
                      ,new SqlParameter("@practiceId",patientLog.PracticeId)
                      ,new SqlParameter("@finalNotestWorksheetStatusId",PatientLogStatusEnum.Final__notes__worksheet)
                      ,new SqlParameter("@examinedPatientStatusId ",PatientLogPatientStatusEnum.Examined)
                     };
                    DataTable dtFollowUpPatients = base.GetDataTable(parametersGetFollowUp, STP_PATIENTLOGS_PATIENTLOGSPATIENTS_GET_FOLLOW_UP_PATIENTS, ref transaction);
                    if (dtFollowUpPatients.Rows.Count > 0)
                    {

                        SqlParameter[] parametersFollowUp = new SqlParameter[5];
                        parametersFollowUp[0] = new SqlParameter("@id", SqlDbType.Int);
                        parametersFollowUp[0].Direction = ParameterDirection.Output;
                        parametersFollowUp[1] = new SqlParameter("@practiceId", patientLog.PracticeId);
                        parametersFollowUp[2] = new SqlParameter("@serviceDate", nextAppointmentDate);
                        parametersFollowUp[3] = new SqlParameter("@createdBy", patientLog.CreatedBy);
                        parametersFollowUp[4] = new SqlParameter("@numOfPatients", dtFollowUpPatients.Rows.Count);

                        base.CMDExecuteNonQuery(STP_FOLLOWUPS_INSERT, parametersFollowUp, ref transaction);

                        int followUpId = (int)parametersFollowUp[0].Value;


                        foreach (DataRow drFollowUpPatient in dtFollowUpPatients.Rows)
                        {
                            this.InsertFollowUpPatient(drFollowUpPatient, followUpId, ref transaction);
                        }

                    }
                }


                transaction.Commit();

                returnValue = true;
            }


            catch (Exception exc)
            {
                transaction.Rollback();

            }
            finally
            {
                if (transaction.Connection != null)
                {

                    transaction.Connection.Close();
                    transaction.Connection.Dispose();
                    //   GC.SuppressFinalize(transaction.Connection);
                }
                if (transaction != null)
                {

                    transaction.Dispose();
                    GC.SuppressFinalize(transaction);

                    //System.Threading.Thread.CurrentThread.Sleep(5000);

                    //Thread.CurrentThread.currentThread().sleep(5000)

                    // Thread.Sleep(5000);

                }


            }

            return returnValue;



        }
        private bool InsertWorksheetPatientCPTCode(int CPT_CodeId, int patientLogPatientId, ref SqlTransaction transaction)
        {
            SqlParameter[] parameters =
            {
                 new SqlParameter("@patientLogPatientId", patientLogPatientId)
                ,new SqlParameter("@CPT_CodeId", CPT_CodeId)
            };
            return base.CMDExecuteNonQuery(STP_PATIENTLOGSPATIENTSCPTCODES_INSERT, parameters, ref transaction) > 0;
        }


        public DataTable GetCPTCodeIdForExcelImport(string CPTCodeName)
        {

              SqlParameter[] parameters =
            {
                 new SqlParameter("@code", CPTCodeName)
               
            };
             return base.GetDataTable(parameters,STP_CPTCODES_FOR_IMPORT);
                    
        }
        


        private void InsertFollowUpPatient(DataRow drFollowUpPatient, int followUpId, ref SqlTransaction transaction)
        {
            SqlParameter[] parameters = 
             {
               new SqlParameter("@followUpId",followUpId) 
              ,new SqlParameter("@patientLogPatientId",(int)drFollowUpPatient["patientLogPatientId"])
              ,new SqlParameter("@patientLogServiceDate",(DateTime)drFollowUpPatient["serviceDate"])
              ,new SqlParameter("@patientId",(int)drFollowUpPatient["patientId"])
              ,new SqlParameter("@glaucoma ",(bool)drFollowUpPatient["glaucoma"])
              ,new SqlParameter("@retina ",(bool)drFollowUpPatient["retina"])
              ,new SqlParameter("@status ",(short)drFollowUpPatient["status"])
             };

            base.CMDExecuteNonQuery(STP_FOLLOWUPPATIENTS_INSERT, parameters, ref transaction);
        }




        public DataTable GetPatientlodserviceDate_ByPracticeId(DateTime serviceDate, Guid practiceId, string appointmentTime)
        {
            SqlParameter[] parameters = {
                   new SqlParameter("@serviceDate", serviceDate)
                  ,new SqlParameter("@practiceId", practiceId)

                  ,new SqlParameter("@appointmentTime", appointmentTime)
                                        };

            DataTable dtNoteCategory = base.GetDataTable(parameters, STP_PATINETLOGSERVICEDATE_BYPRACTICEID);
            return dtNoteCategory;
        }

        public bool UpdatePatientLog(PatientLog patientLog)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {
                SqlParameter[] parameters = 
                {

                        new SqlParameter("@id", patientLog.Id)
                         ,new SqlParameter("@numOfPatients", patientLog.NumOfPatients)
                };

                base.CMDExecuteNonQuery(STP_PATIENTLOGS_UPDATE, parameters, ref transaction);

                

                foreach (PatientLogPatient patientLogPatient in patientLog.PatientLogPatients)
                {
                    new PatientLogPatientDA().InsertPatientLogPatient(patientLogPatient, ref transaction);
                    
                }

                if(transaction != null)
                    transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                if (transaction != null)
                    transaction.Rollback();

            }
            finally
            {
                if (transaction != null && transaction.Connection != null)
                    transaction.Connection.Close();
            }

            return returnValue;



        }


        public bool PatientLogSubmit(PatientLog patientLog)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {
                SqlParameter[] parameters = 
                {

                        new SqlParameter("@id", patientLog.Id)
                       
                        ,new SqlParameter("@status", patientLog.Status)
                       
                };

                base.CMDExecuteNonQuery(STP_PATIENTLOGS_UPDATE_STATUS, parameters, ref transaction);



              

                if(transaction != null)
                    transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                if (transaction != null)
                    transaction.Rollback();

            }
            finally
            {
                if (transaction != null && transaction.Connection != null)
                    transaction.Connection.Close();
            }

            return returnValue;



        }


        public bool DeletePatientLog(int id)
        {

            SqlParameter[] parameters = {
                  new SqlParameter("@id",id) };

            return base.CMDExecuteNonQuery(STP_PATIENTLOGS_PATIENTLOGSPATIENTS_DELETE_PATIENT_LOG, parameters) > 0;

        }



        public bool DeletePatientLogPatient(PatientLog patientLog)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {
                SqlParameter[] parameters = 
                {

                        new SqlParameter("@id", patientLog.Id)
                       
                         ,new SqlParameter("@numOfPatients", patientLog.NumOfPatients)
                };

                base.CMDExecuteNonQuery(STP_PATIENTLOGS_UPDATE, parameters, ref transaction);

                foreach (PatientLogPatient patientLogPatient in patientLog.PatientLogPatients)
                    new PatientLogPatientDA().DeletePatientLogPatient(patientLogPatient, ref transaction);
                

                if (transaction != null)
                    transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                if (transaction != null)
                    transaction.Rollback();

            }
            finally
            {
                if (transaction != null && transaction.Connection != null)
                    transaction.Connection.Close();
            }

            return returnValue;



        }

        /// <summary>
        /// Update patient log desktop imported data(desktopImportedDate and desktopImportedComment) 
        /// </summary>
        /// <param name="patientLogId"></param>
        /// <param name="desktopImportedDate"></param>
        /// <param name="desktopImportedComment"></param>
        /// <returns></returns>
        public bool UpdatePatientLogDesktopImportedData(int patientLogId,DateTime desktopImportedDate,string desktopImportedComment)
        {

            SqlParameter[] parameters = 
            {
                  new SqlParameter("@patientLogId",patientLogId) 
                 ,(desktopImportedDate == DateTime.MinValue) ? new SqlParameter("@desktopImportedDate", DBNull.Value) : new SqlParameter("@desktopImportedDate", desktopImportedDate)
                 ,new SqlParameter("@desktopImportedComment",desktopImportedComment) 
            };

            return base.CMDExecuteNonQuery(STP_PATIENTLOGS_UPDATE_DESKTOP_IMPORTED_DATA, parameters) > 0;

        }

		
        #endregion  //DB CUD methods

      


       

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override PatientLog FillProperties(SqlDataReader dataReader)
        {
            return null;
			
        }

        #endregion //mapping methods

    }
}
