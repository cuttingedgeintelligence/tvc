﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CEI.Common;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

namespace CEI.DA
{
    public class DoctorDA : BaseDA<Doctor>
    {
        #region cache keys

        #endregion //cache keys

        #region Stored procedure names

        private const string STP_DOCTORS_GET_DOCTOR_BY_ID = "dbo.stp_Doctors_GetDoctorById";

        private const string STP_DOCTORS_GET_DOCTORS_BY_PRACTICEID = "dbo.stp_Doctors_GetDoctorsByPracticeId";

        private const string STP_DOCTORS_INSERT_DOCTOR = "dbo.stp_Doctors_InsertDoctor";

        private const string STP_DOCTORS_UPDATE_DOCTOR = "dbo.stp_Doctors_UpdateDoctor";

        private const string STP_DOCTORS_DELETE_DOCTOR = "dbo.stp_Doctors_DeleteDoctor";

        private const string STP_DOCTORS_EXISTS_DOCTOR_WITH_PRACTICE_ID = "dbo.stp_Doctors_ExistsDoctorWithPracticeId"; 

        #endregion //Stored procedure names

        #region DB access methods

        public bool ExistsDoctorWithPracticeId(Guid practiceId)
        {
            SqlParameter[] parameters = { new SqlParameter("@practiceId", practiceId) };
            return base.Exists(STP_DOCTORS_EXISTS_DOCTOR_WITH_PRACTICE_ID, parameters);
        }

        #endregion //DB access methods

        #region DataTable

        public DataTable GetDoctorsByPracticeId(Guid practiceId, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[4];

            parameters[0] = new SqlParameter("@practiceId", practiceId);
            parameters[1] = new SqlParameter("@pageSize", pageSize);
            parameters[2] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[3] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[3].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_DOCTORS_GET_DOCTORS_BY_PRACTICEID);

            totalCount = (int)parameters[3].Value;

            return table;
        }

        public DataRow GetDoctorById(int id)
        {
            SqlParameter[] parameters = new SqlParameter[1];

            parameters[0] = new SqlParameter("@id", id);

            DataTable table = base.GetDataTable(parameters, STP_DOCTORS_GET_DOCTOR_BY_ID);

            return table.Rows[0];
        }

        #endregion //DataTable

        #region DB CUD methods

        public bool InsertDoctor(Doctor doctor, ref SqlTransaction transaction)
        {
            bool returnValue = false;


            SqlParameter[] parameters = new SqlParameter[4];

            parameters[0] = new SqlParameter("@id", SqlDbType.Int);
            parameters[0].Direction = ParameterDirection.Output;

            parameters[1] = new SqlParameter("@firstName", doctor.FirstName);
            parameters[2] = new SqlParameter("@lastName", doctor.LastName);
            parameters[3] = new SqlParameter("@practiceId", doctor.PracticeId);

            base.CMDExecuteNonQuery(STP_DOCTORS_INSERT_DOCTOR, parameters, ref transaction);

            doctor.Id = (int)parameters[0].Value;

            returnValue = true;


            return returnValue;
        }

        public bool InsertDoctor(Doctor doctor)
        {
            bool returnValue = false;

            SqlParameter[] parameters = new SqlParameter[4];

            parameters[0] = new SqlParameter("@id", SqlDbType.Int);
            parameters[0].Direction = ParameterDirection.Output;

            parameters[1] = new SqlParameter("@firstName", doctor.FirstName);
            parameters[2] = new SqlParameter("@lastName", doctor.LastName);
            parameters[3] = new SqlParameter("@practiceId", doctor.PracticeId);

            base.CMDExecuteNonQuery(STP_DOCTORS_INSERT_DOCTOR, parameters);

            doctor.Id = (int)parameters[0].Value;

            returnValue = true;

            return returnValue;
        }

        public bool UpdateDoctor(Doctor doctor)
        {

            SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",doctor.Id)
                 ,new SqlParameter("@firstName", doctor.FirstName)
                 ,new SqlParameter("@lastName", doctor.LastName)
                };

            return base.CMDExecuteNonQuery(STP_DOCTORS_UPDATE_DOCTOR, parameters) > 0;

        }

        public bool DeleteDoctor(int id)
        {
            SqlParameter[] parameters = {
                  new SqlParameter("@id",id) };

            base.CMDExecuteNonQuery(STP_DOCTORS_DELETE_DOCTOR, parameters);
            
            return true;            
        }

        #endregion  //DB CUD methods


        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override Doctor FillProperties(SqlDataReader dataReader)
        {
            return null;
        }

        #endregion //mapping methods
    }
}
