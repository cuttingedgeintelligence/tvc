﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class NoteTypesDA : BaseDA<NoteTypes>
    {
        #region Stored procedure names

        private const string STP_NOTE_CATEGORIES_INSERT = "dbo.stp_NoteCategories_Insert";
        private const string STP_NOTE_CATEGORIES_UPDATE = "dbo.stp_NoteCategories_Update";
        private const string STP_NOTE_CATEGORIES_DELETE = "dbo.stp_NoteCategories_Delete";
        private const string STP_NOTE_TYPES_GET_ALL = "dbo.stp_NoteTypes_GetAll";

        #endregion //Stored procedure names

        #region cache keys

        private const string CACHE_KEY_DATATABLE_NOTE_CATEGORIES = "CACHE_KEY_DATATABLE:NOTECATEGORIES";

        #endregion // cache keys

        #region DB CUD methods


        //public bool InsertNoteCategory(NoteCategory  noteCategory)
        //{


        //    SqlParameter[] parameters = 
        //        {
                  
        //          new SqlParameter("@category", noteCategory.Category )
                
        //        };

        //    return base.CMDExecuteNonQuery(STP_NOTE_CATEGORIES_INSERT, parameters) > 0;

        //}



        //public bool UpdateNoteCategory(NoteCategory noteCategory)
        //{

        //    SqlParameter[] parameters = 
        //        {
        //          new SqlParameter("@id",noteCategory.Id)
        //         ,new SqlParameter("@category", noteCategory.Category )

        //        };

        //    return base.CMDExecuteNonQuery(STP_NOTE_CATEGORIES_UPDATE, parameters) > 0;

        //}


        //public bool DeleteNoteCategory(int id)
        //{

        //    SqlParameter[] parameters = {
        //          new SqlParameter("@id",id) };

        //    return base.CMDExecuteNonQuery(STP_NOTE_CATEGORIES_DELETE, parameters) > 0;

        //}




        #endregion  //DB CUD methods

        #region DataTable

        public DataTable GetAllNoteTypes()
        {
            //DataTable dtNoteCategories = base.GetFromCache(CACHE_KEY_DATATABLE_NOTE_CATEGORIES) as DataTable;
            //if (dtNoteCategories == null)
            //{

            //    dtNoteCategories = base.GetDataTable(STP_NOTE_CATEGORIES_GET_ALL);
            //    base.InsertInCache(CACHE_KEY_DATATABLE_NOTE_CATEGORIES, dtNoteCategories);
            //    return dtNoteCategories;
            //}
            //else
            //    return dtNoteCategories;

            DataTable dtNoteTypes = base.GetDataTable(STP_NOTE_TYPES_GET_ALL);
            return dtNoteTypes;
        }

        #endregion //DataTable

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override NoteTypes FillProperties(SqlDataReader dataReader)
        {
            return null;


        }

        #endregion //mapping methods
    }
}
