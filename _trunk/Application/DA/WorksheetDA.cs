using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class WorksheetDA : BaseDA<Worksheet>
    {
        #region cache keys


        #endregion //cache keys

        #region Stored procedure names

        
        
        private const string STP_PATIENTLOGS_UPDATE_WORKSHEET = "dbo.stp_PatientLogs_UpdateWorksheet";        
        private const string STP_PATIENTLOGS_USERS_GET_WORKSHEETS_SEARCH = "dbo.stp_PatientLogs_Users_GetWorksheetsSearch";

        private const string STP_PATIENTLOGSPATIENTS_PATIENTLOGSPATIENTSCPTCODES_GET_COUNT_OF_PATIENTS_WITH_ASSIGNED_CPT_CODES_BY_PATIENT_LOG_ID = "dbo.stp_PatientLogsPatients_PatientLogsPatientsCPTCodes_GetCountOfPatientsWithAssignedCPTCodesByPatientLogId";

        private const string STP_PATIENTLOGS_EXISTS_WORKSHEET_WITH_TECHNICIAN_ID = "dbo.stp_PatientLogs_ExistsWorksheetWithTechnicianId";

        private const string STP_PATIENTLOGS_USERS_GET_COMPLETED_WORKSHEETS_SEARCH = "dbo.stp_PatientLogs_Users_GetCompletedWorksheetsSearch";

		#endregion //Stored procedure names


        #region DB access methods

        /// <summary>
        /// returns number of patients with assigned CPT codes(at least one) by patient log id
        /// </summary>
        /// <param name="patientLogId"></param>
        /// <returns></returns>
        public int GetCountOfPatientsWithAssignedCPTCodesByPatientLogId(int patientLogId)
        {
            SqlParameter[] parameters = { new SqlParameter("@patientLogId", patientLogId) };
            return base.GetCount(STP_PATIENTLOGSPATIENTS_PATIENTLOGSPATIENTSCPTCODES_GET_COUNT_OF_PATIENTS_WITH_ASSIGNED_CPT_CODES_BY_PATIENT_LOG_ID, parameters);
        }



       /// <summary>
        ///  Check if technicianId is assigned to some worksheet
       /// </summary>
       /// <param name="technicianId"></param>
       /// <returns></returns>
        public bool ExistsWorksheetWithTechnicianId(Guid technicianId)
        {
            SqlParameter[] parameters = { new SqlParameter("@technicianId", technicianId) };
            return base.Exists(STP_PATIENTLOGS_EXISTS_WORKSHEET_WITH_TECHNICIAN_ID, parameters);
        }
    
        #endregion //DB access methods

        #region DataTable


        public DataTable GetWorksheetsSearch(Guid technicianId, Guid practiceId, PatientLogStatusEnum status, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[6];

            if (technicianId.Equals(Guid.Empty))
                parameters[0] = new SqlParameter("@technicianId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@technicianId", technicianId);

            if (practiceId.Equals(Guid.Empty))
                parameters[1] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@practiceId", practiceId);

            parameters[2] = new SqlParameter("@status", status);

            parameters[3] = new SqlParameter("@pageSize", pageSize);
            parameters[4] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[5] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[5].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_PATIENTLOGS_USERS_GET_WORKSHEETS_SEARCH);

            totalCount = (int)parameters[5].Value;

            return table;
        }


        public DataTable GetCompletedWorksheetsSearch(Guid technicianId, Guid practiceId, PatientLogStatusEnum status, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[6];

            if (technicianId.Equals(Guid.Empty))
                parameters[0] = new SqlParameter("@technicianId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@technicianId", technicianId);

            if (practiceId.Equals(Guid.Empty))
                parameters[1] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@practiceId", practiceId);

            parameters[2] = new SqlParameter("@status", status);

            parameters[3] = new SqlParameter("@pageSize", pageSize);
            parameters[4] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[5] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[5].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_PATIENTLOGS_USERS_GET_COMPLETED_WORKSHEETS_SEARCH);

            totalCount = (int)parameters[5].Value;

            return table;
        }

        #endregion //DataTable

        #region DB CUD methods


       


        /// <summary>
        /// Set status of the pending patient log to "Worksheet" and assigns a technician to the log
        /// </summary>
        /// <param name="worksheet"></param>
        /// <returns></returns>
        public bool UpdateWorksheet(Worksheet worksheet)
        {
           
                SqlParameter[] parameters = 
                {

                        new SqlParameter("@id", worksheet.Id)
                        ,new SqlParameter("@technicianId", worksheet.TechnicianId)
                        ,new SqlParameter("@status", worksheet.Status)
                };

               return  base.CMDExecuteNonQuery(STP_PATIENTLOGS_UPDATE_WORKSHEET, parameters)> 0;


        }

       

        

		
        #endregion  //DB CUD methods

      


       

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override Worksheet FillProperties(SqlDataReader dataReader)
        {
            return null;
			
        }

        #endregion //mapping methods

    }
}
