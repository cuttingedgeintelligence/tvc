using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class PracticeOneTimeAppointmentDA : BaseDA<PracticeOneTimeAppointment>
    {
        #region cache keys

        #endregion //cache keys

        #region Stored procedure names

       


        private const string STP_PRACTICESONETIMEAPPOINTMENTS_INSERT = "dbo.stp_PracticesOneTimeAppointments_Insert";
        private const string STP_PRACTICESONETIMEAPPOINTMENTS_DELETE = "dbo.stp_PracticesOneTimeAppointments_Delete";

        private const string STP_PRACTICESONETIMEAPPOINTMENTS_GET_PRACTICE_ONE_TIME_APPPOINTMENTS_SEARCH = "dbo.stp_PracticesOneTimeAppointments_GetPracticeOneTimeAppointmentsSearch";
        private const string STP_PRACTICESONETIMEAPPOINTMENTS_EXISTS_APPOINTMENT_WITH_PRACTICE_ID_AND_APPOINTMENT_DATE = "dbo.stp_PracticesOneTimeAppointments_ExistsAppointmentWithPracticeIdAndAppointmentDate";

        private const string STP_PRACTICESONETIMEAPPOINTMENTS_GET_BY_PRACTICE_ID = "dbo.stp_PracticesOneTimeAppointments_GetByPracticeId";

		#endregion //Stored procedure names


        #region DB access methods

        public bool ExistsOneTimeAppointmentWithPracticeIdAndAppointmentDate(Guid practiceId,DateTime appointmentDate)
        {
            SqlParameter[] parameters = 
            { 
                 new SqlParameter("@practiceId", practiceId) 
                ,new SqlParameter("@appointmentDate", appointmentDate) 
            };
            return base.Exists(STP_PRACTICESONETIMEAPPOINTMENTS_EXISTS_APPOINTMENT_WITH_PRACTICE_ID_AND_APPOINTMENT_DATE, parameters);
        }
      
        #endregion //DB access methods

        #region DataTable


        /// <summary>
        /// Get top numberOfAppointments by practiceId with appointmentDate greater than currentDate
        /// </summary>
        /// <param name="practiceId"></param>
        /// <param name="currentDate"></param>
        /// <param name="numberOfAppointments"></param>
        /// <returns></returns>
        public DataTable GetOneTimeAppointmentsByPracticeId(Guid practiceId,DateTime currentDate,int numberOfAppointments)
        {
            SqlParameter[] parameters = 
            { 
                 new SqlParameter("@practiceId", practiceId) 
                ,new SqlParameter("@currentDate", currentDate) 
                ,new SqlParameter("@numOfAppointments", numberOfAppointments) 
            };
            return base.GetDataTable(parameters, STP_PRACTICESONETIMEAPPOINTMENTS_GET_BY_PRACTICE_ID);
        }


        public DataTable GetPracticeOneTimeAppointmentsSearch(Guid practiceId,int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[4];

            if (practiceId.Equals(Guid.Empty))
                parameters[0] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@practiceId", practiceId);


            parameters[1] = new SqlParameter("@pageSize", pageSize);
            parameters[2] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[3] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[3].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_PRACTICESONETIMEAPPOINTMENTS_GET_PRACTICE_ONE_TIME_APPPOINTMENTS_SEARCH);

            totalCount = (int)parameters[3].Value;

            return table;
        }


        #endregion //DataTable

        #region DB CUD methods


        public bool InsertPracticeOneTimeAppointment(PracticeOneTimeAppointment practiceOneTimeAppointment)
        {
           
                SqlParameter[] parameters = 
                {
                  new SqlParameter("@practiceId",practiceOneTimeAppointment.PracticeId)
                 ,new SqlParameter("@appointmentDate", practiceOneTimeAppointment.AppointmentDate)
                 ,new SqlParameter("@createdBy", practiceOneTimeAppointment.CreatedBy)
                
                };

                return base.CMDExecuteNonQuery(STP_PRACTICESONETIMEAPPOINTMENTS_INSERT, parameters) > 0;



        }



      




        public bool DeletePracticeOneTimeAppointment(int id)
        {

            SqlParameter[] parameters = {
                  new SqlParameter("@id",id) };

            return base.CMDExecuteNonQuery(STP_PRACTICESONETIMEAPPOINTMENTS_DELETE,parameters) > 0;

        }



        #endregion  //DB CUD methods

      


       

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override PracticeOneTimeAppointment FillProperties(SqlDataReader dataReader)
        {
            return null;

			
        }

        #endregion //mapping methods

    }
}
