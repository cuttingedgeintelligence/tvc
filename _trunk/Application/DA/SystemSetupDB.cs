using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using System.Data;

using System.Web;

using CEI.Common;

namespace CEI.DA
{
    public class SystemSetupDB : BaseDA<SystemSetup>
    {

        private const string CACHE_KEY_SYSTEM_SETUP_VALUES = "CACHED_SYSTEM_SETUP_VALUES";

        #region stored procedure names

		private const string STP_SYSTEMSETUPVALUES_GET_ALL = "dbo.stp_SystemSetupValues_GetAll";
        private const string STP_SYSTEMSETUPVALUES_UPDATE = "dbo.stp_SystemSetupValues_Update";
      

        #endregion //stored procedure names

        #region DB access methods

        public DataTable GetAllSystemSetupValues()
        {
            DataTable systemSetupValues = HttpRuntime.Cache[CACHE_KEY_SYSTEM_SETUP_VALUES] as DataTable;
            if (systemSetupValues == null)
            {
                systemSetupValues = base.GetDataTable(STP_SYSTEMSETUPVALUES_GET_ALL);
                base.InsertInCache(CACHE_KEY_SYSTEM_SETUP_VALUES, systemSetupValues);
            }

            systemSetupValues.PrimaryKey = new DataColumn[] { systemSetupValues.Columns["id"] };
            return systemSetupValues;
           
        }
        #endregion //DB access methods


        #region DB CUD methods

        public bool UpdateSystemSetupValues(List<SystemSetup> systemSetupValues)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
           
            try
            {
                foreach(SystemSetup systemSetup in systemSetupValues)
                {
                    SqlParameter[] parameters = 
                    {
                        new SqlParameter("@id", systemSetup.Id) ,
                        new SqlParameter("@value", systemSetup.SetupValue) 
                    };
                    base.CMDExecuteNonQuery(STP_SYSTEMSETUPVALUES_UPDATE, parameters, ref transaction);
                }

                base.ClearCache(CACHE_KEY_SYSTEM_SETUP_VALUES);
                transaction.Commit();
                returnValue = true;
            }
            catch (Exception exc)
            {
                transaction.Rollback();
            }
            finally
            {
                if (transaction.Connection != null)
                    transaction.Connection.Close();
            }

            return returnValue;

        }

        #endregion //DB CUD methods

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = null;
            return hashTable;
        }
        protected override SystemSetup FillProperties(SqlDataReader dataReader)
        {
            return null;
        }

        #endregion //mapping methods
    }
}
