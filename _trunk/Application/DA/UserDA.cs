using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class UserDA : BaseDA<User>
    {
        #region cache keys

        private const string CACHE_KEY_ALL_ADMINS = "CACHED_ALL_ADMINS";
        private const string CACHE_KEY_ALL_OFFICE_MANAGERS = "CACHED_ALL_OFFICE_MANAGERS";
        private const string CACHE_KEY_ALL_TECHNICIANS = "CACHED_ALL_TECHNICIANS";

        #endregion //cache keys

        #region Stored procedure names


        private const string STP_USERS_GET_BY_EMAIL = "dbo.stp_Users_GetByEmail";
        private const string STP_USERS_GET_ADMIN_BY_ID = "dbo.stp_Users_GetAdminById";
        private const string STP_USERS_GET_OFFICE_MANAGER_BY_ID = "dbo.stp_Users_GetOfficeManagerById";
        private const string STP_USERS_GET_TECHNICIAN_BY_ID = "dbo.stp_Users_GetTechnicianById";
        private const string STP_USERS_GET_PRACTICE_BY_ID = "dbo.stp_Users_GetPracticeById";

        private const string STP_USERS_USERSROLES_GET_ALL_PRACTICES_NAME_AND_ID = "dbo.stp_Users_UsersRoles_GetAllPracticesNameAndId";


        private const string STP_USERSROLES_INSERT = "dbo.stp_UsersRoles_Insert";

        private const string STP_USERS_INSERT_PRACTICE = "dbo.stp_Users_InsertPractice";
        private const string STP_USERS_INSERT_ADMIN = "dbo.stp_Users_InsertAdmin";
        private const string STP_USERS_INSERT_OFFICE_MANAGER = "dbo.stp_Users_InsertOfficeManager";
        private const string STP_USERS_INSERT_TECHNICIAN = "dbo.stp_Users_InsertTechnician";

        private const string STP_USERS_UPDATE_ADMIN = "dbo.stp_Users_UpdateAdmin";
        private const string STP_USERS_UPDATE_OFFICE_MANAGER = "dbo.stp_Users_UpdateOfficeManager";
        private const string STP_USERS_UPDATE_TECHNICIAN = "dbo.stp_Users_UpdateTechnician";
        private const string STP_USERS_UPDATE_PRACTICE = "dbo.stp_Users_UpdatePractice";

        private const string STP_USERS_USERSROLES_DELETE_USER = "dbo.stp_Users_UsersRoles_DeleteUser";
       
        private const string STP_USERS_USERSROLES_GET_ADMINS_MANAGE_SEARCH = "dbo.stp_Users_UsersRoles_GetAdminsManageSearch";
        private const string STP_USERS_USERSROLES_GET_OFFICE_MANAGERS_MANAGE_SEARCH = "dbo.stp_Users_UsersRoles_GetOfficeManagersManageSearch";
        private const string STP_USERS_USERSROLES_COUNTIES_GET_TECHNICIANS_MANAGE_SEARCH = "dbo.stp_Users_UsersRoles_Counties_GetTechniciansManageSearch";
        //private const string STP_USERS_USERSROLES_COUNTIES_GET_PRACTICES_MANAGE_SEARCH = "dbo.stp_Users_UsersRoles_Counties_GetPracticesManageSearch"; 

        private const string STP_USERS_USERSROLES_COUNTIES_GET_PRACTICES_MANAGE_SEARCH = "dbo.stp_Users_UsersRoles_Counties_GetPracticesManageSearchWithPagingAndSorting";

        private const string STP_USERS_EXISTS_USER_WITH_EMAIL = "dbo.stp_Users_ExistsUserWithEmail";
        private const string STP_USERS_EXISTS_USER_WITH_EMAIL_ON_UPDATE = "dbo.stp_Users_ExistsUserWithEmailOnUpdate";

        private const string STP_USERS_EXISTS_USER_WITH_COUNTY_ID = "dbo.stp_Users_ExistsUserWithCountyId";


        private const string STP_USERS_USERSROLES_GET_ALL_OFFICEMANAGERS = "dbo.stp_Users_UsersRoles_GetAllOfficeManagers";
        private const string STP_USERS_USERSROLES_GET_ALL_ADMINS = "dbo.stp_Users_UsersRoles_GetAllAdmins";

        private const string STP_USERS_USERSROLES_GET_ALL_TECHNICIANS = "dbo.stp_Users_UsersRoles_GetAllTechnicians";

        private const string STP_TECHNICIANSCOUNTIES_INSERT = "dbo.stp_TechniciansCounties_Insert";
        private const string STP_TECHNICIANSCOUNTIES_DELETE = "dbo.stp_TechniciansCounties_Delete";

        private const string STP_USERS_USERSROLES_TECHNICIANSCOUNTIES_DELETE_TECHNICIAN = "dbo.stp_Users_UsersRoles_TechniciansCounties_DeleteTechnician";


        private const string STP_USERS_DOCTORS_COUNTIES_STATES_GET_PRACTICE_BY_ID_FOR_PROFILE_PAGE = "dbo.stp_Users_Doctors_Counties_States_GetPracticeByIdForProfilePage";
        private const string STP_PATIENTS_PATIENTSLOG_PATIENTSPATIENTLOG_FOR_FTP = "stp_Patients_PatientsLog_PatientLogPatients_GetPatientsForFTP_by_practiceId_and_serviceDate";

        private const string STP_UPDATE_PATIENT_LOGPATIENTS = "stp_PatientLogsPatients_Update_isComplited";
        private const string STP_USERS_GET_ALL = "stp_Users_GetAll";
        private const string STP_GETALL_FROMEXCELFORIMPORTPATIENT = "stp_GetAll_FromExcelForImportPetient";
        private const string STP_GETALL_SERVICE_DATE_FROMEXCELFORIMPORTPATIENT = "stp_GetAll_ServiceDateFromExcelForImportPetient";



        private const string STP_CATCH_IMPORT_ERRORS = "stp_CatchImportErrors";




        #endregion //Stored procedure names


        #region DB access methods

        public bool ExistsUserWithEmail(string email)
        {
            SqlParameter[] parameters = { new SqlParameter("@email", email) };
            return base.Exists(STP_USERS_EXISTS_USER_WITH_EMAIL, parameters);
        }

        public bool ExistsUserWithCountyId(int countyId)
        {
            SqlParameter[] parameters = { new SqlParameter("@countyId", countyId) };
            return base.Exists(STP_USERS_EXISTS_USER_WITH_COUNTY_ID, parameters);
        }


        public bool ExistsUserWithEmailOnUpdate(Guid id,string email)
        {
            SqlParameter[] parameters = 
            {   
                 new SqlParameter("@email", email) 
                ,new SqlParameter("@id", id) 
            };
            return base.Exists(STP_USERS_EXISTS_USER_WITH_EMAIL_ON_UPDATE, parameters);
        }
      
        #endregion //DB access methods

        #region DataTable

        public DataTable GetUserByEmailAsDataTable(string email)
        {
            SqlParameter[] parameters = { new SqlParameter("@email", email) };
            return base.GetDataTable(parameters, STP_USERS_GET_BY_EMAIL);
        }


        public DataTable GetAdminById(Guid userId)
        {
            SqlParameter[] parameters = { new SqlParameter("@id", userId) };
            return base.GetDataTable(parameters, STP_USERS_GET_ADMIN_BY_ID);
        }

        public DataTable GetOfficeManagerById(Guid userId)
        {
            SqlParameter[] parameters = { new SqlParameter("@id", userId) };
            return base.GetDataTable(parameters, STP_USERS_GET_OFFICE_MANAGER_BY_ID);
        }

        public DataTable GetTechnicianById(Guid userId)
        {
            SqlParameter[] parameters = { new SqlParameter("@id", userId) };
            return base.GetDataTable(parameters, STP_USERS_GET_TECHNICIAN_BY_ID);
        }


        public DataTable GetPracticeById(Guid userId)
        {
            SqlParameter[] parameters = { new SqlParameter("@id", userId) };
            return base.GetDataTable(parameters, STP_USERS_GET_PRACTICE_BY_ID);
        }


        public DataTable GetAllPracticesNameAndId()
        {
            SqlParameter[] parameters = { new SqlParameter("@roleId", (int)RoleEnum.Practice) };
            return base.GetDataTable(parameters, STP_USERS_USERSROLES_GET_ALL_PRACTICES_NAME_AND_ID);
        }

        //get all admins with accountIsEnabled == true
        public DataTable GetAllAdmins()
        {

            DataTable dtAllAdmins = HttpRuntime.Cache[CACHE_KEY_ALL_ADMINS] as DataTable;

            if (dtAllAdmins == null)
            {
                SqlParameter[] parameters =
                { 
                 new SqlParameter("@roleId", (int)RoleEnum.Ultimate__admin) 
                ,new SqlParameter("@accountIsEnabled", true) 
                };
                dtAllAdmins = base.GetDataTable(parameters, STP_USERS_USERSROLES_GET_ALL_ADMINS);
                base.InsertInCache(CACHE_KEY_ALL_ADMINS, dtAllAdmins);
            }

            return dtAllAdmins;
           
        }

         //get all office managers with accountIsEnabled == true
        public DataTable GetAllOfficeManagers()
        {

            DataTable dtAllOfficeManagers = HttpRuntime.Cache[CACHE_KEY_ALL_OFFICE_MANAGERS] as DataTable;

            if (dtAllOfficeManagers == null)
            {
                SqlParameter[] parameters =
                 { 
                 new SqlParameter("@roleId", (int)RoleEnum.Office__manager) 
                ,new SqlParameter("@accountIsEnabled", true) 
                  };
                dtAllOfficeManagers = base.GetDataTable(parameters, STP_USERS_USERSROLES_GET_ALL_OFFICEMANAGERS);
                base.InsertInCache(CACHE_KEY_ALL_OFFICE_MANAGERS, dtAllOfficeManagers);
            }

            return dtAllOfficeManagers;

        }

        /// <summary>
        /// get all technicians with accountIsEnabled == true 
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllTechnicians()
        {
            DataTable dtAllTechnicians = HttpRuntime.Cache[CACHE_KEY_ALL_TECHNICIANS] as DataTable;

            if (dtAllTechnicians == null)
            {
                SqlParameter[] parameters =
                 { 
                 new SqlParameter("@roleId", (int)RoleEnum.Technician) 
                ,new SqlParameter("@accountIsEnabled", true) 
                  };
                dtAllTechnicians = base.GetDataTable(parameters, STP_USERS_USERSROLES_GET_ALL_TECHNICIANS);
                base.InsertInCache(CACHE_KEY_ALL_TECHNICIANS, dtAllTechnicians);
            }

            return dtAllTechnicians;
        }


        public DataTable GetAdminsManageSearch(int roleId, string title, string firstName, string lastName, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[7];

            parameters[0] = new SqlParameter("@roleId", roleId);

            if (title.Equals("-1"))
                parameters[1] = new SqlParameter("@title", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@title", title);

            if (firstName.Equals(string.Empty))
                parameters[2] = new SqlParameter("@firstName", DBNull.Value);
            else
                parameters[2] = new SqlParameter("@firstName", firstName);

            if (lastName.Equals(string.Empty))
                parameters[3] = new SqlParameter("@lastName", DBNull.Value);
            else
                parameters[3] = new SqlParameter("@lastName", lastName);


            parameters[4] = new SqlParameter("@pageSize", pageSize);
            parameters[5] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[6] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[6].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_USERS_USERSROLES_GET_ADMINS_MANAGE_SEARCH);

            totalCount = (int)parameters[6].Value;

            return table;
        }


        public DataTable GetOfficeManagersManageSearch(int roleId, string title, string firstName, string lastName, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[7];

            parameters[0] = new SqlParameter("@roleId", roleId);

            if (title.Equals("-1"))
                parameters[1] = new SqlParameter("@title", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@title", title);

            if (firstName.Equals(string.Empty))
                parameters[2] = new SqlParameter("@firstName", DBNull.Value);
            else
                parameters[2] = new SqlParameter("@firstName", firstName);

            if (lastName.Equals(string.Empty))
                parameters[3] = new SqlParameter("@lastName", DBNull.Value);
            else
                parameters[3] = new SqlParameter("@lastName", lastName);


            parameters[4] = new SqlParameter("@pageSize", pageSize);
            parameters[5] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[6] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[6].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_USERS_USERSROLES_GET_OFFICE_MANAGERS_MANAGE_SEARCH);

            totalCount = (int)parameters[6].Value;

            return table;
        }


        public DataTable GetTechniciansManageSearch(int roleId,int countyId, string title, string firstName, string lastName, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[8];

            parameters[0] = new SqlParameter("@roleId", roleId);

            if (countyId.Equals(-1))
                parameters[1] = new SqlParameter("@countyId", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@countyId", countyId);

           

            if (title.Equals("-1"))
                parameters[2] = new SqlParameter("@title", DBNull.Value);
            else
                parameters[2] = new SqlParameter("@title", title);

            if (firstName.Equals(string.Empty))
                parameters[3] = new SqlParameter("@firstName", DBNull.Value);
            else
                parameters[3] = new SqlParameter("@firstName", firstName);

            if (lastName.Equals(string.Empty))
                parameters[4] = new SqlParameter("@lastName", DBNull.Value);
            else
                parameters[4] = new SqlParameter("@lastName", lastName);


            parameters[5] = new SqlParameter("@pageSize", pageSize);
            parameters[6] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[7] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[7].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_USERS_USERSROLES_COUNTIES_GET_TECHNICIANS_MANAGE_SEARCH);

            totalCount = (int)parameters[7].Value;

            return table;
        }

        public DataTable GetPracticesManageSearch(int roleId, string name, string address, string zipCode, string city, int countyId, string sortExpression, int pageSize, int startRowIndex, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[10];

            parameters[0] = new SqlParameter("@roleId", roleId);

            if (name.Equals(string.Empty))
                parameters[1] = new SqlParameter("@name", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@name", name);

            if (address.Equals(string.Empty))
                parameters[2] = new SqlParameter("@address", DBNull.Value);
            else
                parameters[2] = new SqlParameter("@address", address);

            if (zipCode.Equals(string.Empty))
                parameters[3] = new SqlParameter("@zipCode", DBNull.Value);
            else
                parameters[3] = new SqlParameter("@zipCode", zipCode);

            if (city.Equals(string.Empty))
                parameters[4] = new SqlParameter("@city", DBNull.Value);
            else
                parameters[4] = new SqlParameter("@city", city);


            if (countyId.Equals(-1))
                parameters[5] = new SqlParameter("@countyId", DBNull.Value);
            else
                parameters[5] = new SqlParameter("@countyId", countyId);

           



            parameters[6] = new SqlParameter("@pageSize", pageSize);
            parameters[7] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[8] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[8].Direction = ParameterDirection.Output;

            parameters[9] = new SqlParameter("@sortExpression", sortExpression);
            
            DataTable table = base.GetDataTable(parameters, STP_USERS_USERSROLES_COUNTIES_GET_PRACTICES_MANAGE_SEARCH);

            totalCount = (int)parameters[8].Value;

            return table;
        }

        public DataTable GetPatientsForFTPByPracticeIdandServiceDate(Guid practiceId, string dateServie)
        {
            SqlParameter[] parameters = new SqlParameter[2];



            if (practiceId.Equals(Guid.Empty.ToString()))
                parameters[0] = new SqlParameter("@practiceId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@practiceId", practiceId);

            if (dateServie.ToString().Equals("Select service date") || dateServie.ToString().Equals("") || dateServie.ToString().Equals("-1"))
          //  if (dateServie.ToString().Equals("-1"))
                parameters[1] = new SqlParameter("@serviceDate", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@serviceDate", DateTime.Parse(dateServie).ToString("MM/dd/yyyy"));
      
         
            DataTable table = base.GetDataTable(parameters, STP_PATIENTS_PATIENTSLOG_PATIENTSPATIENTLOG_FOR_FTP);
            return table;
        }

        public bool UpdatePatientLogPatients1(int id, bool isComplitedValue)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {

                SqlParameter[] parameters = 
                    {
                      new SqlParameter("@id",id)
                     ,new SqlParameter("@isComplited",isComplitedValue)
                  };

                base.CMDExecuteNonQuery(STP_UPDATE_PATIENT_LOGPATIENTS, parameters, ref transaction);

                transaction.Commit();
                returnValue = true;
            }
            catch (Exception exc)
            {
                transaction.Rollback();

            }
            finally
            {
                if (transaction.Connection != null)
                    transaction.Connection.Close();
            }


            return returnValue;
        }
        public bool UpdatePatientLogPatients(List<string> listOfIdAndValue)
        {

            bool returnValue = false;
        
           try
            {
               foreach (string value in listOfIdAndValue)
                {
                    char[]  delimiters = new char[] { '_' };

                    string[] parts = value.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                   if(this.UpdatePatientLogPatients1(int.Parse(parts[0]), bool.Parse(parts[1])))
                       returnValue = true;
                    //if (county.ActionType.Equals(ActionTypeEnum.Insert))
                    //    this.InsertTechnicianCounty(technician.UserId, county.Id, ref transaction);
                    //else if (county.ActionType.Equals(ActionTypeEnum.Delete))
                    //    this.DeleteTechnicianCounty(technician.UserId, county.Id, ref transaction);
                }


               // base.ClearCache(CACHE_KEY_ALL_TECHNICIANS);

             //   transaction.Commit();
                //returnValue = true;
            }

            catch (Exception exc)
            {
               // transaction.Rollback();

            }
            finally
            {
                //if (transaction.Connection != null)
                //    transaction.Connection.Close();
            }


            return returnValue;


        }

        #endregion //DataTable

        #region DataSet

        /// <summary>
        /// Get practice with doctors 
        /// </summary>
        /// <param name="practiceId"></param>
        /// <returns>dataset table 0 - practice table 1 - doctors</returns>
        public DataSet GetPracticeByIdForProfilePage(Guid practiceId)
        {
            SqlParameter[] parameters = { new SqlParameter("@id", practiceId) };
            DataSet dsPractice = base.GetDataSet(parameters, STP_USERS_DOCTORS_COUNTIES_STATES_GET_PRACTICE_BY_ID_FOR_PROFILE_PAGE);
            dsPractice.Tables[0].TableName = "tblPractice";
            dsPractice.Tables[1].TableName = "tblDoctors";
            return dsPractice;
        }


        #endregion //DataSet

        #region DB CUD methods

        public DataTable GetAllUsers()
        {
            DataTable dtAllUsers = new DataTable();


            dtAllUsers = base.GetDataTable(STP_USERS_GET_ALL);
            return dtAllUsers;
            
        }

        public DataTable GetAllFromExcelForImportPetient(DateTime servicedate, string tableName)
        {
            DataTable dtAllUsers = new DataTable();
            SqlParameter[] parameters = 
                {
                  new SqlParameter("@serviceDate",servicedate),
                  new SqlParameter("@tableName", tableName)
                };

            dtAllUsers = base.GetDataTable(parameters,STP_GETALL_FROMEXCELFORIMPORTPATIENT);
            return dtAllUsers;

        }

        public DataTable GetAllServiceDateFromExcelForImportPetient(string tableName)
        {
            DataTable dtAllUsers = new DataTable();

            SqlParameter[] parameters = 
                {
                  new SqlParameter("@table",tableName)
                };

            dtAllUsers = base.GetDataTable(parameters, STP_GETALL_SERVICE_DATE_FROMEXCELFORIMPORTPATIENT);
            return dtAllUsers;

        }

        

        public bool InsertPractice(User practice)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {

                SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",practice.UserId)
                 ,new SqlParameter("@name", practice.FirstName)
                 ,new SqlParameter("@email", practice.Email)
                 ,new SqlParameter("@password", practice.Password)
                 ,new SqlParameter("@fax", practice.Fax)
                 ,new SqlParameter("@phone", practice.Phone)
                 ,new SqlParameter("@address1", practice.Address1)
                 ,new SqlParameter("@address2", practice.Address2)
                 ,new SqlParameter("@city", practice.City)
                 ,new SqlParameter("@zipCode", practice.ZipCode)
                 ,new SqlParameter("@stateId", practice.StateId)
               
                 ,new SqlParameter("@countyId", practice.CountyId)
                 ,new SqlParameter("@createdBy", practice.CreatedBy)
                 ,new SqlParameter("@accountIsEnabled", practice.AccountIsEnabled)                 
                 ,new SqlParameter("@referredBy", practice.ReferredBy)
                 ,new SqlParameter("@slitlampAndTableAreRequired", practice.SlitlampAndTableAreRequired)
                 ,new SqlParameter("@frequency", practice.Frequency)
                 ,new SqlParameter("@preferredDay1", practice.PreferredDay1)
                 ,new SqlParameter("@assignedTech", practice.AssignedTech)
                 ,new SqlParameter("@preferredDay2", practice.PreferredDay2)
                 //,new SqlParameter("@preferredDay3", practice.PreferredDay3)
                };

                base.CMDExecuteNonQuery(STP_USERS_INSERT_PRACTICE, parameters,ref transaction);


                this.InsertUsersRoles(practice.UserId, (int)RoleEnum.Practice, ref transaction);


                transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                transaction.Rollback();
                
            }
            finally
            {
                if (transaction.Connection != null)
                    transaction.Connection.Close();
            }

            return returnValue;
        

        }

        public bool InsertPracticeWithDoctor(User practice, Doctor doctor)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {

                SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",practice.UserId)
                 ,new SqlParameter("@name", practice.FirstName)
                 ,new SqlParameter("@email", practice.Email)
                 ,new SqlParameter("@password", practice.Password)
                 ,new SqlParameter("@fax", practice.Fax)
                 ,new SqlParameter("@phone", practice.Phone)
                 ,new SqlParameter("@address1", practice.Address1)
                 ,new SqlParameter("@address2", practice.Address2)
                 ,new SqlParameter("@city", practice.City)
                 ,new SqlParameter("@zipCode", practice.ZipCode)
                 ,new SqlParameter("@stateId", practice.StateId)
                 
                 ,new SqlParameter("@countyId", practice.CountyId)
                 ,new SqlParameter("@createdBy", practice.CreatedBy)
                 ,new SqlParameter("@accountIsEnabled", practice.AccountIsEnabled)                 
                 ,new SqlParameter("@referredBy", practice.ReferredBy)
                 ,new SqlParameter("@slitlampAndTableAreRequired", practice.SlitlampAndTableAreRequired)
                 ,new SqlParameter("@frequency", practice.Frequency)
                 ,new SqlParameter("@preferredDay1", practice.PreferredDay1)
                 ,new SqlParameter("@assignedTech", practice.AssignedTech)
                 ,new SqlParameter("@preferredDay2", practice.PreferredDay2)
                 //,new SqlParameter("@preferredDay3", practice.PreferredDay3)
                };

                base.CMDExecuteNonQuery(STP_USERS_INSERT_PRACTICE, parameters, ref transaction);


                this.InsertUsersRoles(practice.UserId, (int)RoleEnum.Practice, ref transaction);

                new DoctorDA().InsertDoctor(doctor, ref transaction);

                transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                transaction.Rollback();

            }
            finally
            {
                if (transaction.Connection != null)
                    transaction.Connection.Close();
            }

            return returnValue;


        }


        public bool InsertAdmin(User admin)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {

                SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",admin.UserId)
                 ,new SqlParameter("@firstName", admin.FirstName)
                 ,new SqlParameter("@lastName", admin.LastName)
                 ,new SqlParameter("@email", admin.Email)
                 ,new SqlParameter("@password", admin.Password)
                 ,new SqlParameter("@createdBy", admin.CreatedBy)
                 ,new SqlParameter("@accountIsEnabled", admin.AccountIsEnabled)
                 ,new SqlParameter("@title", admin.Title)
                

                };

                base.CMDExecuteNonQuery(STP_USERS_INSERT_ADMIN, parameters, ref transaction);


                this.InsertUsersRoles(admin.UserId, (int)RoleEnum.Ultimate__admin, ref transaction);

                this.ClearCache(CACHE_KEY_ALL_ADMINS);

                transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                transaction.Rollback();
                
            }
            finally
            {
                if (transaction.Connection != null)
                    transaction.Connection.Close();
            }

            return returnValue;

            


        }


        public bool InsertOfficeManager(User officeManager)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {

                SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",officeManager.UserId)
                 ,new SqlParameter("@firstName", officeManager.FirstName)
                 ,new SqlParameter("@lastName", officeManager.LastName)
                 ,new SqlParameter("@email", officeManager.Email)
                 ,new SqlParameter("@password", officeManager.Password)
                 ,new SqlParameter("@fax", officeManager.Fax)
                 ,new SqlParameter("@phone", officeManager.Phone)
                 ,new SqlParameter("@createdBy", officeManager.CreatedBy)
                 ,new SqlParameter("@accountIsEnabled", officeManager.AccountIsEnabled)
                 ,new SqlParameter("@title", officeManager.Title)

                };

                base.CMDExecuteNonQuery(STP_USERS_INSERT_OFFICE_MANAGER, parameters, ref transaction);


                this.InsertUsersRoles(officeManager.UserId, (int)RoleEnum.Office__manager, ref transaction);

                base.ClearCache(CACHE_KEY_ALL_OFFICE_MANAGERS);

                transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                transaction.Rollback();
                
            }
            finally
            {
                if (transaction.Connection != null)
                    transaction.Connection.Close();
            }


            return returnValue;


        }


        public bool InsertTechnician(User technician)
        {
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {

                SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",technician.UserId)
                 ,new SqlParameter("@firstName", technician.FirstName)               
                 ,new SqlParameter("@lastName", technician.LastName)
                 ,new SqlParameter("@email", technician.Email)
                 ,new SqlParameter("@password", technician.Password)
               
                 ,new SqlParameter("@createdBy", technician.CreatedBy)
                 ,new SqlParameter("@accountIsEnabled", technician.AccountIsEnabled)
              
                 ,new SqlParameter("@title", technician.Title)
                

                };

                base.CMDExecuteNonQuery(STP_USERS_INSERT_TECHNICIAN, parameters, ref transaction);


                foreach (NameValue county in technician.CountiesList)
                {
                    this.InsertTechnicianCounty(technician.UserId, county.Id, ref transaction);
                }

                this.InsertUsersRoles(technician.UserId, (int)RoleEnum.Technician, ref transaction);
                base.ClearCache(CACHE_KEY_ALL_TECHNICIANS);

                transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                transaction.Rollback();
               
            }
            finally
            {
                if (transaction.Connection != null)
                    transaction.Connection.Close();
            }


            return returnValue;


        }



        public bool UpdateAdmin(User admin)
        {

                SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",admin.UserId)
                 ,new SqlParameter("@firstName", admin.FirstName)
                 ,new SqlParameter("@lastName", admin.LastName)
                 ,new SqlParameter("@email", admin.Email)
                 ,new SqlParameter("@password", admin.Password)
                 ,new SqlParameter("@accountIsEnabled", admin.AccountIsEnabled)
                 ,new SqlParameter("@title", admin.Title)
                };

                if (base.CMDExecuteNonQuery(STP_USERS_UPDATE_ADMIN, parameters) > 0)
                {
                    this.ClearCache(CACHE_KEY_ALL_ADMINS);
                    return true;
                }
                else
                    return false;

                

        }

        public bool UpdateOfficeManager(User officeManager)
        {

            SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",officeManager.UserId)
                 ,new SqlParameter("@firstName", officeManager.FirstName)
                 ,new SqlParameter("@lastName", officeManager.LastName)
                 ,new SqlParameter("@email", officeManager.Email)
                 ,new SqlParameter("@password", officeManager.Password)
                 ,new SqlParameter("@accountIsEnabled", officeManager.AccountIsEnabled)
                 ,new SqlParameter("@title", officeManager.Title)
                 ,new SqlParameter("@fax", officeManager.Fax)
                 ,new SqlParameter("@phone", officeManager.Phone)
                };

            if (base.CMDExecuteNonQuery(STP_USERS_UPDATE_OFFICE_MANAGER, parameters) > 0)
            {
                base.ClearCache(CACHE_KEY_ALL_OFFICE_MANAGERS);
                return true;
            }
            else
                return false;

        }


        public bool UpdateTechnician(User technician)
        {
            
            bool returnValue = false;
            SqlTransaction transaction = null;
            try
            {

                SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",technician.UserId)
                 ,new SqlParameter("@firstName", technician.FirstName)
                 ,new SqlParameter("@lastName", technician.LastName)
                 ,new SqlParameter("@email", technician.Email)
                 ,new SqlParameter("@password", technician.Password)
                 ,new SqlParameter("@countyId", technician.CountyId)
                 ,new SqlParameter("@accountIsEnabled", technician.AccountIsEnabled)
                 ,new SqlParameter("@title", technician.Title)
                };

         

                base.CMDExecuteNonQuery(STP_USERS_UPDATE_TECHNICIAN, parameters, ref transaction);


                foreach (NameValue county in technician.CountiesList)
                {
                    if(county.ActionType.Equals(ActionTypeEnum.Insert))
                        this.InsertTechnicianCounty(technician.UserId, county.Id, ref transaction);
                    else if (county.ActionType.Equals(ActionTypeEnum.Delete))
                        this.DeleteTechnicianCounty(technician.UserId, county.Id, ref transaction);
                }

               
                base.ClearCache(CACHE_KEY_ALL_TECHNICIANS);

                transaction.Commit();
                returnValue = true;
            }

            catch (Exception exc)
            {
                transaction.Rollback();

            }
            finally
            {
                if (transaction.Connection != null)
                    transaction.Connection.Close();
            }


            return returnValue;

        }


        public bool UpdatePractice(User practice)
        {

            SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",practice.UserId)
                 ,new SqlParameter("@name", practice.FirstName)
                 ,new SqlParameter("@email", practice.Email)
                 ,new SqlParameter("@password", practice.Password)
                 ,new SqlParameter("@fax", practice.Fax)
                 ,new SqlParameter("@phone", practice.Phone)
                 ,new SqlParameter("@address1", practice.Address1)
                 ,new SqlParameter("@address2", practice.Address2)
                 ,new SqlParameter("@city", practice.City)
                 ,new SqlParameter("@zipCode", practice.ZipCode)
                 ,new SqlParameter("@stateId", practice.StateId)
               
                 ,new SqlParameter("@countyId", practice.CountyId)
                 ,new SqlParameter("@accountIsEnabled", practice.AccountIsEnabled)              
                 ,new SqlParameter("@referredBy", practice.ReferredBy)
                 ,new SqlParameter("@slitlampAndTableAreRequired", practice.SlitlampAndTableAreRequired)
                 ,new SqlParameter("@frequency", practice.Frequency)
                 ,new SqlParameter("@preferredDay1", practice.PreferredDay1)
                 ,new SqlParameter("@assignedTech", practice.AssignedTech)
                 ,new SqlParameter("@preferredDay2", practice.PreferredDay2)
                 //,new SqlParameter("@preferredDay3", practice.PreferredDay3)
                };


            return base.CMDExecuteNonQuery(STP_USERS_UPDATE_PRACTICE, parameters) > 0;

        }

        


        


        private bool InsertUsersRoles(Guid userId, int roleId, ref  SqlTransaction transaction)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@userId", userId)
                ,new SqlParameter("@roleId", roleId)
            };

            return base.CMDExecuteNonQuery(STP_USERSROLES_INSERT, parameters, ref transaction) > 0;

        }

        public bool DeleteUser(Guid userId,RoleEnum role)
        {

            SqlParameter[] parameters = {
                  new SqlParameter("@id",userId) };

            if (base.CMDExecuteNonQuery(STP_USERS_USERSROLES_DELETE_USER, parameters) > 0)
            {
                if (role.Equals(RoleEnum.Ultimate__admin))
                    base.ClearCache(CACHE_KEY_ALL_ADMINS);

                else if (role.Equals(RoleEnum.Office__manager))
                    base.ClearCache(CACHE_KEY_ALL_OFFICE_MANAGERS);


                return true;
            }
            else
                return false;

        }


        public bool DeleteTechnician(Guid userId)
        {

            SqlParameter[] parameters = {
                  new SqlParameter("@id",userId) };

            if (base.CMDExecuteNonQuery(STP_USERS_USERSROLES_TECHNICIANSCOUNTIES_DELETE_TECHNICIAN, parameters) > 0)
            {
                base.ClearCache(CACHE_KEY_ALL_TECHNICIANS);

                return true;
            }
            else
                return false;

        }


        private bool InsertTechnicianCounty(Guid technicianId, int countyId, ref  SqlTransaction transaction)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@technicianId", technicianId)
                ,new SqlParameter("@countyId", countyId)
            };

            return base.CMDExecuteNonQuery(STP_TECHNICIANSCOUNTIES_INSERT, parameters, ref transaction) > 0;

        }


        private bool DeleteTechnicianCounty(Guid technicianId, int countyId, ref  SqlTransaction transaction)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@technicianId", technicianId)
                ,new SqlParameter("@countyId", countyId)
            };

            return base.CMDExecuteNonQuery(STP_TECHNICIANSCOUNTIES_DELETE, parameters, ref transaction) > 0;

        }


        #endregion  //DB CUD methods



        #region Import errors


        public void CatchImportErrors(int? errPort, string tableName, DateTime? serviceDate, int? doctorId, int? patientId, Guid? practiceId, Guid? technicianId, string exception)
        {
            
                SqlParameter[] parameters = 
                {
                    new SqlParameter("@errPort", errPort)
	                ,new SqlParameter("@tableName", tableName)
	                ,serviceDate.HasValue ? new SqlParameter("@serviceDate", serviceDate.Value) : new SqlParameter("@serviceDate", DBNull.Value)
	                ,doctorId.HasValue ? new SqlParameter("@doctorId", doctorId.Value) : new SqlParameter("@doctorId", DBNull.Value)
	                ,patientId.HasValue ? new SqlParameter("@patientId", patientId.Value) : new SqlParameter("@patientId", DBNull.Value)
	                ,practiceId.HasValue ? new SqlParameter("@practiceId", practiceId.Value) : new SqlParameter("@practiceId", DBNull.Value)
	                ,technicianId.HasValue ? new SqlParameter("@technicianId", technicianId.Value) : new SqlParameter("@technicianId", DBNull.Value)
	                ,new SqlParameter("@exception", exception)
                };

                base.CMDExecuteNonQuery(STP_CATCH_IMPORT_ERRORS, parameters);
           

        }

        #endregion //Import Errors





        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override User FillProperties(SqlDataReader dataReader)
        {
            return null;

			
        }

        #endregion //mapping methods

    }
}
