﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.IO;
using System.Web;

using CEI.Common;
using CEI.Framework;

namespace CEI.DA
{
    public class NoteDA : BaseDA<Note>
    {

        #region Stored procedure names

        private const string STP_NOTES_INSERT = "dbo.stp_Notes_Insert";
        private const string STP_NOTES_UPDATE = "dbo.stp_Notes_Update";
        private const string STP_NOTES_DELETE = "dbo.stp_Notes_Delete";
        private const string STP_NOTES_NOTE_CATEGORIES_GET_NOTES_MANAGE_SEARCH = "dbo.stp_Notes_NoteCategories_GetNotesManageSearch";
        private const string STP_NOTES_GET_ALL_NOTES = "dbo.stp_Notes_GetAll";
        #endregion //Stored procedure names

        #region cache keys

        private const string CACHE_KEY_ALL_INTERNAL_NOTES = "CACHED_ALL_INTERNAL_NOTES";

        private const string CACHE_KEY_ALL_PRACTICE_NOTES = "CACHED_ALL_PRACTICE_NOTES";

        #endregion //cache keys
        

        #region DB CUD methods


        public bool InsertNote(Note note, int noteTypeId)
        {


            SqlParameter[] parameters = 
                {
                  
                  new SqlParameter("@text", note.Text)
                 ,new SqlParameter("@categoryId", note.CategoryId)
                
                };
            if(noteTypeId==1)
            base.ClearCache(CACHE_KEY_ALL_INTERNAL_NOTES);

            if (noteTypeId == 2)
                base.ClearCache(CACHE_KEY_ALL_PRACTICE_NOTES);

            return base.CMDExecuteNonQuery(STP_NOTES_INSERT, parameters) > 0;

        }



        public bool UpdateNote(Note note, int noteTypeId)
        {

            SqlParameter[] parameters = 
                {
                  new SqlParameter("@id",note.Id)
                 ,new SqlParameter("@text", note.Text)
                 ,new SqlParameter("@categoryId", note.CategoryId)

                };
            if(noteTypeId==1)
            base.ClearCache(CACHE_KEY_ALL_INTERNAL_NOTES);
            if (noteTypeId == 2)
                base.ClearCache(CACHE_KEY_ALL_PRACTICE_NOTES);

            return base.CMDExecuteNonQuery(STP_NOTES_UPDATE, parameters) > 0;

        }


        public bool DeleteNote(int id, int noteTypeId)
        {

            SqlParameter[] parameters = {
                  new SqlParameter("@id",id) };

            if (noteTypeId == 1)
                base.ClearCache(CACHE_KEY_ALL_INTERNAL_NOTES);
            if (noteTypeId == 2)
                base.ClearCache(CACHE_KEY_ALL_PRACTICE_NOTES);

           // base.ClearCache(CACHE_KEY_ALL_NOTES); 

            return base.CMDExecuteNonQuery(STP_NOTES_DELETE, parameters) > 0;



        }




        #endregion  //DB CUD methods

        #region DataTable

        public DataTable GetNotesManageSearch(int categoryId, int pageSize, int startRowIndex, int noteTypeId, out int totalCount)
        {
            SqlParameter[] parameters = new SqlParameter[5];

            if (categoryId.Equals(-1))
                parameters[0] = new SqlParameter("@categoryId", DBNull.Value);
            else
                parameters[0] = new SqlParameter("@categoryId", categoryId);

            if (noteTypeId.Equals(-1))
                parameters[1] = new SqlParameter("@noteTypeId", DBNull.Value);
            else
                parameters[1] = new SqlParameter("@noteTypeId", noteTypeId);

            parameters[2] = new SqlParameter("@pageSize", pageSize);
            parameters[3] = new SqlParameter("@startRowIndex", startRowIndex);

            parameters[4] = new SqlParameter("@totalCount", SqlDbType.Int);
            parameters[4].Direction = ParameterDirection.Output;

            DataTable table = base.GetDataTable(parameters, STP_NOTES_NOTE_CATEGORIES_GET_NOTES_MANAGE_SEARCH);

            totalCount = (int)parameters[4].Value;

            return table;
        }

        public DataTable GetAllNotes(int noteTypeId)
        {

            DataTable allNotes = new DataTable();
            if(noteTypeId==1)
            allNotes=HttpRuntime.Cache[CACHE_KEY_ALL_INTERNAL_NOTES] as DataTable;
            if (noteTypeId == 2)
                allNotes = HttpRuntime.Cache[CACHE_KEY_ALL_PRACTICE_NOTES] as DataTable;

            if (allNotes == null)
            {
                SqlParameter[] parameters = 
                {
                new SqlParameter("@noteTypeId", noteTypeId)
                };

                allNotes = base.GetDataTable(parameters,STP_NOTES_GET_ALL_NOTES);
                if (noteTypeId == 1)
                    base.InsertInCache(CACHE_KEY_ALL_INTERNAL_NOTES, allNotes);
                if(noteTypeId==2)
                    base.InsertInCache(CACHE_KEY_ALL_PRACTICE_NOTES, allNotes);
            }

            return allNotes;
        }

        

        #endregion //DataTable

        #region mapping methods

        protected override Hashtable MapTable()
        {
            Hashtable hashTable = new Hashtable();
            return hashTable;
        }

        protected override Note FillProperties(SqlDataReader dataReader)
        {
            return null;
        }

        #endregion //mapping methods
    }
}
