﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;

namespace CEI.Framework
{
    public class ResizeImage
    {
        //public static long AvailableFreeDiskSpace()
        //{
        //    string diskLetter = HttpContext.Current.Request.PhysicalApplicationPath.Substring(0, 1);
        //    DriveInfo drive = new DriveInfo(diskLetter);
        //    return drive.AvailableFreeSpace;
        //}

        //public void ResizeImageGroups(string fileName, string groupId)
        //{
        //    int maxSize = int.Parse(ConfigurationManager.AppSettings["SmallerPictureSize"]);

        //    string Path = HttpContext.Current.Server.MapPath(HttpContext.Current.Request.ApplicationPath) + "\\Organizations\\" + groupId + "\\" + fileName;
        //    Bitmap bmp = CreateThumbnail(Path, maxSize, maxSize);

        //    if (bmp == null)
        //    {
        //        this.ErrorResult();
        //        return;
        //    }

        //    if (fileName != null)
        //    {
        //        //if (this.User.Identity.Name == "")
        //        //{
        //        //    // *** Custom error display here
        //        //    bmp.Dispose();
        //        //    this.ErrorResult();
        //        //}
        //        try
        //        {
        //            bmp.Save(Path);
        //        }
        //        catch (Exception ex)
        //        {
        //            bmp.Dispose();
        //            this.ErrorResult();
        //            return;
        //        }
        //    }
        //    //we need this if we want to show the picture on the site
        //    //Response.ContentType = "image/jpeg";
        //    //bmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
        //    bmp.Dispose();
        //}



        public void ResizeTheImage(string imagePath, int imageWidth, int imageHeight)
        {

            Bitmap bmp = CreateThumbnail(imagePath, imageWidth, imageHeight);
            if (bmp == null)
            {
                this.ErrorResult();
                return;
            }

            if (imagePath != null)
            {

                try
                {
                    bmp.Save(imagePath);
                }
                catch (Exception ex)
                {
                    bmp.Dispose();
                    this.ErrorResult();
                    return;
                }
            }
            //we need this if we want to show the picture on the site
            //Response.ContentType = "image/jpeg";
            //bmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            bmp.Dispose();
        }

        /// 
        /// Creates a resized bitmap from an existing image on disk.
        /// Call Dispose on the returned Bitmap object
        /// Bitmap or null
        public static Bitmap CreateThumbnail(string lcFilename, int lnWidth, int lnHeight)
        {
            System.Drawing.Bitmap bmpOut = null;
            try
            {
                Bitmap loBMP = new Bitmap(lcFilename);
                ImageFormat loFormat = loBMP.RawFormat;
                decimal lnRatio;
                int lnNewWidth = 0;
                int lnNewHeight = 0;

                //*** If the image is smaller than a thumbnail just return it
                if (loBMP.Width < lnWidth && loBMP.Height < lnHeight)
                {
                    //return loBMP;
                    if ((decimal)lnWidth / loBMP.Width < (decimal)lnHeight / loBMP.Height)//
                    {
                        lnRatio = (decimal)lnWidth / loBMP.Width;
                        lnNewWidth = lnWidth;
                        decimal lnTemp = loBMP.Height * lnRatio;
                        lnNewHeight = (int)lnTemp;
                    }
                    else
                    {
                        lnRatio = (decimal)lnHeight / loBMP.Height;
                        lnNewHeight = lnHeight;
                        decimal lnTemp = loBMP.Width * lnRatio;
                        lnNewWidth = (int)lnTemp;
                    }
                }
                else
                {
                    if ((decimal)loBMP.Width / lnWidth > (decimal)loBMP.Height / lnHeight)//
                    {
                        lnRatio = (decimal)lnWidth / loBMP.Width;
                        lnNewWidth = lnWidth;
                        decimal lnTemp = loBMP.Height * lnRatio;
                        lnNewHeight = (int)lnTemp;
                    }
                    else
                    {
                        lnRatio = (decimal)lnHeight / loBMP.Height;
                        lnNewHeight = lnHeight;
                        decimal lnTemp = loBMP.Width * lnRatio;
                        lnNewWidth = (int)lnTemp;
                    }
                }

                //if (loBMP.Width > loBMP.Height)
                //{
                //    lnRatio = (decimal)lnWidth / loBMP.Width;
                //    lnNewWidth = lnWidth;
                //    decimal lnTemp = loBMP.Height * lnRatio;
                //    lnNewHeight = (int)lnTemp;
                //}
                //else
                //{
                //    lnRatio = (decimal)lnHeight / loBMP.Height;
                //    lnNewHeight = lnHeight;
                //    decimal lnTemp = loBMP.Width * lnRatio;
                //    lnNewWidth = (int)lnTemp;
                //}
                // System.Drawing.Image imgOut = 
                //      loBMP.GetThumbnailImage(lnNewWidth,lnNewHeight,
                //                              null,IntPtr.Zero);

                // [buga]*** With this the new picture is actually drawn, without this we'll have
                // [buga]*** only empty rectangle

                // *** This code creates cleaner (though bigger) thumbnails and properly
                // *** and handles GIF files better by generating a white background for
                // *** transparent images (as opposed to black)
                bmpOut = new Bitmap(lnNewWidth, lnNewHeight);
                Graphics g = Graphics.FromImage(bmpOut);
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.FillRectangle(Brushes.White, 0, 0, lnNewWidth, lnNewHeight);
                g.DrawImage(loBMP, 0, 0, lnNewWidth, lnNewHeight);
                loBMP.Dispose();
            }
            catch (Exception ex)
            {
                return null;
            }
            return bmpOut;
        }

        private void ErrorResult()
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.StatusCode = 404;
            HttpContext.Current.Response.End();
        }
    }
}
