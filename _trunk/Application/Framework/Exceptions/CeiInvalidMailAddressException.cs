﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Framework.Exceptions
{
    public class CeiInvalidMailAddressException : CeiException
    {
        public CeiInvalidMailAddressException()
            : base()
        {
        }

        public override string Message
        {
            get
            {
                return base.Message;
            }
        }

        public override string Source
        {
            get
            {
                return base.Source;
            }
            set
            {
                base.Source = value;
            }
        }
    }
}
