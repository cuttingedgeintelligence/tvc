﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Framework.Exceptions
{
    public class CeiException : Exception
    {
        public CeiException()
            : base()
        {
        }

        public CeiException(string message)
            : base(message)
        {
        }
    }
}
