﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CEI.Framework
{
    public class CopyTemplates
    {
        public void Copy(string destinationDirectoryPath, string templatesDirectoryPath)
        {
            DirectoryInfo destinationDir = new DirectoryInfo(destinationDirectoryPath);
            destinationDir.Create();
            DirectoryInfo templatesDir = new DirectoryInfo(templatesDirectoryPath);
            CopyFiles(templatesDir.GetFiles(), destinationDirectoryPath);
            CopyDirectory(templatesDir, destinationDirectoryPath);


        }

        private void CopyFiles(FileInfo[] sourceFiles, string destinationFilePath)
        {
            foreach (FileInfo sourceFile in sourceFiles)
            {
                FileInfo newFile = sourceFile.CopyTo(destinationFilePath + "\\" + sourceFile.Name);
                newFile.IsReadOnly = false;
            }
        }

        private void CopyDirectory(DirectoryInfo sourceDir, string destinationDirPath)
        {
            foreach (DirectoryInfo originalSubdir in sourceDir.GetDirectories())
            {
                DirectoryInfo newDir = new DirectoryInfo(destinationDirPath + "\\" + originalSubdir.Name);
                newDir.Create();
                CopyFiles(originalSubdir.GetFiles(), destinationDirPath + "\\" + originalSubdir.Name);
                CopyDirectory(originalSubdir, destinationDirPath + "\\" + originalSubdir.Name);
            }
        }
    }
}
