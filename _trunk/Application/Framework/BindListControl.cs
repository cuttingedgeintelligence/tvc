﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace CEI.Framework
{
    public class BindListControl<T>
    {
        public static void Bind(ListControl control, List<T> dataSource, string dataTextField, string dataValueField)
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();
        }

        public static void Bind(ListControl control, T dataSource, string dataTextField, string dataValueField)
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();
        }

        public static void BindAndSelectFirstElement(ListControl control, List<T> dataSource, string dataTextField, string dataValueField)
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();
            control.SelectedIndex = 0;
        }

        public static void BindAndAddEmptyListItem(ListControl control, List<T> dataSource, string dataTextField, string dataValueField)
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();
            control.Items.Insert(0, new ListItem(string.Empty, "-1"));
        }

        public static void BindAndAddAllItem(ListControl control, List<T> dataSource, string dataTextField, string dataValueField)
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();
            control.Items.Insert(0, new ListItem("All", "-1"));
        }

        public static void BindAndAddAllItem(ListControl control, T dataSource, string dataTextField, string dataValueField)
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();
            control.Items.Insert(0, new ListItem("All", "-1"));
        }

        public static void BindAndSelectSpecifiedElement(ListControl control, List<T> dataSource, string dataTextField, string dataValueField, string selectedValue)
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();
            control.SelectedValue = selectedValue;
        }

        


        public static void BindAndAddListItem(ListControl control, T dataSource, string dataTextField, string dataValueField,string listItemText,string listItemValue)
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();
            control.Items.Insert(0, new ListItem(listItemText, listItemValue));
        }

        public static void BindAndAddListItem(ListControl control, List<T> dataSource, string dataTextField, string dataValueField,string listItemText,string listItemValue)
        {
            control.DataSource = dataSource;
            control.DataTextField = dataTextField;
            control.DataValueField = dataValueField;
            control.DataBind();
            control.Items.Insert(0, new ListItem(listItemText, listItemValue));
        }


    }
}
