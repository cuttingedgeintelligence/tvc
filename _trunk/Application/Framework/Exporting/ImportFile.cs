﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web;
using System.Xml;
using System.IO;
using System.Configuration;

//using CEI.DS.BO;

namespace CEI.Framework.Exporting
{
    public class ImportFile
    {

        //public ArrayList ImportFromOutlook(string outlookFilePath)
        //{
        //    int count = 0;
        //    StreamReader objReader = new StreamReader(outlookFilePath);

        //    ArrayList outlookContacts = new ArrayList();

        //    string sLine = "";
        //    string firstName = string.Empty;
        //    string middleName = string.Empty;
        //    string lastName = string.Empty;
        //    string emailAddress = string.Empty;
        //    int[] indexes = new int[4];

        //    ArrayList arrText = new ArrayList();
        //    ArrayList arrInsideText = new ArrayList();
        //    char[] delimiterChars = { ',', ';' };

        //    while (sLine != null)
        //    {
        //        sLine = objReader.ReadLine();
        //        if (sLine != null)
        //        {
        //            if (count > 0)
        //            {
        //                arrText.Add(sLine);
        //                string[] words = sLine.Split(delimiterChars);

        //                //changes for parsing outlook 2007 .csv files
        //                if (indexes[3] < words.Length)
        //                {
        //                    firstName = words[indexes[0]].Replace("\"", "").ToString();
        //                    middleName = words[indexes[1]].Replace("\"", "").ToString();
        //                    lastName = words[indexes[2]].Replace("\"", "").ToString();
        //                    emailAddress = words[indexes[3]].Replace("\"", "").ToString();
        //                    if (firstName == "")
        //                        firstName = " ";
        //                    if (middleName == "")
        //                        middleName = " ";
        //                    if (lastName == "")
        //                        lastName = " ";


        //                    AddressBook aBook = new AddressBook();
        //                    aBook.Name = firstName + middleName + lastName;
        //                    aBook.Email = emailAddress;
        //                    aBook.ContactType = ContactTypeEnum.Outlook;
        //                    outlookContacts.Add(aBook);
        //                }

        //            }
        //            else
        //            {
        //                arrText.Add(sLine);
        //                string[] words = sLine.Split(delimiterChars);
        //                for (int j = 0; j < words.Length; j++)
        //                {
        //                    if (words[j].Replace("\"", "").ToString() == "First Name")
        //                        indexes[0] = (int)j;
        //                    else
        //                        if (words[j].Replace("\"", "").ToString() == "Middle Name")
        //                            indexes[1] = (int)j;
        //                        else
        //                            if (words[j].Replace("\"", "").ToString() == "Last Name")
        //                                indexes[2] = (int)j;
        //                            else
        //                                if (words[j].Replace("\"", "").ToString() == "E-mail Address")
        //                                    indexes[3] = (int)j;
        //                }

        //                count++;
        //            }
        //        }
        //    }
        //    objReader.Close();

        //    return outlookContacts;
        //}





        //TODO: new method
        //public bool InsertIntoAddressBook(int userId, ArrayList contacts, int maxId)
        //{

        //    try
        //    {
        //        XmlDocument xmlDoc = new XmlDocument();
        //        xmlDoc.Load(HttpContext.Current.Request.PhysicalApplicationPath + ConfigurationManager.AppSettings["UsersDirectoryPath"].ToString() + userId.ToString() + "//AddressBook.config");
        //        foreach (AddressBook contact in contacts)
        //        {
        //            XmlNode contactNode = xmlDoc.SelectSingleNode("//item[@emailAddress='" + contact.Email + "']");
        //            if (contactNode == null)
        //            {
        //                maxId++;
        //                XmlElement elem = xmlDoc.CreateElement("item");
        //                elem.SetAttribute("id", maxId.ToString());
        //                elem.SetAttribute("name", contact.Name);
        //                elem.SetAttribute("emailAddress", contact.Email);
        //                elem.SetAttribute("insertedFrom", contact.ContactType.ToString());
        //                xmlDoc.DocumentElement.AppendChild(elem);

        //            }
        //            else
        //            {
        //                if (contactNode.Attributes["insertedFrom"].Value.IndexOf(contact.ContactType.ToString()) == -1)
        //                {
        //                    //add new contact type
        //                    contactNode.Attributes["insertedFrom"].Value += "," + contact.ContactType.ToString();

        //                    //sort contact types(inserted from)
        //                    string[] insertedFrom = contactNode.Attributes["insertedFrom"].Value.Split(',');
        //                    List<string> insertedFromList = new List<string>();

        //                    foreach (string insertedFromString in insertedFrom)
        //                        insertedFromList.Add(insertedFromString);

        //                    insertedFromList.Sort();
        //                    insertedFrom = insertedFromList.ToArray();

        //                    contactNode.Attributes["insertedFrom"].Value = string.Join(",", insertedFrom);
        //                }

        //            }
        //        }

        //        xmlDoc.Save(HttpContext.Current.Request.PhysicalApplicationPath + ConfigurationManager.AppSettings["UsersDirectoryPath"].ToString() + userId.ToString() + "//AddressBook.config");
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }


        //}




        //public string InsertSingleContactIntoAddressBook(int userId, string name, string email, int maxId)
        //{
        //    try
        //    {
        //        XmlDocument xmlDoc = new XmlDocument();
        //        xmlDoc.Load(HttpContext.Current.Request.PhysicalApplicationPath + ConfigurationManager.AppSettings["UsersDirectoryPath"].ToString() + userId.ToString() + "//AddressBook.config");
        //        XmlNode contactNode = xmlDoc.SelectSingleNode("//item[@emailAddress='" + email + "']");
        //        if (contactNode == null)
        //        {
        //            maxId++;
        //            XmlElement elem = xmlDoc.CreateElement("item");
        //            elem.SetAttribute("id", maxId.ToString());
        //            elem.SetAttribute("name", name);
        //            elem.SetAttribute("emailAddress", email);
        //            elem.SetAttribute("insertedFrom", ContactTypeEnum.SingleContact.ToString());
        //            xmlDoc.DocumentElement.AppendChild(elem);
        //            xmlDoc.Save(HttpContext.Current.Request.PhysicalApplicationPath + ConfigurationManager.AppSettings["UsersDirectoryPath"].ToString() + userId.ToString() + "//AddressBook.config");

        //            return "ok";
        //        }
        //        else
        //        {
        //            return "alreadyExists";
        //        }
        //    }
        //    catch
        //    {
        //        return "someProblem";
        //    }


        //}

        //private bool CheckExistingEmailAddress(int userId, string emailAddress)
        //{
        //    XmlDocument xmlDoc = new XmlDocument();
        //    //TODO: replace with input parameter
        //    xmlDoc.Load(HttpContext.Current.Request.PhysicalApplicationPath + ConfigurationManager.AppSettings["SubscribersDirectoryPath"] + userId.ToString() + "//AddressBook.config");
        //    XmlNodeList elements = xmlDoc.GetElementsByTagName("item");

        //    foreach (XmlNode node in elements)
        //    {
        //        if (node.Attributes["emailAddress"].Value == emailAddress.ToString())
        //            return true;
        //    }
        //    return false;
        //}

    }
}
