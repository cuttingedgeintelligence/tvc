﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Configuration;

namespace CEI.Framework.Mailer
{
    public class CeiMailer
    {
        private MailMessage _message;
        private SmtpClient _client;
        private string _mailDomain;
        private string _username;
        private string _password;
        private bool _useCredentials;
        private bool _smtpEnableSsl;
        private int _port;

        private int _maxNumOfAddressesPerSend = 2;


        #region Constructors

		/// <summary>
		/// 
		/// </summary>
		/// <param name="smtpHost">>Name or IP address of SMTP mail server</param>
		public CeiMailer(string smtpHost)
		{
			_message = new MailMessage();
			_client = new SmtpClient(smtpHost);

			_message.IsBodyHtml = true;

			_mailDomain = smtpHost;
		}
        


        /// <summary>
        /// 
        /// </summary>
        /// <param name="smtpHost">>Name or IP address of SMTP mail server</param>
        public CeiMailer(string smtpHost, string username, string password, bool useCredentials,int smtpHostPort,bool smtpEnableSsl)
        {
            _message = new MailMessage();
            _client = new SmtpClient(smtpHost, smtpHostPort);
            _port = smtpHostPort;
            _smtpEnableSsl = _client.EnableSsl = smtpEnableSsl;
            if (useCredentials)
            {
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(username, password);
                _client.Credentials = credentials;
            }
            _mailDomain = smtpHost;
            _username = username;
            _password = password;
            _useCredentials = useCredentials;
            _message.IsBodyHtml = true;
        }


        /// <summary>
        /// 
        /// </summary>
        public CeiMailer()
        {
            _message = new MailMessage();

            _message.IsBodyHtml = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="smtpHost">>Name or IP address of SMTP mail server</param>
        /// <param name="maxNumberOfAddressesPerSend">Default 500 mail addresses including From, To, CC and Bcc</param>
        public CeiMailer(string smtpHost, int maxNumberOfAddressesPerSend)
        {
            _message = new MailMessage();
            _client = new SmtpClient(smtpHost);

            _maxNumOfAddressesPerSend = maxNumberOfAddressesPerSend;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="smtpHost">Name or IP address of SMTP mail server</param>
        /// <param name="maxNumberOfAddressesPerSend">Default 500 mail addresses including From, To, CC and Bcc</param>
        /// <param name="port">Default port no: 25</param>
        public CeiMailer(string smtpHost, int maxNumberOfAddressesPerSend, int port)
        {
            _message = new MailMessage();
            _client = new SmtpClient(smtpHost, port);

            _maxNumOfAddressesPerSend = maxNumberOfAddressesPerSend;
        }

        #endregion //Constructors

        /// <summary>
        /// Port number that SMTP mail server should use to send mail through. Default port no: 25
        /// </summary>
        //public int Port
        //{
        //    get
        //    {
        //        return this._port;
        //    }
        //    set
        //    {
        //        this._port = value;
        //    }
        //}

        /// <summary>
        /// Maximum number of mail addresses that should be sent per sent. Default 500 mail addresses including From, To, CC and Bcc
        /// </summary>
        public int MaxNumberOfAddressesPerSend
        {
            get
            {
                return this._maxNumOfAddressesPerSend;
            }
            set
            {
                this._maxNumOfAddressesPerSend = value;
            }
        }

        public void From(string from)
        {
            _message.From = new MailAddress(from);

        }

        public void From(string from, string displayName)
        {
            _message.From = new MailAddress(from, displayName);
        }

        public void From(string from, string displayName, Encoding displayNameEncoding)
        {
            _message.From = new MailAddress(from, displayName, displayNameEncoding);
        }

        public void To(params string[] to)
        {
            for (int i = 0; i < to.Length; i++)
                _message.To.Add(to[i]);
        }

        public void Cc(params string[] cc)
        {
            for (int i = 0; i < cc.Length; i++)
                _message.CC.Add(cc[i]);
        }

        public void Bcc(params string[] bcc)
        {
            for (int i = 0; i < bcc.Length; i++)
                _message.Bcc.Add(bcc[i]);
        }

        public void Subject(string subject)
        {
            _message.Subject = subject;
        }

        public void Subject(string subject, Encoding subjectEncoding)
        {
            _message.Subject = subject;
            _message.SubjectEncoding = subjectEncoding;
        }

        public void Body(string body)
        {
            _message.Body = body;
        }

        public void Body(string body, Encoding bodyEncoding)
        {
            _message.Body = body;
            _message.BodyEncoding = bodyEncoding;
        }


        #region Send Async


        public void SendAsync()
        {

            if (_message.To.Count + _message.CC.Count + _message.Bcc.Count > 0)
            {
                if (_message.To.Count + _message.CC.Count + _message.Bcc.Count > _maxNumOfAddressesPerSend)
                {
                    this.SplitMailerMessages();
                }
                else
                {
                    object userState = _message;
                    _client.SendCompleted += new SendCompletedEventHandler(_client_SendCompleted);
                    _client.SendAsync(_message, userState);
                }
            }

        }

        private void SendAsyncCancel()
        {
            _client.SendAsyncCancel();
        }

        private void _client_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            MailMessage mail = (MailMessage)e.UserState;
            //string subject = mail.Subject;

            if (e.Cancelled)
            {
                //Console.WriteLine("Send canceled for mail with subject [{0}].", subject); 
            }
            if (e.Error != null)
            {
                //Console.WriteLine("Error {1} occurred when sending mail [{0}] ", subject, e.Error.ToString());
            }
            else
            {
                //Console.WriteLine("Message [{0}] sent.", subject);
            }
        }



        #endregion //Send Async


        #region Send Sync

        public void SendSync()
        {
            _client.Send(_message);
        }

        #endregion //Send Sync


        #region Private methods


        private bool CheckMailerValidity()
        {

            return false;
        }


        private void SplitMailerMessages()
        {
            if (_message.To.Count > _maxNumOfAddressesPerSend)
            {
                #region To

                for (int j = 0; j < ((_message.To.Count / _maxNumOfAddressesPerSend) + 1); j++)
                {
                    Mailer.CeiMailer _newMessage = new Mailer.CeiMailer(_mailDomain, _username, _password, _useCredentials,_port,_smtpEnableSsl);
                    _newMessage.Body(_message.Body);
                    _newMessage.From(_message.From.ToString());
                    _newMessage.Subject(_message.Subject);
                    if (((j + 1) * _maxNumOfAddressesPerSend) > _message.To.Count)
                    {
                        string[] newTo = new string[_message.To.Count - (j * _maxNumOfAddressesPerSend)];

                        for (int i = 0; i < _message.To.Count - (j * _maxNumOfAddressesPerSend); i++)//j * _maxNumOfAddressesPerSend; i < ((j + 1) * _maxNumOfAddressesPerSend); i++)
                            newTo[i] = _message.To[(j * _maxNumOfAddressesPerSend) + i].ToString();

                        _newMessage.To(newTo);
                    }
                    else
                    {
                        string[] newTo = new string[_maxNumOfAddressesPerSend];
                        for (int i = 0; i < _maxNumOfAddressesPerSend; i++)
                            newTo[i] = _message.To[(j * _maxNumOfAddressesPerSend) + i].ToString();

                        _newMessage.To(newTo);
                    }
                    _newMessage.SendAsync();
                }

                #endregion //To
            }
            else
            {
                #region To

                for (int j = 0; j < _message.To.Count; j++)
                {
                    Mailer.CeiMailer _newMessage = new Mailer.CeiMailer(_mailDomain, _username, _password, _useCredentials,_port,_smtpEnableSsl);
                    _newMessage.Body(_message.Body);
                    _newMessage.From(_message.From.ToString());
                    _newMessage.Subject(_message.Subject);

                    string[] newTo = new string[_message.To.Count];

                    for (int i = 0; i < _message.To.Count; i++)
                        newTo[i] = _message.To[i].ToString();

                    _newMessage.To(newTo);

                    _newMessage.SendAsync();
                }

                #endregion //To
            }

            if (_message.Bcc.Count > _maxNumOfAddressesPerSend)
            {
                #region Bcc

                for (int j = 0; j < ((_message.Bcc.Count / _maxNumOfAddressesPerSend) + 1); j++)
                {
                    Mailer.CeiMailer _newMessage = new Mailer.CeiMailer(_mailDomain, _username, _password, _useCredentials,_port,_smtpEnableSsl);
                    _newMessage.Body(_message.Body);
                    _newMessage.From(_message.From.ToString());
                    _newMessage.Subject(_message.Subject);
                    if (((j + 1) * _maxNumOfAddressesPerSend) > _message.Bcc.Count)
                    {
                        string[] newBcc = new string[_message.Bcc.Count - (j * _maxNumOfAddressesPerSend)];

                        for (int i = 0; i < _message.Bcc.Count - (j * _maxNumOfAddressesPerSend); i++)//j * _maxNumOfAddressesPerSend; i < ((j + 1) * _maxNumOfAddressesPerSend); i++)
                            newBcc[i] = _message.Bcc[(j * _maxNumOfAddressesPerSend) + i].ToString();

                        _newMessage.Bcc(newBcc);
                    }
                    else
                    {
                        string[] newBcc = new string[_maxNumOfAddressesPerSend];
                        for (int i = 0; i < _maxNumOfAddressesPerSend; i++)
                            newBcc[i] = _message.Bcc[(j * _maxNumOfAddressesPerSend) + i].ToString();

                        _newMessage.Bcc(newBcc);
                    }
                    _newMessage.SendAsync();
                }

                #endregion //Bcc
            }
            else
            {
                #region Bcc

                for (int j = 0; j < _message.Bcc.Count; j++)
                {
                    Mailer.CeiMailer _newMessage = new Mailer.CeiMailer(_mailDomain, _username, _password, _useCredentials,_port,_smtpEnableSsl);
                    _newMessage.Body(_message.Body);
                    _newMessage.From(_message.From.ToString());
                    _newMessage.Subject(_message.Subject);

                    string[] newBcc = new string[_message.Bcc.Count];

                    for (int i = 0; i < _message.Bcc.Count; i++)
                        newBcc[i] = _message.Bcc[i].ToString();

                    _newMessage.Bcc(newBcc);

                    _newMessage.SendAsync();
                }

                #endregion //Bcc
            }

            if (_message.CC.Count > _maxNumOfAddressesPerSend)
            {
                #region CC

                for (int j = 0; j < ((_message.CC.Count / _maxNumOfAddressesPerSend) + 1); j++)
                {
                    Mailer.CeiMailer _newMessage = new Mailer.CeiMailer(_mailDomain, _username, _password, _useCredentials,_port,_smtpEnableSsl);
                    _newMessage.Body(_message.Body);
                    _newMessage.From(_message.From.ToString());
                    _newMessage.Subject(_message.Subject);
                    if (((j + 1) * _maxNumOfAddressesPerSend) > _message.CC.Count)
                    {
                        string[] newCc = new string[_message.CC.Count - (j * _maxNumOfAddressesPerSend)];

                        for (int i = 0; i < _message.CC.Count - (j * _maxNumOfAddressesPerSend); i++)//j * _maxNumOfAddressesPerSend; i < ((j + 1) * _maxNumOfAddressesPerSend); i++)
                            newCc[i] = _message.CC[(j * _maxNumOfAddressesPerSend) + i].ToString();

                        _newMessage.Cc(newCc);
                    }
                    else
                    {
                        string[] newCc = new string[_maxNumOfAddressesPerSend];
                        for (int i = 0; i < _maxNumOfAddressesPerSend; i++)
                            newCc[i] = _message.CC[(j * _maxNumOfAddressesPerSend) + i].ToString();

                        _newMessage.Cc(newCc);
                    }
                    _newMessage.SendAsync();
                }

                #endregion //CC
            }
            else
            {
                #region Cc

                for (int j = 0; j < _message.CC.Count; j++)
                {
                    Mailer.CeiMailer _newMessage = new Mailer.CeiMailer(_mailDomain, _username, _password, _useCredentials,_port,_smtpEnableSsl);
                    _newMessage.Body(_message.Body);
                    _newMessage.From(_message.From.ToString());
                    _newMessage.Subject(_message.Subject);

                    string[] newCc = new string[_message.CC.Count];

                    for (int i = 0; i < _message.CC.Count; i++)
                        newCc[i] = _message.CC[i].ToString();

                    _newMessage.Cc(newCc);

                    _newMessage.SendAsync();
                }

                #endregion //Cc
            }
        }

        private void SendSplitedMessageTo()
        {

        }

        #endregion //Private methods

    }
}
