﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using CEI.Common;
using CEI.BR;
using CEI.Framework;
using CEI.Common.RecurringAppointment;
using System.Collections.Generic;



namespace CEI.Web.ManageAppointmentDates
{
    public partial class ManageAppointmentDates : BasePage
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindListControl<DataTable>.BindAndAddListItem(ddlPractices, new UserBR().GetAllPracticesNameAndId(), "name", "id", "Select Practice", "-1");

                //Set default values
                this.InitWeeklyPanel();
                this.InitMonthlyPanel();
                txtOccurences.Text = "10";
                ucRangeStartDate.SetYearFromYearTo(DateTime.Today.Year - 20, DateTime.Today.Year + 20);
                ucRangeEndDate.SetYearFromYearTo(DateTime.Today.Year - 20, DateTime.Today.Year + 20);
                ucOneTimeAppointmentDate.SetYearFromYearTo(DateTime.Today.Year - 20, DateTime.Today.Year + 20);


            }
        }

        protected void ddlPractices_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblErrorMessage.Style.Add("display", "none");
            if (ddlPractices.SelectedValue != "-1")
            {

                DataRow drPracticeAppointmentsSettings = new PracticeAppointmentsSettingsBR().GetPracticeAppointmentSettingsByPracticeId(new Guid(ddlPractices.SelectedValue));
                if (drPracticeAppointmentsSettings != null)
                {

                    ViewState["insertappointmentssettings"] = null;
                    lblSelectedPractice.Text = "Update Practice Appointments";
                    btnSaveAppointment.Text = "Save";
                    btnRemoveRecurrence.Style.Add("display", "");
                    this.FillForm(drPracticeAppointmentsSettings);
                }
                else
                {
                    ViewState["insertappointmentssettings"] = "true";
                    rblRecurrencePattern.SelectedValue = RecurrenceType.Weekly.ToString();
                    this.InitWeeklyPanel();
                    pnlWeeklySettings.Style.Add("display", "");
                    pnlMonthlySettings.Style.Add("display", "none");
                    lblSelectedPractice.Text = "Add Recurrent Appointments";
                    btnSaveAppointment.Text = "Add";
                    btnRemoveRecurrence.Style.Add("display", "none");

                }

                ViewState["practiceId"] = ddlPractices.SelectedValue;
                this.BindGridViewOneTimeAppointments();
                this.SelectCalendarDates();
            }
            else
            {
                rblRecurrencePattern.SelectedValue = RecurrenceType.Weekly.ToString();
                this.InitWeeklyPanel();
                pnlWeeklySettings.Style.Add("display", "");
                pnlMonthlySettings.Style.Add("display", "none");
                lblSelectedPractice.Text = "Add Recurrent Appointments";
                btnSaveAppointment.Text = "Add";
                btnRemoveRecurrence.Style.Add("display", "none");
                gvOneTimeAppointments.DataSource = null;
                gvOneTimeAppointments.DataBind();

                calApointments.SelectedDates.Clear();
            }
        }


        protected void gvOneTimeAppointments_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvOneTimeAppointments.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvOneTimeAppointments_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvOneTimeAppointments.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvOneTimeAppointments.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvOneTimeAppointments.PageIndex == (gvOneTimeAppointments.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }


        protected void btnSaveAppointment_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string errorMessage;
                if (rblRecurrencePattern.SelectedValue.Equals(RecurrenceType.Weekly.ToString())) //WEEKLY  recurrence
                {

                    //validate weekly recurrence data

                    if (this.ValidateWeeklyRecurrenceData(out errorMessage))
                    {
                        lblErrorMessage.Style.Add("display", "none");
                        if (ViewState["insertappointmentssettings"] != null) //insert appointments settings
                        {

                            PracticeAppointmentsSettings practiceAppointmentsSettings = new PracticeAppointmentsSettings();
                            practiceAppointmentsSettings.PracticeId = new Guid(ddlPractices.SelectedValue);
                            practiceAppointmentsSettings.CreatedBy = new Guid(User.Identity.Name);
                            practiceAppointmentsSettings.RecurrencePattern = this.CreateWeeklyRecurrencePattern();




                            if (new PracticeAppointmentsSettingsBR().InsertPracticeAppointmentsSettings(practiceAppointmentsSettings))
                            {
                                //set edit mode
                                ViewState["insertappointmentssettings"] = null;
                                btnRemoveRecurrence.Style.Add("display", "");
                                btnSaveAppointment.Text = "Save";
                                lblSelectedPractice.Text = "Update Practice Appointments";
                                this.SelectCalendarDates();
                            }
                            else
                            {
                                //TODO: error message;
                            }

                        }
                        else //update appointments settings
                        {
                            PracticeAppointmentsSettings practiceAppointmentsSettings = new PracticeAppointmentsSettings();
                            practiceAppointmentsSettings.PracticeId = new Guid(ddlPractices.SelectedValue);
                            practiceAppointmentsSettings.RecurrencePattern = this.CreateWeeklyRecurrencePattern();



                            if (new PracticeAppointmentsSettingsBR().UpdatePracticeAppointmentsSettings(practiceAppointmentsSettings))
                            {
                                //stay in edit mode
                                this.SelectCalendarDates();

                                this.ShowSuccessAlert();
                            }
                            else
                            {
                                //TODO: error message;
                            }

                        }

                    }
                    else
                    {
                        lblErrorMessage.Text = "Invalid recurrence pattern." + errorMessage;
                        lblErrorMessage.Style.Add("display", "");
                    }

                }
                else //MONTHLY recurrence
                {
                    //validate monthly recurrence data
                    if (this.ValidateMonthlyRecurrenceData(out errorMessage))
                    {
                        lblErrorMessage.Style.Add("display", "none");
                        if (ViewState["insertappointmentssettings"] != null) //insert appointments settings
                        {

                            PracticeAppointmentsSettings practiceAppointmentsSettings = new PracticeAppointmentsSettings();
                            practiceAppointmentsSettings.PracticeId = new Guid(ddlPractices.SelectedValue);
                            practiceAppointmentsSettings.CreatedBy = new Guid(User.Identity.Name);
                            practiceAppointmentsSettings.RecurrencePattern = this.CreateMonthlyRecurrencePattern();




                            if (new PracticeAppointmentsSettingsBR().InsertPracticeAppointmentsSettings(practiceAppointmentsSettings))
                            {
                                //set edit mode
                                ViewState["insertappointmentssettings"] = null;
                                btnRemoveRecurrence.Style.Add("display", "");
                                btnSaveAppointment.Text = "Save";
                                this.SelectCalendarDates();

                            }
                            else
                            {
                                //TODO: error message;
                            }

                        }
                        else //update appointments settings
                        {
                            PracticeAppointmentsSettings practiceAppointmentsSettings = new PracticeAppointmentsSettings();
                            practiceAppointmentsSettings.PracticeId = new Guid(ddlPractices.SelectedValue);
                            practiceAppointmentsSettings.RecurrencePattern = this.CreateMonthlyRecurrencePattern();





                            if (new PracticeAppointmentsSettingsBR().UpdatePracticeAppointmentsSettings(practiceAppointmentsSettings))
                            {
                                //stay in edit mode
                                this.SelectCalendarDates();

                                this.ShowSuccessAlert();
                            }
                            else
                            {
                                //TODO: error message;
                            }

                        }
                    }
                    else
                    {
                        lblErrorMessage.Text = "Invalid recurrence pattern." + errorMessage;
                        lblErrorMessage.Style.Add("display", "");
                    }


                }
            }
        }

        protected void btnRemoveRecurrence_Click(object sender, EventArgs e)
        {
            lblErrorMessage.Style.Add("display", "none");
            if (new PracticeAppointmentsSettingsBR().DeletePracticeAppointmentsSettings(new Guid(ddlPractices.SelectedValue)))
            {
                //hide remove button go in insert mode
                rblRecurrencePattern.SelectedValue = RecurrenceType.Weekly.ToString();
                this.InitWeeklyPanel();
                pnlWeeklySettings.Style.Add("display", "");
                pnlMonthlySettings.Style.Add("display", "none");
                lblSelectedPractice.Text = "Add Recurrent Appointments";
                btnSaveAppointment.Text = "Add";
                btnRemoveRecurrence.Style.Add("display", "none");
                this.SelectCalendarDates();
            }
            else
            {
                //TODO: error message
            }
        }




        protected void rblRecurrencePattern_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblErrorMessage.Style.Add("display", "none");
            if ((sender as RadioButtonList).SelectedValue.Equals(RecurrenceType.Monthly.ToString()))
            {
                pnlWeeklySettings.Style.Add("display", "none");
                pnlMonthlySettings.Style.Add("display", "");

            }
            else //Weekly
            {

                pnlWeeklySettings.Style.Add("display", "");
                pnlMonthlySettings.Style.Add("display", "none");

            }
        }


        protected void odsOneTimeAppointments_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["practiceId"] = ViewState["practiceId"];
        }

        protected void gvOneTimeAppointments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvOneTimeAppointments.PageIndex = e.NewPageIndex;
            gvOneTimeAppointments.DataSource = odsOneTimeAppointments;
            gvOneTimeAppointments.DataBind();

        }

        protected void btnAddOneTimeAppointment_Click(object sender, EventArgs e)
        {
            if (ddlPractices.SelectedValue != "-1") //server side validation
            {
                if (!new PracticeOneTimeAppointmentBR().ExistsOneTimeAppointmentWithPracticeIdAndAppointmentDate(new Guid(ddlPractices.SelectedValue), new DateTime(ucOneTimeAppointmentDate.Year, ucOneTimeAppointmentDate.Month, ucOneTimeAppointmentDate.Day)))
                {
                    PracticeOneTimeAppointment oneTimeAppointment = new PracticeOneTimeAppointment();
                    oneTimeAppointment.PracticeId = new Guid(ddlPractices.SelectedValue);
                    oneTimeAppointment.AppointmentDate = new DateTime(ucOneTimeAppointmentDate.Year, ucOneTimeAppointmentDate.Month, ucOneTimeAppointmentDate.Day);
                    oneTimeAppointment.CreatedBy = new Guid(User.Identity.Name);
                    if (new PracticeOneTimeAppointmentBR().InsertPracticeOneTimeAppointment(oneTimeAppointment))
                    {
                        this.BindGridViewOneTimeAppointments();
                        this.SelectCalendarDates();
                    }
                }


            }
            else
            {
                cvPractice.IsValid = false;
            }
        }


        protected void gvOneTimeAppointments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("removeappointment"))
            {

                if (new PracticeOneTimeAppointmentBR().DeletePracticeOneTimeAppointment((int)gvOneTimeAppointments.DataKeys[int.Parse(e.CommandArgument.ToString())].Value))
                {
                    this.BindGridViewOneTimeAppointments();
                    this.SelectCalendarDates();
                }
            }


        }

        protected void calApointments_DayRender(object sender, DayRenderEventArgs e)
        {
            e.Day.IsSelectable = false;
        }

        #endregion //Event handlers



        #region Private methods

        private void InitWeeklyPanel()
        {
            txtRecurEveryWeeks.Text = "1";



            cbMonday.Checked = DateTime.Today.DayOfWeek.Equals(DayOfWeek.Monday);
            cbTuesday.Checked = DateTime.Today.DayOfWeek.Equals(DayOfWeek.Tuesday);
            cbWednesday.Checked = DateTime.Today.DayOfWeek.Equals(DayOfWeek.Wednesday);
            cbThursday.Checked = DateTime.Today.DayOfWeek.Equals(DayOfWeek.Thursday);
            cbFriday.Checked = DateTime.Today.DayOfWeek.Equals(DayOfWeek.Friday);
            cbSaturday.Checked = DateTime.Today.DayOfWeek.Equals(DayOfWeek.Saturday);





        }



        private void InitMonthlyPanel()
        {
            txtRecurEveryMonths.Text = "1";


        }



        private string CreateWeeklyRecurrencePattern()
        {
            WeeklyRecurrenceSettings we;
            SelectedDayOfWeekValues selectedValues = new SelectedDayOfWeekValues();

            if (rbEndAfterOccurences.Checked)
                we = new WeeklyRecurrenceSettings(new DateTime(ucRangeStartDate.Year, ucRangeStartDate.Month, ucRangeStartDate.Day), int.Parse(txtOccurences.Text));
            else if (rbEndBy.Checked)
                we = new WeeklyRecurrenceSettings(new DateTime(ucRangeStartDate.Year, ucRangeStartDate.Month, ucRangeStartDate.Day), new DateTime(ucRangeEndDate.Year, ucRangeEndDate.Month, ucRangeEndDate.Day));
            else //rbNoEndDate.Checked
                we = new WeeklyRecurrenceSettings(new DateTime(ucRangeStartDate.Year, ucRangeStartDate.Month, ucRangeStartDate.Day));

            selectedValues.Sunday = false;
            selectedValues.Monday = cbMonday.Checked;
            selectedValues.Tuesday = cbTuesday.Checked;
            selectedValues.Wednesday = cbWednesday.Checked;
            selectedValues.Thursday = cbThursday.Checked;
            selectedValues.Friday = cbFriday.Checked;
            selectedValues.Saturday = cbSaturday.Checked;

            //RecurrenceValues values = we.GetValues(int.Parse(txtRecurEveryWeeks.Text), selectedValues);
            return we.GetRecurrencePattern(int.Parse(txtRecurEveryWeeks.Text), selectedValues);
        }


        private string CreateMonthlyRecurrencePattern()
        {
            MonthlyRecurrenceSettings mo;
            if (rbEndAfterOccurences.Checked)
                mo = new MonthlyRecurrenceSettings(new DateTime(ucRangeStartDate.Year, ucRangeStartDate.Month, ucRangeStartDate.Day), int.Parse(txtOccurences.Text));
            else if (rbEndBy.Checked)
                mo = new MonthlyRecurrenceSettings(new DateTime(ucRangeStartDate.Year, ucRangeStartDate.Month, ucRangeStartDate.Day), new DateTime(ucRangeEndDate.Year, ucRangeEndDate.Month, ucRangeEndDate.Day));
            else  //rbNoEndDate.Checked
                mo = new MonthlyRecurrenceSettings(new DateTime(ucRangeStartDate.Year, ucRangeStartDate.Month, ucRangeStartDate.Day));

            //values = mo.GetValues((MonthlySpecificDatePartOne)comboBox2.SelectedIndex, (MonthlySpecificDatePartTwo)comboBox3.SelectedIndex, int.Parse(textBox3.Text));

            return mo.GetRecurrencePattern((MonthlySpecificDatePartOne)Enum.Parse(typeof(MonthlySpecificDatePartOne), ddlMonthlySpecificDatePartOne.SelectedValue), (MonthlySpecificDatePartTwo)Enum.Parse(typeof(MonthlySpecificDatePartTwo), ddlMonthlySpecificDatePartTwo.SelectedValue), int.Parse(txtRecurEveryMonths.Text));
        }



        private bool ValidateWeeklyRecurrenceData(out string errorMessage)
        {

            errorMessage = string.Empty;
            if (!Regex.IsMatch(txtRecurEveryWeeks.Text, @"^\d+$"))
                errorMessage += "Week(s) must be a number greater than 0.";
            else if (int.Parse(txtRecurEveryWeeks.Text) == 0)
                errorMessage += "Week(s) must be a number greater than 0.";


            if (!(cbMonday.Checked || cbTuesday.Checked || cbWednesday.Checked || cbThursday.Checked || cbFriday.Checked || cbSaturday.Checked))
                errorMessage += "At least one day in the week must be selected.";


            if (rbEndBy.Checked)
            {
                DateTime startDate = new DateTime(ucRangeStartDate.Year, ucRangeStartDate.Month, ucRangeStartDate.Day);
                DateTime endDate = new DateTime(ucRangeEndDate.Year, ucRangeEndDate.Month, ucRangeEndDate.Day);
                if (startDate > endDate)
                    errorMessage += "Start date must be greater than end date.";
            }

            return errorMessage.Equals(string.Empty);
        }


        private bool ValidateMonthlyRecurrenceData(out string errorMessage)
        {

            errorMessage = string.Empty;

            if (!Regex.IsMatch(txtRecurEveryMonths.Text, @"^\d+$"))
                errorMessage += "Month(s) must be a number greater than 0.";
            else if (int.Parse(txtRecurEveryMonths.Text) == 0)
                errorMessage += "Month(s) must be a number greater than 0.";

            if (rbEndBy.Checked)
            {
                DateTime startDate = new DateTime(ucRangeStartDate.Year, ucRangeStartDate.Month, ucRangeStartDate.Day);
                DateTime endDate = new DateTime(ucRangeEndDate.Year, ucRangeEndDate.Month, ucRangeEndDate.Day);
                if (startDate > endDate)
                    errorMessage += "Start date must be greater than end date.";
            }

            return errorMessage.Equals(string.Empty);
        }


        private void FillForm(DataRow drPracticeAppointmentsSettings)
        {




            // Get reccurrence info object to use for setting controls
            RecurrenceInfo info = RecurrenceHelper.GetFriendlySeriesInfo(drPracticeAppointmentsSettings["recurrencePattern"].ToString());




            if (info.RecurrenceType.Equals(RecurrenceType.Monthly))
            {
                rblRecurrencePattern.SelectedValue = RecurrenceType.Monthly.ToString();
                pnlWeeklySettings.Style.Add("display", "none");
                pnlMonthlySettings.Style.Add("display", "");

                txtRecurEveryMonths.Text = info.MonthlyRegenEveryXMonths.ToString();

                ddlMonthlySpecificDatePartOne.SelectedValue = info.MonthlySpecificDatePartOne.ToString();
                ddlMonthlySpecificDatePartTwo.SelectedValue = info.MonthlySpecificDatePartTwo.ToString();

                ucRangeStartDate.SetDate(info.StartDate);
                if (info.EndDateType.Equals(EndDateType.NoEndDate))
                {
                    rbNoEndDate.Checked = true;
                    rbEndAfterOccurences.Checked = false;
                    rbEndBy.Checked = false;
                }

                else if (info.EndDateType.Equals(EndDateType.NumberOfOccurrences))
                {
                    rbEndAfterOccurences.Checked = true;
                    rbNoEndDate.Checked = false;
                    rbEndBy.Checked = false;
                    txtOccurences.Text = info.NumberOfOccurrences.ToString();
                }
                else //EndDateType.SpecificDate
                {
                    rbEndBy.Checked = true;
                    rbEndAfterOccurences.Checked = false;
                    rbNoEndDate.Checked = false;
                    ucRangeEndDate.SetDate(info.EndDate.Value);
                }

            }
            else if (info.RecurrenceType.Equals(RecurrenceType.Weekly))
            {
                rblRecurrencePattern.SelectedValue = RecurrenceType.Weekly.ToString();
                pnlWeeklySettings.Style.Add("display", "");
                pnlMonthlySettings.Style.Add("display", "none");
                txtRecurEveryWeeks.Text = info.WeeklyRegenEveryXWeeks.ToString();
                cbMonday.Checked = info.WeeklySelectedDays.Monday;
                cbTuesday.Checked = info.WeeklySelectedDays.Tuesday;
                cbWednesday.Checked = info.WeeklySelectedDays.Wednesday;
                cbThursday.Checked = info.WeeklySelectedDays.Thursday;
                cbFriday.Checked = info.WeeklySelectedDays.Friday;
                cbSaturday.Checked = info.WeeklySelectedDays.Saturday;


                ucRangeStartDate.SetDate(info.StartDate);
                if (info.EndDateType.Equals(EndDateType.NoEndDate))
                {
                    rbNoEndDate.Checked = true;
                    rbEndAfterOccurences.Checked = false;
                    rbEndBy.Checked = false;
                }
                else if (info.EndDateType.Equals(EndDateType.NumberOfOccurrences))
                {
                    rbEndAfterOccurences.Checked = true;
                    rbNoEndDate.Checked = false;
                    rbEndBy.Checked = false;
                    txtOccurences.Text = info.NumberOfOccurrences.ToString();
                }
                else //EndDateType.SpecificDate
                {
                    rbEndBy.Checked = true;
                    rbEndAfterOccurences.Checked = false;
                    rbNoEndDate.Checked = false;
                    ucRangeEndDate.SetDate(info.EndDate.Value);
                }

            }

        }

        private void BindGridViewOneTimeAppointments()
        {
            gvOneTimeAppointments.PageIndex = 0;
            gvOneTimeAppointments.DataSource = odsOneTimeAppointments;
            gvOneTimeAppointments.DataBind();
        }



        private List<DateTime> GetNextRecurringAppointmentDates()
        {
            List<DateTime> nextAppointmentDates = new List<DateTime>();
            DataRow drPracticeAppointmentsSettings = new PracticeAppointmentsSettingsBR().GetPracticeAppointmentSettingsByPracticeId(new Guid(ddlPractices.SelectedValue));
            if (drPracticeAppointmentsSettings != null)
            {
                // Get reccurrence info object 
                RecurrenceInfo info = RecurrenceHelper.GetFriendlySeriesInfo(drPracticeAppointmentsSettings["recurrencePattern"].ToString());
                string endDateMessage = string.Empty;
                switch (info.EndDateType)
                {
                    case EndDateType.NoEndDate:
                        endDateMessage = "No end date.";
                        break;
                    case EndDateType.NumberOfOccurrences:
                        endDateMessage = "End after: " + info.NumberOfOccurrences.ToString() + " occurences.";
                        break;
                    case EndDateType.SpecificDate:
                        endDateMessage = "End by: " + info.EndDate.Value.ToString("dd MMMM yyyy ddddd") + ".";
                        break;

                }


                if (info.RecurrenceType.Equals(RecurrenceType.Monthly))
                {

                    MonthlyRecurrenceSettings mo;

                    if (info.EndDateType.Equals(EndDateType.NumberOfOccurrences))
                    {
                        mo = new MonthlyRecurrenceSettings(info.StartDate, info.NumberOfOccurrences);
                        nextAppointmentDates = mo.GetValues(info.MonthlySpecificDatePartOne, info.MonthlySpecificDatePartTwo, info.MonthlyRegenEveryXMonths).Values;



                    }
                    else if ((info.EndDateType.Equals(EndDateType.SpecificDate) && DateTime.Today < info.EndDate) ||
                               info.EndDateType.Equals(EndDateType.NoEndDate)
                             )
                    {

                        //set interval end date 
                        DateTime intervalEndDate;

                        //add info.MonthlyRegenEveryXMonths * 10 months on the start date(or today) to set interval  end date 
                        if (info.StartDate > DateTime.Today)
                            intervalEndDate = info.StartDate.AddMonths(info.MonthlyRegenEveryXMonths * 10);
                        else
                            intervalEndDate = DateTime.Today.AddMonths(info.MonthlyRegenEveryXMonths * 10);



                        if (info.EndDateType.Equals(EndDateType.SpecificDate) && intervalEndDate > info.EndDate)
                            intervalEndDate = info.EndDate.Value;


                        mo = new MonthlyRecurrenceSettings(info.StartDate, intervalEndDate);
                        nextAppointmentDates = mo.GetValues(info.MonthlySpecificDatePartOne, info.MonthlySpecificDatePartTwo, info.MonthlyRegenEveryXMonths).Values;





                    }

                }
                else if (info.RecurrenceType.Equals(RecurrenceType.Weekly))
                {

                    string weeklySelectedDays = string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Monday ? DayOfWeek.Monday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Tuesday ? DayOfWeek.Tuesday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Wednesday ? DayOfWeek.Wednesday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Thursday ? DayOfWeek.Thursday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Friday ? DayOfWeek.Friday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Saturday ? DayOfWeek.Saturday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Sunday ? DayOfWeek.Sunday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays = weeklySelectedDays.Substring(0, weeklySelectedDays.Length - 1);

                    WeeklyRecurrenceSettings we;

                    if (info.EndDateType.Equals(EndDateType.NumberOfOccurrences))
                    {
                        we = new WeeklyRecurrenceSettings(info.StartDate, info.NumberOfOccurrences);
                        nextAppointmentDates = we.GetValues(info.WeeklyRegenEveryXWeeks, info.WeeklySelectedDays).Values;




                    }
                    else if ((info.EndDateType.Equals(EndDateType.SpecificDate) && DateTime.Today < info.EndDate) ||
                                info.EndDateType.Equals(EndDateType.NoEndDate)
                             )
                    {

                        //set interval end date 
                        DateTime intervalEndDate;


                        //add (info.WeeklyRegenEveryXWeeks < 4) ? 10 : info.WeeklyRegenEveryXWeeks / 4 * 10  months on the interval start date(or today) to set interval  end date ; 
                        if (info.StartDate > DateTime.Today)
                            intervalEndDate = info.StartDate.AddMonths((info.WeeklyRegenEveryXWeeks < 4) ? 10 : info.WeeklyRegenEveryXWeeks / 4 * 10);
                        else
                            intervalEndDate = DateTime.Today.AddMonths((info.WeeklyRegenEveryXWeeks < 4) ? 10 : info.WeeklyRegenEveryXWeeks / 4 * 10);




                        if (info.EndDateType.Equals(EndDateType.SpecificDate) && intervalEndDate > info.EndDate)
                            intervalEndDate = info.EndDate.Value;

                        we = new WeeklyRecurrenceSettings(info.StartDate, intervalEndDate);
                        nextAppointmentDates = we.GetValues(info.WeeklyRegenEveryXWeeks, info.WeeklySelectedDays).Values;



                    }



                }
            }


            return nextAppointmentDates;
        }

        private List<DateTime> GetNextAppointmentDates(List<DateTime> appointmentsList, List<DateTime> lsOneTimeAppointments, int numOfAppointments)
        {
            //append oneTimeAppointments to a recurring appointment dates
            appointmentsList.AddRange(lsOneTimeAppointments);
            appointmentsList.Sort();

            //remove duplicates
            IEnumerable<DateTime> appointmentListWithoutDuplicates = appointmentsList.Distinct();

            List<DateTime> nextAppointmentDates = new List<DateTime>();

            int counter = 0;
            foreach (DateTime appointmentsDate in appointmentListWithoutDuplicates.ToList())
            {
                if (appointmentsDate >= DateTime.Today)
                {
                    nextAppointmentDates.Add(appointmentsDate);
                    counter++;
                }
                if (counter == numOfAppointments)
                    break;
            }

            return nextAppointmentDates;
        }

        private void SelectCalendarDates()
        {
            DataTable dtOneTimeAppointments = new PracticeOneTimeAppointmentBR().GetOneTimeAppointmentsByPracticeId(new Guid(ddlPractices.SelectedValue), DateTime.Today, 10);
            List<DateTime> lsOneTimeAppointments = new List<DateTime>();
            foreach (DataRow drOneTimeAppointments in dtOneTimeAppointments.Rows)
                lsOneTimeAppointments.Add((DateTime)drOneTimeAppointments["appointmentDate"]);

            List<DateTime> nextAppointmentDates = this.GetNextAppointmentDates(this.GetNextRecurringAppointmentDates(), lsOneTimeAppointments, 10);

            calApointments.SelectedDates.Clear();
            foreach (DateTime dateTime in nextAppointmentDates)
                calApointments.SelectedDates.Add(dateTime);
        }


        private void ShowSuccessAlert()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "AppointmentSaved", "alert('Appointment has been saved');", true);

            upAddAppointmentDates.Update();
        }

        #endregion //Private methods





    }
}
