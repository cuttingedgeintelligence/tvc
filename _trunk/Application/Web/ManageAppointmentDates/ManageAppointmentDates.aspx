<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ManageAppointmentDates.aspx.cs" Inherits="CEI.Web.ManageAppointmentDates.ManageAppointmentDates"
    Title="Manage Appointments" %>

<%@ Register Src="../UserControls/DayMonthYear.ascx" TagName="DayMonthYear" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style3
        {
            width: 91px;
            height: 90px;
        }
        .style4
        {
            height: 28px;
        }
        .style8
        {
            width: 100px;
            height: 28px;
        }
        .style9
        {
            width: 385px;
        }
        .style11
        {
            width: 449px;
            height: 25px;
        }
        .style12
        {
            width: 449px;
            height: 30px;
        }
        .style13
        {
            width: 853px;
        }
        .style15
        {
            width: 62px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">

        function ValidateSelectedPractice(source, arguments) {
            if (arguments.Value != '-1')
                arguments.IsValid = true;
            else
                arguments.IsValid = false;
        }
   
    </script>

    <asp:Label ID="lblHeaderText" runat="server" Text="Manage Practice Service Dates"
        CssClass="lblTitle" Style="float: none"></asp:Label>
    <asp:UpdatePanel ID="upAddAppointmentDates" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <fieldset class="fieldsetLeft" style="margin-bottom: 10px; overflow: hidden; width: 535px; height: 350px;">
                <legend>
                    <asp:Label ID="lblSelectedPractice" runat="server" Text="Add Appointments"></asp:Label>
                </legend>
                <asp:Panel ID="pnlAddAppointment" runat="server" Width="535px">
                    <table>
                        <tr>
                            <td style="padding-bottom: 10px; width: 100px;">
                                <asp:Label ID="lblSelectPractice" runat="server" Text="Select Practice:"></asp:Label>
                            </td>
                            <td style="padding-bottom: 10px;">
                                <asp:DropDownList ID="ddlPractices" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPractices_SelectedIndexChanged">
                                </asp:DropDownList><br />
                                <asp:CustomValidator ID="cvPractice" runat="server" ControlToValidate="ddlPractices"
                                    ClientValidationFunction="ValidateSelectedPractice" Display="Dynamic" ErrorMessage="Please select practice"
                                    ValidationGroup="vgAddAppointment" SetFocusOnError="True"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td style="font-weight: bold;" class="style13">
                                <asp:Label ID="lblRecurrencePattern" runat="server" Text="Service Frequency"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style13">
                                <table>
                                    <tr>
                                        <td style="margin: 0px 0 0 0px; padding: 0; border-right: #d0d0bf 1px solid; padding-left: 30px"
                                            valign="top" class="style3">
                                            <asp:RadioButtonList ID="rblRecurrencePattern" runat="server" AutoPostBack="True"
                                                Height="70px" OnSelectedIndexChanged="rblRecurrencePattern_SelectedIndexChanged">
                                                <asp:ListItem Selected="True">Weekly</asp:ListItem>
                                                <asp:ListItem>Monthly</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td style="padding-left: 20px;">
                                            <asp:Panel ID="pnlWeeklySettings" runat="server">
                                                <table>
                                                    <tr>
                                                        <td colspan="3" class="style4" style="padding-left: 5px;">
                                                            <asp:Label ID="lblRecurEveryWeeks" runat="server" Text="To Occur Every"></asp:Label>
                                                            <asp:TextBox ID="txtRecurEveryWeeks" runat="server" MaxLength="2" Width="25px" SkinID="txtApointmentSettings"></asp:TextBox>
                                                            <asp:Label ID="lblWeeksOn" runat="server" Text="Week(s) On:"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style8">
                                                            <asp:CheckBox ID="cbMonday" runat="server" Text="Monday" />
                                                        </td>
                                                        <td class="style8">
                                                            <asp:CheckBox ID="cbTuesday" runat="server" Text="Tuesday" />
                                                        </td>
                                                        <td class="style8">
                                                            <asp:CheckBox ID="cbWednesday" runat="server" Text="Wednesday" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style8">
                                                            <asp:CheckBox ID="cbThursday" runat="server" Text="Thursday" />
                                                        </td>
                                                        <td class="style8">
                                                            <asp:CheckBox ID="cbFriday" runat="server" Text="Friday" />
                                                        </td>
                                                        <td class="style8">
                                                            <asp:CheckBox ID="cbSaturday" runat="server" Text="Saturday" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlMonthlySettings" runat="server" Style="display: none;">
                                                <asp:Label ID="lblThe" runat="server" Text="The"></asp:Label>&nbsp;<asp:DropDownList
                                                    ID="ddlMonthlySpecificDatePartOne" runat="server">
                                                    <asp:ListItem Selected="True" Value="First">First</asp:ListItem>
                                                    <asp:ListItem Value="Second">Second</asp:ListItem>
                                                    <asp:ListItem Value="Third">Third</asp:ListItem>
                                                    <asp:ListItem Value="Fourth">Fourth</asp:ListItem>
                                                    <asp:ListItem Value="Last">Last</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddlMonthlySpecificDatePartTwo" runat="server">
                                                    <asp:ListItem Value="Day">Day</asp:ListItem>
                                                    <asp:ListItem Value="Weekday">Weekday</asp:ListItem>
                                                    <asp:ListItem Value="Monday">Monday</asp:ListItem>
                                                    <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                                                    <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                                                    <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                                                    <asp:ListItem Value="Friday">Triday</asp:ListItem>
                                                    <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                                                </asp:DropDownList>
                                                &nbsp;<asp:Label ID="lblOfEvery" runat="server" Text="Of Every"></asp:Label>
                                                &nbsp;<asp:TextBox ID="txtRecurEveryMonths" runat="server" MaxLength="2" Width="25px"
                                                    Style="display: inline;" SkinID="txtApointmentSettings"></asp:TextBox>
                                                &nbsp;<asp:Label ID="lblMonths" runat="server" Text="Month(s)"></asp:Label>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                                <table style="padding-top: 10px;" width="535" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table width="535" border="0" cellspacing="0" cellpadding="0">
                                                <%--<tr>
                                                    <td style="font-weight: bold;">
                                                        <asp:Label ID="lblRangeOfRecurrence" runat="server" Text="Range Of Recurrence"></asp:Label>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td>
                                                        <table width="535" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td valign="top" style="margin: 5px 0 0 0; display: block;" class="style9">
                                                                                <div>
                                                                                    <div style="float: left; margin: 0; padding: 0; height: 25px; width: 75px; line-height: 25px;
                                                                                        font-weight: bold">
                                                                                        <asp:Label ID="lblRangeStart" runat="server" Text="Start Date:"></asp:Label>
                                                                                    </div>
                                                                                    <div style="float: left; padding-top: 2px; width: 300px; height: 25px; line-height: 22px;">
                                                                                        <uc1:DayMonthYear ID="ucRangeStartDate" runat="server" />
                                                                                    </div>
                                                                                    <div style="clear: both; float: none;">
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 10px;">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="0" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td valign="top" class="style15" style="vertical-align: top; font-weight: bold; line-height: 25px;">
                                                                                <asp:Label ID="Label1" runat="server" Text="End Date:"></asp:Label>
                                                                            </td>
                                                                            <td valign="top" style="padding-left: 10px;">
                                                                                <table width="0" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td class="style11">
                                                                                            <asp:RadioButton ID="rbNoEndDate" runat="server" Checked="True" GroupName="rangeEndDate"
                                                                                                Text="No End Date" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="style12">
                                                                                            <asp:RadioButton ID="rbEndAfterOccurences" runat="server" GroupName="rangeEndDate"
                                                                                                Text="End After:" />
                                                                                            <asp:TextBox ID="txtOccurences" runat="server" MaxLength="3" Style="display: inline;"
                                                                                                Width="25px" SkinID="txtApointmentSettings"></asp:TextBox>
                                                                                            <asp:Label ID="lblOccurences" runat="server" Text="Service Dates"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="style11">
                                                                                            <div style="float: left; padding-right: 5px; margin-left: 0px;">
                                                                                                <asp:RadioButton ID="rbEndBy" runat="server" GroupName="rangeEndDate" Text="End By:" />
                                                                                            </div>
                                                                                            <div style="float: left; padding-top: 1px;">
                                                                                                <uc1:DayMonthYear ID="ucRangeEndDate" runat="server" />
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnSaveAppointment" runat="server" OnClick="btnSaveAppointment_Click"
                                                            OnClientClick="__defaultFired = false;" Text="Add" UseSubmitBehavior="False"
                                                            ValidationGroup="vgAddAppointment" />
                                                        <asp:Button ID="btnRemoveRecurrence" runat="server" CausesValidation="False" OnClick="btnRemoveRecurrence_Click"
                                                            Text="Delete Date" Style="display: none;" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red" Style="display: none;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </fieldset>
            <fieldset class="fieldsetLeft" style="margin-bottom: 10px; overflow: hidden; width: 333px;
                height: 350px; margin-left: 10px;">
                <legend>
                    <asp:Label ID="lblLegendCalendar" runat="server" Text="Preview Of Service Dates"></asp:Label>
                </legend>
                <div style="font-weight: bold; padding-left: 5px; padding-bottom: 20px;">
                    <asp:Label ID="Label2" runat="server" Text="Starting from today, future service dates can be viewed by selecting the right arrow."></asp:Label>
                </div>
                <div style="margin: 0 auto;padding-left: 37px;">
                    <asp:Calendar ID="calApointments" style="width:255px; height:215px;" runat="server" CellPadding="0" DayNameFormat="Shortest"
                        SkinID="theCalendarStyle" OnDayRender="calApointments_DayRender">
                        <TitleStyle BackColor="Transparent" HorizontalAlign="Center" 
                            VerticalAlign="Middle" />
                    </asp:Calendar>
                </div>
            </fieldset>
            <fieldset class="fieldsetLeft" style="margin-bottom: 10px; overflow: hidden; width: 900px">
                <legend>
                    <asp:Label ID="lblPreviewAppointments" runat="server" Text="One Time Service Date"></asp:Label>
                </legend>
                <table>
                    <tr>
                        <td style="padding-right: 5px;">
                            <asp:Label ID="lblAppointmentDate" runat="server" Text="
                Select Date:"></asp:Label>
                        </td>
                        <td>
                            <uc1:DayMonthYear ID="ucOneTimeAppointmentDate" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 0 5px 0;">
                            <asp:Button ID="btnAddOneTimeAppointment" runat="server" Text="Add" CausesValidation="false"
                                OnClick="btnAddOneTimeAppointment_Click" />
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="gvOneTimeAppointments" runat="server" AutoGenerateColumns="False"
                    OnPageIndexChanging="gvOneTimeAppointments_PageIndexChanging" OnRowCommand="gvOneTimeAppointments_RowCommand"
                    AllowPaging="True" SkinID="gridViewAdmin" Width="300px" DataKeyNames="id" 
                    PageSize="10" ondatabound="gvOneTimeAppointments_DataBound" 
                    onrowcreated="gvOneTimeAppointments_RowCreated">
                    <Columns>
                        <asp:BoundField DataField="appointmentDate" HeaderText="Appointment Date(s)" DataFormatString="{0:MM/dd/yyyy}" >
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:ButtonField ButtonType="Button" HeaderText="Delete" Text="Delete" CommandName="removeappointment">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ControlStyle CssClass="btnClass" />
                        </asp:ButtonField>
                    </Columns>
                         <PagerTemplate>
                    <div style="padding: 5px;">
                        <div>
                            <asp:Button CommandArgument="Prev" CommandName="Page" Text="Previous" ID="btnPrevious"
                                runat="server" meta:resourcekey="ibPrevResource1" />&nbsp;
                            <asp:Button CommandArgument="Next" CommandName="Page" Text="Next" ID="btnNext" runat="server"
                                meta:resourcekey="ibNextResource1" />
                        </div>
                        <span id="spannumeric" runat="server" class="spannumeric"></span>
                    </div>
                </PagerTemplate>
                </asp:GridView>
                <asp:ObjectDataSource ID="odsOneTimeAppointments" runat="server" EnablePaging="True"
                    MaximumRowsParameterName="pageSize" OnSelecting="odsOneTimeAppointments_Selecting"
                    SelectCountMethod="GetPracticeOneTimeAppointmentsSearchTotalCount" SelectMethod="GetPracticeOneTimeAppointmentsSearch"
                    TypeName="CEI.BR.PracticeOneTimeAppointmentBR">
                    <SelectParameters>
                        <asp:Parameter Name="practiceId" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
