﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;
using CEI.Framework.Encryption;
using System.Collections.Generic;

namespace CEI.Web.ManageTechnicians
{
    public partial class ManageTechnicians : BasePage
    {
        #region GridView columns


        private const int GV_USERS_FIRST_NAME = 0;
        private const int GV_USERS_LAST_NAME = 1;
        private const int GV_USERS_COUNTY_NAME = 2;
        private const int GV_USERS_EDIT = 3;
        private const int GV_USERS_DELETE = 4;
        private const int GV_USERS_VIEW_ACTIVITY = 5;

        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
               
                DataTable dtCounties = new NameValueBR().GetAllCounties();
                BindListControl<DataTable>.Bind(cblCounties, dtCounties, "name", "id");
                BindListControl<DataTable>.BindAndAddListItem(ddlCountiesSearch, dtCounties, "name", "id","All","-1");
                
                ViewState["firstName"] = txtFirstNameSearch.Text.Trim();
                ViewState["lastName"] = txtLastNameSearch.Text.Trim();
                ViewState["title"] = ddlTitleSearch.SelectedValue;
                ViewState["countyId"] = ddlCountiesSearch.SelectedValue;


                

                this.BindGridViewWithSearchResults();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            ViewState["firstName"] = txtFirstNameSearch.Text.Trim();
            ViewState["lastName"] = txtLastNameSearch.Text.Trim();
            ViewState["title"] = ddlTitleSearch.SelectedValue;
            ViewState["countyId"] = ddlCountiesSearch.SelectedValue;

            this.BindGridViewWithSearchResults();
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (ViewState["userId"] != null) //update technician
                {
                    if (!new UserBR().ExistsUserWithEmailOnUpdate((Guid)ViewState["userId"], txtEmail.Text.Trim()))
                    {

                        User technician = new User();
                        technician.UserId = (Guid)ViewState["userId"];
                        technician.FirstName = txtFirstName.Text;
                        technician.LastName = txtLastName.Text;
                        technician.Email = txtEmail.Text;
                        technician.Password = txtPassword.Text;
                        
                        technician.CountiesList = this.GetCountiesList(ActionTypeEnum.Update);
                        technician.AccountIsEnabled = cbAccountIsActive.Checked;
                        technician.Title = ddlTitle.SelectedValue;

                        if (new UserBR().UpdateTechnician(technician))
                        {
                            this.BindGridViewWithSearchResults();
                            this.ClearInsertUpdateForm();
                            
                        }
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, typeof(ManageTechnicians), "existsUserWithSameEmailScript", "alert('User with same user name(email) already exists');", true);

                }
                else //insert technician
                {
                    if (!new UserBR().ExistsUserWithEmail(txtEmail.Text.Trim()))
                    {

                        User technician = new User();
                        technician.UserId = Guid.NewGuid();

                        technician.FirstName = txtFirstName.Text;
                        technician.LastName = txtLastName.Text;
                        technician.Email = txtEmail.Text;
                        technician.Password = txtPassword.Text;
                        technician.CountiesList = this.GetCountiesList(ActionTypeEnum.Insert);
                        technician.CreatedBy = new Guid(User.Identity.Name);
                        technician.AccountIsEnabled = cbAccountIsActive.Checked;
                        technician.Title = ddlTitle.SelectedValue;

                        if ( new UserBR().InsertTechnician(technician))
                        {
                            this.BindGridViewWithSearchResults();
                            this.ClearInsertUpdateForm();
                            
                        }
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, typeof(ManageTechnicians), "existsUserWithSameEmailScript", "alert('User with same user name(email) already exists');", true);

                }


            }
        }


        protected void gvUsers_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvUsers.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvUsers_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvUsers.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvUsers.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvUsers.PageIndex == (gvUsers.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }


        protected void gvUsers_RowEditing(object sender, GridViewEditEventArgs e)
        {

            Guid userId = (Guid)gvUsers.DataKeys[e.NewEditIndex].Value;
            ViewState["userId"] = userId;
            btnSave.Text = "Save";
            DataRow drTechnician = new UserBR().GetTechnicianById(userId);

            lblSelectedUser.Text = "Update Technician: " + drTechnician["firstName"].ToString() + " " + drTechnician["lastName"].ToString();


            txtFirstName.Text = drTechnician["firstName"].ToString();
            txtLastName.Text = drTechnician["lastName"].ToString();
            txtEmail.Text = drTechnician["email"].ToString();
            string password = new EncryptDecrypt().Decrypt(drTechnician["password"].ToString(), string.Empty);
            txtPassword.Attributes.Add("value", password);
            txtConfirmPassword.Attributes.Add("value", password);
            cbAccountIsActive.Checked = (bool)drTechnician["accountIsEnabled"];
            ddlTitle.SelectedValue = drTechnician["title"].ToString();
            ViewState["countiesIds"] = drTechnician["countiesIds"].ToString();
            this.SelectCounties(drTechnician["countiesIds"].ToString());
            upInsertUpdate.Update();
            
        }


        protected void gvUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            //Check if technicianId is assigned to some worksheet document
            if (new WorksheetBR().ExistsWorksheetWithTechnicianId((Guid)gvUsers.DataKeys[e.RowIndex].Value))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManageTechnicians), "existsWorksheetWithTechnicianId", "alert('You cannot delete this technician because is already assigned to a worksheet document');", true);
                return;
            }


            if (new UserBR().DeleteTechnician((Guid)gvUsers.DataKeys[e.RowIndex].Value))
            {
                if (ViewState["userId"] != null && (Guid)gvUsers.DataKeys[e.RowIndex].Value == (Guid)ViewState["userId"])
                {
                    this.ClearInsertUpdateForm();
                    upInsertUpdate.Update();
                }


                this.BindGridViewWithSearchResults();
            }

        }


        protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            
            gvUsers.PageIndex = e.NewPageIndex;
            gvUsers.DataSource = odsUsers;
            gvUsers.DataBind();

        }

        protected void odsUsers_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["roleId"] = (int)RoleEnum.Technician;
            e.InputParameters["firstName"] = ViewState["firstName"].ToString();
            e.InputParameters["lastName"] = ViewState["lastName"].ToString();
            e.InputParameters["title"] = ViewState["title"].ToString();
            e.InputParameters["countyId"] = ViewState["countyId"].ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.ClearInsertUpdateForm();
        }

        protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button btnViewAvtivity = e.Row.Cells[GV_USERS_VIEW_ACTIVITY].FindControl("btnActivity") as Button;
                btnViewAvtivity.PostBackUrl = "~/UserActions/UserActions.aspx?Tech=" + gvUsers.DataKeys[e.Row.RowIndex].Value.ToString();
            }
        }

        #endregion //Event handlers



        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvUsers.PageIndex = 0;
            gvUsers.DataSource = odsUsers;
            gvUsers.DataBind();
        }

        private void ClearInsertUpdateForm()
        {
            ViewState["userId"] = null;
            lblSelectedUser.Text = "Create New Technician Account";
            btnSave.Text = "Create";
            //clear form
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtPassword.Attributes.Remove("value");
            txtConfirmPassword.Text = string.Empty;
            txtConfirmPassword.Attributes.Remove("value");
            ddlTitle.SelectedIndex = -1;
            
            cblCounties.ClearSelection();
            cbAccountIsActive.Checked = true;
        }


        private void SelectCounties(string countiesIds)
        {
            //clear selection
            cblCounties.ClearSelection();

            //countiesIds comma separated
            char[] separator = { ','};
            string[] countiesIdsList = countiesIds.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            foreach (string countyId in countiesIdsList)
            {
                ListItem cblItem = cblCounties.Items.FindByValue(countyId);
                if (cblItem != null)
                    cblItem.Selected = true;
            }
        }

        private List<NameValue> GetCountiesList(ActionTypeEnum actionType)
        {
            List<NameValue> countiesList = new List<NameValue>();
            if (actionType.Equals(ActionTypeEnum.Insert))
            {
                foreach (ListItem liCounty in cblCounties.Items)
                {
                    if (liCounty.Selected)
                        countiesList.Add(new NameValue(liCounty.Text, int.Parse(liCounty.Value)));
                }
            }
            else //actionType.Equals(ActionTypeEnum.Update)
            {
                
                if (ViewState["countiesIds"].ToString().Equals(string.Empty))
                {
                     
                    foreach (ListItem liCounty in cblCounties.Items)
                    {
                        if (liCounty.Selected)
                            countiesList.Add(new NameValue(liCounty.Text, int.Parse(liCounty.Value)));
                    }
                }
                else
                {
                    List<string> oldCountiesIds = ViewState["countiesIds"].ToString().Split(',').ToList<string>();
                    foreach (ListItem liCounty in cblCounties.Items)
                    {
                        if (liCounty.Selected)
                        {
                            if(!oldCountiesIds.Exists(delegate(string countyId) { return countyId == liCounty.Value; }))
                                countiesList.Add(new NameValue(liCounty.Text, int.Parse(liCounty.Value)));
                        }
                        else // !liCounty.Selected
                        {
                            if (oldCountiesIds.Exists(delegate(string countyId) { return countyId == liCounty.Value; }))
                            {
                                NameValue countyForDelete = new NameValue(liCounty.Text, int.Parse(liCounty.Value));
                                countyForDelete.ActionType = ActionTypeEnum.Delete;
                                countiesList.Add(countyForDelete);
                            }
                        }
                    }
                }
            }
            return countiesList;
             
        }

        #endregion //Private methods

        

        
       
    }
}
