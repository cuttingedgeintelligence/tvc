﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;

namespace CEI.Web.FollowUps
{
    public partial class ViewFollowUps : BasePage 
    {
        #region GridView columns


        private const int GV_FOLLOWUPS_PRACTICE_NAME = 0;
        private const int GV_FOLLOWUPS_DATE = 1;
        private const int GV_FOLLOWUPS_VIEW = 2;
        



        #endregion //GridView columns


        #region Event handlers


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (Roles.IsUserInRole(RoleEnum.Practice.ToString()))
                {
                    ViewState["practiceId"] = new Guid(User.Identity.Name);
                    searchPanel.Visible = false;
                }
                else
                {
                    BindListControl<DataTable>.BindAndAddListItem(ddlPracticesSearch, new UserBR().GetAllPracticesNameAndId(), "name", "id", "All", Guid.Empty.ToString());
                    ViewState["practiceId"] = ddlPracticesSearch.SelectedValue;
                }

               


                this.BindGridViewWithSearchResults();
            }
        }

        protected void gvFollowUps_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("view"))
            {
                Response.Redirect("~/FollowUps/ViewFollowUpPatients.aspx?logId=" + gvFollowUps.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["Id"].ToString() + "&prName=" + gvFollowUps.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["PracticeName"].ToString() + "&servDate=" + gvFollowUps.Rows[int.Parse(e.CommandArgument.ToString())].Cells[GV_FOLLOWUPS_DATE].Text, true);
            }
        }


        protected void gvFollowUps_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvFollowUps.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvFollowUps_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvFollowUps.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvFollowUps.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvFollowUps.PageIndex == (gvFollowUps.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }


        protected void gvFollowUps_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvFollowUps.PageIndex = e.NewPageIndex;
            gvFollowUps.DataSource = odsFollowUps;
            gvFollowUps.DataBind();

        }

        protected void odsFollowUps_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
          
            e.InputParameters["practiceId"] = ViewState["practiceId"];
          
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {


            ViewState["practiceId"] = ddlPracticesSearch.SelectedValue;

            this.BindGridViewWithSearchResults();
        }


        #endregion //Event handlers

        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvFollowUps.PageIndex = 0;
            gvFollowUps.DataSource = odsFollowUps;
            gvFollowUps.DataBind();
        }

        #endregion //Private methods
    }
}
