﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;
using System.Collections.Generic;
using Microsoft.Reporting.WebForms;


namespace CEI.Web.FollowUps
{
    public partial class ViewFollowUpPatients : BasePage 
    {
        #region GridView columns


        private const int GV_PATIENTS_NAME = 0;
        private const int GV_PATIENTS_SERVICE_DATE = 1;
        private const int GV_PATIENTS_GLAUCOMA = 2;
        private const int GV_PATIENTS_RETINA = 3;
        private const int GV_PATIENTS_STATUS = 4;
       

        #endregion //GridView columns



        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {



            if (!Page.IsPostBack)
            {
                if (this.ValidateRequestParams())
                {

                   

                    lblPracticeNameData.Text = Request.QueryString["prName"];
                    lblServiceDateData.Text = Request.QueryString["servDate"];
                    DataTable dtPatients = new FinalNotesWorksheetBR().GetFollowUpPatientsByFollowUpId(int.Parse(Request.QueryString["logId"]));
                    ViewState["PatientsList"] = dtPatients;
                    this.BingGridViewPatients(dtPatients);
                }
                else
                {
                    //TODO: show error message
                }
            }
        }




        protected void gvPatients_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drPatient = e.Row.DataItem as DataRowView;
                if ((bool)drPatient["glaucoma"])
                {
                    // e.Row.Cells[GV_PATIENTS_GLAUCOMA].Text = "G";

                    decimal hrt_glaucoma = (decimal)drPatient["HRT_Glaucoma"];
                    if (hrt_glaucoma == 1 || hrt_glaucoma == 0)
                        e.Row.Cells[GV_PATIENTS_GLAUCOMA].Text = hrt_glaucoma.ToString("#");
                    else
                        e.Row.Cells[GV_PATIENTS_GLAUCOMA].Text = hrt_glaucoma.ToString();
                }
                if ((bool)drPatient["retina"])
                {
                    // e.Row.Cells[GV_PATIENTS_RETINA].Text = "R";
                    decimal hrt_retina = (decimal)drPatient["HRT_Retina"];
                    if (hrt_retina == 1 || hrt_retina == 0)
                        e.Row.Cells[GV_PATIENTS_GLAUCOMA].Text = hrt_retina.ToString("#");
                    else
                        e.Row.Cells[GV_PATIENTS_RETINA].Text = hrt_retina.ToString();
                }


                e.Row.Cells[GV_PATIENTS_STATUS].Text = ((PatientLogPatientStatusEnum)Enum.Parse(typeof(PatientLogPatientStatusEnum), drPatient["status"].ToString())).ToString().Replace("__"," ");
                
            }
        }

        protected void btnPrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Pdf);
        }

        protected void btnPrintExcel_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Excel);
        }

        protected void btnPrintPreview_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Printer);
        }

        #endregion //Event handlers

        #region Private methods
        private bool ValidateRequestParams()
        {
            bool isValid = false;
            int patientLogId;
            if (

                    Request.QueryString["prName"] != null &&
                    Request.QueryString["servDate"] != null &&
                    Request.QueryString["logId"] != null &&
                    int.TryParse(Request.QueryString["logId"], out patientLogId) &&
                    this.ValidateServiceDate(Request.QueryString["servDate"])
                )
            {
                isValid = true;
                ViewState["patientLogId"] = patientLogId;
            }




            return isValid;


        }





        private bool ValidateServiceDate(string serviceDate)
        {
            try
            {
                DateTime.ParseExact(serviceDate, "M/d/yyyy", null);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }

        }


        private void BingGridViewPatients(DataTable dtPatients)
        {
            gvPatients.DataSource = dtPatients;
            gvPatients.DataBind();
        }






        #endregion //Private methods

        #region MICROSOFT REPORTING

        private void PrintReport(ExportType exportType)
        {

            TotalVisionReport _totalVisionReport = new TotalVisionReport();

            DataTable patientsList = ViewState["PatientsList"] as DataTable;

            DataView dvExaminedPatients = new DataView(patientsList);
           // dvExaminedPatients.RowFilter = "status <> 1";
            dvExaminedPatients.RowFilter = "status = 2";

            DataView dvNoShowPatients = new DataView(patientsList);
           // dvNoShowPatients.RowFilter = "status = 1";
            dvNoShowPatients.RowFilter = "status <> 2";
           

            LocalReport localReport = new LocalReport();
            localReport.ReportPath = Server.MapPath("~/Reports/rptFollowUpPatients.rdlc");
            //if ((exportType == ExportType.Pdf) || (exportType == ExportType.Printer))
            //{
                ReportParameter[] param = new ReportParameter[2];
                param[0] = new ReportParameter("rptParamePracticeNameFollowUp", lblPracticeNameData.Text);
                param[1] = new ReportParameter("rptParamDateFollowUp", lblServiceDateData.Text);
                //param[1] = new ReportParameter("rptParamDate", lblServiceDateData.Text);

                localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            //}

            ReportDataSource reportDataSource = new ReportDataSource("DataSet1_stp_Patients_FollowUpPatients_GetFollowUpPatientsByFollowUpId", dvExaminedPatients.ToTable());
            ReportDataSource reportDataSourceDv = new ReportDataSource("DataSet1_stp_Patients_FollowUpPatients_GetFollowUpPatientsByFollowUpId1", dvNoShowPatients.ToTable());
            localReport.DataSources.Add(reportDataSource);
            localReport.DataSources.Add(reportDataSourceDv);

            //ReportParameter[] param = new ReportParameter[2];
            //param[0] = new ReportParameter("rptParamPracticeName", lblPracticeNameData.Text);
            //param[1] = new ReportParameter("rptParamDate", lblServiceDateData.Text);

            //localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            if (exportType == ExportType.Printer)
            {
                _totalVisionReport.ListItems = patientsList;
                _totalVisionReport.Pameters.Add("rptParamPracticeName", lblPracticeNameData.Text);
                _totalVisionReport.Pameters.Add("rptParamDate", lblServiceDateData.Text);
                Session["PrintingParams"] = _totalVisionReport;

                Response.Redirect("~/Reports/ReportPreview.aspx?ReportName=PatientLog");
            }
            else if (exportType == ExportType.Pdf)
            {
                DownloadPDF(localReport);

            }
            else if (exportType == ExportType.Excel)
            {
                DownloadExcel(localReport);

            }
        }

        private void DownloadPDF(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>PDF</OutputFormat>" +
            "  <PageWidth>8in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.1in</MarginTop>" +
            "  <MarginLeft>0.6in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.1in</MarginBottom>" +
            "</DeviceInfo>";

   //         string deviceInfo =
   //"<DeviceInfo>" +
   //"  <OutputFormat>PDF</OutputFormat>" +
   //"  <PageWidth>11in</PageWidth>" +
   //"  <PageHeight>8.5in</PageHeight>" +
   //"  <MarginTop>0.2in</MarginTop>" +
   //"  <MarginLeft>0.6in</MarginLeft>" +
   //"  <MarginRight>0.1in</MarginRight>" +
   //"  <MarginBottom>0.2in</MarginBottom>" +
   //"</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=FollowUp." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DownloadExcel(LocalReport localReport)
        {
            string reportType = "Excel";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
             "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=MontlyReport." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }


        #endregion
    }
}
