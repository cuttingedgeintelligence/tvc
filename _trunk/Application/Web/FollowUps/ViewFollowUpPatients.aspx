﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true" CodeBehind="ViewFollowUpPatients.aspx.cs" Inherits="CEI.Web.FollowUps.ViewFollowUpPatients" Title="Follow Up Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:Label ID="lblHeaderText" runat="server" Text="Follow Up Report"
        CssClass="lblTitle" Style="float: none"></asp:Label>
    <table width="400px">
        <tr>
            <td>
                <asp:Label ID="lblPracticeName" runat="server" Text="Practice Name:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblPracticeNameData" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblServiceDate" runat="server" Text="Service Date:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblServiceDateData" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="padding:10px 0;">
        <tr>
            <td style ="padding-top:10px;">
                <asp:ImageButton ID="btnPrintPDF" runat="server" 
                    ImageUrl="~/Images/Mockups/pdfIcon.png" ToolTip="Download PDF" 
                    onclick="btnPrintPDF_Click"  style="padding-right:3px;" />
            </td>
            <td style ="padding-top:10px; padding-left:5px">
                <asp:ImageButton ID="btnPrintExcel" runat="server" 
                    ImageUrl="~/Images/Mockups/exelIcon.png"  ToolTip="Download Excel" 
                    onclick="btnPrintExcel_Click"/>
            </td>
            <td style ="padding-top:10px; padding-left:5px">
                <asp:ImageButton ID="btnPrintPreview" runat="server" 
                    ImageUrl="~/Images/Mockups/printIcon.png" ToolTip="View and Print" 
                    onclick="btnPrintPreview_Click"  style="display:none;" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upPatients" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvPatients" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                DataKeyNames="Id,PatientId"  SkinID="gridViewAdmin" Width="920px"
                Style="clear: both;" OnRowDataBound="gvPatients_RowDataBound" EmptyDataText="There are no patients in the follow up"
               >
                <Columns>
                   
                    <asp:HyperLinkField DataNavigateUrlFields="PatientId" 
                                    DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}" 
                                    DataTextField="patientName" HeaderText="Patient Name" >
                                    <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                                    <ItemStyle HorizontalAlign="Left"  />
                                </asp:HyperLinkField>
                    <asp:BoundField DataField="patientLogServiceDate" HeaderText="Service Date" 
                        DataFormatString="{0:MM/dd/yyyy}" >
                   
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Glaucoma">
                        <ItemTemplate>
                          
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center"  CssClass="tdActions"/>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Retina">
                        <ItemTemplate>
                          
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                  
                </Columns>
            </asp:GridView>
            <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="false"
                PostBackUrl="~/FollowUps/ViewFollowUps.aspx" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
