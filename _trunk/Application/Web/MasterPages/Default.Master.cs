﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Services;
using System.Web.Script.Services;

namespace CEI.Web.MasterPages
{
    public partial class Default : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string JqueryString
        {
            get
            {
                return "http://" + Request.Url.Host + ResolveUrl("~/js/jquery-1.3.2.min.js");
            }
        }


        [System.Web.Services.WebMethod(true)]
        [System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
        public static void AbandonSession()
        {
            HttpContext.Current.Session.Abandon();
        }
    }
}
