﻿<%@ Page Title="View Practice Profile" Language="C#" MasterPageFile="~/MasterPages/Default.Master"
    AutoEventWireup="true" CodeBehind="ViewPracticeProfile.aspx.cs" Inherits="CEI.Web.ViewPracticeProfile.ViewPracticeProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input id="btnBack" type="button" class="btnClass" value="Back" onclick="history.go(-1)" />
    <asp:Label ID="lblHeaderText" runat="server" Text="View Practice Profile" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <asp:Panel ID="pnlViewPracticeProfile" runat="server" Style="display: none;">
        <fieldset class="fieldsetLeft" style="width: 900px">
            <legend>
                <asp:Label ID="lblSelectedPractice" runat="server"></asp:Label>
            </legend>
            <table width="900" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="266" valign="top" style="vertical-align: top;">
                        <table width="266" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblPractiveUsername" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Username: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPractiveUsernameData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPracticeName" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Practice: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPracticeNameData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblReferredBy" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Referred By: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblReferredByData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPhone" CssClass="viewPatientProfileTxtBold" runat="server" Text="Phone: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPhoneData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFax" CssClass="viewPatientProfileTxtBold" runat="server" Text="Fax: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblFaxData" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="266" valign="top" style="vertical-align: top;">
                        <table width="266" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblPracticePassword" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Password: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPracticePasswordData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAddress1" CssClass="viewPatientProfileTxtBold" runat="server" Text="Address 1: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAddress1Data" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAddress2" CssClass="viewPatientProfileTxtBold" runat="server" Text="Address 2: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAddress2Data" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblCity" CssClass="viewPatientProfileTxtBold" runat="server" Text="City: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCityData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblState" CssClass="viewPatientProfileTxtBold" runat="server" Text="State: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblStateData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblZipCode" CssClass="viewPatientProfileTxtBold" runat="server" Text="Zip Code: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblZipCodeData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblCounty" CssClass="viewPatientProfileTxtBold" runat="server" Text="County: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCountyData" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="266" valign="top" style="vertical-align: top;">
                        <table width="266" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblActiveAccount" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Active Account: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblActiveAccountData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblNeedSlitLampAndTable" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Need Slit Lamp & Table: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblNeedSlitLampAndTableData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPreferredDaysOfTheWeek" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Preferred Day(s): "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPreferredDaysOfTheWeekData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAssignedTech" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Assigned Tech: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAssignedTechData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblServiceFrequency" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Service Frequency: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblServiceFrequencyData" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-top: 10px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblDoctors" CssClass="viewPatientProfileTxtBold" runat="server" Text="Doctors: "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDoctors" runat="server" AutoGenerateColumns="False" SkinID="gridViewAdmin"
                                        Width="100%" Style="clear: both;">
                                        <Columns>
                                            <asp:BoundField DataField="firstName" HeaderText="First Name" />
                                            <asp:BoundField DataField="lastName" HeaderText="Last Name" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
</asp:Content>
