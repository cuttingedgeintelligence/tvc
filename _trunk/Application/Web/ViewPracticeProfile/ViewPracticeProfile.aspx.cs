﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CEI.BR;
using System.Web.Security;
using CEI.Common;
using CEI.Framework.Encryption;

namespace CEI.Web.ViewPracticeProfile
{
    public partial class ViewPracticeProfile : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Guid practiceId;
                if (Request.QueryString["id"] != null && this.IsGuid(Request.QueryString["id"], out practiceId))
                {
                    DataSet dsPractice = new UserBR().GetPracticeByIdForProfilePage(practiceId);
                    if (dsPractice != null)
                    {
                        DataRow drPractice = dsPractice.Tables["tblPractice"].Rows[0];
                        DataTable dtDoctors = dsPractice.Tables["tblDoctors"];
                        if (
                            !Roles.IsUserInRole(RoleEnum.Practice.ToString())
                            ||
                            (Roles.IsUserInRole(RoleEnum.Practice.ToString()) && drPractice["id"].ToString().Equals(User.Identity.Name))
                            )
                        {
                            pnlViewPracticeProfile.Style.Add("display", "");
                            this.FillForm(drPractice, dtDoctors);
                        }
                    }
                }
            }
        }


        private void FillForm(DataRow drPractice, DataTable dtDoctors)
        {
            lblSelectedPractice.Text  = drPractice["name"].ToString();
            lblPracticeNameData.Text = drPractice["name"].ToString();
            lblReferredByData.Text = drPractice["referredBy"].ToString();
            lblFaxData.Text = drPractice["fax"].ToString();
            lblPhoneData.Text = drPractice["phone"].ToString();
            lblAddress1Data.Text = drPractice["address1"].ToString();

            lblAddress2Data.Text = drPractice["address2"].ToString();
            lblCityData.Text = drPractice["city"].ToString();
            lblZipCodeData.Text = drPractice["zipCode"].ToString();
            lblCountyData.Text = drPractice["countyName"].ToString();
            lblStateData.Text = drPractice["stateName"].ToString();

            if(drPractice["accountIsEnabled"].ToString() == "True")
                lblActiveAccountData.Text = "Yes";
            else
                lblActiveAccountData.Text = "No";

            if (drPractice["slitlampAndTableAreRequired"].ToString() == "True")
                lblNeedSlitLampAndTableData.Text = "Yes";
            else 
                lblNeedSlitLampAndTableData.Text = "No";
            lblPractiveUsernameData.Text = drPractice["email"].ToString();
            lblPracticePasswordData.Text = new EncryptDecrypt().Decrypt(drPractice["password"].ToString(), string.Empty);

            //admin.Password = new EncryptDecrypt().Encrypt(admin.Password, string.Empty);


            //lblPreferredDaysOfTheWeekData.Text = drPractice["preferredDay1"].ToString();// +" " + drPractice["preferredDay2"].ToString() + " " + drPractice["preferredDay3"].ToString();
           //lblPreferredDaysOfTheWeekData.Text = drPractice["preferredDay1"].ToString() +", " + drPractice["preferredDay2"].ToString(); //+ " " + drPractice["preferredDay3"].ToString();
            lblPreferredDaysOfTheWeekData.Text = drPractice["preferredDay1"].ToString() + "<br> " + drPractice["preferredDay2"].ToString(); //+ " " + drPractice["preferredDay3"].ToString();
           
            
            FrequencyEnum frequency = drPractice["frequency"] != DBNull.Value ? (FrequencyEnum)Enum.Parse(typeof(FrequencyEnum), drPractice["frequency"].ToString()) : FrequencyEnum.Quarterly;



            lblAssignedTechData.Text = drPractice["assignedTech"].ToString();

            lblServiceFrequencyData.Text = frequency.Equals(FrequencyEnum.BiMonthly) ? "Bi-Monthly" : frequency.ToString();

            gvDoctors.DataSource = dtDoctors;
            gvDoctors.DataBind();
        }

        private bool IsGuid(string s,out Guid g)
        {
            g = Guid.Empty;
            try
            {
                g = new Guid(s);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }
        }
    }
}
