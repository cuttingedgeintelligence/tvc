﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Collections.Generic;
using CEI.Common;
using CEI.BR;

namespace CEI.Web.ManageExams
{
    public partial class ManageExams : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.RetinaExamsRecurrence)["setupValue"] != null)
                    ddlRetinaExams.SelectedValue = SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.RetinaExamsRecurrence)["setupValue"].ToString();
                if (SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.GlaucomaExamsRecurrence)["setupValue"] != null)
                    ddlGlaucomaExams.SelectedValue = SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.GlaucomaExamsRecurrence)["setupValue"].ToString();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<SystemSetup> systemSetupValues = new List<SystemSetup>();
            systemSetupValues.Add(new SystemSetup((int)SystemSetupEnum.RetinaExamsRecurrence, ddlRetinaExams.SelectedValue.ToString()));
            systemSetupValues.Add(new SystemSetup((int)SystemSetupEnum.GlaucomaExamsRecurrence, ddlGlaucomaExams.SelectedValue.ToString()));
            if (new SystemSetupBR().UpdateSystemSetupValues(systemSetupValues))
            {
                lblMessage.Text = "Settings saved"; 
            }
        }
    }
}
