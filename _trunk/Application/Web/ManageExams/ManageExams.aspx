﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ManageExams.aspx.cs" Inherits="CEI.Web.ManageExams.ManageExams" Title="Manage Exams" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Manage Exams" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <asp:UpdatePanel runat = "server">
        <ContentTemplate>
            <fieldset class="fieldsetLeft">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblRetinaExams" runat="server" Text="Retina exams should be performed every:"></asp:Label>
                        </td>
                        <td style ="padding-left:5px;padding-right:5px">
                            <asp:DropDownList ID="ddlRetinaExams" runat="server">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="months."></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:6px">
                        </td>
                        <td style="height:6px">
                           
                        </td>
                        <td style="height:6px">
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblGlaucomaExams" runat="server" Text="Glaucoma exams should be performed every:"></asp:Label>
                        </td>
                        <td style ="padding-left:5px;padding-right:5px">
                            <asp:DropDownList ID="ddlGlaucomaExams" runat="server">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="months."></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style ="padding-top:10px">
                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" />
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
