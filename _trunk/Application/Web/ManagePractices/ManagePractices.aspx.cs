﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

using CEI.Common;
using CEI.BR;
using CEI.Framework;
using CEI.Framework.Encryption;

namespace CEI.Web.ManagePractices
{
    public partial class ManagePractices : BasePage
    {

        #region GridView columns


        private const int GV_USERS_NAME = 0;
        private const int GV_USERS_ZIP_CODE = 1;
        private const int GV_USERS_CITY = 2;
        private const int GV_USERS_COUNTY_NAME = 3;
        private const int GV_USERS_EDIT = 4;
        private const int GV_USERS_DELETE = 5;

        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindListControl<DataTable>.Bind(ddlStates, new NameValueBR().GetAllStates(), "name", "id");

                DataTable dtCounties = new NameValueBR().GetAllCounties();
             
                BindListControl<DataTable>.Bind(ddlCounties, dtCounties, "name", "id");
                

                BindListControl<DataTable>.BindAndAddListItem(ddlCountiesSearch, dtCounties, "name", "id", "All", "-1");
               

                ViewState["name"] = txtNameSearch.Text.Trim();
                ViewState["countyId"] = ddlCountiesSearch.SelectedValue;
                ViewState["address"] = txtAddressSearch.Text.Trim();
                ViewState["zipCode"] = txtZipCodeSearch.Text.Trim();
                ViewState["city"] = txtCitySearch.Text.Trim();
               

                this.BindGridViewWithSearchResults();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            ViewState["name"] = txtNameSearch.Text.Trim();


            ViewState["countyId"] = ddlCountiesSearch.SelectedValue;

            ViewState["address"] = txtAddressSearch.Text.Trim();
            ViewState["zipCode"] = txtZipCodeSearch.Text.Trim();
            ViewState["city"] = txtCitySearch.Text.Trim();
          

            this.BindGridViewWithSearchResults();
            if (gvUsers.Rows.Count > 0)
                lblError.Text = "&nbsp;";
            else
                lblError.Text = "Sorry! Your requested records are not found in the database.<br />Please try searching with different keyword. Thank you.";

           // upInsertUpdate.Update();
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (ViewState["userId"] != null) //update practice
                {
                    if (!new UserBR().ExistsUserWithEmailOnUpdate((Guid)ViewState["userId"], txtEmail.Text.Trim()))
                    {

                        User practice = new User();
                        practice.UserId = (Guid)ViewState["userId"];

                        practice.FirstName = txtName.Text;
                        practice.Email = txtEmail.Text;
                        practice.Password = txtPassword.Text;
                        practice.Fax = txtFax.Text;
                        practice.Phone = txtPhone.Text;
                        practice.Address1 = txtAddress1.Text;
                        practice.Address2 = txtAddress2.Text;
                        practice.City = txtCity.Text;
                        practice.ZipCode = txtZipCode.Text;
                        practice.StateId = int.Parse(ddlStates.SelectedValue);
                       
                        practice.CountyId = int.Parse(ddlCounties.SelectedValue);
                        practice.AccountIsEnabled = cbAccountIsActive.Checked;
                        practice.ReferredBy = txtReferredBy.Text;


                        practice.SlitlampAndTableAreRequired = bool.Parse(rblSlitlampTableNeeded.SelectedValue);


                        practice.PreferredDay1 = txtPreferredDay1.Text;
                        practice.AssignedTech = txtAssignedTech.Text; 
                        practice.PreferredDay2 = txtPreferredDay2.Text;
                        //practice.PreferredDay3 = txtPreferredDay3.Text;

                        //practice.Frequency = (FrequencyEnum)rblFrequency.SelectedIndex; 
                        practice.Frequency = (FrequencyEnum)Enum.Parse(typeof(FrequencyEnum), rblFrequency.SelectedValue);

                        if (new UserBR().UpdatePractice(practice))
                        {
                            this.BindGridViewWithSearchResults();
                            this.ClearInsertUpdateForm();
                            this.ClearDoctorSection();   
                            this.BindDoctorsGridView();
                            //lblInfo.Visible = true;
                            upDoctors.Update(); 
                        }
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePractices), "existsUserWithSameEmailScript", "alert('User with same user name(email) already exists');", true);

                }
                else //insert practice
                {
                    if (!new UserBR().ExistsUserWithEmail(txtEmail.Text.Trim()))
                    {

                        User practice = new User();
                        practice.UserId = Guid.NewGuid();

                        practice.FirstName = txtName.Text;
                        practice.Email = txtEmail.Text;
                        practice.Password = txtPassword.Text;
                        practice.Fax = txtFax.Text;
                        practice.Phone = txtPhone.Text;
                        practice.Address1 = txtAddress1.Text;
                        practice.Address2 = txtAddress2.Text;
                        practice.City = txtCity.Text;
                        practice.ZipCode = txtZipCode.Text;
                        practice.StateId = int.Parse(ddlStates.SelectedValue);
                     
                        practice.CountyId = int.Parse(ddlCounties.SelectedValue);
                        practice.CreatedBy = new Guid(User.Identity.Name);
                        practice.AccountIsEnabled = cbAccountIsActive.Checked;
                        practice.ReferredBy = txtReferredBy.Text;


                        practice.SlitlampAndTableAreRequired = bool.Parse(rblSlitlampTableNeeded.SelectedValue);


                        practice.PreferredDay1 = txtPreferredDay1.Text;
                        practice.AssignedTech = txtAssignedTech.Text; 
                        practice.PreferredDay2 = txtPreferredDay2.Text;
                        //practice.PreferredDay3 = txtPreferredDay3.Text;

                        //practice.Frequency = (FrequencyEnum)rblFrequency.SelectedIndex; 
                        practice.Frequency = (FrequencyEnum)Enum.Parse(typeof(FrequencyEnum), rblFrequency.SelectedValue);

                        if (new UserBR().InsertPractice(practice))
                        {
                            this.BindGridViewWithSearchResults();
                            ViewState["userId"] = practice.UserId;
                            btnSave.Text = "Save";
                            lblSelectedUser.Text = "Update Practice: " + practice.FirstName;
                            upInsertUpdate.Update();
                            doctorSection.Visible = true;
                            upDoctors.Update(); 
                        }
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePractices), "existsUserWithSameEmailScript", "alert('User with same user name(email) already exists');", true);

                }



            }
        }

        protected void btnSaveDoctor_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //if (ViewState["userId"] == null) //insert first doctor for practice
                //{
                //    rfvName.Validate();
                //    rfvEmail.Validate();
                //    rfvPassword.Validate();

                //    if (!(rfvName.IsValid && rfvEmail.IsValid && rfvPassword.IsValid))
                //        upInsertUpdate.Update();
                //    else
                //        InsertPracticeWithDoctor();                 
                //}
                //else
                //{
                    if (ViewState["doctorId"] != null) //update doctor
                    {
                        Doctor doctor = new Doctor();
                        doctor.Id = (int)ViewState["doctorId"];
                        doctor.FirstName = txtFirstName.Text;
                        doctor.LastName = txtLastName.Text;
                        doctor.PracticeId = (Guid)ViewState["userId"];
                        if (new DoctorBR().UpdateDoctor(doctor))
                        {
                            this.BindDoctorsGridView();  
                            this.ClearDoctorSection();
                        }
                    }
                    else //insert doctor
                    {
                        Doctor doctor = new Doctor();
                        doctor.FirstName = txtFirstName.Text;
                        doctor.LastName = txtLastName.Text;
                        doctor.PracticeId = (Guid)ViewState["userId"];
                        if (new DoctorBR().InsertDoctor(doctor))
                        {
                            this.BindDoctorsGridView();
                            this.ClearDoctorSection();
                        }

                    }
                //}
            }
        }


        protected void gvDoctors_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvDoctors.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvDoctors_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvDoctors.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvDoctors.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvDoctors.PageIndex == (gvDoctors.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }

        protected void gvUsers_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvUsers.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvUsers_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvUsers.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvUsers.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvUsers.PageIndex == (gvUsers.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }


        protected void gvUsers_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Guid userId = (Guid)gvUsers.DataKeys[e.NewEditIndex].Value;
            ViewState["userId"] = userId;
            btnSave.Text = "Save";
            DataRow drPractice = new UserBR().GetPracticeById(userId);

            lblSelectedUser.Text = "Update Practice: " + drPractice["name"].ToString();


            txtName.Text = drPractice["name"].ToString();

            txtEmail.Text = drPractice["email"].ToString();
            string password = new EncryptDecrypt().Decrypt(drPractice["password"].ToString(), string.Empty);
            txtPassword.Text = password;

            txtFax.Text = drPractice["fax"].ToString();
            txtPhone.Text = drPractice["phone"].ToString();
            txtAddress1.Text = drPractice["address1"].ToString();
            txtAddress2.Text = drPractice["address2"].ToString();
            txtCity.Text = drPractice["city"].ToString();
            txtZipCode.Text = drPractice["zipCode"].ToString();
            if((drPractice["stateId"] != null) && (drPractice["stateId"].ToString() !=""))
            ddlStates.SelectedValue = drPractice["stateId"].ToString();
            
            txtReferredBy.Text = drPractice["referredBy"].ToString();
            cbAccountIsActive.Checked = (bool)drPractice["accountIsEnabled"];
            ddlCounties.SelectedValue = drPractice["countyId"].ToString();



            rblSlitlampTableNeeded.SelectedValue = drPractice["slitlampAndTableAreRequired"] != DBNull.Value ? ((bool)drPractice["slitlampAndTableAreRequired"]).ToString() : Boolean.FalseString;



            txtPreferredDay1.Text = drPractice["preferredDay1"] != DBNull.Value ? drPractice["preferredDay1"].ToString() : string.Empty;
            txtAssignedTech.Text = drPractice["assignedTech"] != DBNull.Value ? drPractice["assignedTech"].ToString() : string.Empty;
            txtPreferredDay2.Text = drPractice["preferredDay2"] != DBNull.Value ? drPractice["preferredDay2"].ToString() : string.Empty;
            //txtPreferredDay3.Text = drPractice["preferredDay3"] != DBNull.Value ? drPractice["preferredDay3"].ToString() : string.Empty;

            rblFrequency.SelectedValue = drPractice["frequency"] != DBNull.Value ? ((FrequencyEnum)Enum.Parse(typeof(FrequencyEnum), drPractice["frequency"].ToString())).ToString() : FrequencyEnum.Quarterly.ToString();

            upInsertUpdate.Update();

            this.BindDoctorsGridView();

            this.ClearDoctorSection();

            doctorSection.Visible = true;

            upDoctors.Update();  
        }


        protected void gvUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (new PatientBR().ExistsPatientWithPracticeId((Guid)gvUsers.DataKeys[e.RowIndex].Value))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePractices), "existsPatientWithPracticeId", "alert('You cannot delete this practice because there is a patient assigned to it');", true);
                return;
            }

            if (new DoctorBR().ExistsDoctorWithPracticeId((Guid)gvUsers.DataKeys[e.RowIndex].Value))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePractices), "existsPatientWithPracticeId", "alert('You cannot delete this practice because there is a doctor assigned to it');", true);
                return;
            }


            if (new UserBR().DeleteUser((Guid)gvUsers.DataKeys[e.RowIndex].Value, RoleEnum.Practice))
            {

                if (ViewState["userId"] != null && (Guid)gvUsers.DataKeys[e.RowIndex].Value == (Guid)ViewState["userId"])
                {
                    this.ClearInsertUpdateForm();
                    upInsertUpdate.Update();
                    doctorSection.Visible = false;
                    upDoctors.Update();
                }

                this.BindGridViewWithSearchResults();
                
            }

        }

        protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgBtnDelete = e.Row.Cells[GV_USERS_DELETE].FindControl("imgBtnDelete") as ImageButton;
                imgBtnDelete.CommandArgument = e.Row.RowIndex.ToString();
            }
        }

        protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {                
            if (e.CommandName == "DeletePractice") 
            {
                int rowNumber = int.Parse(e.CommandArgument.ToString());

                if (new PatientBR().ExistsPatientWithPracticeId((Guid)gvUsers.DataKeys[rowNumber].Value))
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePractices), "existsPatientWithPracticeId", "alert('You cannot delete this practice because there is a patient assigned to it');", true);
                    return;
                }

                if (new DoctorBR().ExistsDoctorWithPracticeId((Guid)gvUsers.DataKeys[rowNumber].Value))
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePractices), "existsPatientWithPracticeId", "alert('You cannot delete this practice because there is a doctor assigned to it');", true);
                    return;
                }


                if (new UserBR().DeleteUser((Guid)gvUsers.DataKeys[rowNumber].Value, RoleEnum.Practice))
                {

                    if (ViewState["userId"] != null && (Guid)gvUsers.DataKeys[rowNumber].Value == (Guid)ViewState["userId"])
                    {
                        this.ClearInsertUpdateForm();
                        upInsertUpdate.Update();
                        doctorSection.Visible = false;
                        upDoctors.Update();
                    }

                    this.BindGridViewWithSearchResults();
                }

            }

        }

        protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUsers.PageIndex = e.NewPageIndex;
            gvUsers.DataSourceID = "odsUsers";
            gvUsers.DataBind();
            
        }

        protected void gvUsers_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void odsUsers_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["roleId"] = (int)RoleEnum.Practice;
            e.InputParameters["name"] = ViewState["name"].ToString();
            e.InputParameters["countyId"] = ViewState["countyId"].ToString();
            e.InputParameters["address"] = ViewState["address"];
            e.InputParameters["zipCode"] = ViewState["zipCode"];
            e.InputParameters["city"] = ViewState["city"];
          
        }

        protected void gvDoctors_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int doctorId = (int)gvDoctors.DataKeys[e.NewEditIndex].Value;

            ViewState["doctorId"] = doctorId;

            DataRow drDoctor = new DoctorBR().GetDoctorById(doctorId);

            txtFirstName.Text = drDoctor["firstName"].ToString();

            txtLastName.Text = drDoctor["lastName"].ToString();

            btnSaveDoctor.Text = "Save";

            lblManageDoctorsHeader.Text = "Update Doctor: " + drDoctor["firstName"].ToString() + " " + drDoctor["lastName"].ToString();
        }

        protected void gvDoctors_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            if (new PatientBR().ExistsPatientWithDoctorId((int)gvDoctors.DataKeys[e.RowIndex].Value))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePractices), "existsPatientWithDoctorId", "alert('You cannot delete this doctor because there is a patient assigned to it');", true);
                return;
            }

            if (new DoctorBR().DeleteDoctor((int)gvDoctors.DataKeys[e.RowIndex].Value))
            {
                if (ViewState["doctorId"] != null && (int)gvDoctors.DataKeys[e.RowIndex].Value == (int)ViewState["doctorId"])
                {
                    this.ClearDoctorSection();
                }

                this.BindDoctorsGridView();
            }
        }

        protected void gvDoctors_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDoctors.PageIndex = e.NewPageIndex;
            gvDoctors.DataSource = odsDoctors;
            gvDoctors.DataBind();
        }

        protected void odsDoctors_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (ViewState["userId"] != null)
                e.InputParameters["practiceId"] = ViewState["userId"].ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.ClearInsertUpdateForm();
            this.ClearDoctorSection();
            this.BindDoctorsGridView();
            doctorSection.Visible = false;
            //lblInfo.Visible = true;
            upDoctors.Update(); 
        }

        protected void btnCancelDoctor_Click(object sender, EventArgs e)
        {
            this.ClearDoctorSection();
        }

        #endregion //Event handlers

        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvUsers.PageIndex = 0;
            gvUsers.DataSourceID = "odsUsers";
            gvUsers.DataBind();
        }

        private void BindDoctorsGridView()
        {
            gvDoctors.PageIndex = 0;
            gvDoctors.DataSource = odsDoctors;
            gvDoctors.DataBind();
        }

        private void ClearInsertUpdateForm()
        {
            ViewState["userId"] = null;
            lblSelectedUser.Text = "Create New Practice Account";
            btnSave.Text = "Create";
            //clear form
            txtName.Text = string.Empty;

            txtEmail.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtZipCode.Text = string.Empty;
            ddlStates.SelectedIndex = -1;
           
            ddlCounties.SelectedIndex = -1;
            txtReferredBy.Text = string.Empty;
            cbAccountIsActive.Checked = true;

            txtPreferredDay1.Text = string.Empty;
            txtAssignedTech.Text = string.Empty;
            txtPreferredDay2.Text = string.Empty;
            //txtPreferredDay3.Text = string.Empty;

            rblSlitlampTableNeeded.SelectedIndex = 0;
            rblFrequency.SelectedIndex = 0;
        }

        private void ClearDoctorSection()
        {
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            ViewState["doctorId"] = null;
            btnSaveDoctor.Text = "Add";
            lblManageDoctorsHeader.Text = "Add Doctor To Existing Account";
            //lblInfo.Visible = false;
        }

        private void InsertPracticeWithDoctor()
        {
            if (!new UserBR().ExistsUserWithEmail(txtEmail.Text.Trim()))
            {

                User practice = new User();
                practice.UserId = Guid.NewGuid();

                practice.FirstName = txtName.Text;
                practice.Email = txtEmail.Text;
                practice.Password = txtPassword.Text;
                practice.Fax = txtFax.Text;
                practice.Phone = txtPhone.Text;
                practice.Address1 = txtAddress1.Text;
                practice.Address2 = txtAddress2.Text;
                practice.City = txtCity.Text;
                practice.ZipCode = txtZipCode.Text;
                practice.StateId = int.Parse(ddlStates.SelectedValue);
              
                practice.CountyId = int.Parse(ddlCounties.SelectedValue);
                practice.CreatedBy = new Guid(User.Identity.Name);
                practice.AccountIsEnabled = cbAccountIsActive.Checked;
                practice.ReferredBy = txtReferredBy.Text;


                practice.SlitlampAndTableAreRequired = bool.Parse(rblSlitlampTableNeeded.SelectedValue);


                practice.PreferredDay1 = txtPreferredDay1.Text;
                practice.AssignedTech = txtAssignedTech.Text; 
                practice.PreferredDay2 = txtPreferredDay2.Text;
                //practice.PreferredDay3 = txtPreferredDay3.Text;

                practice.Frequency = (FrequencyEnum)Enum.Parse(typeof(FrequencyEnum), rblFrequency.SelectedValue);

                Doctor doctor = new Doctor();
                doctor.FirstName = txtFirstName.Text;
                doctor.LastName = txtLastName.Text;
                doctor.PracticeId = practice.UserId;

                if (new UserBR().InsertPracticeWithDoctor(practice, doctor))
                {
                    this.BindGridViewWithSearchResults();
                    upUsers.Update();  
                    ViewState["userId"] = practice.UserId;
                    lblSelectedUser.Text = "Update Practice: " + practice.FirstName;
                    btnSave.Text = "Save";
                    upInsertUpdate.Update();  
                    this.BindDoctorsGridView();
                    this.ClearDoctorSection();
                    upDoctors.Update();
                }
            }
            else
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePractices), "existsUserWithSameEmailScript", "alert('User with same user name(email) already exists');", true);
        }

        #endregion //Private methods


    }
}
