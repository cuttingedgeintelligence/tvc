<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="True"
    CodeBehind="ManagePractices.aspx.cs" Inherits="CEI.Web.ManagePractices.ManagePractices"
    Title="Manage Practices" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            height: 25px;
        }
        .style2
        {
            width: 186px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Manage Practices" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <div style="vertical-align: bottom; margin-bottom: 10px; overflow: hidden;">
        <asp:UpdatePanel ID="upInsertUpdate" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                <fieldset class="fieldsetLeft">
                    <legend>
                        <asp:Label ID="lblSelectedUser" runat="server" Text="Create New Practice Account"></asp:Label>
                    </legend>
                    <asp:Panel ID="pnlSavePractice" runat="server" DefaultButton="btnSave">
                        <table border="0">
                            <tr>
                                <td colspan="2">
                                   &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblName" runat="server" Text="Name: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                        Display="Dynamic" ErrorMessage="Please enter name" ValidationGroup="vgManagePractice"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblEmail" runat="server" Text="User Name (e-mail): "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                        Display="Dynamic" ErrorMessage="Please enter user name (e-mail)" ValidationGroup="vgManagePractice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                        Display="Dynamic" ErrorMessage="Please enter valid email address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ValidationGroup="vgManagePractice"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPassword" runat="server" Text="Password: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                        Display="Dynamic" ErrorMessage="Please enter the password" ValidationGroup="vgManagePractice"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblReferredBy" runat="server" Text="Referred By: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReferredBy" runat="server" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFax" runat="server" Text="Fax: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFax" runat="server" MaxLength="20"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPhone" runat="server" Text="Phone: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPhone" runat="server" MaxLength="20"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAddress1" runat="server" Text="Address 1: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress1" runat="server" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAddress2" runat="server" Text="Address 2: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress2" runat="server" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblCity" runat="server" Text="City: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCity" runat="server" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblZipCode" runat="server" Text="Zip Code: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtZipCode" runat="server" MaxLength="5"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="margin: 10px 0;">
                                <td style ="padding-top:10px">
                                    <asp:Label ID="lblCounty" runat="server" Text="County: "></asp:Label>
                                </td>
                                <td style ="padding-top:10px">
                                    <asp:DropDownList ID="ddlCounties" runat="server" Style="margin: 5px 0;">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style ="padding-top:10px">
                                    <asp:Label ID="lblState" runat="server" Text="State: "></asp:Label>
                                </td>
                                <td style ="padding-top:10px">
                                    <asp:DropDownList ID="ddlStates" runat="server" Style="margin: 5px 0;">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="style1" style ="padding-top:10px">
                                    <asp:Label ID="lblAccountIsActive" runat="server" Text="Active Account:"></asp:Label>
                                </td>
                                <td class="style1" style ="padding-top:10px">
                                    <asp:CheckBox ID="cbAccountIsActive" runat="server" Checked="true" />
                                </td>
                            </tr>
                            <tr>
                                <td style ="padding-top:10px">
                                    <asp:Label ID="lblSlitlampTableNeeded" runat="server" Text="Need Slit Lamp & Table:"></asp:Label>
                                </td>
                                <td style ="padding-top:10px">
                                    <asp:RadioButtonList ID="rblSlitlampTableNeeded" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True" Value="True">Yes</asp:ListItem>
                                        <asp:ListItem Value="False">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style ="padding-top:10px">
                                    <asp:Label ID="lblPreferredDay" runat="server" Text="Preferred Day(s):"></asp:Label>
                                </td>
                                <td style ="padding-top:10px">
                                   <%-- <asp:TextBox ID="txtPreferredDay1" runat="server" MaxLength="20"></asp:TextBox>--%>
                                <table>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtPreferredDay1" runat="server" MaxLength="20"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtPreferredDay2" runat="server" MaxLength="20"></asp:TextBox>
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <td>
                                                <asp:TextBox ID="txtPreferredDay3" runat="server" MaxLength="20"></asp:TextBox>
                                            </td>
                                        </tr>--%>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAssignedTech" runat="server" Text="Assigned Tech:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAssignedTech" runat="server" MaxLength="20"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style ="padding-top:10px">
                                    <asp:Label ID="lblFrequency" runat="server" Text="Service Frequency:"></asp:Label>
                                </td>
                                <td style ="padding-top:10px">
                                    <asp:RadioButtonList ID="rblFrequency" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True" Value="Monthly">Monthly</asp:ListItem>
                                        <asp:ListItem style="padding-left: 10px;" Value="BiMonthly">Bi-Monthly</asp:ListItem>
                                        <asp:ListItem style="padding-left: 10px;" Value="Quarterly">Quarterly</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 6px">
                                </td>
                                <td style="height: 6px">
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    &nbsp;
                                </td>
                                <td style ="padding-top:10px">
                                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" OnClientClick="__defaultFired = false;"
                                        Text="Create" UseSubmitBehavior="False" ValidationGroup="vgManagePractice" />
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                                        OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
         <asp:UpdatePanel ID="UpdatePaneSearch" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
        <fieldset class="fieldsetLeft" style="float: right;">
            <legend>
                <asp:Label ID="lblSearchHeader" runat="server" Text="Search Practice Accounts"></asp:Label></legend>
            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblError" runat="server"  style="color:Red;" >&nbsp;</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:Label ID="lblNameSearch" runat="server" Text="Name:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNameSearch" runat="server" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:Label ID="lblAddressSearch" runat="server" Text="Address:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddressSearch" runat="server" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:Label ID="lblSearchZipCode" runat="server" Text="Zip Code:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtZipCodeSearch" runat="server" MaxLength="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:Label ID="lblCitySearch" runat="server" Text="City:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCitySearch" runat="server" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style ="padding-top:10px" class="style2">
                            <asp:Label ID="lblCountySearch" runat="server" Text="County: "></asp:Label>
                        </td>
                        <td style ="padding-top:10px">
                            <asp:DropDownList ID="ddlCountiesSearch" runat="server" Style="margin: 5px 0;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            &nbsp;
                        </td>
                        <td style ="padding-top:10px">
                            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Search"
                                OnClientClick="__defaultFired = false;" UseSubmitBehavior="False" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </fieldset>
        </ContentTemplate>
        </asp:UpdatePanel>       
        <asp:UpdatePanel ID="upDoctors" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                <fieldset id="doctorSection" class="fieldsetLeft" style="float: right;" runat="server"
                    visible="false">
                    <legend>
                        <asp:Label ID="lblManageDoctorsHeader" runat="server" Text="Add Doctor To Existing Account"></asp:Label></legend>
                    <%--<asp:Label ID="lblInfo" runat="server" Text="Practice account needs to be created or selected before creating a new doctor for that practice."
                        Style="color: Red" Visible="true"></asp:Label>--%>
                    <table>
                        <tr>
                            <td style="width:188px;">
                                <asp:Label ID="lblFirstName"  runat="server" Text="First Name: "></asp:Label>
                            </td>
                            <td>
                            
                                <asp:TextBox ID="txtFirstName" runat="server" MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                    Display="Dynamic" ErrorMessage="Please enter first name" ValidationGroup="vgManageDoctor"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLastName" runat="server" Text="Last Name: "></asp:Label>                            
                            </td>
                            <td>
                                <asp:TextBox ID="txtLastName" runat="server" MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                    Display="Dynamic" ErrorMessage="Please enter last name" ValidationGroup="vgManageDoctor"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                        
                            </td>
                            <td style="padding:10px 0 5px 0;">
                                <asp:Button ID="btnSaveDoctor" runat="server" OnClick="btnSaveDoctor_Click" OnClientClick="__defaultFired = false;"
                                    Text="Add" UseSubmitBehavior="False" ValidationGroup="vgManageDoctor" />
                                <asp:Button ID="btnCancelDoctor" runat="server" CausesValidation="False" Text="Cancel"
                                    OnClick="btnCancelDoctor_Click" />
                            </td>
                        </tr>
                    </table>
                    <asp:GridView ID="gvDoctors" runat="server" AllowPaging="True" PageSize="5" AutoGenerateColumns="False"
                        DataKeyNames="id" OnPageIndexChanging="gvDoctors_PageIndexChanging" SkinID="gridViewAdmin"
                        OnRowEditing="gvDoctors_RowEditing" Width="420px" Style="clear: both;" 
                        OnRowDeleting="gvDoctors_RowDeleting" ondatabound="gvDoctors_DataBound" 
                        onrowcreated="gvDoctors_RowCreated">
                        <Columns>
                            <asp:BoundField DataField="firstName" HeaderText="First Name" >
                            <FooterStyle CssClass="moveLeft" />
                            </asp:BoundField>
                            <asp:BoundField DataField="lastName" HeaderText="Last Name">
                             <FooterStyle CssClass="moveLeft" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                        ImageUrl="~/App_Themes/Default/icons/edit.gif" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Default/icons/delete.gif"
                                        CommandName="Delete" />
                                    <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this doctor?"
                                        TargetControlID="imgBtnDelete">
                                    </cc1:ConfirmButtonExtender>
                                </ItemTemplate>
                                <ItemStyle CssClass="tdActions" />
                            </asp:TemplateField>
                        </Columns>
                             <PagerTemplate>
                    <div style="padding: 5px;">
                        <div>
                            <asp:Button CommandArgument="Prev" CommandName="Page" Text="Previous" ID="btnPrevious"
                                runat="server" meta:resourcekey="ibPrevResource1" />&nbsp;
                            <asp:Button CommandArgument="Next" CommandName="Page" Text="Next" ID="btnNext" runat="server"
                                meta:resourcekey="ibNextResource1" />
                        </div>
                        <span id="spannumeric" runat="server" class="spannumeric"></span>
                    </div>
                </PagerTemplate>
                    </asp:GridView>
                    <asp:ObjectDataSource ID="odsDoctors" runat="server" EnablePaging="True" MaximumRowsParameterName="pageSize"
                        SelectCountMethod="GetDoctorsByPracticeIdTotalCount" SelectMethod="GetDoctorsByPracticeId"
                        OnSelecting="odsDoctors_Selecting" TypeName="CEI.BR.DoctorBR">
                        <SelectParameters>
                            <asp:Parameter Name="practiceId" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upUsers" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:GridView ID="gvUsers" runat="server"  AllowSorting="true" 
                PagerSettings-NextPageText="Next" PagerSettings-PreviousPageText="Previous"  AllowPaging="True"
                PageSize="10" AutoGenerateColumns="False" DataKeyNames="id" OnPageIndexChanging="gvUsers_PageIndexChanging"
                SkinID="gridViewAdmin" OnRowEditing="gvUsers_RowEditing" Width="920px" Style="clear: both;"
                OnRowDeleting="gvUsers_RowDeleting" OnSorting="gvUsers_Sorting" OnRowDataBound="gvUsers_RowDataBound"
                OnRowCommand="gvUsers_RowCommand" ondatabound="gvUsers_DataBound" 
                onrowcreated="gvUsers_RowCreated">
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="id" DataNavigateUrlFormatString="~/ViewPracticeProfile/ViewPracticeProfile.aspx?id={0}"
                        DataTextField="name" HeaderText="Name" SortExpression="name">
                        <HeaderStyle CssClass="moveLeft" HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:BoundField DataField="zipCode" HeaderText="Zip Code" SortExpression="zipCode"
                        ReadOnly="true" >
                        <ItemStyle HorizontalAlign="Center" />                         
                        </asp:BoundField>
                    <asp:BoundField DataField="city" HeaderText="City" SortExpression="city" ReadOnly="true" >
                     <ItemStyle HorizontalAlign="Center" />                         
                        </asp:BoundField>
                    <asp:BoundField DataField="countyName" HeaderText="County" SortExpression="countyName"
                        ReadOnly="true" >
                         <ItemStyle HorizontalAlign="Center" />                         
                        </asp:BoundField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                ImageUrl="~/App_Themes/Default/icons/edit.gif" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Default/icons/delete.gif"
                                CommandName="DeletePractice" />
                            <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this practice account?"
                                TargetControlID="imgBtnDelete">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="tdActions" />
                    </asp:TemplateField>
                </Columns>
                     <PagerTemplate>
                    <div style="padding: 5px;">
                        <div>
                            <asp:Button CommandArgument="Prev" CommandName="Page" Text="Previous" ID="btnPrevious"
                                runat="server" meta:resourcekey="ibPrevResource1" />&nbsp;
                            <asp:Button CommandArgument="Next" CommandName="Page" Text="Next" ID="btnNext" runat="server"
                                meta:resourcekey="ibNextResource1" />
                        </div>
                        <span id="spannumeric" runat="server" class="spannumeric"></span>
                    </div>
                </PagerTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsUsers" runat="server" EnablePaging="True" MaximumRowsParameterName="pageSize"
                OnSelecting="odsUsers_Selecting" SelectCountMethod="GetPracticesManageSearchTotalCount"
                SelectMethod="GetPracticesManageSearch" TypeName="CEI.BR.UserBR" SortParameterName="sortExpression">
                <SelectParameters>
                    <asp:Parameter Name="roleId" />
                    <asp:Parameter Name="countyId" />
                    <asp:Parameter Name="name" />
                    <asp:Parameter Name="address" />
                    <asp:Parameter Name="zipCode" />
                    <asp:Parameter Name="city" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
