﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CEI.Common;
using CEI.BR;
using CEI.DA;
using CEI.Framework;
using CEI.Framework.Encryption;
using System.Data;
using System.Configuration;
using System.IO;

namespace CEI.Web
{
    public partial class InsertPatientLog_PatientLogPatientFromExcel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            return;
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

            int CPTCodeId = -1;
            DataTable dtCPTCodeId = new PatientLogBR().GetCPTCodeIdForExcelImport("CPT Import");
            if (dtCPTCodeId.Rows.Count > 0)
                CPTCodeId = (int)dtCPTCodeId.Rows[0]["id"];
            else
                return;


            DataTable dtTables = new PatientLogBR().GetTablesToInsertFromExcel();


            foreach (DataRow drTable in dtTables.Rows)
            {


                string tableName = drTable["name"].ToString();

                try
                {

                    // bool exists = false;
                    DataTable dtAll = new UserBR().GetAllServiceDateFromExcelForImportPetient(tableName);


                    DateTime glaucomaExamDate = DateTime.MinValue;
                    DateTime retinaExamDate = DateTime.MinValue;
                    DataRowCollection systemSetupValues = SystemSetupBR.GetAllSystemSetupValues().Rows;
                    int numOfMonthsBetweenEachGlaucomaExam = int.Parse(systemSetupValues.Find(SystemSetupEnum.GlaucomaExamsRecurrence)["setupValue"].ToString());
                    int numOfMonthsBetweenEachRetinaExam = int.Parse(systemSetupValues.Find(SystemSetupEnum.RetinaExamsRecurrence)["setupValue"].ToString());



                    //if (drUser != null && drUser["password"].ToString() == new EncryptDecrypt().Encrypt(loginControl.Password, string.Empty) && (bool)drUser["accountIsEnabled"])
                    for (int i = 0; i < dtAll.Rows.Count; i++)
                    {
                        // DataTable dtExistPationLogPatient=new PatientLogBR().GetPatientlodserviceDate_ByPracticeId((DateTime)dtAll.Rows[i]["Date"], (Guid)dtAll.Rows[i]["practiceId"], dtAll.Rows[i]["appointmentTime"].ToString());
                        // if (dtExistPationLogPatient.Rows.Count == 0)
                        //  {
                        DataTable GetAllPatientForSD = new UserBR().GetAllFromExcelForImportPetient((DateTime)dtAll.Rows[i]["servicedate"], tableName);

                        if (GetAllPatientForSD.Rows.Count > 0)
                        {

                            DateTime nextAppointmentDate = DateTime.MinValue;
                            if (i != (dtAll.Rows.Count - 1))
                            {
                                nextAppointmentDate = (DateTime)dtAll.Rows[i + 1]["serviceDate"];
                                glaucomaExamDate = nextAppointmentDate.AddMonths(-numOfMonthsBetweenEachGlaucomaExam);
                                retinaExamDate = nextAppointmentDate.AddMonths(-numOfMonthsBetweenEachRetinaExam);
                            }





                            FinalNotesWorksheet patientLog = new FinalNotesWorksheet();
                            patientLog.FinalNotesWorksheetPatients = new List<FinalNotesWorksheetPatient>();
                            patientLog.PracticeId = (Guid)GetAllPatientForSD.Rows[0]["practiceId"];
                            patientLog.TechnicianId = (Guid)GetAllPatientForSD.Rows[0]["technicianId"];
                            patientLog.CreatedBy = new Guid(User.Identity.Name);
                            patientLog.ServiceDate = (DateTime)GetAllPatientForSD.Rows[0]["ServiceDate"];
                            patientLog.Status = nextAppointmentDate != DateTime.MinValue ? PatientLogStatusEnum.Final__notes__worksheet : PatientLogStatusEnum.Worksheet;
                            patientLog.NumOfPatients = (short)1;
                            patientLog.DateCreated = DateTime.Now;
                            if (GetAllPatientForSD.Rows[0]["HRTDataDesktopImportDate"] != null)
                                if (GetAllPatientForSD.Rows[0]["HRTDataDesktopImportDate"].ToString() != "")
                                    patientLog.DesktopImportedDate = Convert.ToDateTime(GetAllPatientForSD.Rows[0]["HRTDataDesktopImportDate"]);


                            if (GetAllPatientForSD.Rows[0]["HRTImagesComment"] != null)
                            {
                                // if (GetAllPatientForSD.Rows[i]["HRTImagesComment"].ToString() != "")
                                patientLog.DesktopImportedComment = GetAllPatientForSD.Rows[0]["HRTImagesComment"].ToString();
                            }
                            else
                                patientLog.DesktopImportedComment = "";
                            patientLog.CompletedById = new Guid(User.Identity.Name);

                            patientLog.CompletedOn = DateTime.Now;

                            for (int j = 0; j < GetAllPatientForSD.Rows.Count; j++)
                            {

                                try
                                {

                                    FinalNotesWorksheetPatient patientLogPatient = new FinalNotesWorksheetPatient();
                                    patientLogPatient.DoctorId = (int)GetAllPatientForSD.Rows[j]["doctorId"];

                                    patientLogPatient.PatientId = (int)GetAllPatientForSD.Rows[j]["patientId"];

                                    // set patient log patient status(no show or examined)
                                    //if (GetAllPatientForSD.Rows[j]["HRTGlaucoma"].ToString().ToUpper().Equals("NS") || GetAllPatientForSD.Rows[j]["HRTRetina"].ToString().ToUpper().Equals("NS"))
                                    if (GetAllPatientForSD.Rows[j]["BFA"].ToString().ToUpper().Equals(string.Empty) && GetAllPatientForSD.Rows[j]["HRTGlaucoma"].ToString().ToUpper().Equals(string.Empty) && GetAllPatientForSD.Rows[j]["HRTRetina"].ToString().ToUpper().Equals(string.Empty))
                                        patientLogPatient.Status = PatientLogPatientStatusEnum.No__Show;
                                    else
                                        patientLogPatient.Status = PatientLogPatientStatusEnum.Examined;



                                    patientLogPatient.PatientHistory = PatientHistoryEnum.New__Patient;

                                    patientLogPatient.AppointmentTime = GetAllPatientForSD.Rows[j]["appointmentTime"].ToString();
                                    patientLogPatient.InsuranceCompanyId = 52;//(int)GetAllPatientForSD.Rows[j]["insuranceId"];


                                    patientLogPatient.Glaucoma = !GetAllPatientForSD.Rows[j]["HRTGlaucoma"].ToString().Equals(string.Empty);
                                    patientLogPatient.Retina = !GetAllPatientForSD.Rows[j]["HRTRetina"].ToString().Equals(string.Empty);

                                    if (GetAllPatientForSD.Rows[j]["BFA"].ToString().Equals("0,5") || GetAllPatientForSD.Rows[j]["BFA"].ToString().Equals("0.5"))
                                        patientLogPatient.BFA = 0.5M;

                                    if (GetAllPatientForSD.Rows[j]["BFA"].ToString().Equals("1"))
                                        patientLogPatient.BFA = 1M;


                                    if (GetAllPatientForSD.Rows[j]["HRTGlaucoma"].ToString().Equals("0,5") || GetAllPatientForSD.Rows[j]["HRTGlaucoma"].ToString().Equals("0.5"))
                                        patientLogPatient.HRT_Glaucoma = 0.5M;

                                    if (GetAllPatientForSD.Rows[j]["HRTGlaucoma"].ToString().Equals("1"))
                                        patientLogPatient.HRT_Glaucoma = 1M;

                                    if (GetAllPatientForSD.Rows[j]["HRTRetina"].ToString().Equals("0,5") || GetAllPatientForSD.Rows[j]["HRTRetina"].ToString().Equals("0.5"))
                                        patientLogPatient.HRT_Retina = 0.5M;

                                    if (GetAllPatientForSD.Rows[j]["HRTRetina"].ToString().Equals("1"))
                                        patientLogPatient.HRT_Retina = 1M;




                                    patientLogPatient.InternalCustomNotes = GetAllPatientForSD.Rows[j]["Internalnotes"].ToString();
                                    patientLogPatient.PracticeCustomNotes = GetAllPatientForSD.Rows[j]["PracticeNotes"].ToString();
                                    if (patientLogPatient.InternalCustomNotes != "")
                                        // patientLogPatient.internalNotes = "<b>Custom Note</b><br /> -" + patientLogPatient.InternalCustomNotes + "<br />";
                                        patientLogPatient.internalNotes = "-" + patientLogPatient.InternalCustomNotes + "<br />";
                                    else
                                        patientLogPatient.internalNotes = "";
                                    if (patientLogPatient.PracticeCustomNotes != "")
                                        // patientLogPatient.practiceNotes = "<b>Custom Note</b><br /> -" + patientLogPatient.PracticeCustomNotes + "<br />";
                                        patientLogPatient.practiceNotes = "-" + patientLogPatient.PracticeCustomNotes + "<br />";
                                    else
                                        patientLogPatient.practiceNotes = "";



                                    int someNumber = -1;
                                    bool isNumberOs = false;
                                    bool isNumberOd = false;
                                    isNumberOs = int.TryParse(GetAllPatientForSD.Rows[j]["OSPachy"].ToString(), out someNumber);
                                    if (isNumberOs)
                                        patientLogPatient.OS_Pachymetry = short.Parse(GetAllPatientForSD.Rows[j]["OSPachy"].ToString());

                                    // isNumberOd = int.TryParse(GetAllPatientForSD.Rows[j]["OD_Pachy"].ToString(), out someNumber);
                                    // if (isNumberOd)
                                    if (GetAllPatientForSD.Rows[j]["ODPachy"].ToString().Contains('/'))
                                    {
                                        isNumberOd = int.TryParse(GetAllPatientForSD.Rows[j]["ODPachy"].ToString().TrimEnd('/'), out someNumber);
                                        if (isNumberOd)
                                            patientLogPatient.OD_Pachymetry = (short)someNumber;

                                    }
                                    else
                                    {
                                        isNumberOd = int.TryParse(GetAllPatientForSD.Rows[j]["ODPachy"].ToString(), out someNumber);
                                        if (isNumberOd)
                                            patientLogPatient.OD_Pachymetry = (short)someNumber;
                                    }

                                    //string[] part1 = GetAllPatientForSD.Rows[j]["OD_IOP"].ToString().Split('/');
                                    //isNumberOd = int.TryParse(part1[0].ToString(), out someNumber);
                                    //if (isNumberOd)
                                    //patientLogPatient.OD_IOP = part1[0].ToString();
                                    isNumberOd = int.TryParse(GetAllPatientForSD.Rows[j]["ODIOP"].ToString().TrimEnd('/'), out someNumber);
                                    if (isNumberOd)
                                        patientLogPatient.OD_IOP = someNumber.ToString();
                                    else
                                    {
                                        if (GetAllPatientForSD.Rows[j]["ODIOP"].ToString().TrimEnd('/').Contains('.'))
                                            patientLogPatient.OD_IOP = GetAllPatientForSD.Rows[j]["ODIOP"].ToString().TrimEnd('/');
                                    }

                                    isNumberOd = int.TryParse(GetAllPatientForSD.Rows[j]["ODBFA"].ToString(), out someNumber);
                                    if (isNumberOd)
                                        patientLogPatient.OD_BFA = (short)someNumber;

                                    //string[] part2 = GetAllPatientForSD.Rows[j]["OS_IOP"].ToString().Split('/');
                                    //isNumberOd = int.TryParse(part2[0].ToString(), out someNumber);
                                    //if (isNumberOd)
                                    //patientLogPatient.OS_IOP = part2[0].ToString();
                                    isNumberOd = int.TryParse(GetAllPatientForSD.Rows[j]["OSIOP"].ToString().TrimEnd('/'), out someNumber);
                                    if (isNumberOd)
                                        patientLogPatient.OS_IOP = someNumber.ToString();
                                    else
                                    {
                                        if (GetAllPatientForSD.Rows[j]["OSIOP"].ToString().TrimEnd('/').Contains('.'))
                                            patientLogPatient.OS_IOP = GetAllPatientForSD.Rows[j]["OSIOP"].ToString().TrimEnd('/');
                                    }


                                    isNumberOd = int.TryParse(GetAllPatientForSD.Rows[j]["OSBFA"].ToString(), out someNumber);
                                    if (isNumberOd)
                                        patientLogPatient.OS_BFA = (short)someNumber;

                                    patientLog.FinalNotesWorksheetPatients.Add(patientLogPatient);

                                }
                                catch (Exception exc)
                                {

                                    new UserDA().CatchImportErrors(
                                        1,
                                        tableName,
                                        GetAllPatientForSD.Rows[0]["ServiceDate"] as DateTime?,
                                        GetAllPatientForSD.Rows[j]["doctorId"] as Int32?,
                                        GetAllPatientForSD.Rows[j]["patientId"] as Int32?,
                                        GetAllPatientForSD.Rows[0]["practiceId"] as Guid?,
                                        GetAllPatientForSD.Rows[0]["technicianId"] as Guid?,
                                        exc.GetBaseException().Message);                                    
                                   
                                }


                            }




                            try
                            {
                                if (new PatientLogBR().InsertPatientLogFromExcel(patientLog, nextAppointmentDate, retinaExamDate, glaucomaExamDate, CPTCodeId))
                                {

                                    // new PatientLogPatientBR().InsertPatientLogPatient(patientLogPatient, ref transaction);
                                    //this.BindGridViewWithSearchResults();
                                    //this.ClearInsertUpdateForm();

                                }
                            }
                            catch (Exception exc)
                            {
                                new UserDA().CatchImportErrors(
                                        2,
                                        tableName,
                                        GetAllPatientForSD.Rows[0]["ServiceDate"] as DateTime?,
                                        null,
                                        null,
                                        GetAllPatientForSD.Rows[0]["practiceId"] as Guid?,
                                        GetAllPatientForSD.Rows[0]["technicianId"] as Guid?,
                                        exc.GetBaseException().Message);
                            }

                        }

                    }

                }
                catch (Exception exc)
                {
                    new UserDA().CatchImportErrors(
                                        0,
                                        tableName,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        exc.GetBaseException().Message);
                }
            }

            Response.Redirect("~/Default.aspx");
        }
    }
}
