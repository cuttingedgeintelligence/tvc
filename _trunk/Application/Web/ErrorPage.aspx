﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="CEI.Web.ErrorPage" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<b>Sorry! an unexpected error has occurred.</b>
                <br /><br />
                <b>This error has been forwarded to our technical group.</b>
                <br /><br />
                Please click <a href="#" onclick="history.back()">here</a> to go back and try again.
</asp:Content>
