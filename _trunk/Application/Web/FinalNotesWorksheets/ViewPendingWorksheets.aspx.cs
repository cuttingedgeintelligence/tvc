﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;
using CEI.Common.RecurringAppointment;

namespace CEI.Web.FinalNotesWorksheets
{
    public partial class ViewPendingWorksheets : BasePage
    {

        #region GridView columns


        private const int GV_FINALNOTESWORKSHEETS_PRACTICE_NAME = 0;
        private const int GV_FINALNOTESWORKSHEETS_DATE = 1;
        private const int GV_FINALNOTESWORKSHEETS_INSERT = 2;
       
      

        #endregion //GridView columns


        #region Event handlers


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                
               
                BindListControl<DataTable>.BindAndAddListItem(ddlPracticesSearch, new UserBR().GetAllPracticesNameAndId(), "name", "id", "All", Guid.Empty.ToString());

              
                ViewState["practiceId"] = ddlPracticesSearch.SelectedValue;

                if (Roles.IsUserInRole(RoleEnum.Technician.ToString()))
                    ViewState["technicianId"] = new Guid(User.Identity.Name);
                else
                    ViewState["technicianId"] = Guid.Empty;


                this.BindGridViewWithSearchResults();
            }
        }

        protected void gvWorksheets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("insertnotes"))
            {
                Response.Redirect("~/FinalNotesWorksheets/ManageFinalNotesWorksheet.aspx?logId=" + gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["Id"].ToString() + "&prName=" + gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["PracticeName"].ToString() + "&prId=" + gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["PracticeId"].ToString() + "&servDate=" + gvWorksheets.Rows[int.Parse(e.CommandArgument.ToString())].Cells[GV_FINALNOTESWORKSHEETS_DATE].Text, true);               
            }

           
        }


        protected void gvWorksheets_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvWorksheets.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvWorksheets_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvWorksheets.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvWorksheets.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvWorksheets.PageIndex == (gvWorksheets.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }

        protected void gvWorksheets_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvWorksheets.PageIndex = e.NewPageIndex;
            gvWorksheets.DataSource = odsWorksheets;
            gvWorksheets.DataBind();

        }

        protected void odsWorksheets_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["technicianId"] = ViewState["technicianId"];
            e.InputParameters["practiceId"] = ViewState["practiceId"];
            e.InputParameters["status"] = PatientLogStatusEnum.Worksheet;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

           
            ViewState["practiceId"] = ddlPracticesSearch.SelectedValue;

            this.BindGridViewWithSearchResults();
        }


        #endregion //Event handlers

        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvWorksheets.PageIndex = 0;
            gvWorksheets.DataSource = odsWorksheets;
            gvWorksheets.DataBind();
        }

        #endregion //Private methods

    }
}
