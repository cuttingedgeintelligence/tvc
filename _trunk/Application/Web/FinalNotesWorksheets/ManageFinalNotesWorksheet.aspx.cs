﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;
using CEI.Common.RecurringAppointment;
using Microsoft.Reporting.WebForms;
using CEI.Common.PatientsNotes;
using System.Text;



namespace CEI.Web.FinalNotesWorksheets
{
    public partial class ManageFinalNotesWorksheet : BasePage
    {

        #region GridView columns


        private const int GV_PATIENTS_NAME = 0;
        private const int GV_PATIENTS_BFA = 1;
        private const int GV_PATIENTS_HRT_GLAUCOMA = 2;
        private const int GV_PATIENTS_HRT_RETINA = 3;
        private const int GV_PATIENTS_STATUS = 4;
        private const int GV_PATIENTS_INTERNAL_NOTES = 5;
        private const int GV_PATIENTS_DOCTOR_NOTES = 6;
        private const int GV_PATIENTS_OD = 7;
        private const int GV_PATIENTS_OS = 8;

        #endregion //GridView columns

        

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (this.ValidateRequestParams())
                {
                   


                    lblPracticeNameData.Text = Request.QueryString["prName"];
                    lblServiceDateData.Text = Request.QueryString["servDate"];
                    List<FinalNotesWorksheetPatient> patientsList = new FinalNotesWorksheetBR().GetFinalNotesWorksheetPatientsByPatientLogId(int.Parse(Request.QueryString["logId"]));
                    this.BingGridViewPatients(patientsList);
                    ViewState["PatientsList"] = patientsList;
                     
                   
                }
                else
                {
                    //TODO: show error message
                }
            }
        }

       

      
     

        protected void repCheckBoxHolder_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lbl = (Label)e.Item.FindControl("lblCategoryHeaderText");
                lbl.Text = e.Item.DataItem.ToString();

                CheckBoxList chkLIst = (CheckBoxList)e.Item.FindControl("chkListSubCategories");
                if (chkLIst != null)
                {
                    List<Note> listNote = GetNotes(e.Item.DataItem.ToString());
                    chkLIst.DataSource = listNote;
                    chkLIst.DataTextField = "text";
                    chkLIst.DataValueField = "id";
                    chkLIst.DataBind();
                }
            }
        }

        protected void btnSaveFinalNoteWorksheet_Click(object sender, EventArgs e)
        {
            new FinalNotesWorksheetBR().UpdateFinalNotesWorksheet(GenerateWorksheetFromGridView());
            Response.Redirect("~/FinalNotesWorksheets/ViewPendingWorksheets.aspx");
        }

        protected void btnSaveAndSubmitFinalNoteWorksheet_Click(object sender, EventArgs e)
        {


            Guid practiceId = (Guid)ViewState["practiceId"];
            DateTime serviceDate = DateTime.ParseExact(lblServiceDateData.Text, "MM/dd/yyyy", null);

            DataRow drPracticeAppointmentsSettings = new PracticeAppointmentsSettingsBR().GetPracticeAppointmentSettingsByPracticeId(practiceId);
            bool createFollowUp = false;
            DateTime retinaExamDate = DateTime.MinValue;
            DateTime glaucomaExamDate = DateTime.MinValue;
            DateTime nextAppointmentDate = DateTime.MinValue;
            DateTime nextAppointmentDateRecurrance = DateTime.MinValue;
            DateTime nextAppointmentDateOneTime = DateTime.MinValue;

            if (drPracticeAppointmentsSettings != null)
            {
                //Get the next date in the series 
                nextAppointmentDateRecurrance = RecurrenceHelper.GetNextDate(serviceDate, drPracticeAppointmentsSettings["recurrencePattern"].ToString());
            }

            DataTable dtOneTimeAppointments = new PracticeOneTimeAppointmentBR().GetOneTimeAppointmentsByPracticeId(practiceId, serviceDate, 1);

            if (dtOneTimeAppointments.Rows.Count > 0)
                nextAppointmentDateOneTime = (DateTime)dtOneTimeAppointments.Rows[0]["appointmentDate"];

            //nextAppointmentDate is lower from two nextAppointmentDateRecurrance and nextAppointmentDateOneTime
            //but no MinValue

            if (!nextAppointmentDateRecurrance.Equals(DateTime.MinValue) && !nextAppointmentDateOneTime.Equals(DateTime.MinValue))
                nextAppointmentDate = (nextAppointmentDateRecurrance < nextAppointmentDateOneTime) ? nextAppointmentDateRecurrance : nextAppointmentDateOneTime;
            else if (!nextAppointmentDateRecurrance.Equals(DateTime.MinValue)) //if nextAppointmentDateRecurrance is DateTime.MinValue then set the nextAppointmentDateOneTime as nextAppointmentDate regardless of its value 
                nextAppointmentDate = nextAppointmentDateRecurrance;
            else //the last chance is nextAppointmentDateOneTime not to be DateTime.MinValue
                nextAppointmentDate = nextAppointmentDateOneTime;


            if (!nextAppointmentDate.Equals(DateTime.MinValue))
            {
                createFollowUp = true;
                DataRowCollection systemSetupValues = SystemSetupBR.GetAllSystemSetupValues().Rows;
                int numOfMonthsBetweenEachGlaucomaExam = int.Parse(systemSetupValues.Find(SystemSetupEnum.GlaucomaExamsRecurrence)["setupValue"].ToString());
                int numOfMonthsBetweenEachRetinaExam = int.Parse(systemSetupValues.Find(SystemSetupEnum.RetinaExamsRecurrence)["setupValue"].ToString());
                glaucomaExamDate = nextAppointmentDate.AddMonths(-numOfMonthsBetweenEachGlaucomaExam);
                retinaExamDate = nextAppointmentDate.AddMonths(-numOfMonthsBetweenEachRetinaExam);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(ManageFinalNotesWorksheet), "AlertFinalNotes", "alert('The Final Notes can not be completed because there are no future service dates setup. Please go to the Manage Appointments section for this practice and insert the future service dates before you complete this Final Notes.');", true);

                return;
            }



            if (new FinalNotesWorksheetBR().UpdateFinalNotesWorksheetStatus((int)ViewState["patientLogId"], this.GenerateWorksheetFromGridView().FinalNotesWorksheetPatients, PatientLogStatusEnum.Final__notes__worksheet, createFollowUp, nextAppointmentDate, retinaExamDate, glaucomaExamDate, practiceId, new Guid(User.Identity.Name), new Guid(User.Identity.Name), DateTime.Now))
            {
                new FinalNotesWorksheetBR().SendMailFinalNotesWorksheetSubmit(
                    Session["loggedUserFullName"] != null ? Session["loggedUserFullName"].ToString() : string.Empty,
                    Session["loggedUserEmail"] != null ? Session["loggedUserEmail"].ToString() : string.Empty,
                    "New final notes worksheet is created",
                    lblServiceDateData.Text,
                    lblPracticeNameData.Text,
                    (Guid)ViewState["practiceId"],
                    Request.QueryString["logId"].ToString(),
                    ResolveUrl("~/FinalNotesWorksheets/ViewCompletedFinalNotesWorksheet.aspx")
                    //ResolveUrl("~/FinalNotesWorksheets/ViewCompletedFinalNotesWorksheet.aspx?logId=" + Request.QueryString["logId"] + "&prName=" + lblPracticeNameData.Text + "&servDate=" + lblServiceDateData.Text+"")
                    );


                Response.Redirect("~/FinalNotesWorksheets/ViewPendingWorksheets.aspx");
            }

        }

        protected void gvPatients_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                FinalNotesWorksheetPatient finalNotesWorksheetPatient = e.Row.DataItem as FinalNotesWorksheetPatient;
                if (finalNotesWorksheetPatient.Glaucoma)
                {
                    RadioButtonList rblBFA =  e.Row.Cells[GV_PATIENTS_BFA].FindControl("rblBFA") as RadioButtonList;
                    rblBFA.Style.Add("display", "");
                    rblBFA.SelectedValue = finalNotesWorksheetPatient.BFA.ToString();
                    RadioButtonList rblHRTGlaucoma = e.Row.Cells[GV_PATIENTS_HRT_GLAUCOMA].FindControl("rblHRTGlaucoma") as RadioButtonList;
                    rblHRTGlaucoma.Style.Add("display", "");
                    rblHRTGlaucoma.SelectedValue = finalNotesWorksheetPatient.HRT_Glaucoma.ToString();
                    
                }

                if (finalNotesWorksheetPatient.Retina)
                {
                   RadioButtonList rblHRTRetina = e.Row.Cells[GV_PATIENTS_HRT_RETINA].FindControl("rblHRTRetina") as RadioButtonList;
                   rblHRTRetina.Style.Add("display", "");
                   rblHRTRetina.SelectedValue = finalNotesWorksheetPatient.HRT_Retina.ToString();
                }

                (e.Row.Cells[GV_PATIENTS_INTERNAL_NOTES].FindControl("btnInsertInternalNotes") as Button).CommandArgument = e.Row.RowIndex.ToString();
                (e.Row.Cells[GV_PATIENTS_DOCTOR_NOTES].FindControl("btnInsertDoctorNotes") as Button).CommandArgument = e.Row.RowIndex.ToString();

                (e.Row.Cells[GV_PATIENTS_INTERNAL_NOTES].FindControl("lblInternalNotes") as Label).Text = finalNotesWorksheetPatient.InternalNotes;
                (e.Row.Cells[GV_PATIENTS_DOCTOR_NOTES].FindControl("lblDoctorNotes") as Label).Text = finalNotesWorksheetPatient.PracticeNotes; 

                (e.Row.Cells[GV_PATIENTS_STATUS].FindControl("ddlStatus") as DropDownList).SelectedValue = ((int)finalNotesWorksheetPatient.Status).ToString();

                if (finalNotesWorksheetPatient.OD_IOP != null && finalNotesWorksheetPatient.OD_BFA != null)
                    (e.Row.Cells[GV_PATIENTS_OD].FindControl("txtOD") as TextBox).Text = finalNotesWorksheetPatient.OD_IOP + "/ " + finalNotesWorksheetPatient.OD_BFA.ToString();
                if (finalNotesWorksheetPatient.OS_IOP != null && finalNotesWorksheetPatient.OS_BFA != null)
                    (e.Row.Cells[GV_PATIENTS_OD].FindControl("txtOS") as TextBox).Text = finalNotesWorksheetPatient.OS_IOP + "/ " + finalNotesWorksheetPatient.OS_BFA.ToString();

               
            }
        }

        protected void gvPatients_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("insertinternalnotes"))
            {
                ViewState["selectedRowIndex"] = int.Parse(e.CommandArgument.ToString());
                ViewState["updatecolumnname"] = "internalnotes";
                lblNotesHeader.Text = "Internal notes for patient: " + gvPatients.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["FullName"].ToString();
             
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManageFinalNotesWorksheet), "jQueryCollapseScript", "$('.panelHolder ').slideDown('slow');", true);
                upNotes.Update();

                BindRepeater(1);
                MapCheckedItems("internalnotes");
                txtRepeaterCustomNote.Text = GetWorkingFinalNotesWorksheetPatient((int)gvPatients.DataKeys[(int)ViewState["selectedRowIndex"]].Values["Id"]).InternalCustomNotes;
            }
            else if (e.CommandName.Equals("insertpracticenotes"))
            {
                ViewState["updatecolumnname"] = "practicenotes";
                ViewState["selectedRowIndex"] = int.Parse(e.CommandArgument.ToString());
                lblNotesHeader.Text = "Practice notes for patient: " + gvPatients.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["FullName"].ToString();
                
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManageFinalNotesWorksheet), "jQueryCollapseScript", "$('.panelHolder ').slideDown('slow');", true);
                upNotes.Update();

                BindRepeater(2);
                MapCheckedItems("practicenotes");
                txtRepeaterCustomNote.Text = GetWorkingFinalNotesWorksheetPatient((int)gvPatients.DataKeys[(int)ViewState["selectedRowIndex"]].Values["Id"]).PracticeCustomNotes;
            }



        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Dictionary<string, List<string>> checkedHeadersTextValues = new Dictionary<string, List<string>>();

            if (ViewState["updatecolumnname"].ToString() == "practicenotes")
            {
                int workingRowID = (int)gvPatients.DataKeys[(int)ViewState["selectedRowIndex"]].Values["Id"];

                List<PracticePatientsNotes> pNotes = ExtractSelectedCheckedListBoxes("practicenotes", checkedHeadersTextValues) as List<PracticePatientsNotes>;

                if(pNotes.Count.Equals(0))
                    pNotes.Add(new PracticePatientsNotes{ PatientLogPatientId = workingRowID});

                string previewNote = BuildTheNotesString(checkedHeadersTextValues);

                new PatientsNotesBR<PracticePatientsNotes>().InsertPatientsNotes(pNotes, txtRepeaterCustomNote.Text, previewNote);

                (gvPatients.Rows[(int)ViewState["selectedRowIndex"]].Cells[GV_PATIENTS_DOCTOR_NOTES].FindControl("lblDoctorNotes") as Label).Text = previewNote; //lblNotes.Text;//txtNote.Text;


                UpdateCurrentFinalNoteCustomNote(workingRowID, "practicenotes",txtRepeaterCustomNote.Text, previewNote); 

                ViewState["DictionaryItems"] = null;
            }
            else
            {
                int workingRowID = (int)gvPatients.DataKeys[(int)ViewState["selectedRowIndex"]].Values["Id"];

                List<InternalPatientsNotes> iNotes = ExtractSelectedCheckedListBoxes("internalnotes", checkedHeadersTextValues) as List<InternalPatientsNotes>;

                if (iNotes.Count.Equals(0))
                    iNotes.Add(new InternalPatientsNotes { PatientLogPatientId = workingRowID });

                string previewNote = BuildTheNotesString(checkedHeadersTextValues);

                new PatientsNotesBR<InternalPatientsNotes>().InsertPatientsNotes(iNotes, txtRepeaterCustomNote.Text, previewNote);

                (gvPatients.Rows[(int)ViewState["selectedRowIndex"]].Cells[GV_PATIENTS_INTERNAL_NOTES].FindControl("lblInternalNotes") as Label).Text = previewNote; //lblNotes.Text; //txtNote.Text.Replace("\r\n","<BR />");


                UpdateCurrentFinalNoteCustomNote(workingRowID, "internalenotes", txtRepeaterCustomNote.Text, previewNote); 

                ViewState["DictionaryItems"] = null;

            }
            upPatients.Update();
            ScriptManager.RegisterClientScriptBlock(this, typeof(ManageFinalNotesWorksheet), "jQueryCollapseScript", "$('.panelHolder ').slideUp('slow');", true);


        }

        protected void btnPrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Pdf);
        }

        protected void btnPrintExcel_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Excel);
        }

        protected void btnPrintPreview_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Printer);
        }

        #endregion //Event handlers

        #region Private methods

        private bool ValidateRequestParams()
        {
            bool isValid = false;
            int patientLogId;
                if (

                        Request.QueryString["prId"] != null &&
                        Request.QueryString["prName"] != null &&
                        Request.QueryString["servDate"] != null &&
                        Request.QueryString["logId"] != null &&
                        this.IsGuid(Request.QueryString["prId"]) &&
                        int.TryParse(Request.QueryString["logId"],out patientLogId) && 
                        this.ValidateServiceDate(Request.QueryString["servDate"])
                    )
                    {
                        isValid = true;
                        ViewState["patientLogId"] = patientLogId;
                        ViewState["practiceId"] =  new Guid(Request.QueryString["prId"]);
                    }




            return isValid;


        }

       

        private void BindRepeater(int noteTypeId)
        {
            Dictionary<string, List<Note>> categorySubCategory = PrepareDictionaryForRepeater(noteTypeId);
            ViewState["DictionaryItems"] = categorySubCategory;
            repCheckBoxHolder.DataSource = categorySubCategory.Keys;
            repCheckBoxHolder.DataBind();
        }

        private void MapCheckedItems(string commandName)
        {
            int id = (int)gvPatients.DataKeys[(int)ViewState["selectedRowIndex"]].Values["Id"];
            if (commandName.Equals("practicenotes"))
            {
                List<PracticePatientsNotes> checkedPractice = new PatientsNotesBR<PracticePatientsNotes>().GetPatientNotes(id);

                foreach (RepeaterItem r in repCheckBoxHolder.Items)
                {
                    foreach (Control c in r.Controls)
                    {
                       if (c is CheckBoxList)
                        {
                            CheckBoxList cl = c as CheckBoxList;

                            foreach (ListItem i in cl.Items)
                            {
                                i.Selected = IsIdContained(Convert.ToInt32(i.Value), "practicenotes", checkedPractice);
                            }
                        }
                    }
                }

            }
            else
            {
                List<InternalPatientsNotes> checkedInternal = new PatientsNotesBR<InternalPatientsNotes>().GetPatientNotes(id);

                foreach (RepeaterItem r in repCheckBoxHolder.Items)
                {
                    foreach (Control c in r.Controls)
                    {
                        if (c is CheckBoxList)
                        {
                            CheckBoxList cl = c as CheckBoxList;

                            foreach (ListItem i in cl.Items)
                            {
                                i.Selected = IsIdContained(Convert.ToInt32(i.Value), "internalenotes", checkedInternal);
                            }
                        }
                    }
                }
            }
        }

        private bool IsIdContained(int checkBoxId, string castType, object listPatientNotes)
        {
            bool retVal = false;
            if (castType.Equals("practicenotes"))
            {
                List<PracticePatientsNotes> checkedPractice = listPatientNotes as List<PracticePatientsNotes>;

                foreach (PracticePatientsNotes itm in checkedPractice)
                    if (itm.NoteId.Equals(checkBoxId))
                    {
                        retVal = true;
                        break;
                    }
            }
            else
            {
                List<InternalPatientsNotes> checkedPractice = listPatientNotes as List<InternalPatientsNotes>;

                foreach (InternalPatientsNotes itm in checkedPractice)
                    if (itm.NoteId.Equals(checkBoxId))
                    {
                        retVal = true;
                        break;
                    }
            }

            return retVal;
        }

        private Dictionary<string, List<Note>> PrepareDictionaryForRepeater(int noteTypeId)
        {
            Dictionary<string, List<Note>> categorySubCategory = new Dictionary<string, List<Note>>();

            DataTable dtCategories = new NoteCategoryBR().GetAllNoteCategories(noteTypeId);

            List<Note> allNotes = new NoteBR().GetAllNotes(noteTypeId).AsEnumerable()
                                                    .Select(dr => new Note
                                                    {
                                                        Id = dr.Field<int>("id")
                                                       ,
                                                        Text = dr.Field<string>("text")
                                                       ,
                                                        CategoryId = dr.Field<int>("categoryId")
                                                    }).ToList();

            foreach (DataRow dr in dtCategories.Rows)
            {
                List<Note> notesByCategory = allNotes.Where(n => n.CategoryId == int.Parse(dr["id"].ToString())).ToList();

                if (!categorySubCategory.Keys.Contains<string>(dr["category"].ToString()))
                    categorySubCategory.Add(dr["category"].ToString(), notesByCategory);
            }

            return categorySubCategory;
        }

        private FinalNotesWorksheetPatient GetWorkingFinalNotesWorksheetPatient(int id)
        {
            FinalNotesWorksheetPatient retVal = new FinalNotesWorksheetPatient();
            List<FinalNotesWorksheetPatient> finalNotesList = ViewState["PatientsList"] as List<FinalNotesWorksheetPatient>;

            foreach (FinalNotesWorksheetPatient f in finalNotesList)
            {
                if (f.Id.Equals(id))
                {
                    retVal = f;
                    break;
                }
            }

            return retVal;
        }

        private void UpdateCurrentFinalNoteCustomNote(int id, string columnName, string customNote, string fullPreviewNote)
        {
            List<FinalNotesWorksheetPatient> finalNotesList = ViewState["PatientsList"] as List<FinalNotesWorksheetPatient>;

            foreach (FinalNotesWorksheetPatient f in finalNotesList)
            {
                if (f.Id.Equals(id))
                {
                    if (columnName.Equals("practicenotes"))
                    {
                        f.PracticeNotes = fullPreviewNote;
                        f.PracticeCustomNotes = customNote;
                    }
                    else
                    {
                        f.InternalNotes = fullPreviewNote;
                        f.InternalCustomNotes = customNote;
                    }
                }
            }

            ViewState["PatientsList"] = null;
            ViewState["PatientsList"] = finalNotesList;

        }

        private List<Note> GetNotes(string key)
        {
            Dictionary<string, List<Note>> categorySubCategory = ViewState["DictionaryItems"] as Dictionary<string, List<Note>>;

            return categorySubCategory[key];
        }

        private bool ValidateServiceDate(string serviceDate)
        {
            try
            {
                DateTime.ParseExact(serviceDate, "MM/dd/yyyy", null);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }

        }

        private bool IsGuid(string s)
        {
            try
            {
                new Guid(s);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }
        }

        private void BingGridViewPatients(List<FinalNotesWorksheetPatient> patientsList)
        {

            gvPatients.DataSource = patientsList;
            gvPatients.DataBind();
            if (gvPatients.Rows.Count > 0)
            {
                btnSaveFinalNoteWorksheet.Style.Add("display", "");
               
            }
            else
            {
                btnSaveFinalNoteWorksheet.Style.Add("display", "none");                            
            }
        }

        private FinalNotesWorksheet GenerateWorksheetFromGridView()
        {
            List<FinalNotesWorksheetPatient> finalNotesWorksheetPatients = 
                gvPatients.Rows.Cast<GridViewRow>().Select(r =>
                new FinalNotesWorksheetPatient
                {
                    Id = int.Parse(gvPatients.DataKeys[r.RowIndex].Values["Id"].ToString())   
                   ,Status = (PatientLogPatientStatusEnum)Enum.Parse(typeof(PatientLogPatientStatusEnum),((DropDownList)r.FindControl("ddlStatus")).SelectedValue)
                   ,InternalNotes = ((Label)r.FindControl("lblInternalNotes")).Text
                   ,PracticeNotes = ((Label)r.FindControl("lblDoctorNotes")).Text
                   ,OD_IOP = !string.IsNullOrEmpty(((TextBox)r.FindControl("txtOD")).Text) ? ((TextBox)r.FindControl("txtOD")).Text.Substring(0, 4) : ""
                   ,OD_BFA = !string.IsNullOrEmpty(((TextBox)r.FindControl("txtOD")).Text) ? short.Parse(((TextBox)r.FindControl("txtOD")).Text.Substring(5)) : (short?)null
                   ,OS_IOP = !string.IsNullOrEmpty(((TextBox)r.FindControl("txtOS")).Text) ? ((TextBox)r.FindControl("txtOS")).Text.Substring(0, 4) : ""
                   ,OS_BFA = !string.IsNullOrEmpty(((TextBox)r.FindControl("txtOS")).Text) ? short.Parse(((TextBox)r.FindControl("txtOS")).Text.Substring(5)) : (short?)null

                   ,BFA = int.Parse((r.FindControl("ddlStatus") as DropDownList).SelectedValue) == (int)PatientLogPatientStatusEnum.Examined ? decimal.Parse((r.FindControl("rblBFA") as RadioButtonList).SelectedValue) : 0.0M
                   ,HRT_Glaucoma = int.Parse((r.FindControl("ddlStatus") as DropDownList).SelectedValue) == (int)PatientLogPatientStatusEnum.Examined ? decimal.Parse((r.FindControl("rblHRTGlaucoma") as RadioButtonList).SelectedValue) :  0.0M
                   ,HRT_Retina = int.Parse((r.FindControl("ddlStatus") as DropDownList).SelectedValue) == (int)PatientLogPatientStatusEnum.Examined  ?  decimal.Parse((r.FindControl("rblHRTRetina") as RadioButtonList).SelectedValue) : 0.0M
                }).ToList();

            FinalNotesWorksheet finalNoteWorksheet = new FinalNotesWorksheet();
            finalNoteWorksheet.FinalNotesWorksheetPatients = finalNotesWorksheetPatients;

            return finalNoteWorksheet;
        }

        protected string BuildTheNotesString(Dictionary<string, List<string>> checkedHeadersTextValues)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string key in checkedHeadersTextValues.Keys)
            {
                //sb.AppendLine("<b>" + key + "</b><br />"); //this is to show notes categoty
                foreach (string val in checkedHeadersTextValues[key])
                    sb.AppendLine(" -" + val + "<br />");
            }

            return sb.ToString();
        }

        protected object ExtractSelectedCheckedListBoxes(string columnName, Dictionary<string, List<string>> checkedHeadersTextValues)
        {
            List<PracticePatientsNotes> pNotes = null;
            List<InternalPatientsNotes> iNotes = null;

            if (columnName.Equals("practicenotes"))
            {
                pNotes = new List<PracticePatientsNotes>();
            }
            else
                iNotes = new List<InternalPatientsNotes>();

            TextBox tCustomNote = upNotes.FindControl("txtRepeaterCustomNote") as TextBox;
            if (tCustomNote != null)
            {
                if (!String.IsNullOrEmpty(tCustomNote.Text))
                {
                    List<string> cNotesList = new List<string>();
                    cNotesList.Add(tCustomNote.Text);

                    checkedHeadersTextValues.Add("Custom Notes", cNotesList);//Custom Notes
                }
            }

            foreach (RepeaterItem r in repCheckBoxHolder.Items)
            {
                string catecoryKey = string.Empty;
                List<string> checkedTextValues = new List<string>();
                foreach (Control c in r.Controls)
                {
                    if (c is Label)
                    {
                        Label l = c as Label;
                        catecoryKey = l.Text;
                    }
                    else if (c is CheckBoxList)
                    {
                        CheckBoxList cl = c as CheckBoxList;

                        foreach (ListItem i in cl.Items)
                        {
                            if (columnName.Equals("practicenotes"))
                            {
                                if (i.Selected)
                                {
                                    PracticePatientsNotes pNote = new PracticePatientsNotes();
                                    pNote.NoteId = Convert.ToInt32(i.Value);
                                    pNote.PatientLogPatientId = (int)gvPatients.DataKeys[(int)ViewState["selectedRowIndex"]].Values["Id"];
                                    pNotes.Add(pNote);

                                    checkedTextValues.Add(i.Text);
                                }
                            }
                            else
                            {
                                if (i.Selected)
                                {
                                    InternalPatientsNotes iNote = new InternalPatientsNotes();
                                    iNote.NoteId = Convert.ToInt32(i.Value);
                                    iNote.PatientLogPatientId = (int)gvPatients.DataKeys[(int)ViewState["selectedRowIndex"]].Values["Id"];
                                    iNotes.Add(iNote);

                                    checkedTextValues.Add(i.Text);
                                }
                            }
                        }
                    }
                }

                checkedHeadersTextValues.Add(catecoryKey, checkedTextValues);
            }

            Dictionary<string, List<string>> keysToBeRemoved = new Dictionary<string, List<string>>();

            foreach (string key in checkedHeadersTextValues.Keys)
                if (checkedHeadersTextValues[key].Count.Equals(0))
                    keysToBeRemoved.Add(key, checkedHeadersTextValues[key]);

            foreach (string key in keysToBeRemoved.Keys)
                checkedHeadersTextValues.Remove(key);

            if (columnName.Equals("practicenotes"))
                return pNotes;
            else
                return iNotes;

        }

        //private void AddNotes()
        //{
        //    //check if note from this category is already added
        //    int indexOfNoteCategory = lblNotes.Text.IndexOf(ddlNoteCategories.SelectedItem.Text + "<BR />");
        //    if (indexOfNoteCategory != -1)
        //    {
        //        if(ddlNoteCategories.SelectedValue == "-1" && !txtCustomNote.Text.Trim().Equals(string.Empty)) //custom note
        //            lblNotes.Text = lblNotes.Text.Insert(indexOfNoteCategory + ddlNoteCategories.SelectedItem.Text.Length + "<BR />".Length, "-" + txtCustomNote.Text + "<BR />");            
        //        else if(ddlNoteCategories.SelectedValue != "-1") //predefined note
        //            lblNotes.Text = lblNotes.Text.Insert(indexOfNoteCategory + ddlNoteCategories.SelectedItem.Text.Length + "<BR />".Length, "-" + ddlPredefinedNotes.SelectedItem.Text + "<BR />");            
        //    }
        //    else
        //    {
        //        if (ddlNoteCategories.SelectedValue == "-1" && !txtCustomNote.Text.Trim().Equals(string.Empty)) //custom note
        //            lblNotes.Text += ddlNoteCategories.SelectedItem.Text + "<BR />-" + txtCustomNote.Text + "<BR />";
        //         else if(ddlNoteCategories.SelectedValue != "-1") //predefined note
        //            lblNotes.Text += ddlNoteCategories.SelectedItem.Text + "<BR />-" + ddlPredefinedNotes.SelectedItem.Text + "<BR />";
        //    }
        //}

        #endregion //Private methods

        #region MICROSOFT REPORTING

        private void PrintReport(ExportType exportType)
        {

            TotalVisionReport _totalVisionReport = new TotalVisionReport();

            List<FinalNotesWorksheetPatient> patientsList = ViewState["PatientsList"] as List<FinalNotesWorksheetPatient>;



            LocalReport localReport = new LocalReport();

            localReport.ReportPath = Server.MapPath("~/Reports/rptFinalNotesWorksheet.rdlc");

            ReportDataSource reportDataSource = new ReportDataSource("FinalNotesWorksheetPatient", patientsList);
            localReport.DataSources.Add(reportDataSource);

            ReportParameter[] param = new ReportParameter[2];
            param[0] = new ReportParameter("rptParamPracticeName", lblPracticeNameData.Text);
            param[1] = new ReportParameter("rptParamDate", lblServiceDateData.Text);

            localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            if (exportType == ExportType.Printer)
            {
                _totalVisionReport.ListItems = patientsList;
                _totalVisionReport.Pameters.Add("rptParamPracticeName", lblPracticeNameData.Text);
                _totalVisionReport.Pameters.Add("rptParamDate", lblServiceDateData.Text);
                Session["PrintingParams"] = _totalVisionReport;

                Response.Redirect("~/Reports/ReportPreview.aspx?ReportName=PatientLog");
            }
            else if (exportType == ExportType.Pdf)
            {
                DownloadPDF(localReport);

            }
            else if (exportType == ExportType.Excel)
            {
                DownloadExcel(localReport);

            }
        }

        private void DownloadPDF(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PDF</OutputFormat>" +
            //"  <PageWidth>11in</PageWidth>" +
            //"  <PageHeight>8.5in</PageHeight>" +
            //"  <MarginTop>0.2in</MarginTop>" +
            //"  <MarginLeft>0.1in</MarginLeft>" +
            //"  <MarginRight>0.1in</MarginRight>" +
            //"  <MarginBottom>0.2in</MarginBottom>" +
            //"</DeviceInfo>";

         //   string deviceInfo =
         //"<DeviceInfo>" +
         //"  <OutputFormat>Excel</OutputFormat>" +
         //"  <PageWidth>11in</PageWidth>" +
         //"  <PageHeight>8.5in</PageHeight>" +
         //"  <MarginTop>0.2in</MarginTop>" +
         //"  <MarginLeft>0.1in</MarginLeft>" +
         //"  <MarginRight>0.1in</MarginRight>" +
         //"  <MarginBottom>0.2in</MarginBottom>" +
         //"</DeviceInfo>";

            string deviceInfo =
 "<DeviceInfo>" +
 "  <OutputFormat>PDF</OutputFormat>" +
 "  <PageWidth>11in</PageWidth>" +
 "  <PageHeight>8.5in</PageHeight>" +
 "  <MarginTop>0.2in</MarginTop>" +
 "  <MarginLeft>0.6in</MarginLeft>" +
 "  <MarginRight>0.1in</MarginRight>" +
 "  <MarginBottom>0.2in</MarginBottom>" +
 "</DeviceInfo>";


            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=FinalNotesWorksheet." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DownloadExcel(LocalReport localReport)
        {
            string reportType = "Excel";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
            "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";

           





            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=FinalNotesWorksheet." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }


        #endregion

        protected void btnViewHistory_Click(object sender, EventArgs e)
        {
            DataTable dt = new FinalNotesWorksheetBR().GetPatientsHistoryByPatientLogId(int.Parse(Request.QueryString["logId"]), PatientLogStatusEnum.Final__notes__worksheet);

            LocalReport localReport = new LocalReport();

            localReport.ReportPath = Server.MapPath("~/Reports/rptPatientsHistory.rdlc");

            ReportDataSource reportDataSource = new ReportDataSource("DataSet1_stp_PatientLogs_PatientLogsPatients_Patients_GetPatientsHistoryByPatientLogId", dt);
            localReport.DataSources.Add(reportDataSource);

            ReportParameter[] param = new ReportParameter[2];
            param[0] = new ReportParameter("rptParamPracticeName", lblPracticeNameData.Text);
            param[1] = new ReportParameter("rptParamDate", lblServiceDateData.Text);

            localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>PDF</OutputFormat>" +
            "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.5in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=PatientHistory." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }
        

    }
}
