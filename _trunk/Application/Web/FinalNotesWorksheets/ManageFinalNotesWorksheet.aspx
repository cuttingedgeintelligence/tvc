<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="True"
    CodeBehind="ManageFinalNotesWorksheet.aspx.cs" Inherits="CEI.Web.FinalNotesWorksheets.ManageFinalNotesWorksheet"
    Title="Manage Final Notes Worksheet" Async="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControls/DayMonthYear.ascx" TagName="DayMonthYear" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:Label ID="lblHeaderText" runat="server" Text="Manage Final Notes Worksheet"
        CssClass="lblTitle" Style="float: none"></asp:Label>
    <table width="400px">
        <tr>
            <td width="100px">
                <asp:Label ID="lblPracticeName" runat="server" Text="Practice Name:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblPracticeNameData" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100px">
                <asp:Label ID="lblServiceDate" runat="server" Text="Service Date:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblServiceDateData" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="padding: 10px 0;">
        <tr>
            <td style="padding-top: 10px;">
                <asp:ImageButton ID="btnPrintPDF" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png"
                    ToolTip="Download PDF" OnClick="btnPrintPDF_Click" Style="padding-right: 3px;" />
            </td>
            <td style="padding-top: 10px; padding-left: 5px">
                <asp:ImageButton ID="btnPrintExcel" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png"
                    ToolTip="Download Excel" OnClick="btnPrintExcel_Click" />
            </td>
            <td style="padding-top: 10px; padding-left: 5px">
                <asp:ImageButton ID="btnPrintPreview" runat="server" ImageUrl="~/Images/Mockups/printIcon.png"
                    ToolTip="View and Print" OnClick="btnPrintPreview_Click" Style="display: none;" />
            </td>
        </tr>
    </table>
    <div class="panelHolder" style="display: none;">
        <asp:UpdatePanel ID="upNotes" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="400px">
                    <tr>
                        <td>
                            <asp:Label ID="lblNotesHeader" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trRepeaterHolder">
                        <td>
                            <asp:Label ID="lblRepeaterCustomNote" runat="server" Text="Custom Note"></asp:Label>
                            <asp:TextBox ID="txtRepeaterCustomNote" runat="server" TextMode="MultiLine" Height="100px"
                                Width="380px"></asp:TextBox>
                            <asp:Repeater ID="repCheckBoxHolder" runat="server" OnItemDataBound="repCheckBoxHolder_ItemDataBound">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCategoryHeaderText" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBoxList runat="server" ID="chkListSubCategories">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" ValidationGroup="vgRole" ToolTip="Save" Text="Save"
                                OnClick="btnSave_Click" />&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" OnClientClick=" $('.panelHolder').slideUp('slow');return false;"
                                Text="Cancel" ToolTip="cancel" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upPatients" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvPatients" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                DataKeyNames="Id,PatientId,FullName" SkinID="gridViewAdmin" Width="920px" Style="clear: both;"
                OnRowDataBound="gvPatients_RowDataBound" EmptyDataText="There are no patients in the final notes worksheet"
                OnRowCommand="gvPatients_RowCommand">
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}"
                        DataTextField="fullName" HeaderText="Patient Name">
                        <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:TemplateField HeaderText="BFA">
                        <ItemTemplate>
                            <asp:RadioButtonList ID="rblBFA" runat="server" Style="display: none;" RepeatDirection="Vertical">
                                <asp:ListItem Selected="True" Value="0.0">0</asp:ListItem>
                                <asp:ListItem Value="0.5">0.5</asp:ListItem>
                                <asp:ListItem Value="1.0">1</asp:ListItem>
                            </asp:RadioButtonList>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRT-G">
                        <ItemTemplate>
                            <asp:RadioButtonList ID="rblHRTGlaucoma" runat="server" Style="display: none;" RepeatDirection="Vertical">
                                <asp:ListItem Selected="True" Value="0.0">0</asp:ListItem>
                                <asp:ListItem Value="0.5">0.5</asp:ListItem>
                                <asp:ListItem Value="1.0">1</asp:ListItem>
                            </asp:RadioButtonList>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRT-R">
                        <ItemTemplate>
                            <asp:RadioButtonList ID="rblHRTRetina" runat="server" Style="display: none;" RepeatDirection="Vertical">
                                <asp:ListItem Selected="True" Value="0.0">0</asp:ListItem>
                                <asp:ListItem Value="0.5">0.5</asp:ListItem>
                                <asp:ListItem Value="1.0">1</asp:ListItem>
                            </asp:RadioButtonList>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Exam Status">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlStatus" runat="server">
                                <asp:ListItem Value="1" Text="No Show"></asp:ListItem>
                                <asp:ListItem Value="2" Selected="True" Text="Examined"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Canceled"></asp:ListItem>
                            </asp:DropDownList>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Internal Notes">
                        <ItemTemplate>
                            <asp:Label ID="lblInternalNotes" runat="server" Style="display: none;"></asp:Label>
                            <input type="button" value="Show" class="klikni_me" id="<%# Eval("id")+"_internal" %>" onclick="InternalNotesClicked($(this)); return false;" />
                            <asp:Button ID="btnInsertInternalNotes" runat="server" CommandName="insertinternalnotes"
                                CssClass="btnClass" Text="Insert" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Practice Notes" HeaderStyle-VerticalAlign="Top">
                        <ItemTemplate>
                            <asp:Label ID="lblDoctorNotes" runat="server" Style="display: none;"></asp:Label>
                              <input type="button" value="Show" class="click_me" id="<%# Eval("id")+"_practice" %>" onclick="InternalNotesClicked($(this)); return false;" />
                             <asp:Button ID="btnInsertDoctorNotes" runat="server" CssClass="btnClass innerBottom"
                                Text="Insert" CommandName="insertpracticenotes" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OD: IOP/BFA">
                        <ItemTemplate>
                            <asp:TextBox ID="txtOD" runat="server" MaxLength="11" Width="70"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revOD" runat="server" ValidationExpression="^(\d{2}\.\d{1})\/ ((\d{3})|(\d{4}))$"
                                ValidationGroup="vgFinalNotes" ErrorMessage="Valid format is dd.d/ ddd or dd.d/ dddd"
                                ControlToValidate="txtOD" Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OS: IOP/BFA">
                        <ItemTemplate>
                            <asp:TextBox ID="txtOS" runat="server" MaxLength="11" Width="70"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revOS" runat="server" ValidationExpression="^(\d{2}\.\d{1})\/ ((\d{3})|(\d{4}))$"
                                ValidationGroup="vgFinalNotes" ErrorMessage="Valid format is dd.d/ ddd or dd.d/ dddd"
                                ControlToValidate="txtOS" Display="Dynamic">
                            </asp:RegularExpressionValidator>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Button ID="btnSaveFinalNoteWorksheet" runat="server" CausesValidation="True"
                OnClick="btnSaveFinalNoteWorksheet_Click" Text="Save & Edit Later" ValidationGroup="vgFinalNotes" />
            <asp:Button ID="btnSaveAndSubmitFinalNoteWorksheet" runat="server" CausesValidation="True"
                OnClick="btnSaveAndSubmitFinalNoteWorksheet_Click" Text="Save & Complete" ValidationGroup="vgFinalNotes" />
            <asp:Button ID="btnPrintPatientHistory" runat="server" Text="Print Patient's History"
                OnClick="btnViewHistory_Click" CausesValidation="False" />
            <asp:Button ID="btnCancelFinalNoteWorksheet" runat="server" Text="Cancel" CausesValidation="false"
                PostBackUrl="~/FinalNotesWorksheets/ViewPendingWorksheets.aspx" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnPrintPatientHistory" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">
    

   function InternalNotesClicked(btnObj)
   {
 
       if (btnObj.val() == "Show")
       {
        ShowInternalNotes(btnObj);
        return false;
       }
   
   if (btnObj.val() == "Hide")
   {

   HideInternalNotes(btnObj);
   return false;
   }

   }


     function ShowInternalNotes(btnObj){
  //  debugger;
      
        if(btnObj.attr("className") == "click_me")
        {
          btnObj.val("Hide");
          $("#tr_"+ (btnObj.attr("id")).split("_")[0]+"_internal").remove();
           $("#"+ (btnObj.attr("id")).split("_")[0]+"_internal").val("Show");
        if(btnObj.siblings("span").html() == "")
        btnObj.parent().parent().after("<tr  id=\"tr_"+ btnObj.attr("id") +"\"><td style=\"padding-left:5px\" colspan=\"12\">No notes for this patient</td></tr>");
        else
        btnObj.parent().parent().after("<tr  id=\"tr_"+ btnObj.attr("id") +"\"><td style=\"padding-left:5px\" colspan=\"12\">"+ btnObj.siblings("span").html() +"</td></tr>");
        }
        
         if(btnObj.attr("className") ==  "klikni_me")
         {
           btnObj.val("Hide");
            $("#tr_"+ (btnObj.attr("id")).split("_")[0]+"_practice").remove();
            $("#"+ (btnObj.attr("id")).split("_")[0]+"_practice").val("Show");
        if(btnObj.siblings("span").html() == "")
        btnObj.parent().parent().after("<tr  id=\"tr_"+ btnObj.attr("id") +"\"><td style=\"padding-left:5px\" colspan=\"12\">No notes for this patient</td></tr>");
        else
        btnObj.parent().parent().after("<tr  id=\"tr_"+ btnObj.attr("id") +"\"><td style=\"padding-left:5px\" colspan=\"12\">"+ btnObj.siblings("span").html() +"</td></tr>");
        }
    }
    
    
    function HideInternalNotes(btnObj){
    
   if(btnObj.attr("className") == "click_me")
   {
   btnObj.val("Show");
    $("#tr_"+ btnObj.attr("id")).remove();
    }
    if(btnObj.attr("className") ==  "klikni_me")
    {
    btnObj.val("Show");
     $("#tr_"+ btnObj.attr("id")).remove();
     }
    }
    
    
   </script>
</asp:Content>
