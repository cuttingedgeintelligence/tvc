﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ViewCompletedFinalNotesWorksheet.aspx.cs" Inherits="CEI.Web.FinalNotesWorksheets.ViewCompletedFinalNotesWorksheet"
    Title="View Final Notes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <style type="text/css">
        .klikni_me
        {
            text-decoration: none;
            border: none;
            background: #5a6a84 url(../btnBg.jpg) top left repeat-x;
            border: 1px solid #5a6a84 !important;
            color: #fff;
            font-family: Trebuchet MS;
            font-size: 13px;
            font-weight: bold;
            border-collapse: collapse;
            height: 25px;
            line-height: 25px;
            padding: 0px !important;
            padding: 0px 6px !important;
            display: inline;
            margin: 0px;
            width: auto;
            overflow: visible;
        }
        .klikni_me:hover
        {
            background: #5a6a84 url(btnBgOver.jpg) bottom left repeat-x;
        }
    </style>--%>
    <asp:Label ID="lblHeaderText" runat="server" Text="View Final Notes" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <table width="400px">
        <tr>
            <td width="100px">
                <asp:Label ID="lblPracticeName" runat="server" Text="Practice Name:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblPracticeNameData" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblServiceDate" runat="server" Text="Service Date:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblServiceDateData" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="padding: 10px 0;">
        <tr>
            <td style="padding-top: 10px">
                <asp:ImageButton ID="btnPrintPDF" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png"
                    ToolTip="Download PDF" OnClick="btnPrintPDF_Click" Style="padding-right: 3px;" />
            </td>
            <td style="padding-top: 10px; padding-left: 5px">
                <asp:ImageButton ID="btnPrintExcel" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png"
                    ToolTip="Download Excel" OnClick="btnPrintExcel_Click" />
            </td>
            <td style="padding-top: 10px; padding-left: 5px">
                <asp:ImageButton ID="btnPrintPreview" runat="server" ImageUrl="~/Images/Mockups/printIcon.png"
                    ToolTip="View and Print" OnClick="btnPrintPreview_Click" Style="display: none;" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upPatients" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvPatients" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                DataKeyNames="Id,PatientId" SkinID="gridViewAdmin" Width="920px" Style="clear: both;"
                OnRowDataBound="gvPatients_RowDataBound" EmptyDataText="There are no patients in the schedule">
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="PatientId" HeaderStyle-Width="150px" DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}"
                        DataTextField="fullName" HeaderText="Patient Name">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:TemplateField HeaderText="BFA">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRT-G" HeaderStyle-Width="30px">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRT-R" HeaderStyle-Width="30px">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Exam Status" HeaderStyle-Width="50px">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Internal Notes">
                        <ItemTemplate>
                            <asp:Label ID="lblInternalNotes" runat="server" Text="" Style="display: none;"></asp:Label>
                            <input type="button" value="Show" class="klikni_me" id="<%# Eval("id")+"_internal" %>" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Practice Notes">
                        <ItemTemplate>
                            <asp:Label ID="lblDoctorNotes" runat="server" Text="" Style="display: none;"></asp:Label>
                            <input type="button" value="Show" class="click_me" id="<%# Eval("id")+"_practice" %>" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Insurance">
                        <ItemTemplate>
                            <asp:Label ID="lblInsurance" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OD: IOP/BFA">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblOD"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OS: IOP/BFA">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblOS"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OD: Pachy">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPachyOD"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OS: Pachy">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPachyOS"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Button ID="btnCancelFinalNoteWorksheet" runat="server" Text="Back" CausesValidation="false"
                PostBackUrl="~/FinalNotesWorksheets/ViewCompletedFinalNotesWorksheets.aspx" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">

        $(document).ready(function() {


            $(".klikni_me").click(function() {

                if ($(this).val() == "Show") {
                    ShowInternalNotes($(this));
                    return false;
                }

                if ($(this).val() == "Hide") {
                    HideInternalNotes($(this));
                    return false;
                }


            });
            $(".click_me").click(function() {

                if ($(this).val() == "Show") {
                    ShowInternalNotes($(this));
                    return false;
                }

                if ($(this).val() == "Hide") {
                    HideInternalNotes($(this));
                    return false;
                }


            });

        });


        function ShowInternalNotes(btnObj) {
            //  debugger;

            if (btnObj.attr("className") == "click_me") {
                btnObj.val("Hide");
                $("#tr_" + (btnObj.attr("id")).split("_")[0] + "_internal").remove();
                $("#" + (btnObj.attr("id")).split("_")[0] + "_internal").val("Show");
                if (btnObj.siblings("span").html() == "")
                    btnObj.parent().parent().after("<tr  id=\"tr_" + btnObj.attr("id") + "\"><td style=\"padding-left:5px\" colspan=\"12\">No notes for this patient</td></tr>");
                else
                    btnObj.parent().parent().after("<tr  id=\"tr_" + btnObj.attr("id") + "\"><td style=\"padding-left:5px\" colspan=\"12\">" + btnObj.siblings("span").html() + "</td></tr>");
            }

            if (btnObj.attr("className") == "klikni_me") {
                btnObj.val("Hide");
                $("#tr_" + (btnObj.attr("id")).split("_")[0] + "_practice").remove();
                $("#" + (btnObj.attr("id")).split("_")[0] + "_practice").val("Show");
                if (btnObj.siblings("span").html() == "")
                    btnObj.parent().parent().after("<tr  id=\"tr_" + btnObj.attr("id") + "\"><td style=\"padding-left:5px\" colspan=\"12\">No notes for this patient</td></tr>");
                else
                    btnObj.parent().parent().after("<tr  id=\"tr_" + btnObj.attr("id") + "\"><td style=\"padding-left:5px\" colspan=\"12\">" + btnObj.siblings("span").html() + "</td></tr>");
            }
        }


        function HideInternalNotes(btnObj) {

            if (btnObj.attr("className") == "click_me") {
                btnObj.val("Show");
                $("#tr_" + btnObj.attr("id")).remove();
            }
            if (btnObj.attr("className") == "klikni_me") {
                btnObj.val("Show");
                $("#tr_" + btnObj.attr("id")).remove();
            }
        }
    
    </script>

</asp:Content>
