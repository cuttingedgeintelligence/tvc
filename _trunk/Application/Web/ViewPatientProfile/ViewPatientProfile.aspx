﻿<%@ Page Title="View Patient Profile" Language="C#" MasterPageFile="~/MasterPages/Default.Master"
    AutoEventWireup="true" CodeBehind="ViewPatientProfile.aspx.cs" Inherits="CEI.Web.ViewPatientProfile.ViewPatientProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input id="btnBack" type="button" class="btnClass" value="Back" onclick="history.go(-1)" />
    <asp:Label ID="lblHeaderText" runat="server" Text="View Patient Profile" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <asp:Panel ID="pnlViewPatientProfile" runat="server" Style="display: none;">
        <fieldset class="fieldsetLeft" style="width: 900px">
            <legend>
                <asp:Label ID="lblSelectedPatient" runat="server"></asp:Label>
            </legend>
            <table width="900" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="266" valign="top" style="vertical-align: top;">
                        <table width="266" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblFirstName" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="First Name: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblFirstNameData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblMiddleInitial" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Middle Initial: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblMiddleInitialData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblLastName" CssClass="viewPatientProfileTxtBold" runat="server" Text="Last Name: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblLastNameData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblGender" CssClass="viewPatientProfileTxtBold" runat="server" Text="Gender: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblGenderData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDateOfBirth" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Date Of Birth:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDateOfBirthData" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="300" valign="top" style="vertical-align: top;">
                        <table width="232" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblIsActive" CssClass="viewPatientProfileTxtBold" runat="server" Text="Active Patient: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblIsActiveData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblInsuranceCompany" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Insurance: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblInsuranceCompanyData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPracticeName" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Practice: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPracticeNameData" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDoctorName" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Doctor: "></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDoctorNameData" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="232" valign="top" style="vertical-align: top;">
                        <table width="300" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="2" style="padding-bottom: 5px;">
                                    <asp:Label ID="lblManifestRefraction" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Manifest Refraction:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-bottom: 0px;">
                                    <table style="margin-left: 40px;">
                                        <tr>
                                            <td  style="vertical-align: middle !important; width: 25px; font-weight: bold;
                                                padding-right: 7px;">
                                                <asp:Label ID="lblMR_OD" runat="server" Style="padding-right: 4px;" Text="OD:"></asp:Label>
                                            </td>
                                            <td  style="vertical-align: middle !important; width: 20px; font-weight: bold;">
                                                <asp:Label ID="lblOD_Sph" runat="server" Text="sph" Style="margin-right: 5px;"></asp:Label>
                                            </td>
                                            <td  style="vertical-align: bottom !important; text-align:right;">
                                                <asp:Label ID="lblOD_SphData" runat="server" style="padding-right: 14px;"></asp:Label>
                                            </td>
                                            <td  style="vertical-align: middle !important; width: 15px; font-weight: bold;">
                                                <asp:Label ID="lblOD_Cyl" runat="server" Text="cyl" Style="margin-right: 5px;"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important; text-align: right;
                                                padding-right: 14px;">
                                                <asp:Label ID="lblOD_CylData" runat="server"></asp:Label>
                                            </td>
                                            <td  style="vertical-align: middle !important; width: 10px; font-weight: bold;">
                                                <asp:Label ID="lblOD_Sep" runat="server" Text="x" Style="margin-right: 5px;"></asp:Label>
                                            </td>
                                            <td  style="vertical-align: bottom !important;">
                                                <asp:Label ID="lblOD_Axis_Of_AstigmatismData" runat="server" Style="margin-right: 3px;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle !important; font-weight: bold; padding-right: 7px;">
                                                <asp:Label ID="lblMR_OS" Style="padding-right: 4px; text-align: right;" runat="server"
                                                    Text="OS:"></asp:Label>
                                            </td>
                                            <td style="vertical-align: middle !important; font-weight: bold;">
                                                <asp:Label ID="lblOS_Sph" runat="server" Text="sph"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important; text-align: right; padding-right: 14px;"
                                                valign="bottom">
                                                <asp:Label ID="lblOS_SphData" runat="server"></asp:Label>
                                            </td>
                                            <td style="vertical-align: middle !important; font-weight: bold;">
                                                <asp:Label ID="lblOS_Cyl" runat="server" Text="cyl"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important; text-align: right;"
                                                valign="bottom">
                                                <asp:Label ID="lblOS_CylData" runat="server" style="padding-right: 14px;"></asp:Label>
                                            </td>
                                            <td style="vertical-align: middle !important; font-weight: bold;">
                                                <asp:Label ID="lblOS_Sep" runat="server" Text="x" Style="margin-right: 5px;"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important;" valign="bottom">
                                                <asp:Label ID="lblOS_Axis_Of_AstigmatismData" runat="server" Style="margin-right: 3px;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table width="300" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="2" style="padding-bottom: 5px;">
                                    <asp:Label ID="lblPachymetry" CssClass="viewPatientProfileTxtBold" runat="server"
                                        Text="Pachymetry:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left">
                                    <table width="170px" style="margin-left: 40px;">
                                        <tr>
                                            <td style="vertical-align: middle !important; width: 12%; font-weight: bold;" valign="bottom">
                                                <asp:Label ID="lblOD_Pachymetry" runat="server" Text="OD:"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important; width: 21%;" valign="bottom">
                                                <asp:Label ID="lblOD_PachymetryData" runat="server"></asp:Label>
                                            </td>
                                            <td style="vertical-align: middle !important; width: 12%; font-weight: bold;">
                                                <asp:Label ID="lblOS_Pachymetry" runat="server" Text="OS:"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important; width: 21%;" valign="bottom">
                                                <asp:Label ID="lblOS_PachymetryData" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
</asp:Content>
