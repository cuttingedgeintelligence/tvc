﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

using System.Data;

using CEI.Common;
using CEI.BR;

namespace CEI.Web.ViewPatientProfile
{
    public partial class ViewPatientProfile : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack )
            {
                int patientId;
                if(Request.QueryString["id"] != null && int.TryParse(Request.QueryString["id"],out patientId))
                {
                    DataRow drPatient = new PatientBR().GetPatientByIdForProfilePage(patientId);
                    if (drPatient != null)
                    {
                        if (
                            !Roles.IsUserInRole(RoleEnum.Practice.ToString())
                            ||
                            (Roles.IsUserInRole(RoleEnum.Practice.ToString()) && drPatient["practiceId"].ToString().Equals(User.Identity.Name))
                            )
                        {
                            pnlViewPatientProfile.Style.Add("display", "");
                            this.FillFrom(drPatient);
                        }
                    }
                }
            }
        }


        private void FillFrom(DataRow drPatient)
        {
            lblSelectedPatient.Text = drPatient["firstName"].ToString() + " " + drPatient["lastName"].ToString();
            lblPracticeNameData.Text = drPatient["practiceName"].ToString();
            lblDoctorNameData.Text = drPatient["doctorName"].ToString();
            lblFirstNameData.Text = drPatient["firstName"].ToString();
            lblMiddleInitialData.Text = drPatient["middleInitial"].ToString();
            lblLastNameData.Text = drPatient["lastName"].ToString();
            lblGenderData.Text = drPatient["gender"].ToString();

            lblDateOfBirthData.Text  = ((DateTime)drPatient["dateOfBirth"]).ToString("MM/dd/yyyy");

            lblOD_SphData.Text = drPatient["OD_Sphere"].ToString();
            lblOD_CylData.Text = drPatient["OD_Cylinder"].ToString();
            lblOD_Axis_Of_AstigmatismData.Text = drPatient["OD_AxisOfAstigmatism"].ToString();

            lblOS_SphData.Text = drPatient["OS_Sphere"].ToString();
            lblOS_CylData.Text = drPatient["OS_Cylinder"].ToString();
            lblOS_Axis_Of_AstigmatismData.Text = drPatient["OS_AxisOfAstigmatism"].ToString();

            lblOD_PachymetryData.Text = drPatient["OD_Pachymetry"].ToString();
            lblOS_PachymetryData.Text = drPatient["OS_Pachymetry"].ToString();

            lblInsuranceCompanyData.Text = drPatient["insuranceCompanyName"].ToString();

            if(drPatient["isActive"].ToString() == "True")
                lblIsActiveData.Text = "Yes";
            else
                lblIsActiveData.Text = "No";

        }
    }
}
