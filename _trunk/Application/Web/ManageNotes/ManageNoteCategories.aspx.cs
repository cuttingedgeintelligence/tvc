﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;

namespace CEI.Web.ManageNotes
{
    public partial class ManageNoteCategories : BasePage
    {

        #region GridView columns


        private const int GV_NOTE_CATEGORY_CATEGORY = 0;
        private const int GV_NOTE_CATEGORY_TYPE = 3;



        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataTable dtNoteTypes = new NoteTypesBR().GetAllNoteTypes();
                BindListControl<DataTable>.Bind(ddlNoteType, dtNoteTypes, "text", "id");
              
                this.BindGridView();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (ViewState["noteCategoryId"] != null) //update Note Category
                {


                    NoteCategory noteCategory = new NoteCategory();
                    noteCategory.Id = (int)ViewState["noteCategoryId"];
                    noteCategory.Category = txtCategory.Text;

                    if (new NoteCategoryBR().UpdateNoteCategory(noteCategory,int.Parse(ddlNoteType.SelectedValue.ToString())))
                    {
                        this.BindGridView();
                        this.ClearInsertUpdateForm();
                    }


                }
                else //insert Note Category
                {


                    NoteCategory noteCategory = new NoteCategory();
                    noteCategory.Category = txtCategory.Text;


                    if (new NoteCategoryBR().InsertNoteCategory(noteCategory,int.Parse(ddlNoteType.SelectedValue.ToString())))
                    {
                        this.BindGridView();
                        this.ClearInsertUpdateForm();
                    }


                }
            }
        }


        protected void gvNoteCategories_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Label lblNoteType = (Label)gvNoteCategories.Rows[e.NewEditIndex].FindControl("lblNoteType");

            ViewState["noteCategoryId"] = (int)gvNoteCategories.DataKeys[e.NewEditIndex].Value;
            btnSave.Text = "Save";
            lblSelectedNoteCategory.Text = "Update Note Category: " + gvNoteCategories.Rows[e.NewEditIndex].Cells[GV_NOTE_CATEGORY_CATEGORY].Text;
            txtCategory.Text = gvNoteCategories.Rows[e.NewEditIndex].Cells[GV_NOTE_CATEGORY_CATEGORY].Text;
            ddlNoteType.SelectedValue = lblNoteType.Text;
            upInsertUpdate.Update();

        }


        protected void gvNoteCategories_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            
            if (new NoteCategoryBR().DeleteNoteCategory((int)gvNoteCategories.DataKeys[e.RowIndex].Value))
            {

                if (ViewState["noteCategoryId"] != null && (int)gvNoteCategories.DataKeys[e.RowIndex].Value == (int)ViewState["noteCategoryId"])
                {
                    this.ClearInsertUpdateForm();
                    upInsertUpdate.Update();
                }

                this.BindGridView();

            }



        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.ClearInsertUpdateForm();
        }

        #endregion //Event handlers


        #region Private methods

        private void BindGridView()
        {
            gvNoteCategories.DataSource = new NoteCategoryBR().GetAllNoteCategories(0);
            gvNoteCategories.DataBind();
        }

        private void ClearInsertUpdateForm()
        {
            ViewState["noteCategoryId"] = null;
            lblSelectedNoteCategory.Text = "Create New Note Category";
            btnSave.Text = "Create";
            //clear form
            txtCategory.Text = string.Empty;
        }


        #endregion //Private methods
            
    }
}
