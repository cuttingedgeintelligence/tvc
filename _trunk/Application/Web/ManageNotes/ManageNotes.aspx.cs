﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;
using CEI.Framework.Encryption;

namespace CEI.Web.ManageNotes
{
    public partial class ManageNotes : BasePage
    {
        #region GridView columns


        private const int GV_NOTES_TEXT = 0;
        private const int GV_NOTES_CATEGORY = 1;


        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                DataTable dtCategories = new NoteCategoryBR().GetAllNoteCategories(0);
                BindListControl<DataTable>.BindAndAddListItem(ddlCategories, dtCategories, "category", "id", "Select", "0");
                BindListControl<DataTable>.BindAndAddListItem(ddlNoteCategoriesSearch, dtCategories, "category", "id", "All", "-1");
                //BindListControl<DataTable>.Bind(ddlNoteCategory, dtCategories, "ctagoty", "id");
                ViewState["categoryId"] = ddlNoteCategoriesSearch.SelectedValue;

                DataTable dtNoteTypes = new NoteTypesBR().GetAllNoteTypes();
                BindListControl<DataTable>.BindAndAddListItem(ddlNoteType, dtNoteTypes, "text", "id","Select","0");
                BindListControl<DataTable>.BindAndAddListItem(ddlNoteTypeSearch, dtNoteTypes, "text", "id", "All", "-1");
                ViewState["noteTypeId"] = ddlNoteTypeSearch.SelectedValue;

                
              

                this.BindGridViewWithSearchResults();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            ViewState["categoryId"] = ddlNoteCategoriesSearch.SelectedValue;
            ViewState["noteTypeId"] = ddlNoteTypeSearch.SelectedValue;


            this.BindGridViewWithSearchResults();
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (ViewState["noteId"] != null) //update note
                {
                    Note note = new Note();
                    note.Id = (int)ViewState["noteId"];
                    note.Text = txtNote.Text;
                    note.CategoryId = int.Parse(ddlCategories.SelectedValue);
                    DataTable selectedCategory = new NoteCategoryBR().GetNoteCategoriesById(int.Parse(ddlCategories.SelectedValue));


                    if (new NoteBR().UpdateNote(note, int.Parse(selectedCategory.Rows[0]["noteTypeId"].ToString())))
                    {
                        this.BindGridViewWithSearchResults();
                        this.ClearInsertUpdateForm();
                    }                   
                }
                else //insert note
                {
                    Note note = new Note();
                    note.Text = txtNote.Text;
                    note.CategoryId = int.Parse(ddlCategories.SelectedValue);

                    DataTable selectedCategory = new NoteCategoryBR().GetNoteCategoriesById(int.Parse(ddlCategories.SelectedValue));

                    if (new NoteBR().InsertNote(note,int.Parse(selectedCategory.Rows[0]["noteTypeId"].ToString())))
                    {
                        this.BindGridViewWithSearchResults();
                        this.ClearInsertUpdateForm();
                    }

                }
            }
        }


        protected void gvNotes_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvNotes.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvNotes_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvNotes.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvNotes.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvNotes.PageIndex == (gvNotes.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }


        protected void gvNotes_RowEditing(object sender, GridViewEditEventArgs e)
        {

            int noteId = (int)gvNotes.DataKeys[e.NewEditIndex].Values["id"];
            ViewState["noteId"] = noteId;
            btnSave.Text = "Save";

            lblSelectedNote.Text = "Update note: " + gvNotes.Rows[e.NewEditIndex].Cells[GV_NOTES_TEXT].Text;
            txtNote.Text = gvNotes.Rows[e.NewEditIndex].Cells[GV_NOTES_TEXT].Text;

            ddlCategories.SelectedValue = gvNotes.DataKeys[e.NewEditIndex].Values["categoryId"].ToString();

            upInsertUpdate.Update();
        }


        protected void gvNotes_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            if (new NoteBR().DeleteNote((int)gvNotes.DataKeys[e.RowIndex].Values["id"],int.Parse(ViewState["categoryId"].ToString())))
            {
                if (ViewState["noteId"] != null && (int)gvNotes.DataKeys[e.RowIndex].Values["id"] == (int)ViewState["noteId"])
                {
                    this.ClearInsertUpdateForm();
                    upInsertUpdate.Update();
                }

                this.BindGridViewWithSearchResults();
            }

        }


        protected void gvNotes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvNotes.PageIndex = e.NewPageIndex;
            gvNotes.DataSource = odsNotes;
            gvNotes.DataBind();

        }

        protected void odsNotes_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["categoryId"] = ViewState["categoryId"].ToString();
            e.InputParameters["noteTypeId"] = ViewState["noteTypeId"].ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.ClearInsertUpdateForm();
        }

        #endregion //Event handlers



        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvNotes.PageIndex = 0;
            gvNotes.DataSource = odsNotes;
            gvNotes.DataBind();
        }

        private void ClearInsertUpdateForm()
        {
            ViewState["noteId"] = null;
            lblSelectedNote.Text = "Create new note";
            btnSave.Text = "Create";
            //clear form
            txtNote.Text = string.Empty;
            ddlCategories.SelectedIndex = -1;
        }

        #endregion //Private methods

        protected void ddlNoteType_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataTable dtCategories = new NoteCategoryBR().GetAllNoteCategories(int.Parse(ddlNoteType.SelectedValue.ToString()));
            BindListControl<DataTable>.Bind(ddlCategories, dtCategories, "category", "id");

            upInsertUpdate.Update();
               
            
        }

        


    }
}
