﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="True"
    CodeBehind="ManageNoteCategories.aspx.cs" Inherits="CEI.Web.ManageNotes.ManageNoteCategories"
    Title="Manage Note Categories" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Note Categories" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <div style="vertical-align: bottom; margin-bottom: 10px; overflow: hidden;">
        <asp:UpdatePanel ID="upInsertUpdate" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                <fieldset class="fieldsetLeft">
                    <legend>
                        <asp:Label ID="lblSelectedNoteCategory" runat="server" Text="Create New Note Category"></asp:Label>
                    </legend>
                    <asp:Panel ID="pnlSaveNoteCategory" runat="server" DefaultButton="btnSave">
                        <table border="0">
                            <tr>
                                <td colspan="4">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="Note Type:"></asp:Label>&nbsp;
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlNoteType" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2" style="height: 11px;">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblCategory" runat="server" Text="Note Category:"></asp:Label>&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCategory" runat="server" MaxLength="200"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCategory" runat="server" ControlToValidate="txtCategory"
                                        Display="Dynamic" ErrorMessage="Please enter Note Category" ValidationGroup="vgManageNoteCategory"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 11px;">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" OnClientClick="__defaultFired = false;"
                                        Text="Create" UseSubmitBehavior="False" ValidationGroup="vgManageNoteCategory" />
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                                        OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upNoteCategories" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:GridView ID="gvNoteCategories" runat="server" AllowPaging="False" PageSize="10"
                AutoGenerateColumns="False" DataKeyNames="id" SkinID="gridViewAdmin" OnRowEditing="gvNoteCategories_RowEditing"
                Width="920px" Style="clear: both;" OnRowDeleting="gvNoteCategories_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="category" HeaderText="Note Category">
                        <HeaderStyle CssClass="moveLeft" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                ImageUrl="~/App_Themes/Default/icons/edit.gif" /><asp:Label ID="lblNoteType" Visible="false" runat="server" Text='<%# Eval("noteTypeId") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Default/icons/delete.gif"
                                CommandName="Delete" />
                            <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this Note Category and all Notes in this category?"
                                TargetControlID="imgBtnDelete">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="tdActions" />
                    </asp:TemplateField>
                   </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
