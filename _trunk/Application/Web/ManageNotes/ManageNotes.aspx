﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ManageNotes.aspx.cs" Inherits="CEI.Web.ManageNotes.ManageNotes" Title="Manage Notes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"
    style="width: 100%;">
    <asp:Label ID="lblHeaderText" runat="server" Text="Manage Notes" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <div style="margin: 0 0 10px 0; width: 920px; overflow: hidden;">
        <div style="float: left; width: 450px;">
            <asp:UpdatePanel ID="upInsertUpdate" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <fieldset class="fieldsetLeft" style="width:450px;">
                        <legend>
                            <asp:Label ID="lblSelectedNote" runat="server" Text="Create New Note"></asp:Label>
                        </legend>
                        <asp:Panel ID="pnlSaveNote" runat="server" DefaultButton="btnSave">
                            <table border="0">
                                <tr>
                                    <td colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Note Type:"></asp:Label>&nbsp;
                                    </td>
                                    <td style="padding-left: 5px;">
                                        <asp:DropDownList ID="ddlNoteType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNoteType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px">
                                        <asp:Label ID="lblNote" runat="server" Text="Note: "></asp:Label>
                                    </td>
                                    <td style="padding-left: 5px; padding-top: 10px">
                                        <asp:TextBox ID="txtNote" runat="server" MaxLength="200"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvNote" runat="server" ControlToValidate="txtNote"
                                            Display="Dynamic" ErrorMessage="Please enter Note" ValidationGroup="vgManageNote"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px">
                                        <asp:Label ID="lblCategory" runat="server" Text="Category: "></asp:Label>
                                    </td>
                                    <td style="padding-top: 10px; padding-left: 5px">
                                        <asp:DropDownList ID="ddlCategories" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px">
                                    </td>
                                    <td style="padding-left: 5px; padding-top: 10px">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="vgManageNote"
                                            Display="Dynamic" ErrorMessage="Please select Category" InitialValue="0" ControlToValidate="ddlCategories"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 6px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td style="padding-top: 10px; padding-left: 5px">
                                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" OnClientClick="__defaultFired = false;"
                                            Text="Save" UseSubmitBehavior="False" ValidationGroup="vgManageNote" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                                            OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </fieldset>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlNoteType" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div style="float: right; width: 433px;">
            <fieldset class="fieldsetLeft" style="">
                <legend>
                    <asp:Label ID="lblSearchHeader" runat="server" Text="Filter Notes By Category"></asp:Label></legend>
                <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                    <table class="style1">
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblNoteCategorySearch" runat="server" Text="Category: "></asp:Label>
                            </td>
                            <td style="padding-left: 5px">
                                <asp:DropDownList ID="ddlNoteCategoriesSearch" runat="server" Style="margin: 5px 0;">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Note Type:"></asp:Label>&nbsp;
                            </td>
                            <td style="padding-left: 5px;">
                                <asp:DropDownList ID="ddlNoteTypeSearch" runat="server" Style="margin: 5px 0;">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td style="padding-top: 10px; padding-left: 5px">
                                <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Filter"
                                    OnClientClick="__defaultFired = false;" UseSubmitBehavior="False" OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </fieldset>
        </div>
    </div>
    <asp:UpdatePanel ID="upNotes" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:GridView ID="gvNotes" runat="server" AllowPaging="True" PageSize="10" AutoGenerateColumns="False"
                DataKeyNames="id,categoryId" SkinID="gridViewAdmin" OnRowEditing="gvNotes_RowEditing"
                Width="920px" Style="clear: both;" OnRowDeleting="gvNotes_RowDeleting" OnPageIndexChanging="gvNotes_PageIndexChanging"
                OnDataBound="gvNotes_DataBound" OnRowCreated="gvNotes_RowCreated">
                <Columns>
                    <asp:BoundField DataField="text" HeaderStyle-Width="440px" HeaderText="Note">
                        <HeaderStyle CssClass="moveLeft" />
                    </asp:BoundField>
                    <asp:BoundField DataField="noteType" HeaderText="Note Type" HeaderStyle-Width="180px">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="category" HeaderText="Category">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                ImageUrl="~/App_Themes/Default/icons/edit.gif" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Default/icons/delete.gif"
                                CommandName="Delete" />
                            <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this Note?"
                                TargetControlID="imgBtnDelete">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="tdActions" />
                    </asp:TemplateField>
                </Columns>
                <PagerTemplate>
                    <div style="padding: 5px;">
                        <div>
                            <asp:Button CommandArgument="Prev" CommandName="Page" Text="Previous" ID="btnPrevious"
                                runat="server" meta:resourcekey="ibPrevResource1" />&nbsp;
                            <asp:Button CommandArgument="Next" CommandName="Page" Text="Next" ID="btnNext" runat="server"
                                meta:resourcekey="ibNextResource1" />
                        </div>
                        <span id="spannumeric" runat="server" class="spannumeric"></span>
                    </div>
                </PagerTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsNotes" runat="server" EnablePaging="True" MaximumRowsParameterName="pageSize"
                OnSelecting="odsNotes_Selecting" SelectCountMethod="GetNotesManageSearchTotalCount"
                SelectMethod="GetNotesManageSearch" TypeName="CEI.BR.NoteBR">
                <SelectParameters>
                    <asp:Parameter Name="categoryId" />
                    <asp:Parameter Name="noteTypeId" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
