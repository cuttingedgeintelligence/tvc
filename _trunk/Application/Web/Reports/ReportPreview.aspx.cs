﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Collections.Generic;
using CEI.Common;
using Microsoft.Reporting.WebForms;

namespace CEI.Web.Reports
{
    public partial class ReportPreview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                object _requestName = Request.QueryString["ReportName"];

                if (_requestName != null)
                    GenerateReport(_requestName.ToString());
            }
        }

        private void GenerateReport(string reportName)
        {
            if (reportName.Equals("PatientLog"))
            {
                TotalVisionReport _totalVisionReport =  Session["PrintingParams"] as TotalVisionReport;

                List<PatientLogPatient> patientsList = _totalVisionReport.ListItems as List<PatientLogPatient>;

                ReportDataSource reportDataSource = new ReportDataSource("PatientLogPatient", patientsList);

                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/rptPatientLog.rdlc");
                rptViewer.LocalReport.DataSources.Add(reportDataSource);
                rptViewer.LocalReport.SetParameters(ReportParams(_totalVisionReport.Pameters));
                rptViewer.ZoomMode = ZoomMode.PageWidth;
                rptViewer.LocalReport.Refresh();
            }
            else if (reportName.Equals("CompletedWorksheet"))
            {
                TotalVisionReport _totalVisionReport = Session["PrintingParams"] as TotalVisionReport;

                List<WorksheetPatient> patientsList = _totalVisionReport.ListItems as List<WorksheetPatient>;

                ReportDataSource reportDataSource = new ReportDataSource("WorksheetPatient", patientsList);

                rptViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/CompletedWorksheet.rdlc");
                rptViewer.LocalReport.DataSources.Add(reportDataSource);
                rptViewer.LocalReport.SetParameters(ReportParams(_totalVisionReport.Pameters));
                rptViewer.ZoomMode = ZoomMode.PageWidth;
                rptViewer.LocalReport.Refresh();
            }
        }

        private IEnumerable<ReportParameter> ReportParams(Dictionary<string, object> passedParameters)
        {
            int paramCount = passedParameters.Count;

            ReportParameter[] param = new ReportParameter[paramCount];

            int count = 0;

            foreach (string s in passedParameters.Keys)
            {
                if (count.Equals(paramCount))
                {
                    break;
                }
                else
                {
                    param[count] = new ReportParameter(s, passedParameters[s].ToString());
                    count++;
                }
            }

            return param.AsEnumerable<ReportParameter>();

        }
    }
}
