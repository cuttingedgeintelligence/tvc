﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true" CodeBehind="ReportPreview.aspx.cs" Inherits="CEI.Web.Reports.ReportPreview" Title="Report Preview" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%" 
        BorderColor="#336699" BorderStyle="Groove" SizeToReportContent="True">
    </rsweb:ReportViewer>
</asp:Content>
