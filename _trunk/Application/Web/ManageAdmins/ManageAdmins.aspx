﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ManageAdmins.aspx.cs" Inherits="CEI.Web.ManageAdmins.ManageAdmins"
    Title="Manage Admins" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Manage Admins" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <div style="vertical-align: bottom; margin-bottom: 10px; overflow: hidden;">
        <asp:UpdatePanel ID="upInsertUpdate" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                <fieldset class="fieldsetLeft">
                    <legend>
                        <asp:Label ID="lblSelectedUser" runat="server" Text="Create New Admin Account"></asp:Label></legend>
                    <asp:Panel ID="pnlSaveAdmin" runat="server" DefaultButton="btnSave">
                        <table border="0">
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblTitle" runat="server" Text="Title: "></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTitle" runat="server">
                                        <asp:ListItem Text="Mr." Value="Mr."></asp:ListItem>
                                        <asp:ListItem Text="Ms." Value="Ms."></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFirstName" runat="server" Text="First Name: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                        Display="Dynamic" ErrorMessage="Please enter first name" EnableClientScript="false"
                                        ValidationGroup="vgManageAdmin"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblLastName" runat="server" Text="Last Name: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                        Display="Dynamic" ErrorMessage="Please enter last name" ValidationGroup="vgManageAdmin"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblEmail" runat="server" Text="User Name (e-mail):"></asp:Label>&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                        Display="Dynamic" ErrorMessage="Please enter user name (e-mail)" ValidationGroup="vgManageAdmin"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                        Display="Dynamic" ErrorMessage="Please enter valid email address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ValidationGroup="vgManageAdmin"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPassword" runat="server" Text="Password: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                        Display="Dynamic" ErrorMessage="Please enter the password" ValidationGroup="vgManageAdmin"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm Password:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtConfirmPassword" runat="server" MaxLength="50" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                                        Display="Dynamic" ErrorMessage="Please confirm the password" ValidationGroup="vgManageAdmin"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cmpvConfirmPassword" runat="server" ControlToCompare="txtPassword"
                                        ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Enter same value for password in both fields"
                                        ValidationGroup="vgManageAdmin"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAccountIsActive" runat="server" Text="Active Account:"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbAccountIsActive" runat="server" Checked="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td style="padding: 10px 0;">
                                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" OnClientClick="__defaultFired = false;"
                                        Text="Create" UseSubmitBehavior="False" ValidationGroup="vgManageAdmin" />
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                                        OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
        <fieldset class="fieldsetLeft" style="float: right;">
            <legend>
                <asp:Label ID="lblSearchHeader" runat="server" Text="Search Ultimate Admin Accounts"></asp:Label></legend>
            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <table class="style1">
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px;">
                            <asp:Label ID="lblTitleSearch" runat="server" Text="Title:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlTitleSearch" runat="server">
                                <asp:ListItem Text="All" Value="-1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Mr." Value="Mr."></asp:ListItem>
                                <asp:ListItem Text="Ms." Value="Ms."></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFirstNameSearch" runat="server" Text="First Name:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFirstNameSearch" runat="server" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblLastNameSearch" runat="server" Text="Last Name:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastNameSearch" runat="server" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="padding-top: 10px;">
                            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Search"
                                OnClientClick="__defaultFired = false;" UseSubmitBehavior="False" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </fieldset>
    </div>
    <asp:UpdatePanel ID="upUsers" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:GridView ID="gvUsers" runat="server" AllowPaging="True" PageSize="10" AutoGenerateColumns="False"
                DataKeyNames="id" OnPageIndexChanging="gvUsers_PageIndexChanging" SkinID="gridViewAdmin"
                OnRowEditing="gvUsers_RowEditing" Width="920px" Style="clear: both;" 
                OnRowDeleting="gvUsers_RowDeleting" ondatabound="gvUsers_DataBound" 
                onrowcreated="gvUsers_RowCreated">
                <Columns>
                    <asp:BoundField DataField="firstName" HeaderText="First Name">
                        <HeaderStyle CssClass="moveLeft" Width="440px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="lastName" HeaderText="Last Name">
                        <HeaderStyle CssClass="moveLeft" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                ImageUrl="~/App_Themes/Default/icons/edit.gif" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Default/icons/delete.gif"
                                CommandName="Delete" />
                            <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this admin account?"
                                TargetControlID="imgBtnDelete">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="tdActions" />
                    </asp:TemplateField>
                </Columns>
                     <PagerTemplate>
                    <div style="padding: 5px;">
                        <div>
                            <asp:Button CommandArgument="Prev" CommandName="Page" Text="Previous" ID="btnPrevious"
                                runat="server" meta:resourcekey="ibPrevResource1" />&nbsp;
                            <asp:Button CommandArgument="Next" CommandName="Page" Text="Next" ID="btnNext" runat="server"
                                meta:resourcekey="ibNextResource1" />
                        </div>
                        <span id="spannumeric" runat="server" class="spannumeric"></span>
                    </div>
                </PagerTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsUsers" runat="server" EnablePaging="True" MaximumRowsParameterName="pageSize"
                OnSelecting="odsUsers_Selecting" SelectCountMethod="GetAdminsManageSearchTotalCount"
                SelectMethod="GetAdminsManageSearch" TypeName="CEI.BR.UserBR">
                <SelectParameters>
                    <asp:Parameter Name="roleId" />
                    <asp:Parameter Name="title" />
                    <asp:Parameter Name="firstName" />
                    <asp:Parameter Name="lastName" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
