﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.BR;
using CEI.Common;
using CEI.Framework.Encryption;

namespace CEI.Web.ManageAdmins
{
    public partial class ManageAdmins : BasePage
    {

        #region GridView columns


        private const int GV_USERS_FIRST_NAME = 0;
        private const int GV_USERS_LAST_NAME = 1;
        private const int GV_USERS_EDIT = 2;
        private const int GV_USERS_DELETE = 3;

        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {

                ViewState["firstName"] = txtFirstNameSearch.Text.Trim();
                ViewState["lastName"] = txtLastNameSearch.Text.Trim();
                ViewState["title"] = ddlTitleSearch.SelectedValue;

                this.BindGridViewWithSearchResults();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            
            ViewState["firstName"] = txtFirstNameSearch.Text.Trim();
            ViewState["lastName"] = txtLastNameSearch.Text.Trim();
            ViewState["title"] = ddlTitleSearch.SelectedValue;
           
            this.BindGridViewWithSearchResults();
        }


       


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (ViewState["userId"] != null) //update admin
                {
                    if (!new UserBR().ExistsUserWithEmailOnUpdate((Guid)ViewState["userId"], txtEmail.Text.Trim()))
                    {

                        User admin = new User();
                        admin.UserId = (Guid)ViewState["userId"];
                        admin.FirstName = txtFirstName.Text;
                        admin.LastName = txtLastName.Text;
                        admin.Email = txtEmail.Text.Trim();
                        admin.Password = txtPassword.Text;
                        admin.AccountIsEnabled = cbAccountIsActive.Checked;
                        admin.Title = ddlTitle.SelectedValue;

                        if (new UserBR().UpdateAdmin(admin))
                        {
                            this.BindGridViewWithSearchResults();
                            this.ClearInsertUpdateForm();
                            
                        }
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, typeof(ManageAdmins), "existsUserWithSameEmailScript", "alert('User with same user name(email) already exists');", true);

                }
                else //insert admin
                {
                    if (!new UserBR().ExistsUserWithEmail(txtEmail.Text.Trim()))
                    {

                        User admin = new User();
                        admin.UserId = Guid.NewGuid();
                        admin.FirstName = txtFirstName.Text;
                        admin.LastName = txtLastName.Text;
                        admin.Email = txtEmail.Text.Trim();
                        admin.Password = txtPassword.Text;
                        admin.AccountIsEnabled = cbAccountIsActive.Checked;
                        admin.Title = ddlTitle.SelectedValue;
                        admin.CreatedBy = new Guid(User.Identity.Name);

                        if (new UserBR().InsertAdmin(admin))
                        {
                            this.BindGridViewWithSearchResults();
                            this.ClearInsertUpdateForm();
                        }
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this, typeof(ManageAdmins), "existsUserWithSameEmailScript", "alert('User with same user name(email) already exists');", true);
                    
                }
            }
        }

        protected void gvUsers_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvUsers.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvUsers_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvUsers.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvUsers.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvUsers.PageIndex == (gvUsers.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }


        protected void gvUsers_RowEditing(object sender, GridViewEditEventArgs e)
        {
            
            Guid userId = (Guid)gvUsers.DataKeys[e.NewEditIndex].Value;
            ViewState["userId"] = userId;
            btnSave.Text = "Save";
            DataRow drAdmin = new UserBR().GetAdminById(userId);

            lblSelectedUser.Text = "Update Admin: " + drAdmin["firstName"].ToString() + " " + drAdmin["lastName"].ToString() ;

        
            txtFirstName.Text = drAdmin["firstName"].ToString();
            txtLastName.Text = drAdmin["lastName"].ToString();
            txtEmail.Text = drAdmin["email"].ToString();
            string password = new EncryptDecrypt().Decrypt(drAdmin["password"].ToString(), string.Empty);
            txtPassword.Attributes.Add("value", password);
            txtConfirmPassword.Attributes.Add("value",password);
            cbAccountIsActive.Checked = (bool)drAdmin["accountIsEnabled"];
            ddlTitle.SelectedValue = drAdmin["title"].ToString(); 


            upInsertUpdate.Update();
            
        }


        protected void gvUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            if (new UserBR().DeleteUser((Guid)gvUsers.DataKeys[e.RowIndex].Value,RoleEnum.Ultimate__admin))
            {

                if (ViewState["userId"] != null && (Guid)gvUsers.DataKeys[e.RowIndex].Value == (Guid)ViewState["userId"])
                {
                    this.ClearInsertUpdateForm();
                    upInsertUpdate.Update();
                }


                this.BindGridViewWithSearchResults();
            }
            
        }


        protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUsers.PageIndex = e.NewPageIndex;
            gvUsers.DataSource = odsUsers;
            gvUsers.DataBind();

        }

        protected void odsUsers_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["roleId"] = (int)RoleEnum.Ultimate__admin;
            e.InputParameters["firstName"] = ViewState["firstName"].ToString();
            e.InputParameters["lastName"] = ViewState["lastName"].ToString();
            e.InputParameters["title"] = ViewState["title"].ToString();
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
           this.ClearInsertUpdateForm();
        }



        #endregion //Event handlers

        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvUsers.PageIndex = 0;
            gvUsers.DataSource = odsUsers;
            gvUsers.DataBind();
        }

        private void ClearInsertUpdateForm()
        {
            ViewState["userId"] = null;
            lblSelectedUser.Text = "Create new admin account";
            btnSave.Text = "Create";
            //clear form
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtPassword.Attributes.Remove("value");
            txtConfirmPassword.Text = string.Empty;
            txtConfirmPassword.Attributes.Remove("value");
            ddlTitle.SelectedIndex = -1;
            cbAccountIsActive.Checked = true;
        }

        #endregion //Private methods

        

      

        
    }
}
