﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ManageCPTCodes.aspx.cs" Inherits="CEI.Web.ManageCPTCodes.ManageCPTCodes"
    Title="Manage CPT Codes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Manage CPT Codes" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <div style="vertical-align: bottom; margin-bottom: 10px; overflow: hidden;">
        <asp:UpdatePanel ID="upInsertUpdate" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                <fieldset class="fieldsetLeft">
                    <legend>
                        <asp:Label ID="lblSelectedCPTCode" runat="server" Text="Create New CPT Code"></asp:Label>
                    </legend>
                    <asp:Panel ID="pnlSaveCPTCode" runat="server" DefaultButton="btnSave">
                        <table border="0">
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <asp:Label ID="lblCode" runat="server" Text="CPT Code: "></asp:Label>
                                </td>
                                <td style ="padding-left:10px;">
                                    <asp:TextBox ID="txtCode" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtCode"
                                        Display="Dynamic" ErrorMessage="Please Enter CPT Code" ValidationGroup="vgManageCPTCode"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style ="padding-top:10px">
                                    <asp:Label ID="lblType" runat="server" Text="Test Type: "></asp:Label>
                                </td>
                                <td style ="padding-left:5px; padding-top:10px">
                                    <asp:RadioButtonList ID="rblType" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True">Glaucoma</asp:ListItem>
                                        <asp:ListItem>Retina</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td style ="padding-top:10px">
                                    <asp:Label ID="lblDescription" runat="server" Text="Description: "></asp:Label>
                                </td>
                                <td style ="padding-left:10px; padding-top:10px">
                                    <asp:TextBox ID="txtDescription" runat="server" MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" 
                                        ControlToValidate="txtDescription" Display="Dynamic" 
                                        ErrorMessage="Please Enter CPT Code Description" 
                                        ValidationGroup="vgManageCPTCode"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td style ="padding-left:10px; padding-top:10px">
                                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" 
                                        OnClientClick="__defaultFired = false;" Text="Create" UseSubmitBehavior="False" 
                                        ValidationGroup="vgManageCPTCode" />
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" 
                                        OnClick="btnCancel_Click" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upCPTCodes" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:GridView ID="gvCPTCodes" runat="server" AutoGenerateColumns="False"
                DataKeyNames="id" SkinID="gridViewAdmin" OnRowEditing="gvCPTCodes_RowEditing"
                Width="920px" Style="clear: both;" OnRowDeleting="gvCPTCodes_RowDeleting" 
                onrowdatabound="gvCPTCodes_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="code" HeaderText="CPT Code"  >
                        <ItemStyle Width="20%" HorizontalAlign="Center" />
                    </asp:BoundField>
                     <asp:TemplateField HeaderText="Test Type">
                        <ItemTemplate>
                        </ItemTemplate>
                         <ItemStyle Width="20%" HorizontalAlign="Center" />
                     </asp:TemplateField>
                    <asp:BoundField DataField="description" HeaderText="Description" >
                     <ItemStyle  HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                ImageUrl="~/App_Themes/Default/icons/edit.gif" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Default/icons/delete.gif"
                                CommandName="Delete" />
                            <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this CPT code?"
                                TargetControlID="imgBtnDelete">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="tdActions" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
