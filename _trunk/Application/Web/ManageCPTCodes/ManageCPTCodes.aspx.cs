﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;

namespace CEI.Web.ManageCPTCodes
{
    public partial class ManageCPTCodes : BasePage
    {

        #region GridView columns


        private const int GV_CPT_CODES_CODE = 0;
        private const int GV_CPT_CODES_TYPE = 1;
        private const int GV_CPT_CODES_DESCRIPTION = 2;

        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridView();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                CPTCode cptCode = new CPTCode();
                cptCode.Code = txtCode.Text;
                cptCode.Description = txtDescription.Text;
                cptCode.CodeType = (CPTCodeTypeEnum)Enum.Parse(typeof(CPTCodeTypeEnum), rblType.SelectedValue, true);

                if (ViewState["cptCodeId"] != null) //update CPT code
                {


                   
                    cptCode.Id = (int)ViewState["cptCodeId"];
                  


                    if (new CPTCodeBR().UpdateCPTCode(cptCode))
                    {
                        this.BindGridView();
                        this.ClearInsertUpdateForm();
                    }


                }
                else //insert CPT Code
                {


                    if (new CPTCodeBR().InsertCPTCode(cptCode))
                    {
                        this.BindGridView();
                        this.ClearInsertUpdateForm();
                    }


                }
            }
        }


        protected void gvCPTCodes_RowEditing(object sender, GridViewEditEventArgs e)
        {
            ViewState["cptCodeId"] = (int)gvCPTCodes.DataKeys[e.NewEditIndex].Value;
            btnSave.Text = "Save";
            lblSelectedCPTCode.Text = "Update CPT Code: " + gvCPTCodes.Rows[e.NewEditIndex].Cells[GV_CPT_CODES_CODE].Text;
            txtCode.Text = gvCPTCodes.Rows[e.NewEditIndex].Cells[GV_CPT_CODES_CODE].Text; 
            txtDescription.Text = gvCPTCodes.Rows[e.NewEditIndex].Cells[GV_CPT_CODES_DESCRIPTION].Text;
            rblType.SelectedValue = gvCPTCodes.Rows[e.NewEditIndex].Cells[GV_CPT_CODES_TYPE].Text;
            upInsertUpdate.Update();

        }


        protected void gvCPTCodes_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            //Check if CPT Code is assigned to some patient in some worksheet document
            if (new WorksheetBR().ExistsWorksheetPatientWithCPTCodeId((int)gvCPTCodes.DataKeys[e.RowIndex].Value))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManageCPTCodes), "existsPatientWithCPTCodeId", "alert('You cannot delete this CPT code because is already assigned to a patient in a worksheet document');", true);
                return;
            }


            if (new CPTCodeBR().DeleteCPTCode((int)gvCPTCodes.DataKeys[e.RowIndex].Value))
            {

                if (ViewState["cptCodeId"] != null && (int)gvCPTCodes.DataKeys[e.RowIndex].Value == (int)ViewState["cptCodeId"])
                {
                    this.ClearInsertUpdateForm();
                    upInsertUpdate.Update();
                }

                this.BindGridView();

            }



        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.ClearInsertUpdateForm();
        }

        #endregion //Event handlers


        #region Private methods

        private void BindGridView()
        {
            gvCPTCodes.DataSource = new CPTCodeBR().GetAllCPTCodes();
            gvCPTCodes.DataBind();
        }

        private void ClearInsertUpdateForm()
        {
            ViewState["cptCodeId"] = null;
            lblSelectedCPTCode.Text = "Create New CPT Code";
            btnSave.Text = "Create";
            //clear form
            txtCode.Text = string.Empty;
            txtDescription.Text = string.Empty;
            rblType.SelectedValue = CPTCodeTypeEnum.Glaucoma.ToString();
        }


        #endregion //Private methods

        protected void gvCPTCodes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drCPTCode = e.Row.DataItem as DataRowView;

                e.Row.Cells[GV_CPT_CODES_TYPE].Text =
               ((CPTCodeTypeEnum)Enum.Parse(typeof(CPTCodeTypeEnum), drCPTCode["codeType"].ToString())).ToString();
              
              
            }
        }
    }
}
