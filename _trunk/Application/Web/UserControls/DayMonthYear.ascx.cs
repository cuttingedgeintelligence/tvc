﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


public partial class DayMonthYear : System.Web.UI.UserControl
{
    //int year, month;
    private int _day;
    private int _month;
    private int _year;
    private int _yearFrom;
    private int _yearTo;

    public int Day
    {
        get
        {
            return int.Parse(this.ddlDay.SelectedValue);
        }
		set
		{
			this._day = value;
		}
    }
    
    public int Month
    {
        get
        {
            return int.Parse(this.ddlMonth.SelectedValue);
        }
		set
		{
			this._month = value;
		}
	}
    
    public int Year
    {
        get
        {
            return int.Parse(this.ddlYear.SelectedValue);
        }
		set
		{
			this._year = value;
		}
	}

    public DateTime Date
    {
        get
        {
            return new DateTime(this.Year, this.Month, this.Day);
        }
    }
    
    public int YearFrom
    {
        get
        {
            return _yearFrom;
        }
        set
        {
            this._yearFrom = value;
        }
    }

    public int YearTo
    {
        get
        {
            return _yearTo;
        }
        set
        {
            this._yearTo = value;
        }
    }

    public void SetDate(DateTime date)
    {
        ddlYear.SelectedValue = date.Year.ToString();
        ddlMonth.SelectedValue = date.Month.ToString();
        BindDays(date.Year,date.Month);
        ddlDay.SelectedValue = date.Day.ToString();
    }


    public void SetYearFromYearTo(int yearFromData, int yearToData)
    {
        this.YearFrom = yearFromData;
        this.YearTo = yearToData;
    }


    public void SetInitDate(DateTime date)
    {
        this._year = date.Year;
        this._month = date.Month;
        this._day = date.Day;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!this.IsPostBack)
        {

           
            ArrayList AlYear = new ArrayList();

            for (int i = this.YearFrom; i <= this.YearTo; i++)
                AlYear.Add(i);
            
           
			ddlYear.DataSource = AlYear;
			ddlYear.DataBind();

            if (_year == 0) //init date was not set
            {
                DateTime tnow = DateTime.Today;
                ddlYear.SelectedValue = tnow.Year.ToString();
                ddlMonth.SelectedValue = tnow.Month.ToString();
                _year = int.Parse(ddlYear.SelectedValue);
                _month = int.Parse(ddlMonth.SelectedValue);
                BindDays(_year, _month);
                ddlDay.SelectedValue = tnow.Day.ToString();
                _day = int.Parse(ddlDay.SelectedValue);
            }
            else
            {
                
                ddlYear.SelectedValue = this._year.ToString();
                ddlMonth.SelectedValue = this._month.ToString();
                BindDays(this._year,this. _month);
                ddlDay.SelectedValue = this._day.ToString();
            }
          
        }
      

    }
   
    //binding every month day
    private void BindDays(int year, int month)
    {
        int i;
        ArrayList AlDay = new ArrayList();
        
        switch (month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                for (i = 1; i <= 31; i++)
                    AlDay.Add(i);
                break;
            case 2:
                if (DateTime.IsLeapYear(year))
                {
                    for (i = 1; i <= 29; i++)
                        AlDay.Add(i);
                }
                else
                {
                    for (i = 1; i <= 28; i++)
                        AlDay.Add(i);
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                for (i = 1; i <= 30; i++)
                    AlDay.Add(i);
                break;
        }
        ddlDay.DataSource = AlDay;
        ddlDay.DataBind();
    }
    //select year
    public void ddlYear_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (ddlMonth.SelectedValue == "2") //February
        {
            string selectedDay = ddlDay.SelectedValue;
            BindDays(int.Parse(ddlYear.SelectedValue), int.Parse(ddlMonth.SelectedValue));
            if  (      !(  
                        selectedDay == "29" 
                        && 
                        !DateTime.IsLeapYear(int.Parse(ddlYear.SelectedValue)) 
                         )  
                )
                ddlDay.SelectedValue = selectedDay;

        }
    }
    //select month
    public void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
    {
       
        BindDays(int.Parse(ddlYear.SelectedValue),int.Parse(ddlMonth.SelectedValue));
    }
}
