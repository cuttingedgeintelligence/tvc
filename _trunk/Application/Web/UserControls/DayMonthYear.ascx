﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="DayMonthYear" CodeBehind="DayMonthYear.ascx.cs" %>
<div style="width: 325px;">
    <asp:UpdatePanel runat="server" ID="upCalendar" UpdateMode="Conditional">
        <ContentTemplate>
            &nbsp;&nbsp;<asp:Label runat="server" ID="lblMonth" Text="Month:" />
            <asp:DropDownList ID="ddlMonth" runat="server" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged"
                AutoPostBack="True">
                <asp:ListItem Value="1">January</asp:ListItem>
                <asp:ListItem Value="2">February</asp:ListItem>
                <asp:ListItem Value="3">March</asp:ListItem>
                <asp:ListItem Value="4">April</asp:ListItem>
                <asp:ListItem Value="5">May</asp:ListItem>
                <asp:ListItem Value="6">June</asp:ListItem>
                <asp:ListItem Value="7">July</asp:ListItem>
                <asp:ListItem Value="8">August</asp:ListItem>
                <asp:ListItem Value="9">September</asp:ListItem>
                <asp:ListItem Value="10">October</asp:ListItem>
                <asp:ListItem Value="11">November</asp:ListItem>
                <asp:ListItem Value="12">December</asp:ListItem>
            </asp:DropDownList>
            <asp:Label runat="server" ID="lblDay" Text="Day:" />
            <asp:DropDownList ID="ddlDay" runat="server" AutoPostBack="False">
            </asp:DropDownList>
            <asp:Label runat="server" ID="lblYear" Text="Year:" />
            <asp:DropDownList ID="ddlYear" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged"
                AutoPostBack="True">
            </asp:DropDownList>
          
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
