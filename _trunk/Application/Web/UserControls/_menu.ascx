﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_menu.ascx.cs" Inherits="CEI.Web.UserControls._menu" %>
<script type="text/javascript" src='<%= this.MenuScript %>' ></script>
<asp:Menu ID="menuAdmin" runat="server" DataSourceID="xmlDataSourceMenu" DynamicEnableDefaultPopOutImage="False"
     MaximumDynamicDisplayLevels="10"  DynamicVerticalOffset="5" 
    StaticSubMenuIndent="0px" Orientation="Horizontal"  
      CssClass="divNav"  StaticEnableDefaultPopOutImage="False" 
    ItemWrap="True" BorderStyle="None" 
    onmenuitemdatabound="menuAdmin_MenuItemDataBound" 
   >

    
    
    <DataBindings>
        <asp:MenuItemBinding DataMember="MenuItem"  NavigateUrlField="Url" TextField="Title"
            SelectableField="IsSelectable"    ValueField="Permissions" />
    </DataBindings>
</asp:Menu>
<asp:XmlDataSource ID="xmlDataSourceMenu"  runat="server" 
    DataFile="~/App_Data/Menu.xml"  >
</asp:XmlDataSource>
