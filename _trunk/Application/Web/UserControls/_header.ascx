﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_header.ascx.cs" Inherits="CEI.Web.UserControls._header" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="_menu.ascx" TagName="_menu" TagPrefix="uc1" %>
<div id="header">
    <div style="width: 900px; height: 114px;">
        <%--<asp:Image ID="imgLogo" ImageUrl="~/App_Themes/Default/images_new/logo.png" runat="server"
            CssClass="imgLogo" />--%>
    </div>
    <%--<div id="divSearchHolder">
        <div class="below_search_box">
            <div style="padding-top: 0px; height: 52px;">
                <asp:LoginStatus ID="lsLogout" runat="server" LogoutAction="RedirectToLoginPage"
                    Style="text-decoration: underline; cursor: pointer; display: none;" OnLoggedOut="lsLogout_LoggedOut" />
                <br />
            </div>
        </div>
    </div>--%>
    <div style="clear: both; float: none;">
    </div>
    <div id="divNavHolder" style="padding: 15px 0 0 10px; margin: 0; width: 820px;">
        <uc1:_menu ID="ucMenu" runat="server" />
    </div>
</div>
