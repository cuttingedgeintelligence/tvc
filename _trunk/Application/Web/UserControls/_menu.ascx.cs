﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace CEI.Web.UserControls
{
    public partial class _menu : System.Web.UI.UserControl
    {

        public string MenuScript
        {
            get
            {
                return "http://" + Request.Url.Host + ResolveUrl("~/MenuJquery/ddsmoothmenu.js");
            }
        }

        private string[] _userPermissions;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                menuAdmin.Visible = true;

                //string[] userPermissions = Roles.GetRolesForUser();
                this._userPermissions = Roles.GetRolesForUser();

                foreach (string userPermission in _userPermissions)
                    xmlDataSourceMenu.XPath += "MenuItems/MenuItem[contains(@Permissions,'" + userPermission + "') ] |";

                if (xmlDataSourceMenu.XPath.Length > 0)
                    xmlDataSourceMenu.XPath = xmlDataSourceMenu.XPath.TrimEnd('|');
                else
                    xmlDataSourceMenu.XPath = "SelectNothing"; //Note: empty menu will be shown



                string initMenuScript = @"ddsmoothmenu.init({
            	mainmenuid: '" + menuAdmin.ClientID + @"' , 
            	
            	contentsource: 'markup' 
            });";



                ScriptManager.RegisterStartupScript(this.Page, typeof(string), "InitMenuScript", initMenuScript, true);

            }
            else
            {
                menuAdmin.Visible = false;
            }
        }

        protected void menuAdmin_MenuItemDataBound(object sender, MenuEventArgs e)
        {

            if (e.Item.Depth > 0)
            {
                bool hasPermission = false;
                foreach (string userPermission in _userPermissions)
                {
                    if (e.Item.Value.IndexOf(userPermission) != -1)
                    {
                        
                        hasPermission = true;
                        break;
                    }
                }

                if(!hasPermission)
                    e.Item.Parent.ChildItems.Remove(e.Item);


                         
            }

           

            
            
        }

       

      
    }
}