<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ManagePatients.aspx.cs" Inherits="CEI.Web.ManagePatients.ManagePatients"
    Title="Manage Patients" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControls/DayMonthYear.ascx" TagName="DayMonthYear" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Manage Patients" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <div style="vertical-align: bottom; margin-bottom: 10px; overflow: hidden;">
        <asp:UpdatePanel ID="upInsertUpdate" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                <fieldset class="fieldsetLeft" style="width: 440px;">
                    <legend>
                        <asp:Label ID="lblSelectedPatient" runat="server" Text="Add New Patient"></asp:Label>
                    </legend>
                    <asp:Panel ID="pnlSavePatient" runat="server" DefaultButton="btnSave">
                        <table border="0">
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPractice" runat="server" Text="Practice: "></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlPractice" runat="server" Style="margin: 5px 0;" Width="200px"
                                        OnSelectedIndexChanged="ddlPractice_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDoctor" runat="server" Text="Doctor: "></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDoctros" runat="server" Style="margin: 5px 0;" DataValueField="Id"
                                        DataTextField="Name" Width="200px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFirstName" Style="width: 50px;" runat="server" Text="First Name: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                        Display="Dynamic" ErrorMessage="Please enter first name" ValidationGroup="vgManagePatient"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblMiddleInitial" runat="server" Text="Middle Initial: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMiddleInitial" runat="server" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblLastName" runat="server" Text="Last Name: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                        Display="Dynamic" ErrorMessage="Please enter last name" ValidationGroup="vgManagePatient"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 10px">
                                    <asp:Label ID="lblGender" runat="server" Text="Gender: "></asp:Label>
                                </td>
                                <td style="padding-top: 10px">
                                    <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True">Female</asp:ListItem>
                                        <asp:ListItem>Male</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px">
                                </td>
                                <td style="height: 10px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDateOfBirth" runat="server" Text="Date Of Birth:"></asp:Label>
                                </td>
                                <td>
                                    <uc1:DayMonthYear ID="dateOfBirth" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px">
                                </td>
                                <td style="height: 10px">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 10px">
                                    <asp:Label ID="lblManifestRefraction" runat="server" Text="Manifest Refraction:"></asp:Label>
                                </td>
                                <td style="padding-top: 10px">
                                    <asp:RadioButtonList ID="rdbManifestRefraction" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="False">Yes</asp:ListItem>
                                        <asp:ListItem Selected="False">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:RegularExpressionValidator ID="revOD_Sph" runat="server" Display="Dynamic" ControlToValidate="txtOD_Sph"
                                        ErrorMessage="Enter + or - for OD: Sph" ValidationGroup="vgManagePatient" ValidationExpression="^(((\+|\-)((\d*[0-9])(\.\d*[0-9])?)))|(0)|(no va)$"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                                        ControlToValidate="txtOD_Cyl" ErrorMessage="Enter + or - for OD: Cyl" ValidationGroup="vgManagePatient"
                                        ValidationExpression="^((\+|\-)((\d*[0-9]|)(\.\d*[0-9])?))$"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic"
                                        ControlToValidate="txtOS_Sph" ErrorMessage="Enter + or - for OS: Sph" ValidationGroup="vgManagePatient"
                                        ValidationExpression="^(((\+|\-)((\d*[0-9])(\.\d*[0-9])?)))|(0)|(no va)$"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="Dynamic"
                                        ControlToValidate="txtOS_Cyl" ErrorMessage="Enter + or - for OS: Cyl" ValidationGroup="vgManagePatient"
                                        ValidationExpression="^((\+|\-)((\d*[0-9]|)(\.\d*[0-9])?))$"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table width="70%" style="margin-left: 50px;">
                                        <tr>
                                            <td valign="bottom" style="vertical-align: middle !important;">
                                                <asp:Label ID="lblMR_OD" runat="server" Text="OD:"></asp:Label>
                                                <asp:Label ID="lblOD_sph" runat="server" Text="sph"></asp:Label>
                                            </td>
                                            <td valign="bottom" style="vertical-align: bottom !important;">
                                                <asp:TextBox ID="txtOD_Sph" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                                            </td>
                                            <td valign="bottom" style="vertical-align: middle !important;">
                                                <asp:Label ID="lblOD_Cyl" runat="server" Text="cyl" Style="margin-right: 3px;"></asp:Label>
                                            </td>
                                            <td valign="bottom" style="vertical-align: bottom !important;">
                                                <asp:TextBox ID="txtOD_Cyl" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                                            </td>
                                            <td valign="bottom" style="vertical-align: middle !important;">
                                                <asp:Label ID="lblOD_Sep" runat="server" Text="x" Style="margin-right: 3px;"></asp:Label>
                                            </td>
                                            <td valign="bottom" style="vertical-align: bottom !important;">
                                                <asp:TextBox ID="txtOD_Axis_Of_Astigmatism" runat="server" MaxLength="4" Width="50px"></asp:TextBox>
                                                <%--    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtOD_Axis_Of_Astigmatism" ErrorMessage="Enter + or - for OS: " ValidationGroup="vgManagePatient"
                                                    ValidationExpression="^((\+|\-)(\d*[0-9](\.\d*[0-9])?))$"></asp:RegularExpressionValidator>
                                   --%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle !important;">
                                                <asp:Label ID="lblMR_OS" runat="server" Text="OS:"></asp:Label>
                                                <asp:Label ID="lblOS_sph" runat="server" Text="sph"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important;" valign="bottom">
                                                <asp:TextBox ID="txtOS_Sph" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                                            </td>
                                            <td style="vertical-align: middle !important;">
                                                <asp:Label ID="lblOS_Cyl" runat="server" Text="cyl" Style="margin-right: 3px;"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important;" valign="bottom">
                                                <asp:TextBox ID="txtOS_Cyl" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                                            </td>
                                            <td style="vertical-align: middle !important;">
                                                <asp:Label ID="lblOS_Sep" runat="server" Text="x" Style="margin-right: 3px;"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important;" valign="bottom">
                                                <asp:TextBox ID="txtOS_Axis_Of_Astigmatism" runat="server" MaxLength="4" Width="50px"></asp:TextBox>
                                                <%--        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtOS_Axis_Of_Astigmatism" ErrorMessage="Enter + or -" ValidationGroup="vgManagePatient"
                                                    ValidationExpression="^((\+|\-)(\d*[0-9](\.\d*[0-9])?))$"></asp:RegularExpressionValidator>
                                     --%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPachymetry" runat="server" Text="Pachymetry:"></asp:Label>
                                </td>
                                <td>
                                    <asp:RegularExpressionValidator ID="revOD_Pachymetry" runat="server" ControlToValidate="txtOD_Pachymetry"
                                        Display="Dynamic" ErrorMessage="Pachymetry OD value must be a number" ValidationExpression="\d*"
                                        ValidationGroup="vgManagePatient"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="revOD_Pachymetry0" runat="server" ControlToValidate="txtOS_Pachymetry"
                                        Display="Dynamic" ErrorMessage="Pachymetry OS value must be a number" ValidationExpression="\d*"
                                        ValidationGroup="vgManagePatient"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left">
                                    <table width="260px" style="margin-left: 55px;">
                                        <tr>
                                            <td style="vertical-align: middle !important; width: 21%;" valign="bottom">
                                                <asp:Label ID="lblOD_Pachymetry" runat="server" Text="OD:"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important; width: 23%;" valign="bottom">
                                                <asp:TextBox ID="txtOD_Pachymetry" runat="server" MaxLength="3" Width="50px"></asp:TextBox>
                                            </td>
                                            <td style="vertical-align: middle !important; width: 10%;">
                                                <asp:Label ID="lblOS_Pachymetry" runat="server" Text="OS:"></asp:Label>
                                            </td>
                                            <td style="vertical-align: bottom !important; width: 46%;" valign="bottom">
                                                <asp:TextBox ID="txtOS_Pachymetry" runat="server" MaxLength="3" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 10px">
                                    <asp:Label ID="lblInsuranceCompany" runat="server" Text="Insurance: "></asp:Label>
                                </td>
                                <td style="padding-top: 10px; padding-left: 5px">
                                    <asp:DropDownList ID="ddlInsuranceCompany" runat="server" Style="margin: 5px 0;"
                                        Width="200px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvInsurance" runat="server" ControlToValidate="ddlInsuranceCompany"
                                        Display="Dynamic" ErrorMessage="Please select insurance" ValidationGroup="vgManagePatient"
                                        InitialValue="-1"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 10px">
                                    <asp:Label ID="lblIsActive" runat="server" Text="Active Patient: "></asp:Label>
                                </td>
                                <td style="padding-top: 10px">
                                    <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td style="padding-top: 10px">
                                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" OnClientClick="__defaultFired = false;"
                                        Text="Add" UseSubmitBehavior="False" ValidationGroup="vgManagePatient" />
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                                        OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
        <fieldset class="fieldsetLeft" style="float: right; width: 420px;">
            <legend>
                <asp:Label ID="lblSearchHeader" runat="server" Text="Search Patients"></asp:Label></legend>
            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <table>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFirstNameSearch" runat="server" Text="First Name:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFirstNameSearch" runat="server" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblLastNameSearch" runat="server" Text="Last Name:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastNameSearch" runat="server" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblBirthDate" runat="server" Text="Date Of Birth:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtBirthDate" runat="server" MaxLength="50" ValidationGroup="vgSearchPatients"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="tbweSearchName" WatermarkText="MM/DD/YYYY" runat="server"
                                TargetControlID="txtBirthDate">
                            </cc1:TextBoxWatermarkExtender>
                            <%--<input type="text" id="inputBirthDate" runat="server" onfocus="" value="CLICK HERE" />--%>
                            <asp:CompareValidator ID="cvBirthDate" runat="server" ErrorMessage="Date Format: MM/DD/YYYY"
                                ControlToValidate="txtBirthDate" ValidationGroup="vgSearchPatients" Operator="DataTypeCheck"
                                Type="Date"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px;">
                            <asp:Label ID="lblPracticeSearch" runat="server" Text="Practice:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlPracticeSearch" runat="server" Style="margin: 5px 0;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="padding-top: 10px">
                            <asp:Button ID="btnSearch" runat="server" CausesValidation="True" Text="Search" ValidationGroup="vgSearchPatients"
                                OnClientClick="__defaultFired = false;" UseSubmitBehavior="False" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </fieldset>
    </div>
    <asp:Button ID="btnTemp" runat="server" Text="View history" Style="display: none;" />
    <asp:UpdatePanel ID="upPatients" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvPatients" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                DataKeyNames="id,firstName,lastName" OnPageIndexChanging="gvPatients_PageIndexChanging"
                SkinID="gridViewAdmin" OnRowEditing="gvPatients_RowEditing" Width="920px" Style="clear: both;"
                OnRowDeleting="gvPatients_RowDeleting" 
                OnRowDataBound="gvPatients_RowDataBound" ondatabound="gvPatients_DataBound" 
                onrowcreated="gvPatients_RowCreated">
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="id" DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}"
                        DataTextField="firstName" HeaderText="First Name">
                        <HeaderStyle CssClass="moveLeft" HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:HyperLinkField DataTextField="lastName" HeaderText="Last Name" DataNavigateUrlFields="id"
                        DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}">
                        <HeaderStyle CssClass="moveLeft" HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:BoundField DataField="dateOfBirth" HeaderText="Date Of Birth" DataFormatString="{0:MM/dd/yyyy}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="insuranceCompanyName" HeaderText="Insurance">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Button ID="btnHistory" runat="server" CausesValidation="False" Text="View History" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                ImageUrl="~/App_Themes/Default/icons/edit.gif" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Default/icons/delete.gif"
                                CommandName="Delete" />
                            <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this patient?"
                                TargetControlID="imgBtnDelete">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="tdActions" />
                    </asp:TemplateField>
                </Columns>
                     <PagerTemplate>
                    <div style="padding: 5px;">
                        <div>
                            <asp:Button CommandArgument="Prev" CommandName="Page" Text="Previous" ID="btnPrevious"
                                runat="server" meta:resourcekey="ibPrevResource1" />&nbsp;
                            <asp:Button CommandArgument="Next" CommandName="Page" Text="Next" ID="btnNext" runat="server"
                                meta:resourcekey="ibNextResource1" />
                        </div>
                        <span id="spannumeric" runat="server" class="spannumeric"></span>
                    </div>
                </PagerTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsPatients" runat="server" EnablePaging="True" MaximumRowsParameterName="pageSize"
                OnSelecting="odsPatients_Selecting" SelectCountMethod="GetPatientsManageSearchTotalCount"
                SelectMethod="GetPatientsManageSearch" TypeName="CEI.BR.PatientBR">
                <SelectParameters>
                    <asp:Parameter Name="practiceId" />
                    <asp:Parameter Name="firstName" />
                    <asp:Parameter Name="lastName" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
