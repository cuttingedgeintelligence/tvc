﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.BR;
using CEI.Framework;
using CEI.Common;
using Microsoft.Reporting.WebForms;

namespace CEI.Web.ManagePatients
{
    public partial class ManagePatients : BasePage
    {

        #region GridView columns


        private const int GV_PATIENTS_FIRST_NAME = 0;
        private const int GV_PATIENTS_LAST_NAME = 1;
        private const int GV_PATIENTS_DATE_OF_BIRTH = 2;
        private const int GV_PATIENTS_INSURANCE_COMPANY = 3;
        private const int GV_PATIENTS_HISTORY = 4;
        private const int GV_PATIENTS_EDIT = 5;
        private const int GV_PATIENTS_DELETE = 6;

        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                BindListControl<DataTable>.BindAndAddListItem(ddlInsuranceCompany, new NameValueBR().GetAllInsuranceCompanies(), "name", "id","Select Insurance","-1");
                DataTable dtPractices = new UserBR().GetAllPracticesNameAndId();
                BindListControl<DataTable>.Bind(ddlPractice, dtPractices, "name", "id");
                BindListControl<DataTable>.BindAndAddListItem(ddlPracticeSearch, dtPractices, "name", "id", "All", Guid.Empty.ToString());




                if(Roles.IsUserInRole(RoleEnum.Practice.ToString()))
                {
                    lblPractice.Style.Add("display","none");
                    lblPracticeSearch.Style.Add("display", "none");
                    ddlPracticeSearch.SelectedValue = User.Identity.Name;
                    ddlPracticeSearch.Style.Add("display", "none");
                    ddlPractice.SelectedValue = User.Identity.Name;
                    ddlPractice.Style.Add("display","none");
                   
                }


                //request from manage patient log page : create new patient functionality
                if (Request.QueryString["prId"] != null)
                {
                    ddlPractice.SelectedValue =  ddlPracticeSearch.SelectedValue = Request.QueryString["prId"];
                    
                }

                ViewState["firstName"] = txtFirstNameSearch.Text.Trim();
                ViewState["lastName"] = txtLastNameSearch.Text.Trim();
                ViewState["practiceId"] = ddlPracticeSearch.SelectedValue;
                ViewState["dateOfBirth"] = "";

                this.BindGridViewWithSearchResults();

             //   dateOfBirth.SetYearFromYearTo(DateTime.Today.Year - 100, DateTime.Today.Year);
                dateOfBirth.SetYearFromYearTo(1900, DateTime.Today.Year);

                Guid practiceId = new Guid(ddlPractice.SelectedValue);
                this.BindDoctorsDropDownList(practiceId);
            }


            if (Request.Form["__EVENTARGUMENT"] != null && Request.Form["__EVENTARGUMENT"].StartsWith("viewreport#"))
            {
                //Request.Form["__EVENTARGUMENT"] == viewreport#patientId#patientName
                string[] patientIdAndName = Request.Form["__EVENTARGUMENT"].Split('#');

                this.ViewReport(int.Parse(patientIdAndName[1]), patientIdAndName[2], patientIdAndName[3]);
            }
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ViewState["firstName"] = txtFirstNameSearch.Text.Trim();
                ViewState["lastName"] = txtLastNameSearch.Text.Trim();
                ViewState["practiceId"] = ddlPracticeSearch.SelectedValue;
                ViewState["dateOfBirth"] = txtBirthDate.Text; 

                this.BindGridViewWithSearchResults();
            }
        }

       


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                bool isChacked = false;
                bool letsGo = false;
                if ((!rdbManifestRefraction.Items[0].Selected) && (!rdbManifestRefraction.Items[1].Selected))
                    ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePatients), "existsUserWithCountyId", "alert('You must pick Yes or No from Manifest Refraction:');", true);
                else
                {
                    if (rdbManifestRefraction.Items[0].Selected)
                    {
                        if ((txtOD_Sph.Text != "") || (txtOD_Cyl.Text != "") || (txtOD_Axis_Of_Astigmatism.Text != "") || (txtOS_Sph.Text != "") || (txtOS_Cyl.Text != "") || (txtOS_Axis_Of_Astigmatism.Text != ""))
                        {
                            isChacked = true;

                        }
                        if (!isChacked)
                            ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePatients), "existsUserWithCountyId", "alert('You must select at least one from Manifest Refraction:');", true);
                        else letsGo = true;                       
                    }
                    else
                    {
                         if (rdbManifestRefraction.Items[1].Selected)
                             letsGo = true;
                    }

                }
                if (letsGo)
                {
                    if (ViewState["patientId"] != null) //update patient
                    {

                        Patient patient = new Patient();
                        patient.Id = (int)ViewState["patientId"];

                        patient.FirstName = txtFirstName.Text;
                        patient.LastName = txtLastName.Text;
                        patient.MiddleInitial = txtMiddleInitial.Text;
                        patient.Gender = rblGender.SelectedValue;
                        patient.DateOfBirth = new DateTime(dateOfBirth.Year, dateOfBirth.Month, dateOfBirth.Day);

                        patient.OS_Sphere = txtOS_Sph.Text;
                        patient.OD_Sphere = txtOD_Sph.Text;
                        patient.OD_AxisOfAstigmatism = txtOS_Axis_Of_Astigmatism.Text;
                        patient.OS_AxisOfAstigmatism = txtOD_Axis_Of_Astigmatism.Text;
                        patient.OS_Cylinder = txtOS_Cyl.Text;
                        patient.OD_Cylinder = txtOD_Cyl.Text;
                        if (txtOD_Pachymetry.Text.Trim() != string.Empty)
                            patient.OD_Pachymetry = short.Parse(txtOD_Pachymetry.Text);
                        if (txtOS_Pachymetry.Text.Trim() != string.Empty)
                            patient.OS_Pachymetry = short.Parse(txtOS_Pachymetry.Text);
                        patient.TVC_IdCode = txtFirstName.Text.Substring(0, 1) + txtLastName.Text.Substring(0, 1) + "-" + patient.DateOfBirth.ToString("MMddyy");

                        patient.InsuranceCompanyId = int.Parse(ddlInsuranceCompany.SelectedValue);
                        patient.PracticeId = new Guid(ddlPractice.SelectedValue);
                        patient.DoctorId = int.Parse(ddlDoctros.SelectedValue);

                        patient.IsActive = chkIsActive.Checked;

						if (new PatientBR().UpdatePatient(patient))
						{
							this.BindGridViewWithSearchResults();
							this.ClearInsertUpdateForm();

							ScriptManager.RegisterStartupScript(this, typeof(ManagePatients), "patientSuccessfullyUpdated", "alert('Patient information has been successfully updated');", true);

						}
						else
						{
							ScriptManager.RegisterStartupScript(this, typeof(ManagePatients), "patientNOTSuccessfullyUpdated", "alert('Problems creating the patient. Please try again or contact the administrator');", true);
						}
                    }
                    else //insert patient
                    {


                        Patient patient = new Patient();

                        patient.FirstName = txtFirstName.Text;
                        patient.LastName = txtLastName.Text;
                        patient.MiddleInitial = txtMiddleInitial.Text;
                        patient.Gender = rblGender.SelectedValue;
                        patient.DateOfBirth = new DateTime(dateOfBirth.Year, dateOfBirth.Month, dateOfBirth.Day);

                        patient.OS_Sphere = txtOS_Sph.Text;
                        patient.OD_Sphere = txtOD_Sph.Text;
                        patient.OD_AxisOfAstigmatism = txtOS_Axis_Of_Astigmatism.Text;
                        patient.OS_AxisOfAstigmatism = txtOD_Axis_Of_Astigmatism.Text;
                        patient.OS_Cylinder = txtOS_Cyl.Text;
                        patient.OD_Cylinder = txtOD_Cyl.Text;
                        if (txtOD_Pachymetry.Text.Trim() != string.Empty)
                            patient.OD_Pachymetry = short.Parse(txtOD_Pachymetry.Text);
                        if (txtOS_Pachymetry.Text.Trim() != string.Empty)
                            patient.OS_Pachymetry = short.Parse(txtOS_Pachymetry.Text);

                        patient.TVC_IdCode = txtFirstName.Text.Substring(0, 1) + txtLastName.Text.Substring(0, 1) + "-" + patient.DateOfBirth.ToString("MMddyy");

                        patient.InsuranceCompanyId = int.Parse(ddlInsuranceCompany.SelectedValue);
                        patient.PracticeId = new Guid(ddlPractice.SelectedValue);
                        patient.DoctorId = int.Parse(ddlDoctros.SelectedValue);
                        patient.CreatedBy = new Guid(User.Identity.Name);

                        patient.IsActive = chkIsActive.Checked;

                        if (new PatientBR().InsertPatient(patient))
                        {
                            //request from manage patient log page : create new patient functionality => redirect to manage patient log page
                            if (Request.QueryString["prId"] != null && patient.PracticeId.ToString() == Request.QueryString["prId"] && Request.QueryString["serviceDate"] != null && Request.QueryString["action"] != null)
                            {
                                if (Request.QueryString["action"] == "create")
                                    Response.Redirect("~/PatientLogs/ManagePatientLog.aspx?action=create&prId=" + patient.PracticeId.ToString() + "&prName=" + ddlPractice.SelectedItem.Text + "&servDate=" + Request.QueryString["serviceDate"] + "&patientId=" + patient.Id.ToString(), true);
                                else if (Request.QueryString["action"] == "update" && Request.QueryString["logId"] != null)
                                    Response.Redirect("~/PatientLogs/ManagePatientLog.aspx?action=update&prId=" + patient.PracticeId.ToString() + "&prName=" + ddlPractice.SelectedItem.Text + "&servDate=" + Request.QueryString["serviceDate"] + "&logId=" + Request.QueryString["logId"] + "&patientId=" + patient.Id.ToString(), true);
                            }
                            else
                            {
                                this.BindGridViewWithSearchResults();
                                this.ClearInsertUpdateForm();

								ScriptManager.RegisterStartupScript(this, typeof(ManagePatients), "patientSuccessfullyInserted", "alert('The patient has been created');", true);
                            }

                        }


                    }
                }
               
            }
        }


        protected void gvPatients_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvPatients.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvPatients_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvPatients.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvPatients.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvPatients.PageIndex == (gvPatients.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }


        protected void gvPatients_RowEditing(object sender, GridViewEditEventArgs e)
        {

            int patientId = (int)gvPatients.DataKeys[e.NewEditIndex].Values["id"];
            ViewState["patientId"] = patientId;
            btnSave.Text = "Save";
            DataRow drPatient = new PatientBR().GetPatientById(patientId);

            lblSelectedPatient.Text = "Update Patient: " + drPatient["firstName"].ToString() + " " + drPatient["lastName"].ToString();


            txtFirstName.Text = drPatient["firstName"].ToString();
            txtLastName.Text = drPatient["lastName"].ToString();
            txtMiddleInitial.Text = drPatient["middleInitial"].ToString();
            rblGender.SelectedValue = drPatient["gender"].ToString();
            DateTime dateOfBirthData = (DateTime)drPatient["dateOfBirth"];
            dateOfBirth.SetDate(dateOfBirthData);
             
            txtOS_Sph.Text = drPatient["OS_Sphere"].ToString();
            txtOD_Sph.Text = drPatient["OD_Sphere"].ToString();
            txtOS_Axis_Of_Astigmatism.Text = drPatient["OS_axisOfAstigmatism"].ToString();
            txtOD_Axis_Of_Astigmatism.Text = drPatient["OD_AxisOfAstigmatism"].ToString();
            txtOS_Cyl.Text = drPatient["OS_Cylinder"].ToString();
            txtOD_Cyl.Text = drPatient["OD_Cylinder"].ToString();
        
            txtOD_Pachymetry.Text = drPatient["OD_Pachymetry"].ToString();
            txtOS_Pachymetry.Text = drPatient["OS_Pachymetry"].ToString();
          
           
       
            ddlInsuranceCompany.SelectedValue =  drPatient["insuranceCompanyId"].ToString();
            ddlPractice.SelectedValue =  drPatient["practiceId"].ToString();
            ddlPractice.Enabled = false;
            
            Guid practiceId = new Guid(ddlPractice.SelectedValue);
            this.BindDoctorsDropDownList(practiceId);
            ddlDoctros.SelectedValue = drPatient["doctorId"] != DBNull.Value ?  drPatient["doctorId"].ToString() : "-1";

            chkIsActive.Checked = (bool)drPatient["IsActive"];

            upInsertUpdate.Update();

			//scroll page to top
			ScriptManager.RegisterStartupScript(Page, typeof(ManagePatients),
		 "ScrollToTop", @"
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
                function pageLoaded(sender, args)
                {
                     scroll(0,0);
                     Sys.WebForms.PageRequestManager.getInstance().remove_pageLoaded(pageLoaded);
                }", true);
        }


        protected void gvPatients_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            if (new PatientLogBR().ExistsPatientLogPatientWithPatientId((int)gvPatients.DataKeys[e.RowIndex].Values["id"]))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePatients), "existsPatientLogPatientWithPatientId", "alert('You cannot delete this patient because is already assigned to a patient schedule');", true);
                return;
            }


            if (new PatientBR().DeletePatient((int)gvPatients.DataKeys[e.RowIndex].Values["id"]))
            {
                if (ViewState["patientId"] != null && (int)gvPatients.DataKeys[e.RowIndex].Values["id"] == (int)ViewState["patientId"])
                {
                    this.ClearInsertUpdateForm();
                    upInsertUpdate.Update();
                }

                this.BindGridViewWithSearchResults();
            }

        }


        protected void gvPatients_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            
            gvPatients.PageIndex = e.NewPageIndex;
            gvPatients.DataSource = odsPatients;
            gvPatients.DataBind();

        }

        protected void odsPatients_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["practiceId"] = ViewState["practiceId"];
            e.InputParameters["firstName"] = ViewState["firstName"].ToString();
            e.InputParameters["lastName"] = ViewState["lastName"].ToString();
            e.InputParameters["dateOfBirth"] = ViewState["dateOfBirth"].ToString(); 
         
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.ClearInsertUpdateForm();
        }

        protected void ddlPractice_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid practiceId = new Guid(((DropDownList)sender).SelectedValue);
            this.BindDoctorsDropDownList(practiceId);
        }

        #endregion //Event handlers

        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvPatients.PageIndex = 0;
            gvPatients.DataSource = odsPatients;
            gvPatients.DataBind();
        }

        private void ClearInsertUpdateForm()
        {
            ViewState["patientId"] = null;
            lblSelectedPatient.Text = "Add New Patient";
            btnSave.Text = "Add";
            //clear form
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtMiddleInitial.Text = string.Empty;
            rblGender.SelectedIndex = 0;
            dateOfBirth.SetDate(DateTime.Today);

            txtOS_Sph.Text = string.Empty;
            txtOD_Sph.Text = string.Empty;
            txtOS_Axis_Of_Astigmatism.Text = string.Empty;
            txtOD_Axis_Of_Astigmatism.Text = string.Empty;
            txtOS_Cyl.Text = string.Empty;
            txtOD_Cyl.Text = string.Empty;

            txtOS_Pachymetry.Text = string.Empty;
            txtOD_Pachymetry.Text = string.Empty;

            chkIsActive.Checked = true;

            ddlInsuranceCompany.SelectedIndex = -1;

            rdbManifestRefraction.Items[0].Selected = false;
            rdbManifestRefraction.Items[1].Selected = false;

            if (!Roles.IsUserInRole(RoleEnum.Practice.ToString()))
            {
                ddlPractice.SelectedIndex = -1;
                ddlPractice.Enabled = true;

                this.BindDoctorsDropDownList(new Guid(ddlPractice.SelectedValue));  

            }
        }

        private void BindDoctorsDropDownList(Guid practiceId)
        {
            ddlDoctros.DataSource =
               (new DoctorBR().GetDoctorsByPracticeId(practiceId, 1000, 0)).Rows.Cast<DataRow>()
               .Select(r => new { Name = r["firstName"] + " " + r["lastName"], Id = r["id"] }).ToList();

            

            ddlDoctros.DataBind();
            ddlDoctros.Items.Insert(0, new ListItem("Select Doctor","-1"));
        }

        #endregion //Private methods       

        

        protected void gvPatients_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drPatient =  e.Row.DataItem as DataRowView;
                Button btnHistory = e.Row.Cells[GV_PATIENTS_HISTORY].FindControl("btnHistory") as Button;
                btnHistory.Attributes["onclick"] = "__doPostBack('" + btnTemp.ClientID + "','viewreport#" + drPatient["id"].ToString() + "#" + drPatient["firstName"].ToString() + " " + drPatient["lastName"].ToString() + "#"+drPatient["practiceName"].ToString() +"');";

            }
        }

        private  void ViewReport(int patientId , string patientName, string practiceName)
        {
            DataTable dt = new FinalNotesWorksheetBR().GetPatientHistoryByPatientId(patientId, PatientLogStatusEnum.Final__notes__worksheet);


            LocalReport localReport = new LocalReport();

            localReport.ReportPath = Server.MapPath("~/Reports/rptPatientHistory.rdlc");

            ReportDataSource reportDataSource = new ReportDataSource("DataSet1_stp_PatientLogs_PatientLogsPatients_GetPatientHistoryByPatientId", dt);
            localReport.DataSources.Add(reportDataSource);

            ReportParameter[] param = new ReportParameter[2];
            param[0] = new ReportParameter("rptParamPatientName", patientName);
            param[1] = new ReportParameter("rptParamPracticeName", practiceName);
           // localReport.GetParameters();
           // param[1] = new ReportParameter("rptParamPracticeName", lblServiceDateData.Text);

            localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PDF</OutputFormat>" +
            //"  <PageWidth>11in</PageWidth>" +
            //"  <PageHeight>8.5in</PageHeight>" +
            //"  <MarginTop>0.2in</MarginTop>" +
            //"  <MarginLeft>0.5in</MarginLeft>" +
            //"  <MarginRight>0.1in</MarginRight>" +
            //"  <MarginBottom>0.2in</MarginBottom>" +
            //"</DeviceInfo>";


            string deviceInfo =
    "<DeviceInfo>" +
    "  <OutputFormat>PDF</OutputFormat>" +
    "  <PageWidth>11in</PageWidth>" +
    "  <PageHeight>6in</PageHeight>" +
    "  <MarginTop>0.2in</MarginTop>" +
    "  <MarginLeft>0.6in</MarginLeft>" +
    "  <MarginRight>0.1in</MarginRight>" +
    "  <MarginBottom>0.2in</MarginBottom>" +
    "</DeviceInfo>";


            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=PatientHistory." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        
    }
}
