﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;

namespace CEI.Web.ReportLogs
{
    public partial class ViewReportLogs : BasePage 
    {
        #region GridView columns


        private const int GV_REPORTLOGS_PRACTICE_NAME = 0;
        private const int GV_REPORTLOGS_DATE = 1;
        private const int GV_REPORTLOGS_VIEW = 2;
       



        #endregion //GridView columns


        #region Event handlers


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (Roles.IsUserInRole(RoleEnum.Practice.ToString()))
                {
                    ViewState["practiceId"] = new Guid(User.Identity.Name);
                    searchPanel.Visible = false;
                }
                else
                {
                    BindListControl<DataTable>.BindAndAddListItem(ddlPracticesSearch, new UserBR().GetAllPracticesNameAndId(), "name", "id", "All", Guid.Empty.ToString());
                    ViewState["practiceId"] = ddlPracticesSearch.SelectedValue;
                }

                if (Roles.IsUserInRole(RoleEnum.Technician.ToString()))
                    ViewState["technicianId"] = new Guid(User.Identity.Name);
                else
                    ViewState["technicianId"] = Guid.Empty;


                this.BindGridViewWithSearchResults();
            }
        }

        protected void gvReportLogs_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvReportLogs.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvReportLogs_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvReportLogs.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvReportLogs.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvReportLogs.PageIndex == (gvReportLogs.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }

        protected void gvReportLogs_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("viewlogpatients"))
            {
                Response.Redirect("~/ReportLogs/ViewReportLogPatients.aspx?logId=" + gvReportLogs.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["Id"].ToString() + "&prName=" + gvReportLogs.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["PracticeName"].ToString() + "&servDate=" + gvReportLogs.Rows[int.Parse(e.CommandArgument.ToString())].Cells[GV_REPORTLOGS_DATE].Text, true);
            }

          

            


        }

     


        protected void gvReportLogs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvReportLogs.PageIndex = e.NewPageIndex;
            gvReportLogs.DataSource = odsReportLogs;
            gvReportLogs.DataBind();

        }

        protected void odsReportLogs_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["technicianId"] = ViewState["technicianId"];
            e.InputParameters["practiceId"] = ViewState["practiceId"];
            e.InputParameters["status"] = PatientLogStatusEnum.Final__notes__worksheet; 
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {


            ViewState["practiceId"] = ddlPracticesSearch.SelectedValue;

            this.BindGridViewWithSearchResults();
        }


        #endregion //Event handlers

        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvReportLogs.PageIndex = 0;
            gvReportLogs.DataSource = odsReportLogs;
            gvReportLogs.DataBind();
        }

        #endregion //Private methods
    }
}
