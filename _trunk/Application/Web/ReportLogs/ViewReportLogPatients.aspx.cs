﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;
using System.Collections.Generic;
using Microsoft.Reporting.WebForms;


namespace CEI.Web.ReportLogs
{
    public partial class ViewReportLogPatients : BasePage 
    {
        #region GridView columns


        private const int GV_PATIENTS_NAME = 0;
        private const int GV_PATIENTS_BFA = 1;
        private const int GV_PATIENTS_HRT_GLAUCOMA = 2;
        private const int GV_PATIENTS_HRT_RETINA = 3;
        private const int GV_PATIENTS_CPT_CODES = 4;
        private const int GV_PATIENTS_STATUS = 9;
        private const int GV_PATIENTS_INTERNAL_NOTES = 5;
        private const int GV_PATIENTS_DOCTOR_NOTES = 10;
        private const int GV_PATIENTS_OD = 6;
        private const int GV_PATIENTS_OS = 7;
        private const int GV_PATIENTS_INSURANCE = 8;

        #endregion //GridView columns



        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {



            if (!Page.IsPostBack)
            {
                if (this.ValidateRequestParams())
                {

                    if (Roles.IsUserInRole(RoleEnum.Practice.ToString()))
                    {
                        gvPatients.Columns[GV_PATIENTS_INTERNAL_NOTES].Visible = false;
                    }


                    lblPracticeNameData.Text = Request.QueryString["prName"];
                    lblServiceDateData.Text = Request.QueryString["servDate"];
                    List<FinalNotesWorksheetPatient> patientsList = new FinalNotesWorksheetBR().GetFinalNotesWorksheetPatientsByPatientLogId(int.Parse(Request.QueryString["logId"]));
                    this.BingGridViewPatients(patientsList);
                    ViewState["PatientsList"] = patientsList;



                }
                else
                {
                    //TODO: show error message
                }
            }
        }









        protected void gvPatients_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                FinalNotesWorksheetPatient finalNotesWorksheetPatient = e.Row.DataItem as FinalNotesWorksheetPatient;
                if (finalNotesWorksheetPatient.Glaucoma)
                {
                   // e.Row.Cells[GV_PATIENTS_BFA].Text = finalNotesWorksheetPatient.BFA.Equals(0.5m) ? finalNotesWorksheetPatient.BFA.ToString() : decimal.Truncate(finalNotesWorksheetPatient.BFA).ToString();
                   // e.Row.Cells[GV_PATIENTS_HRT_GLAUCOMA].Text = finalNotesWorksheetPatient.HRT_Glaucoma.Equals(0.5m) ? finalNotesWorksheetPatient.HRT_Glaucoma.ToString() : decimal.Truncate(finalNotesWorksheetPatient.HRT_Glaucoma).ToString();
                    e.Row.Cells[GV_PATIENTS_BFA].Text = finalNotesWorksheetPatient.BFA.ToString();
                    e.Row.Cells[GV_PATIENTS_HRT_GLAUCOMA].Text = finalNotesWorksheetPatient.HRT_Glaucoma.ToString();
 
                }

                if (finalNotesWorksheetPatient.Retina)
                   // e.Row.Cells[GV_PATIENTS_HRT_RETINA].Text = finalNotesWorksheetPatient.HRT_Retina.Equals(0.5m) ? finalNotesWorksheetPatient.HRT_Retina.ToString() : decimal.Truncate(finalNotesWorksheetPatient.HRT_Retina).ToString(); ;
                    e.Row.Cells[GV_PATIENTS_HRT_RETINA].Text = finalNotesWorksheetPatient.HRT_Retina.ToString(); ;



                if (finalNotesWorksheetPatient.Status.Equals(PatientLogPatientStatusEnum.No__Show))
                    e.Row.Cells[GV_PATIENTS_STATUS].Text = "No Show";
                else
                    e.Row.Cells[GV_PATIENTS_STATUS].Text = finalNotesWorksheetPatient.Status.ToString();


              //  e.Row.Cells[GV_PATIENTS_STATUS].Text = finalNotesWorksheetPatient.Status.ToString();

                Label lblGlaucomaCodes = e.Row.Cells[GV_PATIENTS_CPT_CODES].FindControl("lblGlaucomaCodes") as Label;
                Label lblRetinaCodes = e.Row.Cells[GV_PATIENTS_CPT_CODES].FindControl("lblRetinaCodes") as Label;
                int i = 0;
                int j = 0;
                //finalNotesWorksheetPatient.CPT_Codes comma separeted list CodeName^CodeType
                string[] cptCodeNamesAndTypes = finalNotesWorksheetPatient.CPT_Codes.Split(',');
                foreach (string CPT_CodeNameAndType in cptCodeNamesAndTypes)
                {
                    string[] nameAndType = CPT_CodeNameAndType.Split('^');
                    //if (nameAndType[1].Equals(((int)CPTCodeTypeEnum.Glaucoma).ToString()))
                    //    lblGlaucomaCodes.Text += nameAndType[0] + "<BR/>";
                    //else
                    //    lblRetinaCodes.Text += nameAndType[0] + "<BR/>";
                    if (nameAndType[1].Equals(((int)CPTCodeTypeEnum.Glaucoma).ToString()))
                    {
                        // lblGlaucomaCodes.Text += CPT_Code.Code + "<BR/>";
                        lblGlaucomaCodes.Text += nameAndType[0] + "&nbsp;&nbsp;&nbsp;&nbsp;";
                        i++;
                    }
                    else
                    {
                        //  lblRetinaCodes.Text += CPT_Code.Code + "<BR/>";
                        lblRetinaCodes.Text += nameAndType[0] + "&nbsp;&nbsp;&nbsp;&nbsp;";
                        j++;
                    }

                    if (i == 2)
                    {
                        lblGlaucomaCodes.Text = lblGlaucomaCodes.Text.Substring(0, lblGlaucomaCodes.Text.Length - 24);
                        lblGlaucomaCodes.Text += "<BR/>";
                        i = 0;
                    }
                    if (j == 2)
                    {
                        lblRetinaCodes.Text = lblRetinaCodes.Text.Substring(0, lblRetinaCodes.Text.Length - 24);
                        lblRetinaCodes.Text += "<BR/>";
                        j = 0;
                    }
                }
                if(i==1)
                    lblGlaucomaCodes.Text = lblGlaucomaCodes.Text.Substring(0, lblGlaucomaCodes.Text.Length - 24);
                if (j == 1)
                {
                    lblRetinaCodes.Text = lblRetinaCodes.Text.Substring(0, lblRetinaCodes.Text.Length - 24);
                }

                if (string.IsNullOrEmpty(finalNotesWorksheetPatient.InternalNotes))
                    finalNotesWorksheetPatient.InternalNotes = string.Empty;

                if (string.IsNullOrEmpty(finalNotesWorksheetPatient.PracticeNotes))
                    finalNotesWorksheetPatient.PracticeNotes = string.Empty;

                (e.Row.Cells[GV_PATIENTS_INTERNAL_NOTES].FindControl("lblInternalNotes") as Label).Text = finalNotesWorksheetPatient.InternalNotes;
                (e.Row.Cells[GV_PATIENTS_DOCTOR_NOTES].FindControl("lblDoctorNotes") as Label).Text = finalNotesWorksheetPatient.PracticeNotes;

               

                

                if (finalNotesWorksheetPatient.OD_IOP != null && finalNotesWorksheetPatient.OD_BFA != null)
                    (e.Row.Cells[GV_PATIENTS_OD].FindControl("lblOD") as Label).Text = finalNotesWorksheetPatient.OD_IOP + "/ " + finalNotesWorksheetPatient.OD_BFA.ToString();
                if (finalNotesWorksheetPatient.OS_IOP != null && finalNotesWorksheetPatient.OS_BFA != null)
                    (e.Row.Cells[GV_PATIENTS_OS].FindControl("lblOS") as Label).Text = finalNotesWorksheetPatient.OS_IOP + "/ " + finalNotesWorksheetPatient.OS_BFA.ToString();
                (e.Row.Cells[GV_PATIENTS_INSURANCE].FindControl("lblInsurance") as Label).Text = finalNotesWorksheetPatient.Insurance;


                
            }
        }





        #endregion //Event handlers

        #region Private methods
        private bool ValidateRequestParams()
        {
            bool isValid = false;
            int patientLogId;
            if (

                    Request.QueryString["prName"] != null &&
                    Request.QueryString["servDate"] != null &&
                    Request.QueryString["logId"] != null &&
                    int.TryParse(Request.QueryString["logId"], out patientLogId) &&
                    this.ValidateServiceDate(Request.QueryString["servDate"])
                )
            {
                isValid = true;
                ViewState["patientLogId"] = patientLogId;
            }




            return isValid;


        }





        private bool ValidateServiceDate(string serviceDate)
        {
            try
            {
                DateTime.ParseExact(serviceDate, "M/d/yyyy", null);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }

        }


        private void BingGridViewPatients(List<FinalNotesWorksheetPatient> patientsList)
        {

            gvPatients.DataSource = patientsList;
            gvPatients.DataBind();
        }

        protected void btnPrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Pdf);
        }

        protected void btnPrintExcel_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Excel);
        }

        protected void btnPrintPreview_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Printer);
        }

        #endregion //Private methods

        #region MICROSOFT REPORTING

        private void PrintReport(ExportType exportType)
        {

            TotalVisionReport _totalVisionReport = new TotalVisionReport();

            List<FinalNotesWorksheetPatient> patientsList = ViewState["PatientsList"] as List<FinalNotesWorksheetPatient>;



            LocalReport localReport = new LocalReport();
            if (Roles.IsUserInRole(RoleEnum.Practice.ToString()))
            {
                localReport.ReportPath = Server.MapPath("~/Reports/rptFinalNotesWorksheetPractice.rdlc");
            }
            else
            {
                //localReport.ReportPath = Server.MapPath("~/Reports/rptFinalNotesWorksheet.rdlc");
                
                localReport.ReportPath = Server.MapPath("~/Reports/rptReportLog.rdlc");
            }
            
            ReportDataSource reportDataSource = new ReportDataSource("FinalNotesWorksheetPatient", patientsList);
            localReport.DataSources.Add(reportDataSource);

            ReportParameter[] param = new ReportParameter[2];
            param[0] = new ReportParameter("rptParamPracticeName", lblPracticeNameData.Text);
            param[1] = new ReportParameter("rptParamDate", lblServiceDateData.Text);

            localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            if (exportType == ExportType.Printer)
            {
                _totalVisionReport.ListItems = patientsList;
                _totalVisionReport.Pameters.Add("rptParamPracticeName", lblPracticeNameData.Text);
                _totalVisionReport.Pameters.Add("rptParamDate", lblServiceDateData.Text);
                Session["PrintingParams"] = _totalVisionReport;

                Response.Redirect("~/Reports/ReportPreview.aspx?ReportName=PatientLog");
            }
            else if (exportType == ExportType.Pdf)
            {
                DownloadPDF(localReport);

            }
            else if (exportType == ExportType.Excel)
            {
                DownloadExcel(localReport);

            }
        }

        private void DownloadPDF(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PDF</OutputFormat>" +
            //"  <PageWidth>11in</PageWidth>" +
            //"  <PageHeight>8.5in</PageHeight>" +
            //"  <MarginTop>0.2in</MarginTop>" +
            //"  <MarginLeft>0.6in</MarginLeft>" +
            //"  <MarginRight>0.1in</MarginRight>" +
            //"  <MarginBottom>0.2in</MarginBottom>" +
            //"</DeviceInfo>";


            string deviceInfo =
   "<DeviceInfo>" +
   "  <OutputFormat>PDF</OutputFormat>" +
   "  <PageWidth>11in</PageWidth>" +
   "  <PageHeight>8.5in</PageHeight>" +
   "  <MarginTop>0.2in</MarginTop>" +
   "  <MarginLeft>0.6in</MarginLeft>" +
   "  <MarginRight>0.1in</MarginRight>" +
   "  <MarginBottom>0.2in</MarginBottom>" +
   "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=ServiceHistory." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DownloadExcel(LocalReport localReport)
        {
            string reportType = "Excel";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
            "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=ServiceHistory." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }


        #endregion
    }
}
