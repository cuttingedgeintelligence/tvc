﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ViewReportLogPatients.aspx.cs" Inherits="CEI.Web.ReportLogs.ViewReportLogPatients"
    Title="Service History Patients" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Service History Patients" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <table width="400px">
        <tr>
            <td width="100px">
                <asp:Label ID="lblPracticeName" runat="server" Text="Practice Name:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblPracticeNameData" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblServiceDate" runat="server" Text="Service Date:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblServiceDateData" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="padding: 10px 0;">
        <tr>
            <td style="padding-top: 10px">
                <asp:ImageButton ID="btnPrintPDF" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png"
                    ToolTip="Download PDF" OnClick="btnPrintPDF_Click" Style="padding-right: 3px;" />
            </td>
            <td style="padding-top: 10px; padding-left: 5px">
                <asp:ImageButton ID="btnPrintExcel" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png"
                    ToolTip="Download Excel" OnClick="btnPrintExcel_Click" />
            </td>
            <td style="padding-top: 10px; padding-left: 5px">
                <asp:ImageButton ID="btnPrintPreview" runat="server" ImageUrl="~/Images/Mockups/printIcon.png"
                    ToolTip="View and Print" OnClick="btnPrintPreview_Click" Style="display: none;" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upPatients" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvPatients" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                DataKeyNames="Id,PatientId" SkinID="gridViewAdmin" Width="920px" Style="clear: both;"
                OnRowDataBound="gvPatients_RowDataBound" EmptyDataText="There are no patients in the log">
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}"
                        DataTextField="fullName" HeaderText="Patient Name">
                        <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:TemplateField HeaderText="BFA">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRT-G">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRT-R">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CPT Codes">
                        <ItemTemplate>
                            <table style="width: 315px;">
                                <tr>
                                    <td align="center" style="vertical-align: top; text-align: center;">
                                        <table style="padding-right: 5px; width: 150px;">
                                            <tr>
                                                <td style="text-align: center; width: 90px; font-weight: bold;">
                                                    <asp:Label ID="lblGlaucoma" runat="server" Text="Glaucoma"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblGlaucomaCodes" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="center" style="vertical-align: top; text-align: center; border-left: 1px solid #c2c4d3;">
                                        <table style="padding-left: 5px; width: 150px;">
                                            <tr>
                                                <td style="text-align: center; width: 90px; font-weight: bold;">
                                                    <asp:Label ID="lblRetina" runat="server" Text="Retina"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblRetinaCodes" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="315px" />
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false" HeaderText="Internal Notes">
                        <ItemTemplate>
                            <asp:Label ID="lblInternalNotes" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                   <asp:TemplateField Visible="false" HeaderText="OD: IOP/BFA">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblOD"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false" HeaderText="OS: IOP/BFA">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblOS"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Insurance">
                        <ItemTemplate>
                            <asp:Label ID="lblInsurance" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Practice Notes">
                        <ItemTemplate>
                            <asp:Label ID="lblDoctorNotes" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Button ID="btnCancelFinalNoteWorksheet" runat="server" Text="Back" CausesValidation="false"
                PostBackUrl="~/ReportLogs/ViewReportLogs.aspx" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
