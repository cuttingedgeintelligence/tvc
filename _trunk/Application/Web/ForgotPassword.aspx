﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true" Async="true" Title="Forgot password" CodeBehind="ForgotPassword.aspx.cs" Inherits="CEI.Web.ForgotPassword"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  <asp:Label ID="lblHeaderText" runat="server" Text="Forgot password" 
        CssClass="lblTitle" ></asp:Label>
             <asp:Label ID="lblPageDescription" runat="server" CssClass="lblPageDescription" 
        Text="Please enter your registered e-mail address(user name). You will receive a password via e-mail." 
></asp:Label>

    <asp:UpdatePanel ID="upForgotPassword"  runat="server" UpdateMode="Conditional">
    <ContentTemplate>
    
     <table  width="400px" style="clear:both;">
        
        <tr>
            <td width="300px">
                <asp:Label ID="lblEmail" runat="server" Text="Registered e-mail(user name) :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" 
                    ControlToValidate="txtEmail" Display="Dynamic" 
                    ErrorMessage="Please enter e-mail address" ValidationGroup="vgForgotPassword" 
                    SetFocusOnError="True"></asp:RequiredFieldValidator>
            
                <asp:RegularExpressionValidator ID="revEmail" runat="server" 
                        ControlToValidate="txtEmail" Display="Dynamic" 
                        ErrorMessage="Please enter valid e-mail address" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                        ValidationGroup="vgForgotPassword" SetFocusOnError="True"></asp:RegularExpressionValidator>
               
            </td>
        </tr>
        <tr>
        <td colspan="2">
            <asp:Button ID="btnGetPassword" runat="server" Text="Get Password"   
                ValidationGroup="vgForgotPassword" onclick="btnGetPassword_Click" />
                 <asp:Button ID="bntBackToLogin" runat="server" Text="Back To Login" CausesValidation="false"
                          PostBackUrl="~/Default.aspx"                  />
        </td>
        </tr>
        <tr>
        <td colspan="2">
            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
        </td>
        </tr>
        </table>
    
    </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
