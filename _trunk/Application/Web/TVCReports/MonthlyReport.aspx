<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="MonthlyReport.aspx.cs" Inherits="CEI.Web.TVCReports.MonthlyReport"
    Title="Monthly Report" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Monthly Report" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <table class="style1">
        <tr>
            <td width="120px">
                <asp:Label ID="lblSelectPractice" runat="server" Text="Select Practice:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlSelectPractice" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style ="padding-top:10px">
                <asp:Label ID="lblSelectYear" runat="server" Text="Select Year:"></asp:Label>
            </td>
            <td style ="padding-top:10px">
                <asp:DropDownList ID="ddlYear" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style ="padding-top:10px">
                <asp:Button ID="btnViewReport" runat="server" CausesValidation="False" Text="View report"
                    OnClick="btnViewReport_Click" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div style="width: 100%; height: 5px">
    </div>
    <table>
        <tr>
            <td style ="padding-top:10px">
                <asp:ImageButton ID="btnPrintPDF" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png"
                    ToolTip="Download PDF" OnClick="btnPrintPDF_Click" Style="padding-right: 3px;" />
            </td>
            <td style ="padding-top:10px; padding-left:5px">
                <asp:ImageButton ID="btnPrintExcel" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png"
                    ToolTip="Download Excel" OnClick="btnPrintExcel_Click" Style="padding-right: 3px;" />
            </td>
            <td style ="padding-top:10px; padding-left:5px">
                <asp:ImageButton ID="btnPrintPreview" runat="server" ImageUrl="~/Images/Mockups/printIcon.png"
                    ToolTip="View and Print" OnClick="btnPrintPreview_Click" />
            </td>
        </tr>
    </table>
    <div id="divReport" runat="server" style="display: none">
        <rsweb:ReportViewer ID="rptViewerMontlyReport" runat="server" Width="100%">
        </rsweb:ReportViewer>
    </div>
    <asp:UpdatePanel ID="upMontlyReport" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Repeater ID="repMontlyReport" runat="server" OnItemDataBound="repMontlyReport_ItemDataBound">
                <ItemTemplate>
                    <asp:GridView ID="gvMontlyReport" runat="server" SkinID="gridViewAdmin" Width="920px"
                        AutoGenerateColumns="false" Style="clear: both;" EmptyDataText="There is no data in the report"
                        ShowFooter="true" DataKeyNames="PracticeId">
                        <FooterStyle BackColor="#738299" Font-Bold="true" ForeColor="White" HorizontalAlign="Center"
                            VerticalAlign="Middle" />
                        <Columns>
                            <asp:HyperLinkField DataNavigateUrlFields="PracticeId" HeaderStyle-Width="200px" DataNavigateUrlFormatString="~/ViewPracticeProfile/ViewPracticeProfile.aspx?id={0}"
                                DataTextField="practiceName" HeaderText="Practice Name">
                                <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:HyperLinkField>
                            <asp:BoundField DataField="serviceDate" HeaderText="serviceDate" ItemStyle-VerticalAlign="Middle"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="BFA" HeaderText="BFA" ItemStyle-VerticalAlign="Middle"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="HRTGlaucoma" HeaderText="HRT-G" ItemStyle-VerticalAlign="Middle"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="HRT_Retina" HeaderText="HRT-R" ItemStyle-VerticalAlign="Middle"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="totalPatients" HeaderText="Total Pts." ItemStyle-VerticalAlign="Middle"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="technicianName" HeaderText="Tech" ItemStyle-VerticalAlign="Middle"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                          <%--  <asp:BoundField DataField="desktopImportedDate" HeaderStyle-Width="150px" HeaderText="Desktop: Imported" ItemStyle-VerticalAlign="Middle"
                                ItemStyle-HorizontalAlign="Center" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-HorizontalAlign="Center" />--%>
                            <asp:TemplateField HeaderText="Desktop: Imported" HeaderStyle-Width="150px">
                                <ItemTemplate>
                                <asp:Label ID="lblImportedDate" runat="server" Text='<%# Eval("desktopImportedDate","{0:MM/dd/yyyy}") %>' ></asp:Label>
                                    <asp:HyperLink ID="hlChangeDesktopImportedStatus" runat="server"></asp:HyperLink>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="150px" />
                            </asp:TemplateField>
                             <asp:BoundField DataField="desktopImportedComment"  HeaderStyle-Width="150px" HeaderText="Comments" ItemStyle-VerticalAlign="Middle"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                        </Columns>
                    </asp:GridView>
                </ItemTemplate>
            </asp:Repeater>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnViewReport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
