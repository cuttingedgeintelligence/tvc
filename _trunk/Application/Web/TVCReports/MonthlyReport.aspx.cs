﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.BR;
using CEI.Framework;
using Microsoft.Reporting.WebForms;
using CEI.Common;
using System.Collections.Generic;
using System.Drawing.Printing;

namespace CEI.Web.TVCReports
{
    public partial class MonthlyReport : BasePage
    {

        #region GridView columns


        private const int GV_MONTHLY_REPORT_PRACTICE_NAME = 0;
        private const int GV_MONTHLY_REPORT_SERVICE_DATE = 1;
        private const int GV_MONTHLY_REPORT_BFA = 2;
        private const int GV_MONTHLY_REPORT_HRT_G = 3;
        private const int GV_MONTHLY_REPORT_HRT_R = 4;
        private const int GV_MONTHLY_REPORT_TOTAL_PATIENTS = 5;
        private const int GV_MONTHLY_REPORT_TECHNICIAN_NAME = 6;
        private const int GV_MONTHLY_REPORT_DESKTOP_IMPORTED = 7;
        private const int GV_MONTHLY_REPORT_TECH_TRANSFER = 7;




        #endregion //GridView columns

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                divReport.Visible = false;

                BindListControl<DataTable>.BindAndAddListItem(ddlSelectPractice, new UserBR().GetAllPracticesNameAndId(), "name", "id", "All", Guid.Empty.ToString());

                int startingYear = int.Parse(ConfigurationManager.AppSettings["reportsStartingYear"]); 
               
                for(int i = DateTime.Today.Year; i >= startingYear; i--)
                    ddlYear.Items.Add(new ListItem(i.ToString()));

                btnViewReport_Click(null, null);
              
                
            }
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {

            GetReportData();
            Dictionary<int, DataTable> splitedTables = ViewState["SplitedTables"] as Dictionary<int, DataTable>;
            List<DataTable> tables = ViewState["ListedTables"] as List<DataTable>;
            repMontlyReport.DataSource = tables;
            repMontlyReport.DataBind();
            
        }

        protected void btnPrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            GetReportData();
            PrintReport(ExportType.Pdf);
        }

        protected void btnPrintExcel_Click(object sender, ImageClickEventArgs e)
        {
            GetReportData();
            PrintReport(ExportType.Excel);
        }

        protected void btnPrintPreview_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Printer);
        }

        private void GetReportData()
        {
            DataTable dtMontlyRepot = new FinalNotesWorksheetBR().GetMonthlyReportByPracticeIdAndYear(new Guid(ddlSelectPractice.SelectedValue), new DateTime(int.Parse(ddlYear.SelectedValue), 1, 1));
            Dictionary<int, DataTable> splitedTables = MonthTable(dtMontlyRepot);

            List<DataTable> tables = new List<DataTable>();

            foreach (int i in splitedTables.Keys)
                tables.Add(splitedTables[i]);

            ViewState["ListedTables"] = tables;
            ViewState["SplitedTables"] = splitedTables;
            ViewState["MontlyReport"] = dtMontlyRepot;
        }

        private Dictionary<int, DataTable> MonthTable(DataTable dt)
        {
            Dictionary<int, DataTable> retVal = new Dictionary<int, DataTable>();

            DataTable dtMonths = dt.DefaultView.ToTable(true, "serviceDate");

            foreach (DataRow dr in dtMonths.Rows)
            {
                DateTime dateTime = Convert.ToDateTime(dr["serviceDate"]);
                //int startIndex =dr["serviceDate"].ToString().IndexOf("/");

                if (!retVal.ContainsKey(dateTime.Month))
                {
                    DataView dv = new DataView(dt);
                    //dv.RowFilter = "SUBSTRING(CONVERT(serviceDate,System.String), 1," + startIndex.ToString() + ") = '" + dateTime.Month.ToString() + "'";
                    dv.RowFilter = "serviceDateMonth  = '" + dateTime.Month.ToString() + "'";
                    retVal.Add(dateTime.Month, dv.ToTable());
                }
            }

            return retVal;
        }

        private DataTable GetDataDable(int tableIndex)
        {
            List<DataTable> tables = ViewState["ListedTables"] as List<DataTable>;
            return tables[tableIndex];
        }

        private string GetMonthHeaderText(DataTable dt)
        {
            if (dt.Rows.Count > 0)
                return Convert.ToDateTime(dt.Rows[0]["serviceDate"]).ToString("MMM yy");
            else return string.Empty;
        }

        private void AddValue(ref decimal sumValue, string cellValue)
        {
            decimal i = 0;
            decimal.TryParse(cellValue, out i);

            sumValue += i;
        }

        #region MICROSOFT REPORTING

        private void PrintReport(ExportType exportType)
        {
            DataTable dtMontlyRepot = ViewState["MontlyReport"] as DataTable;

            LocalReport localReport = new LocalReport();

            localReport.ReportPath = Server.MapPath("~/TVCReports/rptMontlyReport.rdlc");

            ReportDataSource reportDataSource = new ReportDataSource("DataSet1_stp_PatientLogs_PatientLogsPatients_Users_GetMonthlyReportByPracticeIdAndYear", dtMontlyRepot);
            localReport.DataSources.Add(reportDataSource);

            ReportParameter[] param = new ReportParameter[2];
            param[0] = new ReportParameter("rptParamPractice", ddlSelectPractice.SelectedItem.Text);
            param[1] = new ReportParameter("rptParamYear", ddlYear.Text);

            localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            if (exportType == ExportType.Pdf)
            {
                DownloadPDF(localReport);

            }
            else if (exportType == ExportType.Excel)
            {
                DownloadExcel(localReport);

            }
            else if (exportType == ExportType.Printer)
            {
                DirectPrint(localReport);
            }
        }

        private void DownloadPDF(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PDF</OutputFormat>" +
            //"  <PageWidth>11in</PageWidth>" +
            //"  <PageHeight>8.5in</PageHeight>" +
            //"  <MarginTop>0.5in</MarginTop>" +
            //"  <MarginLeft>1in</MarginLeft>" +
            //"  <MarginRight>1in</MarginRight>" +
            //"  <MarginBottom>0.5in</MarginBottom>" +
            //"</DeviceInfo>";


            string deviceInfo =
   "<DeviceInfo>" +
   "  <OutputFormat>PDF</OutputFormat>" +
   "  <PageWidth>11in</PageWidth>" +
   "  <PageHeight>8.5in</PageHeight>" +
   "  <MarginTop>0.2in</MarginTop>" +
   "  <MarginLeft>0.6in</MarginLeft>" +
   "  <MarginRight>0.1in</MarginRight>" +
   "  <MarginBottom>0.2in</MarginBottom>" +
   "</DeviceInfo>";


            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            
            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=MontlyReport." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DownloadExcel(LocalReport localReport)
        {
            string reportType = "Excel";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
             "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=MontlyReport." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DirectPrint(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>PDF</OutputFormat>" +
             "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;


            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "inline; filename=MontlyReport." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        #endregion

        protected void repMontlyReport_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                GridView gv = (GridView)e.Item.FindControl("gvMontlyReport");
                if (gv != null)
                {
                    DataTable dt = GetDataDable(e.Item.ItemIndex);
                    gv.DataSource = dt;
                    gv.RowDataBound += new GridViewRowEventHandler(gv_RowDataBound);
                    gv.Columns[1].HeaderText = GetMonthHeaderText(dt);
                    gv.DataBind();
                }
            }  
        }


        decimal m_TotalBFA = 0;
        decimal m_TotalHRTG = 0;
        decimal m_TotalHRTR = 0;
        decimal m_TotalPts = 0;

        void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {


                e.Row.Cells[1].Text = Convert.ToDateTime(e.Row.Cells[1].Text).ToString("dd-MMM");//dd-MMM
                
                AddValue(ref m_TotalBFA, e.Row.Cells[2].Text);
                AddValue(ref m_TotalHRTG, e.Row.Cells[3].Text);
                AddValue(ref m_TotalHRTR, e.Row.Cells[4].Text);
                AddValue(ref m_TotalPts, e.Row.Cells[5].Text);

                DataRowView drvLog = e.Row.DataItem as DataRowView;
                Label lblImportedDate = e.Row.Cells[GV_MONTHLY_REPORT_TECH_TRANSFER].FindControl("lblImportedDate") as Label;
                
                HyperLink hlChangeDesktopImportedStatus = e.Row.Cells[GV_MONTHLY_REPORT_TECH_TRANSFER].FindControl("hlChangeDesktopImportedStatus") as HyperLink;
               
                if (drvLog["desktopImportedDate"] == DBNull.Value)
                {
                    lblImportedDate.Visible = false;
                    hlChangeDesktopImportedStatus.Visible = true;

                    
                   
                      //if (drvLog["desktopImportedComment"] != DBNull.Value && drvLog["desktopImportedComment"].ToString() != string.Empty)
                    //{
                    //    hlChangeDesktopImportedStatus.Text = drvLog["desktopImportedComment"].ToString();

                    //}
                    //else
                        hlChangeDesktopImportedStatus.Text = "Change Status";
                       

                    hlChangeDesktopImportedStatus.NavigateUrl = "~/ManageFTP/ManageFTP.aspx?prId=" + drvLog["practiceId"].ToString() + "&logId=" + drvLog["patientLogId"].ToString();
                }
                else
                {
                   // e.Row.Cells[8].Text = ""; for not showing comments if it si complited
                    lblImportedDate.Visible = true;
                    hlChangeDesktopImportedStatus.Visible = false;
                }

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Total ";
                e.Row.Cells[0].CssClass = "footerRow2";
                e.Row.Cells[2].Text = m_TotalBFA.ToString();
                e.Row.Cells[3].Text = m_TotalHRTG.ToString();
                e.Row.Cells[4].Text = m_TotalHRTR.ToString();
                e.Row.Cells[5].Text = m_TotalPts.ToString();

                m_TotalBFA = 0;
                m_TotalHRTG = 0;
                m_TotalHRTR = 0;
                m_TotalPts = 0;
            }
        }
    }
}
