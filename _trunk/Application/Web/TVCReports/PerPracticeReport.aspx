<%@ Page Title="Report Per Practice" Language="C#" MasterPageFile="~/MasterPages/Default.Master"
    AutoEventWireup="true" CodeBehind="PerPracticeReport.aspx.cs" Inherits="CEI.Web.TVCReports.PerPracticeReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
    
    <script runat="server">
        double TotalPatients = 0.0;
        int TotalPatients1 = 0;
        double TotalPatients2 = 0.0;
        double GetTotalPatients(double Patients)
        {
            TotalPatients += Patients;
            TotalPatients1 += 1;
            return Patients;
        }
        double GetTotal()
        {
            TotalPatients2 = TotalPatients / TotalPatients1;
            TotalPatients2 = Math.Round(TotalPatients2 * Math.Pow(10, 2)) / Math.Pow(10, 2);
            return TotalPatients2;
        }

        double TotalBFA = 0.0;
        int TotalBFA1 = 0;
        double TotalBFA2 = 0.0;
        double GetTotalBFA(double BFA)
        {
           
            TotalBFA += BFA;
            TotalBFA1 +=1;
            
            return BFA;
        }
        double GetBFA()
        {
            TotalBFA2 = TotalBFA / TotalBFA1;
            TotalBFA2 = Math.Round(TotalBFA2 * Math.Pow(10, 2)) / Math.Pow(10, 2);
            return TotalBFA2;
        }



        double TotalHRTG = 0.0;
        int TotalHRTG1 = 0;
        double TotalHRTG2 = 0.0;
        double GetTotalHRTG(double HRTG)
        {

            TotalHRTG += HRTG;
            TotalHRTG1 += 1;

            return HRTG;
        }
        double GetHRTG()
        {
            TotalHRTG2 = TotalHRTG / TotalHRTG1;
            TotalHRTG2 = Math.Round(TotalHRTG2 * Math.Pow(10, 2)) / Math.Pow(10, 2);
            return TotalHRTG2;
        }
    
        
        double TotalHRTR = 0.0;
        int TotalHRTR1 = 0;
        double TotalHRTR2 = 0.0;
        double GetTotalHRTR(double HRTR)
        {

            TotalHRTR += HRTR;
            TotalHRTR1 += 1;
            
            return HRTR;
        }
        double GetHRTR()
        {
            TotalHRTR2 = TotalHRTR / TotalHRTR1;
            TotalHRTR2 = Math.Round(TotalHRTR2 * Math.Pow(10, 2)) / Math.Pow(10, 2);
            return TotalHRTR2;
        }
        double TotalExamined = 0.0;
        int TotalExamined1 = 0;
        double TotalExamined2 = 0.0;
        double GetTotalExamined(double Examined)
        {
            TotalExamined += Examined;
            TotalExamined1 += 1;
            return Examined;
        }
        double GetExamined()
        {
            TotalExamined2 = TotalExamined / TotalExamined1;
            TotalExamined2 = Math.Round(TotalExamined2 * Math.Pow(10, 2)) / Math.Pow(10, 2);
            return TotalExamined2;
        }

        double TotalCancelations = 0.0;
        int TotalCancelations1 = 0;
        double TotalCancelations2 = 0.0;
        double GetTotalCancelations(double Cancelations)
        {
            TotalCancelations += Cancelations;
            TotalCancelations1 += 1;
            return Cancelations;
        }
        double GetCancelations()
        {
            TotalCancelations2 = TotalCancelations / TotalCancelations1;
            TotalCancelations2 = Math.Round(TotalCancelations2 * Math.Pow(10, 2)) / Math.Pow(10, 2);

            return TotalCancelations2;
        }

     
        
        
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Report Per Practice" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <table>
        <tr>
            <td width="120px" style="padding-top: 10px">
                <asp:Label ID="lblSelectPractice" runat="server" Text="Select Practice:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlSelectPractice" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSelectPractice_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="padding-top: 10px">
                <asp:ImageButton ID="btnPrintPDF" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png"
                    ToolTip="Download PDF" OnClick="btnPrintPDF_Click" Style="padding-right: 3px;" />
            </td>
            <td style="padding-top: 10px; padding-left: 5px">
                <asp:ImageButton ID="btnPrintExcel" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png"
                    ToolTip="Download Excel" OnClick="btnPrintExcel_Click" Style="padding-right: 3px;" />
            </td>
            <td style="padding-top: 10px; padding-left: 5px">
                <asp:ImageButton ID="btnPrintPreview" runat="server" ImageUrl="~/Images/Mockups/printIcon.png"
                    ToolTip="View and Print" OnClick="btnPrintPreview_Click" Style="display: none;" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upReportPerPractice" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvPerPracticeReport" ShowFooter="true" runat="server" SkinID="gridViewAdmin" Width="920px"
                AutoGenerateColumns="False" DataKeyNames="id" OnRowDataBound="gvPerPracticeReport_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Service Date">
                        <ItemTemplate>
                            <asp:HyperLink ID="hlServiceDate" runat="server">[hlServiceDate]</asp:HyperLink>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle CssClass="footerRow1" />
                       <FooterTemplate>Averages:</FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Patients" >
                    <ItemTemplate><asp:Label ID="lblTotalNumOfPatients" runat="server" Text='<%# GetTotalPatients(double.Parse(Eval("totalNumOfPatients").ToString())) %>'></asp:Label></ItemTemplate>
                     <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center"  />
                        <FooterTemplate> <%# GetTotal() %></FooterTemplate>
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="BFA Tests" >
                    <ItemTemplate><asp:Label ID="lblBFA_Tests" runat="server" Text='<%# GetTotalBFA(double.Parse(Eval("BFA_Tests").ToString())) %>'></asp:Label></ItemTemplate>
                     <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" />
                        <FooterTemplate><%# GetBFA()%></FooterTemplate>
                    </asp:TemplateField>
                           <asp:TemplateField HeaderText="HRT-G Tests" >
                    <ItemTemplate><asp:Label ID="lblHRT_GlaucomaTests" runat="server" Text='<%# GetTotalHRTG(double.Parse(Eval("HRT_GlaucomaTests").ToString())) %>'></asp:Label></ItemTemplate>
                     <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                            <FooterStyle HorizontalAlign="Center" />
                        <FooterTemplate><%# GetHRTG()%></FooterTemplate>
                    </asp:TemplateField>
                           <asp:TemplateField HeaderText="HRT-R Tests" >
                    <ItemTemplate><asp:Label ID="lblHRT_RetinaTests" runat="server" Text='<%# GetTotalHRTR(double.Parse(Eval("HRT_RetinaTests").ToString())) %>'></asp:Label></ItemTemplate>
                     <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                           <FooterStyle HorizontalAlign="Center" />
                        <FooterTemplate><%# GetHRTR()%></FooterTemplate>
                    </asp:TemplateField>
                               <asp:TemplateField HeaderText="Examined" >
                    <ItemTemplate><asp:Label ID="lblnumberOfExaminedPatients" runat="server" Text='<%# GetTotalExamined(double.Parse(Eval("numberOfExaminedPatients").ToString())) %>'></asp:Label></ItemTemplate>
                     <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                           <FooterStyle HorizontalAlign="Center" />
                        <FooterTemplate><%# GetExamined()%></FooterTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="No Show/Cancelations" >
                    <ItemTemplate><asp:Label ID="lblnumberOfNoShowCancelledPatients" runat="server" Text='<%#GetTotalCancelations(double.Parse(Eval("numberOfNoShowCancelledPatients").ToString())) %>'></asp:Label></ItemTemplate>
                     <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                          <FooterStyle HorizontalAlign="Center" />
                        <FooterTemplate><%# GetCancelations()%> </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Insurance Summary">
                        <ItemTemplate>
                            <asp:DataList ID="dlPatientsPerInsurance"
                                runat="server">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td align="left" width="120px">
                                                <asp:Label ID="lblInsuranceCompanyName" runat="server" Text='<%# Eval("insuranceCompanyName") %>'></asp:Label>
                                            </td>
                                            <td align="right" width="30px">
                                                <asp:Label ID="lblNumOfPatients" runat="server" Text='<%# Eval("numberOfPatients") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlSelectPractice" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
