﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CEI.Framework;
using CEI.BR;
using System.Data;
using Microsoft.Reporting.WebForms;
using CEI.Common;
using System.Xml;
using System.IO;
using System.Text;

namespace CEI.Web.TVCReports
{
    public partial class PerPracticeReport : BasePage
    {


        #region GridView columns


        private const int GV_REPORT_SERVICE_DATE = 0;
        private const int GV_REPORT_TOTAL_PATIENTS = 1;
        private const int GV_REPORT_NUM_BFA_TESTS = 2;
        private const int GV_REPORT_NUM_HRT_G_TESTS = 3;
        private const int GV_REPORT_NUM_HRT_R_TESTS = 4;
        private const int GV_REPORT_NUM_EXAMINED = 5;
        private const int GV_REPORT_NUM_NO_SHOW_CANCELLED = 6;
        private const int GV_REPORT_NUM_PATIENTS_PER_INSURANCE = 7;

        #endregion //GridView columns


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //  BindListControl<DataTable>.BindAndAddListItem(ddlPracticesSearch, new UserBR().GetAllPracticesNameAndId(), "name", "id", "All", Guid.Empty.ToString());

                BindListControl<DataTable>.BindAndAddListItem(ddlSelectPractice, new UserBR().GetAllPracticesNameAndId(), "name", "id", "Select", Guid.Empty.ToString());
               this.BindReportData();
            }
        }

       


       
        protected void ddlSelectPractice_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindReportData();
        }

        private void BindReportData()
        {
            DataTable dt = new FinalNotesWorksheetBR().GetPerPracticeReport(new Guid(ddlSelectPractice.SelectedValue));
            ViewState["PerPractice"] = dt;
            gvPerPracticeReport.DataSource = dt;
            gvPerPracticeReport.DataBind();
        }

        protected void gvPerPracticeReport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drPatientLog = e.Row.DataItem as DataRowView;
                HyperLink hlServiceDate = e.Row.Cells[GV_REPORT_SERVICE_DATE].FindControl("hlServiceDate") as HyperLink;
                hlServiceDate.NavigateUrl = "~/FinalNotesWorksheets/ViewCompletedFinalNotesWorksheet.aspx?logId=" + gvPerPracticeReport.DataKeys[e.Row.RowIndex].Value.ToString() + "&prName=" + ddlSelectPractice.SelectedItem.Text + "&servDate=" + ((DateTime)drPatientLog["serviceDate"]).ToString("MM/dd/yyyy");
                hlServiceDate.Text = ((DateTime)drPatientLog["serviceDate"]).ToString("MM/dd/yyyy");
            
              
                DataSet dsPatientsPerInsuranceCompanies = new DataSet();
                dsPatientsPerInsuranceCompanies.ReadXml(new StringReader(drPatientLog["howManyPerInsurance"].ToString()));
                DataList dlPatientsPerInsurance = e.Row.Cells[GV_REPORT_NUM_PATIENTS_PER_INSURANCE].FindControl("dlPatientsPerInsurance") as DataList;
                DataSet newDataSet = new DataSet();
                DataTable dt2 = new DataTable();
                DataTable dt1=dsPatientsPerInsuranceCompanies.Tables["row"] as DataTable;
                dt2 = dt1.Clone();
                foreach (DataRow oldDataRow in dt1.Rows)
                {
                    if (oldDataRow.ItemArray[1].ToString() != "0")
                        dt2.ImportRow(oldDataRow);
                }
                newDataSet.Tables.Add(dt2);

              
               // dlPatientsPerInsurance.DataSource = dsPatientsPerInsuranceCompanies;
                dlPatientsPerInsurance.DataSource = newDataSet;
                dlPatientsPerInsurance.DataBind();
            }
        }

      
        protected void btnPrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Pdf);
        }

        protected void btnPrintExcel_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Excel);
        }

        protected void btnPrintPreview_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Printer);
        }

        #region MICROSOFT REPORTING

        private string GetPipedCompanyStringFromXML(string xml)
        {
            StringBuilder sb = new StringBuilder();

            XmlDocument doc = new XmlDocument();
            
            doc.LoadXml(xml);
            foreach (XmlNode node in doc.GetElementsByTagName("row"))
            {
                if (node.Attributes["numberOfPatients"].Value.Contains("0"))
                {
                    //sb.Append(" ");
                }
                else
                    sb.Append(node.Attributes["insuranceCompanyName"].Value.Trim() + "|");
            }


            return sb.ToString();
            
        }

        private string GetPipedNumberStringFromXML(string xml)
        {
            StringBuilder sb = new StringBuilder();

            XmlDocument doc = new XmlDocument();

            doc.LoadXml(xml);
            foreach (XmlNode node in doc.GetElementsByTagName("row"))
                if (node.Attributes["numberOfPatients"].Value.Contains("0"))
                {
                    //sb.Append(" ");
                }
                else
                    sb.Append(node.Attributes["numberOfPatients"].Value + "|");


            return sb.ToString();

        }

        private void PrintReport(ExportType exportType)
        {

            TotalVisionReport _totalVisionReport = new TotalVisionReport();

            DataTable dtTemp = ViewState["PerPractice"] as DataTable;

            DataTable dt = dtTemp.Clone();
            

            dt.Columns["id"].DataType = typeof(String);

            foreach (DataRow dr in dtTemp.Rows)
            {
                DataRow newRow = dt.NewRow();
                newRow["id"] = GetPipedNumberStringFromXML(dr["howManyPerInsurance"].ToString());
                newRow["serviceDate"] = dr["serviceDate"];
                newRow["totalNumOfPatients"] = dr["totalNumOfPatients"];
                newRow["BFA_Tests"] = dr["BFA_Tests"];
                newRow["HRT_GlaucomaTests"] = dr["HRT_GlaucomaTests"];
                newRow["HRT_RetinaTests"] = dr["HRT_RetinaTests"];
                newRow["numberOfExaminedPatients"] = dr["numberOfExaminedPatients"];
                newRow["numberOfNoShowCancelledPatients"] = dr["numberOfNoShowCancelledPatients"];
                newRow["howManyPerInsurance"] = GetPipedCompanyStringFromXML(dr["howManyPerInsurance"].ToString());
                dt.Rows.Add(newRow);
               
            }


            LocalReport localReport = new LocalReport();

            localReport.ReportPath = Server.MapPath("~/Reports/rptPerPractice.rdlc");

            ReportDataSource reportDataSource = new ReportDataSource("DataSet1_stp_PatientLogs_PatientLogsPatients_InsuranceCompanies_GetPerPracticeReport", dt);
            localReport.DataSources.Add(reportDataSource);

            ReportParameter[] param = new ReportParameter[1]; 
            param[0] = new ReportParameter("rptParamPracticeName", ddlSelectPractice.SelectedItem.Text);
            //param[1] = new ReportParameter("rptParamDate", lblServiceDateData.Text);

            localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            if (exportType == ExportType.Printer)
            {
                //_totalVisionReport.ListItems = patientsList;
                //_totalVisionReport.Pameters.Add("rptParamPracticeName", lblPracticeNameData.Text);
                //_totalVisionReport.Pameters.Add("rptParamDate", lblServiceDateData.Text);
                //Session["PrintingParams"] = _totalVisionReport;

                //Response.Redirect("~/Reports/ReportPreview.aspx?ReportName=PatientLog");
            }
            else if (exportType == ExportType.Pdf)
            {
                DownloadPDF(localReport);

            }
            else if (exportType == ExportType.Excel)
            {
                DownloadExcel(localReport);

            }
        }

        private void DownloadPDF(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PDF</OutputFormat>" +
            //"  <PageWidth>11in</PageWidth>" +
            //"  <PageHeight>8.5in</PageHeight>" +
            //"  <MarginTop>0.2in</MarginTop>" +
            //"  <MarginLeft>0.1in</MarginLeft>" +
            //"  <MarginRight>0.1in</MarginRight>" +
            //"  <MarginBottom>0.2in</MarginBottom>" +
            //"</DeviceInfo>";
            string deviceInfo =
   "<DeviceInfo>" +
   "  <OutputFormat>PDF</OutputFormat>" +
   "  <PageWidth>11in</PageWidth>" +
   "  <PageHeight>8.5in</PageHeight>" +
   "  <MarginTop>0.2in</MarginTop>" +
   "  <MarginLeft>0.6in</MarginLeft>" +
   "  <MarginRight>0.1in</MarginRight>" +
   "  <MarginBottom>0.2in</MarginBottom>" +
   "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=PerPractice." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DownloadExcel(LocalReport localReport)
        {
            string reportType = "Excel";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
            "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";


          


            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=PerPractice." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }


        #endregion

        protected void dlPatientsPerInsurance_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Label label1 = (Label)e.Item.FindControl("lblNumOfPatients");

            if (label1.Text == "0")
                e.Item.Visible = false;
            else
                e.Item.Visible = true;
        }
    }
}
