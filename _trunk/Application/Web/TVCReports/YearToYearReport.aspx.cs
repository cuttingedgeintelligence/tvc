﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CEI.BR;
using CEI.Common;
using System.Data;
using Microsoft.Reporting.WebForms;

namespace CEI.Web.TVCReports
{
    public partial class YearToYearReport : BasePage
    {

      
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataTable dt = new FinalNotesWorksheetBR().GetYearToYearReport(PatientLogStatusEnum.Final__notes__worksheet, PatientLogPatientStatusEnum.Examined);
                ////marina
                //DataTable dtNew = dt.Clone();
                //foreach (DataRow drYear in dtNew.Rows)
                //{                   
                //    dtNew.Columns.Add(drYear["y"].ToString(), typeof(string));
                //}

                ViewState["YearToYeat"] = dt;

               // ViewState["YearToYeat"] = dtNew;
                //marina

                repYearToYearReport.DataSource = this.GenerateYearToYearDictionary(dt);
                repYearToYearReport.DataBind();
            }
        }

       



        protected void repYearToYearReport_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //begin ---marina--
            //if (e.Item.ItemType == ListItemType.Header)
            //{
            // //   KeyValuePair<int, DataTable> yearWithTable = (KeyValuePair<int, DataTable>)e.Item.DataItem;
            //    (e.Item.FindControl("lblYear") as Label).Text = "22222";
            //}
            //end ---marina---
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
               
                //begin ---marina--
                KeyValuePair<int, DataTable> yearWithTable = (KeyValuePair<int, DataTable>)e.Item.DataItem;
              //  (e.Item.FindControl("lblYear") as Label).Text = yearWithTable.Key.ToString();
                GridView gv = e.Item.FindControl("gvYearReport") as GridView;
                gv.Columns[0].HeaderText = yearWithTable.Key.ToString();
                gv.DataSource = yearWithTable.Value;
                gv.DataBind();
                //end ---marina---
               
                //if (e.Item.ItemIndex == 0)
                //{
                //    gv.ShowHeader = true;
                   
                //}
               
               
            }
        }


        protected void btnPrintPreview_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Printer);
        }

        protected void btnPrintExcel_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Excel);
        }

        protected void btnPrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Pdf);
        }

        #endregion //Event handlers

        #region MICROSOFT REPORTING

        private void PrintReport(ExportType exportType)
        {

            TotalVisionReport _totalVisionReport = new TotalVisionReport();

            DataTable dtYearToYear = ViewState["YearToYeat"] as DataTable;



            LocalReport localReport = new LocalReport();

            localReport.ReportPath = Server.MapPath("~/Reports/rptYearToYear.rdlc");

            ReportDataSource reportDataSource = new ReportDataSource("DataSet1_stp_PatientLogs_PatientLogsPatients_Users_GetYearToYearReport", dtYearToYear);
            localReport.DataSources.Add(reportDataSource);

            //ReportParameter[] param = new ReportParameter[2];
            //param[0] = new ReportParameter("rptParamPracticeName", lblPracticeNameData.Text);
            //param[1] = new ReportParameter("rptParamDate", lblServiceDateData.Text);

            //localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            if (exportType == ExportType.Printer)
            {
                //_totalVisionReport.ListItems = patientsList;
                //_totalVisionReport.Pameters.Add("rptParamPracticeName", lblPracticeNameData.Text);
                //_totalVisionReport.Pameters.Add("rptParamDate", lblServiceDateData.Text);
                //Session["PrintingParams"] = _totalVisionReport;

                //Response.Redirect("~/Reports/ReportPreview.aspx?ReportName=CompletedWorksheet");
            }
            else if (exportType == ExportType.Pdf)
            {
                DownloadPDF(localReport);

            }
            else if (exportType == ExportType.Excel)
            {
                DownloadExcel(localReport);

            }
        }

        private void DownloadPDF(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PDF</OutputFormat>" +
            //"  <PageWidth>11in</PageWidth>" +
            //"  <PageHeight>8.5in</PageHeight>" +
            //"  <MarginTop>1in</MarginTop>" +
            //"  <MarginLeft>0.5in</MarginLeft>" +
            //"  <MarginRight>0.5in</MarginRight>" +
            //"  <MarginBottom>1in</MarginBottom>" +
            //"</DeviceInfo>";

            string deviceInfo =
   "<DeviceInfo>" +
   "  <OutputFormat>PDF</OutputFormat>" +
   "  <PageWidth>11in</PageWidth>" +
   "  <PageHeight>8.5in</PageHeight>" +
   "  <MarginTop>0.2in</MarginTop>" +
   "  <MarginLeft>0.6in</MarginLeft>" +
   "  <MarginRight>0.1in</MarginRight>" +
   "  <MarginBottom>0.2in</MarginBottom>" +
   "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=YearToYear." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DownloadExcel(LocalReport localReport)
        {
            string reportType = "Excel";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
            "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=YearToYear." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }


        #endregion

        #region Private methods

        private Dictionary<int, DataTable> GenerateYearToYearDictionary(DataTable dt)
        {
            Dictionary<int, DataTable> retVal = new Dictionary<int, DataTable>();

            DataTable dtYear = dt.DefaultView.ToTable(true, "y");
            foreach (DataRow drYear in dtYear.Rows)
            {
                DataView dv = new DataView(dt);
                dv.RowFilter = "y = " + drYear["y"].ToString();
                retVal.Add((int)drYear["y"], dv.ToTable());
            }

            //DataTable dtMonths = dt.DefaultView.ToTable(true, "serviceDate");

            //foreach (DataRow dr in dtMonths.Rows)
            //{
            //    DateTime dateTime = Convert.ToDateTime(dr["serviceDate"]);
            //    int startIndex = dr["serviceDate"].ToString().IndexOf("/");

            //    if (!retVal.ContainsKey(dateTime.Month))
            //    {
            //        DataView dv = new DataView(dt);
            //        dv.RowFilter = "SUBSTRING(CONVERT(serviceDate,System.String), 1," + startIndex.ToString() + ") = '" + dateTime.Month.ToString() + "'";
            //        retVal.Add(dateTime.Month, dv.ToTable());
            //    }
            //}

            return retVal;
        }

        protected void gvYearReport_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
              //  (e.Row.FindControl("lblYear1") as Label).Text = ((sender as GridView).Parent.Controls[1] as Label).Text;
                
            }
        }

        #endregion //Private methods
    }
}
