﻿<%@ Page Title="Year to Year Report" Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true" CodeBehind="YearToYearReport.aspx.cs" Inherits="CEI.Web.TVCReports.YearToYearReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <asp:Label ID="lblHeaderText" runat="server" Text="Year to Year Report" CssClass="lblTitle"
        Style="float: none"></asp:Label>
 
   <table>
        <tr>
            <td>
                <asp:ImageButton ID="btnPrintPDF" runat="server" 
                    ImageUrl="~/Images/Mockups/pdfIcon.png" ToolTip="Download PDF" 
                    onclick="btnPrintPDF_Click" style="padding-right:3px;"  />
            </td>
            <td>
                <asp:ImageButton ID="btnPrintExcel" runat="server" 
                    ImageUrl="~/Images/Mockups/exelIcon.png"  ToolTip="Download Excel" 
                    onclick="btnPrintExcel_Click" style="padding-right:3px; padding-left:5px;"  />
            </td>
            <td>
                <asp:ImageButton ID="btnPrintPreview" runat="server" 
                    ImageUrl="~/Images/Mockups/printIcon.png" ToolTip="View and Print" 
                    onclick="btnPrintPreview_Click" style="display:none;" />
            </td>
        </tr>
    </table>
  
    
    
    
    
     <asp:Repeater ID="repYearToYearReport" runat="server" 
            onitemdatabound="repYearToYearReport_ItemDataBound">
           <%-- <HeaderTemplate>
             <table class="gridViewManagePatientLog"  >
            <tr class="gridViewHeaderManagePatientLog" style="text-align:center; background-color:#f5f8fb;">
            <th width="22px" style="border-left:0px solid #c2c4d3;"></th>
            <th width="173px" style="border-left:0px solid #c2c4d3;"></th>
            <th width="50px" style="border-left:0px solid #c2c4d3;">Jan</th>
            <th width="50px" style="border-left:0px solid #c2c4d3;">Feb</th>
            <th width="50px" style="border-left:0px solid #c2c4d3;">Mar</th>
            <th width="50px" style="border-left:0px solid #c2c4d3;">Apr</th>
            <th width="49px" style="border-left:0px solid #c2c4d3;">May</th>
            <th width="50px" style="border-left:0px solid #c2c4d3;">Jun</th>
            <th width="49px" style="border-left:0px solid #c2c4d3;">Jul</th>
            <th width="50px" style="border-left:0px solid #c2c4d3;">Aug</th>
            <th width="50px" style="border-left:0px solid #c2c4d3;">Sep</th>
            <th width="50px" style="border-left:0px solid #c2c4d3;">Oct</th>
            <th width="50px" style="border-left:0px solid #c2c4d3;">Nov</th>
            <th width="50px" style="border-left:0px solid #c2c4d3;">Dec</th>
            </tr>
           
            </HeaderTemplate>            
            --%>
              <HeaderTemplate>
             <table class="gridViewManagePatientLog"  >
            <tr class="gridViewHeaderManagePatientLog1" style="text-align:center; background-color:#f5f8fb;">
            <th width="100%"></th>
            <%--<th width="173px"></th>
            <th width="50px"></th>
            <th width="50px"></th>
            <th width="50px"></th>
            <th width="50px"></th>
            <th width="49px"></th>
            <th width="50px"></th>
            <th width="49px"></th>
            <th width="50px"></th>
            <th width="50px"></th>
            <th width="50px"></th>
            <th width="50px"></th>--%>
         
            </tr>
           
            </HeaderTemplate>
            <ItemTemplate>
            <tr class="gridViewRowManagePatientLog">
           <%-- <td valign="top" style="padding:0;">
             <asp:Label ID="lblYear" runat="server" Text="" style="font-weight:bold;"></asp:Label></td>--%>
            <td colspan="13"  style="padding:0;border:1px solid #c2c4d3">
             <asp:GridView ID="gvYearReport" OnRowDataBound="gvYearReport_OnRowDataBound" runat="server" ShowHeader="true"  SkinID="gridViewYearToYearReport" AutoGenerateColumns="false"
                 EmptyDataText="There are is no data in report" style="border:none;">
                     <Columns>
                    <%-- <asp:TemplateField HeaderText="brrrrr" >
                     <ItemTemplate>
                      <asp:Label ID="lblYear1" runat="server" Text="" style="font-weight:bold;"></asp:Label>
                     </ItemTemplate>
                     </asp:TemplateField>--%>
                      <%--  <asp:BoundField DataField="" HeaderText="" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F2}"  />--%>
                        <asp:BoundField DataField="header" HeaderStyle-Width="300px" HeaderText="" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="168px" />
                        <asp:BoundField DataField="Jan" HeaderText="Jan" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="Feb" HeaderText="Feb" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="Mar" HeaderText="Mar" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center"  ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="Apr" HeaderText="Apr" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="May" HeaderText="May" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="Jun" HeaderText="Jun" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="Jul" HeaderText="Jul" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="Aug" HeaderText="Aug" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="Sep" HeaderText="Sep" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="Oct" HeaderText="Oct" ItemStyle-VerticalAlign="Middle"   HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="Nov" HeaderText="Nov" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}"  />
                        <asp:BoundField DataField="Dec" HeaderText="Dec" ItemStyle-VerticalAlign="Middle"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" DataFormatString="{0:F1}" />
                    </Columns>
                </asp:GridView>
            </td>
            </tr>
           
               
            </ItemTemplate>
            <SeparatorTemplate>
            <tr>
            <td colspan="14">&nbsp;</td>
            </tr>
            </SeparatorTemplate>
           <FooterTemplate>
            </table>
           </FooterTemplate>
           
            
        </asp:Repeater>
</asp:Content>
