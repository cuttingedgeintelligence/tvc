﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;

namespace CEI.Web.ManageCounties
{
    public partial class ManageCounties : BasePage
    {

        #region GridView columns


        private const int GV_COUNTIES_NAME = 0;
        private const int GV_COUNTIES_EDIT = 1;
        private const int GV_COUNTIES_DELETE = 2;

        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridView();
            }
        }


       


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (ViewState["countyId"] != null) //update county
                {


                    NameValue county = new NameValue();
                    county.Id = (int)ViewState["countyId"];
                    county.Name = txtName.Text;


                    if (new NameValueBR().UpdateCounty(county))
                    {
                        this.BindGridView();
                        this.ClearInsertUpdateForm();
                        
                    }


                }
                else //insert insurance company
                {


                    NameValue county = new NameValue();
                    county.Name = txtName.Text;

                    if (new NameValueBR().InsertCounty(county))
                    {
                        this.BindGridView();
                        this.ClearInsertUpdateForm();
                        
                    }


                }
            }
        }


        protected void gvCounties_RowEditing(object sender, GridViewEditEventArgs e)
        {
            ViewState["countyId"] = (int)gvCounties.DataKeys[e.NewEditIndex].Value;
            btnSave.Text = "Save";
            lblSelectedCounty.Text = "Update County: " + gvCounties.Rows[e.NewEditIndex].Cells[GV_COUNTIES_NAME].Text;
            txtName.Text = gvCounties.Rows[e.NewEditIndex].Cells[GV_COUNTIES_NAME].Text;

            upInsertUpdate.Update();
            
        }


        protected void gvCounties_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            if (new UserBR().ExistsUserWithCountyId((int)gvCounties.DataKeys[e.RowIndex].Value))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManageCounties), "existsUserWithCountyId", "alert('You cannot delete this county because is already assigned to a user');", true);
                return;
            }


            if (new NameValueBR().DeleteCounty((int)gvCounties.DataKeys[e.RowIndex].Value))
            {

                if (ViewState["countyId"] != null && (int)gvCounties.DataKeys[e.RowIndex].Value == (int)ViewState["countyId"])
                {
                    this.ClearInsertUpdateForm();
                    upInsertUpdate.Update();
                }



                this.BindGridView();
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.ClearInsertUpdateForm();
        }

        #endregion //Event handlers

        #region Private methods

        private void BindGridView()
        {
            gvCounties.DataSource = new NameValueBR().GetAllCounties();
            gvCounties.DataBind();
        }

        private void ClearInsertUpdateForm()
        {
            ViewState["countyId"] = null;
            lblSelectedCounty.Text = "Create New County";
            btnSave.Text = "Create";
            //clear form
            txtName.Text = string.Empty;

        }

        #endregion //Private methods

       

    }
}
