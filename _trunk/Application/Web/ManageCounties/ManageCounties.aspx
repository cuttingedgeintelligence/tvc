﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="True" CodeBehind="ManageCounties.aspx.cs" Inherits="CEI.Web.ManageCounties.ManageCounties" Title="Manage Counties" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:Label ID="lblHeaderText" runat="server" Text="Manage Counties" CssClass="lblTitle"
        Style="float: none"></asp:Label>
        
         <div style="vertical-align: bottom;margin-bottom:10px; overflow:hidden;">
            
            
        <asp:UpdatePanel ID="upInsertUpdate" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
             <fieldset class="fieldsetLeft" >
        <legend> <asp:Label ID="lblSelectedCounty" runat="server" Text="Create New County"></asp:Label>
          </legend>
      
                 <asp:Panel ID="pnlSaveCounty" runat="server" DefaultButton="btnSave">
                    <table border="0">
                        <tr>
                            <td colspan="2">
                               &nbsp;
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                                <asp:Label ID="lblName" runat="server" Text="County:"></asp:Label>&nbsp;
                            </td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                    Display="Dynamic" ErrorMessage="Please enter county" ValidationGroup="vgManageCounty"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                         <tr>
                                <td colspan="2" style="height: 11px;">
                                </td>
                            </tr
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" OnClientClick="__defaultFired = false;"
                                    Text="Create" UseSubmitBehavior="False" ValidationGroup="vgManageCounty" />
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" 
                                    Text="Cancel" onclick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
           
    </fieldset>
               
            </ContentTemplate>
           
        </asp:UpdatePanel>
        
        </div>
    
    <asp:UpdatePanel ID="upCounties" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:GridView ID="gvCounties" runat="server" AllowPaging="False" PageSize="10" AutoGenerateColumns="False"
                DataKeyNames="id"  
                SkinID="gridViewAdmin" OnRowEditing="gvCounties_RowEditing" Width="920px" Style="clear: both;"
                OnRowDeleting="gvCounties_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="name" HeaderText="County" >
                    <HeaderStyle  CssClass="moveLeft" />                       
                        </asp:BoundField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                ImageUrl="~/App_Themes/Default/icons/edit.gif" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Default/icons/delete.gif"
                                CommandName="Delete" />
                            <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this county?"
                                TargetControlID="imgBtnDelete">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="tdActions" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
           
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
