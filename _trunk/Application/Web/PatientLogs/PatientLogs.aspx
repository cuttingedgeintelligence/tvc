﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="PatientLogs.aspx.cs" Inherits="CEI.Web.PatientLogs.PatientLogs" Title="Pending Patient Schedule"
    Async="true" %>

<%@ Register Src="../UserControls/DayMonthYear.ascx" TagName="DayMonthYear" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Pending Patient Schedule" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <fieldset class="fieldsetLeft" style="margin-bottom: 10px; overflow: hidden;">
        <legend>
            <asp:Label ID="lblSelectedPatientLog" runat="server" Text="Create New Patient Schedule"></asp:Label>
        </legend>
        <table class="style1">
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPractice" runat="server" Text="Practice Name:"></asp:Label>&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="ddlPractice" runat="server" Style="margin: 5px 0;">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="height: 6px;">
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="btnCreatePatientLog" runat="server" CausesValidation="False" OnClick="btnCreatePatientLog_Click"
                        Text="Create Schedule" />
                </td>
            </tr>
        </table>
    </fieldset>
    <asp:UpdatePanel ID="upPatientLog" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:GridView ID="gvPatientLogs" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                DataKeyNames="Id,PracticeId,PracticeName" OnPageIndexChanging="gvPatientLogs_PageIndexChanging"
                OnRowDeleting="gvPatientLogs_RowDeleting" OnRowEditing="gvPatientLogs_RowEditing"
                SkinID="gridViewAdmin" Width="920px" Style="clear: both;" 
                EmptyDataText="There are no pending patient schedule in the system " 
                ondatabound="gvPatientLogs_DataBound" onrowcreated="gvPatientLogs_RowCreated">
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="PracticeId" HeaderStyle-Width="440px"
                        DataNavigateUrlFormatString="~/ViewPracticeProfile/ViewPracticeProfile.aspx?id={0}"
                        DataTextField="practiceName" HeaderText="Practice Name">
                        <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:BoundField DataField="serviceDate" HeaderText="Service Date" DataFormatString="{0:MM/dd/yyyy}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                ImageUrl="~/App_Themes/Default/icons/edit.gif" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Default/icons/delete.gif"
                                CommandName="Delete" />
                            <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this patient schedule?"
                                TargetControlID="imgBtnDelete">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="tdActions" />
                    </asp:TemplateField>
                </Columns>
                     <PagerTemplate>
                    <div style="padding: 5px;">
                        <div>
                            <asp:Button CommandArgument="Prev" CommandName="Page" Text="Previous" ID="btnPrevious"
                                runat="server" meta:resourcekey="ibPrevResource1" />&nbsp;
                            <asp:Button CommandArgument="Next" CommandName="Page" Text="Next" ID="btnNext" runat="server"
                                meta:resourcekey="ibNextResource1" />
                        </div>
                        <span id="spannumeric" runat="server" class="spannumeric"></span>
                    </div>
                </PagerTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsPatientLogs" runat="server" EnablePaging="True" MaximumRowsParameterName="pageSize"
                OnSelecting="odsPatientLogs_Selecting" SelectCountMethod="GetPatientLogsSearchTotalCount"
                SelectMethod="GetPatientLogsSearch" TypeName="CEI.BR.PatientLogBR">
                <SelectParameters>
                    <asp:Parameter Name="practiceId" />
                    <asp:Parameter Name="status" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
