﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;

namespace CEI.Web.PatientLogs
{
    public partial class ViewCompletedPatientLogs : BasePage
    {

        #region GridView columns


        private const int GV_PATIENTLOGS_PRACTICE_NAME = 0;
        private const int GV_PATIENTLOGS_DATE = 1;
        private const int GV_PATIENTLOGS_VIEW = 2;
      

        #endregion //GridView columns


       

        #region Event handlers


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Roles.IsUserInRole(RoleEnum.Practice.ToString()))
                {
                    ViewState["practiceId"] = new Guid(User.Identity.Name);
                    searchPanel.Visible = false;
                }
                else
                {
                    BindListControl<DataTable>.BindAndAddListItem(ddlPracticesSearch, new UserBR().GetAllPracticesNameAndId(), "name", "id", "All", Guid.Empty.ToString());
                    ViewState["practiceId"] = ddlPracticesSearch.SelectedValue;
                }



                this.BindGridViewWithSearchResults();
            }
        }

        protected void gvPatientLogs_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            

            if (e.CommandName.Equals("view"))
            {
                Response.Redirect("~/PatientLogs/ViewCompletedPatientLog.aspx?logId=" + gvPatientLogs.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["Id"].ToString() + "&prName=" + gvPatientLogs.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["PracticeName"].ToString() + "&servDate=" + gvPatientLogs.Rows[int.Parse(e.CommandArgument.ToString())].Cells[GV_PATIENTLOGS_DATE].Text, true);

            }

           
        }


        protected void gvPatientLogs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPatientLogs.PageIndex = e.NewPageIndex;
            gvPatientLogs.DataSource = odsPatientLogs;
            gvPatientLogs.DataBind();

        }

        protected void odsPatientLogs_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["practiceId"] = ViewState["practiceId"];

            e.InputParameters["status"] = PatientLogStatusEnum.Pending__patient__log;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

           
            ViewState["practiceId"] = ddlPracticesSearch.SelectedValue;

            this.BindGridViewWithSearchResults();
        }

      



        #endregion //Event handlers

        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvPatientLogs.PageIndex = 0;
            gvPatientLogs.DataSource = odsPatientLogs;
            gvPatientLogs.DataBind();
        }

        #endregion //Private methods


        protected void gvPatientLogs_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvPatientLogs.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left","5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i-1)
                        myButton.Enabled = false;
                    //pagerRow.Cells[0].Controls.Add(myButton);
                     spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    //pagerRow.Cells[0].Controls.Add(lblSpace);
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvPatientLogs_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvPatientLogs.BottomPagerRow;
            if (gvrPager == null) return;

            //ImageButton first = (ImageButton)gvrPager.Cells[0].FindControl("ibFirstTreatment");
         //   LinkButton prev = (LinkButton)gvrPager.Cells[0].FindControl("ibPrevTreatment");
            //ImageButton last = (ImageButton)gvrPager.Cells[0].FindControl("ibLastTreatment");
         //   LinkButton next = (LinkButton)gvrPager.Cells[0].FindControl("ibNextTreatment");
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvPatientLogs.PageIndex == 0)
            {
               // first.Visible = false;
                prev.Visible = false;
               // last.Visible = true;
                next.Visible = true;
            }
            else
                if (gvPatientLogs.PageIndex == (gvPatientLogs.PageCount - 1))
                {
                   // first.Visible = true;
                    prev.Visible = true;
                   // last.Visible = false;
                    next.Visible = false;
                }

            //Label lblPageCount = (Label)gvrPager.Cells[0].FindControl("lblPageCountTreatment");
            //TextBox txtPageNum = (TextBox)gvrPager.Cells[0].FindControl("txtPageNumTreatment");
            //txtPageNum.Text = (gvPatientLogs.PageIndex + 1).ToString();

            //int outOf = (gvPatientLogs.PageIndex * gvPatientLogs.PageSize) + gvPatientLogs.PageSize;
            //if (outOf > Convert.ToInt32(ViewState["gvTotalTreatmentCount"]))
            //    outOf = Convert.ToInt32(ViewState["gvTotalTreatmentCount"]);

            //Label lblOutOf = (Label)gvrPager.Cells[0].FindControl("lblOutOfTreatment");
            //lblOutOf.Text = "Results " + ((gvPatientLogs.PageIndex * gvPatientLogs.PageSize) + 1) + " - " + outOf + " out of " + ViewState["gvTotalTreatmentCount"];

            //// populate page count
            //if (lblPageCount != null)
            //    lblPageCount.Text = gvPatientLogs.PageCount.ToString();
        }
   
    }
}
