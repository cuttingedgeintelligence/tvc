﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="True"
    CodeBehind="ManagePatientLog.aspx.cs" Inherits="CEI.Web.PatientLogs.ManagePatientLog"
    Title="Manage Patient Schedule" Async="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControls/DayMonthYear.ascx" TagName="DayMonthYear" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style3
        {
            width: 151px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">

        function ValidateSelectedPatient(source, arguments) {
            if (arguments.Value != '-1')
                arguments.IsValid = true;
            else
                arguments.IsValid = false;
        }
   
    </script>

    <div>
        <div style="float: right; padding-top: 15px; display: none;">
            <asp:Label ID="lblNote1" runat="server" Style="color: Blue; font-size: 14px; font-weight: bold;"
                Text="Please note, appointments are scheduled in 20 minute intervals."></asp:Label>
        </div>
        <div style="float: left;">
            <asp:Label ID="lblHeaderText" runat="server" Text="Manage Patient Schedule" CssClass="lblTitle"
                Style="width: 500px;"></asp:Label>
        </div>
        <div style="clear: both;">
        </div>
    </div>
    <asp:Label ID="lblMainErrorMessage" runat="server" ForeColor="Red"></asp:Label>
    <asp:Panel ID="pnlManagePatientLog" runat="server">
        <asp:UpdatePanel ID="upAlreadyCreatedPatientLogMessage" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label ID="lblPatientLogOnTheSameDateIsAlreadyCreated" runat="server" ForeColor="Red"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlServiceDate" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
        <div style="vertical-align: bottom; margin-bottom: 10px; overflow: hidden;">
            <asp:UpdatePanel ID="upAddPatient" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlAddPatientAndFollowUp" runat="server">
                        <fieldset class="fieldsetLeft" style="width: 410px;">
                            <legend style="margin: 0; padding: 0;">
                                <asp:Label ID="lblSelectedPatient" Style="margin: 0; padding: 0;" runat="server"
                                    Text="Add Patient To Schedule"></asp:Label>
                            </legend>
                            <asp:UpdatePanel ID="upPracticeAndServiceDate" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table width="400px">
                                        <tr>
                                            <td style="width: 150px; padding: 5px 0;">
                                                <asp:Label ID="lblPracticeName" runat="server" Text="Practice Name:"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPracticeNameData" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 5px 0;">
                                                <asp:Label ID="lblServiceDate" runat="server" Text="Select Appt Date:"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblServiceDateData" runat="server"></asp:Label>
                                                <asp:DropDownList ID="ddlServiceDate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlServiceDate_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 22px;">
                                            </td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvServiceDate" runat="server" ControlToValidate="ddlServiceDate"
                                                    Display="Dynamic" ErrorMessage="Select the appointment date" ValidationGroup="vgSavePatientLog"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Panel ID="pnlAddPatient" runat="server" DefaultButton="btnAddPatient">
                                <table width="400px">
                                    <tr>
                                        <td class="style3">
                                            <asp:Label ID="lblPatient" runat="server" Text="Add Patient Name:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlPatient" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPatient_SelectedIndexChanged"
                                                Style="margin: 0;" Height="22px" Width="145px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 22px;">
                                        </td>
                                        <td>
                                            <asp:CustomValidator ID="cvPatient" runat="server" ControlToValidate="ddlPatient"
                                                ClientValidationFunction="ValidateSelectedPatient" Display="Dynamic" ErrorMessage="Please select patient"
                                                ValidationGroup="vgAddPatient" SetFocusOnError="True"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            <asp:Label ID="lblTime" runat="server" Text="Select Appt Time:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlHour" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="lblAppointmentTimeSeparator" runat="server" Text=":"></asp:Label>
                                            <asp:DropDownList ID="ddlMinutes" runat="server">
                                                <asp:ListItem>00</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                                <asp:ListItem>40</asp:ListItem>
                                                <asp:ListItem>50</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlAMPM" runat="server">
                                                <asp:ListItem>AM</asp:ListItem>
                                                <asp:ListItem>PM</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                        </td>
                                        <td style="height: 7px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 5px 0;" class="style3">
                                            <asp:Label ID="lblGlaucoma" runat="server" Text="Add Glaucoma Test:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="cbGlaucoma" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 5px 0;" class="style3">
                                            <asp:Label ID="lblRetina" runat="server" Text="Add Retina Test:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="cbRetina" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 10px" class="style3">
                                            <asp:Label ID="lblManifestRefraction" runat="server" Text="Manifest Refraction:"></asp:Label>
                                        </td>
                                        <td style="padding-top: 10px">
                                            <asp:RadioButtonList ID="rdbManifestRefraction" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="False">Yes</asp:ListItem>
                                                <asp:ListItem Selected="False">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 22px;">
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="revOD_Sph" runat="server" Display="Dynamic" ControlToValidate="txtOD_Sph"
                                                ErrorMessage=" Enter + or - for OD: Sph" ValidationGroup="vgAddPatient" ValidationExpression="^(((\+|\-)((\d*[0-9])(\.\d*[0-9])?)))|(0)|(no va)$"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                                                ControlToValidate="txtOD_Cyl" ValidationGroup="vgAddPatient" ErrorMessage=" Enter + or - for OD: Cyl"
                                                ValidationExpression="^((\+|\-)((\d*[0-9]|)(\.\d*[0-9])?))$"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic"
                                                ControlToValidate="txtOS_Sph" ErrorMessage=" Enter + or - for OS: Sph" ValidationGroup="vgAddPatient"
                                                ValidationExpression="^(((\+|\-)((\d*[0-9])(\.\d*[0-9])?)))|(0)|(no va)$"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="Dynamic"
                                                ControlToValidate="txtOS_Cyl" ErrorMessage=" Enter + or - for OS: Cyl" ValidationGroup="vgAddPatient"
                                                ValidationExpression="^((\+|\-)((\d*[0-9]|)(\.\d*[0-9])?))$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table width="60%" style="margin-left: 106px;">
                                                <tr>
                                                    <td valign="bottom" style="vertical-align: middle !important;">
                                                        <asp:Label ID="lblMR_OD" runat="server" Text="OD:"></asp:Label>
                                                        <asp:Label ID="lblOD_sph" runat="server" Text="sph"></asp:Label>
                                                    </td>
                                                    <td valign="bottom" style="vertical-align: middle !important;">
                                                        <asp:TextBox ID="txtOD_Sph" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                                                    </td>
                                                    <td valign="bottom" style="vertical-align: middle !important;">
                                                        <asp:Label ID="lblOD_Cyl" runat="server" Text="cyl" Style="margin-right: 3px;"></asp:Label>
                                                    </td>
                                                    <td valign="bottom" style="vertical-align: middle !important;">
                                                        <asp:TextBox ID="txtOD_Cyl" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                                                    </td>
                                                    <td valign="bottom" style="vertical-align: middle !important;">
                                                        <asp:Label ID="lblOD_Sep" runat="server" Text="x" Style="margin-right: 3px;"></asp:Label>
                                                    </td>
                                                    <td valign="bottom" style="vertical-align: bottom !important;">
                                                        <asp:TextBox ID="txtOD_Axis_Of_Astigmatism" runat="server" MaxLength="4" Width="50px"></asp:TextBox>
                                                        <%--          <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtOD_Axis_Of_Astigmatism" ValidationGroup="vgAddPatient"
                                                            ErrorMessage="Enter + or -" ValidationExpression="^((\+|\-)(\d*[0-9](\.\d*[0-9])?))$"></asp:RegularExpressionValidator>
                                            --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle !important;">
                                                        <asp:Label ID="lblMR_OS" runat="server" Text="OS:"></asp:Label>
                                                        <asp:Label ID="lblOS_sph" runat="server" Text="sph"></asp:Label>
                                                    </td>
                                                    <td style="vertical-align: middle !important;" valign="bottom">
                                                        <asp:TextBox ID="txtOS_Sph" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                                                    </td>
                                                    <td style="vertical-align: middle !important;" valign="bottom">
                                                        <asp:Label ID="lblOS_Cyl" runat="server" Text="cyl" Style="margin-right: 3px;"></asp:Label>
                                                    </td>
                                                    <td style="vertical-align: middle !important;" valign="bottom">
                                                        <asp:TextBox ID="txtOS_Cyl" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                                                    </td>
                                                    <td style="vertical-align: middle !important;" valign="bottom">
                                                        <asp:Label ID="lblOS_Sep" runat="server" Text="x" Style="margin-right: 3px;"></asp:Label>
                                                    </td>
                                                    <td style="vertical-align: middle !important;" valign="bottom">
                                                        <asp:TextBox ID="txtOS_Axis_Of_Astigmatism" runat="server" MaxLength="4" Width="50px"></asp:TextBox>
                                                        <%--     <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" Display="Dynamic"
                                                            ControlToValidate="txtOS_Axis_Of_Astigmatism" ErrorMessage="Enter + or -" ValidationGroup="vgAddPatient"
                                                            ValidationExpression="^((\+|\-)(\d*[0-9](\.\d*[0-9])?))$"></asp:RegularExpressionValidator>
                                              --%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblPachymetry" runat="server" Text="Pachymetry:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="revOD_Pachymetry" runat="server" ControlToValidate="txtOD_Pachymetry"
                                                Display="Dynamic" ErrorMessage="Pachymetry OD value must be a number" ValidationExpression="\d*"
                                                ValidationGroup="vgAddPatient"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="revOD_Pachymetry0" runat="server" ControlToValidate="txtOS_Pachymetry"
                                                Display="Dynamic" ErrorMessage="Pachymetry OS value must be a number" ValidationExpression="\d*"
                                                ValidationGroup="vgAddPatient"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table width="260px" style="margin: 0 0 10px 105px;">
                                                <tr>
                                                    <td style="vertical-align: middle !important; width: 18%; text-align: right; padding-right: 3px;"
                                                        valign="bottom">
                                                        <asp:Label ID="lblOD_Pachymetry" runat="server" Text="OD:"></asp:Label>
                                                    </td>
                                                    <td style="vertical-align: bottom !important; width: 19%" valign="bottom">
                                                        <asp:TextBox ID="txtOD_Pachymetry" runat="server" MaxLength="3" Width="50px"></asp:TextBox>
                                                    </td>
                                                    <td style="vertical-align: middle !important; width: 8%" valign="bottom">
                                                        <asp:Label ID="lblOS_Pachymetry" runat="server" Text="OS:"></asp:Label>
                                                    </td>
                                                    <td style="vertical-align: bottom !important; width: 49%" valign="bottom">
                                                        <asp:TextBox ID="txtOS_Pachymetry" runat="server" MaxLength="3" Width="50px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style3">
                                            <asp:Label ID="lblInsurance" runat="server" Text="Insurance:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlInsurance" runat="server" Height="22px" Width="145px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 22px;">
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="rfvInsurance" Style="line-height: 22px; margin: 0;
                                                padding: 0;" runat="server" ControlToValidate="ddlInsurance" Display="Dynamic"
                                                ErrorMessage="Please select insurance" ValidationGroup="vgAddPatient" InitialValue="-1"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: center; padding: 10px 0 0 24px;">
                                            <asp:Button ID="btnAddPatient" runat="server" OnClick="btnAddPatient_Click" Text="Add"
                                                ValidationGroup="vgAddPatient" OnClientClick="__defaultFired = false;" UseSubmitBehavior="False" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                                                OnClick="btnCancel_Click" />
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red" Style="display: none;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </fieldset>
                        <fieldset class="fieldsetLeft" style="width: 456px; margin: 0 0 10px 10px; height: 80px;">
                            <legend>
                                <asp:Label ID="Label3" runat="server" Text="Appt Reminder"></asp:Label>
                            </legend>
                            <asp:Label ID="Label4" Style="line-height: 20px;" runat="server" Text="- Please note, appointments are scheduled in 20 minute intervals."></asp:Label>
                        </fieldset>
                        <fieldset class="fieldsetLeft" style="width: 456px; margin: 0 0 10px 10px; height: 92px;">
                            <legend>
                                <asp:Label ID="lblFollowUpPatients" runat="server" Text="Follow Up Patients"></asp:Label>
                            </legend>
                            <asp:Label ID="lblFollowUpPatientsMessage" Style="line-height: 20px;" runat="server"
                                Text="- Patients selected from this drop down menu can be added to the Schedule."></asp:Label>
                            <br />
                            <br />
                            <asp:Label ID="lblFollowUpPatient" Style="padding-left: 8px;" runat="server" Text="Patient:"></asp:Label><asp:DropDownList
                                ID="ddlFollowUpPatient" Style="margin-left: 5px; margin-bottom: 0;" runat="server"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlFollowUpPatient_SelectedIndexChanged">
                            </asp:DropDownList>
                        </fieldset>
                        <fieldset class="fieldsetLeft" style="width: 456px; margin: 0 0 10px 10px; height: 93px;">
                            <legend>
                                <asp:Label ID="lblNoShowPatients" runat="server" Text="No Show Patients"></asp:Label>
                            </legend>
                            <asp:Label ID="lblNoShowPatientsMessage" Style="line-height: 20px;" runat="server"
                                Text="- Patients selected from this drop down menu can be added to the Schedule."></asp:Label>
                            <br />
                            <br />
                            <asp:Label ID="lblNoShowPatient" Style="padding-left: 8px;" runat="server" Text="Patient:"></asp:Label>
                            <asp:DropDownList ID="ddlNoShowPatient" Style="margin-left: 5px;" runat="server"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlNoShowPatient_SelectedIndexChanged">
                            </asp:DropDownList>
                        </fieldset>
                        <fieldset class="fieldsetLeft" style="width: 456px; margin: 0 0 10px 10px;">
                            <legend>
                                <asp:Label ID="lblNewPatient" runat="server" Text="Create New Patient"></asp:Label>
                            </legend>
                            <asp:Label ID="lblCreatePatientInfo" runat="server" Text="- If the patient is not found in either the Follow Up List or the No Show List, then the patient is new to the database.<Br />- To create a new patient into the database, please click on this button."></asp:Label>
                            <br />
                            <br />
                            <asp:Button ID="btnCreateNewPatient" Style="float: right; margin-right: 10px; margin-bottom: 9px;"
                                runat="server" Text="Create New Patient" CausesValidation="false" OnClick="btnCreateNewPatient_Click" />
                        </fieldset>
                        
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlServiceDate" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
            
        </div><div style="padding-top: 5px; overflow:hidden;">
                            <asp:Label ID="lblNote2" runat="server" Text=" - The Patient Schedule will automatically be submitted to Total Vision Care Admin (3) days prior to the service on "></asp:Label>(<asp:Label
                                ID="lblNoteDate" runat="server"></asp:Label><asp:Label ID="lblNote3" runat="server"
                                    Text="">).</asp:Label>
                            <br /><br />
                            <asp:Label ID="lblNote4" runat="server" Text="- Should any revisions be needed after this date, please contact Total Vision Care at (310) 305-2020  in order for changes to be made in a timely manner."></asp:Label><br /><br />
                        </div>
        <asp:UpdatePanel ID="upPatients" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlPatients" runat="server">
                    <div style="width: 100%; padding: 10px 0;">
                        <table>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="btnPrintPDF" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png"
                                        ToolTip="Download PDF" OnClick="btnPrintPDF_Click" Style="display: none; padding-right: 3px;" />
                                </td>
                                <td style="padding-left: 5px">
                                    <asp:ImageButton ID="btnPrintExcel" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png"
                                        ToolTip="Download Excel" OnClick="btnPrintExcel_Click" Style="display: none;" />
                                </td>
                                <td style="padding-left: 5px">
                                    <asp:ImageButton ID="btnPrintPreview" runat="server" ImageUrl="~/Images/Mockups/printIcon.png"
                                        ToolTip="View and Print" OnClick="btnPrintPreview_Click" Style="display: none;" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:GridView ID="gvPatients" runat="server" AutoGenerateColumns="False" DataKeyNames="Id,PatientId"
                        SkinID="gridViewManagePatientLog" OnRowEditing="gvPatients_RowEditing" Width="920px"
                        Style="clear: both;" OnRowDeleting="gvPatients_RowDeleting" OnRowDataBound="gvPatients_RowDataBound"
                        EmptyDataText="There are no patients in the patient schedule" 
						EnableModelValidation="True">
                        <Columns>
                            <asp:TemplateField HeaderText="Appt Time">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle Width="55px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="F or M">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:HyperLinkField DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}"
                                DataTextField="firstName" HeaderText="First Name">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:HyperLinkField>
                            <asp:HyperLinkField DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}"
                                DataTextField="lastName" HeaderText="Last Name">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:HyperLinkField>
                            <asp:BoundField DataField="dateOfBirth" DataFormatString="{0:MM/dd/yyyy}" HeaderText="DOB">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="G">
                                <ItemTemplate>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="R">
                                <ItemTemplate>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <%-- <asp:TemplateField HeaderText="Manifest Refraction">
                                <ItemTemplate>
                                    <table  cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="45px" >
                                                <asp:Label ID="lblOD_SphTitle" runat="server" Text="OD:&nbsp<b>sph</b>"></asp:Label>
                                            </td>
                                            <td width="45px" align="right">
                                                <asp:Label ID="lblOD_Sph" runat="server"></asp:Label>
                                            </td>
                                            <td width="20px" style="padding-left: 3px;">
                                                <asp:Label ID="lblOD_CylTitle" runat="server" Text="cyl" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td width="35px">
                                                <asp:Label ID="lblOD_Cyl" runat="server"></asp:Label>
                                            </td>
                                            <td width="10px" style="padding-left: 3px;">
                                                <asp:Label ID="lblOD_AxisOfAstigmatismTitle" Font-Bold="True" runat="server" Text="x"></asp:Label>
                                            </td>
                                            <td width="25px">
                                                <asp:Label ID="lblOD_AxisOfAstigmatism" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblOS_SphTitle" runat="server" Text="OS:&nbsp<b>sph</b>"></asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="lblOS_Sph" runat="server"></asp:Label>
                                            </td>
                                            <td style="padding-left: 3px;">
                                                <asp:Label ID="lblOS_CylTitle" runat="server" Text="cyl" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOS_Cyl" runat="server"></asp:Label>
                                            </td>
                                            <td style="padding-left: 3px;">
                                                <asp:Label ID="lblOS_AxisOfAstigmatismTitle" Font-Bold="True" runat="server" Text="x"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOS_AxisOfAstigmatism" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Manifest Refraction">
                                <ItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="15px">
                                                <asp:Label ID="lblOD_SphTitle" runat="server" Text="OD:"></asp:Label>&nbsp;
                                            </td>
                                            <td width="20px" align="right">
                                                <asp:Label ID="Label1" runat="server" Text="sph" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td width="40px" align="left" style="padding-left: 5px;">
                                                <asp:Label ID="lblOD_Sph" runat="server"></asp:Label>
                                            </td>
                                            <td width="20px" style="padding-left: 14px;">
                                                <asp:Label ID="lblOD_CylTitle" runat="server" Text="cyl" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td width="40px" align="left" style="padding-left: 5px;">
                                                <asp:Label ID="lblOD_Cyl" runat="server"></asp:Label>
                                            </td>
                                            <td width="10px" style="padding-left: 14px;">
                                                <asp:Label ID="lblOD_AxisOfAstigmatismTitle" Font-Bold="True" runat="server" Text="x"></asp:Label>
                                            </td>
                                            <td width="25px" align="left" style="padding-left: 5px;">
                                                <asp:Label ID="lblOD_AxisOfAstigmatism" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblOS_SphTitle" runat="server" Text="OS:"></asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="Label2" runat="server" Text="sph" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td align="left" style="padding-left: 5px;">
                                                <asp:Label ID="lblOS_Sph" runat="server"></asp:Label>
                                            </td>
                                            <td style="padding-left: 14px;">
                                                <asp:Label ID="lblOS_CylTitle" runat="server" Text="cyl" Font-Bold="true"></asp:Label>
                                            </td>
                                            <td align="left" style="padding-left: 5px;">
                                                <asp:Label ID="lblOS_Cyl" runat="server"></asp:Label>
                                            </td>
                                            <td style="padding-left: 14px;">
                                                <asp:Label ID="lblOS_AxisOfAstigmatismTitle" Font-Bold="True" runat="server" Text="x"></asp:Label>
                                            </td>
                                            <td align="left" style="padding-left: 5px;">
                                                <asp:Label ID="lblOS_AxisOfAstigmatism" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pachy">
                                <ItemTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblOD_PachymetryTitle" runat="server" Text="OD:&nbsp"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOD_Pachymetry" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblOS_PachymetryTitle" runat="server" Text="OS:&nbsp"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOS_Pachymetry" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="insuranceCompanyName" HeaderText="Patient Insurance">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                        AlternateText="addpatient" ImageUrl="~/App_Themes/Default/icons/edit.gif" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle CssClass="tdActionsManagePatientLog" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" CommandName="Delete"
                                        ImageUrl="~/App_Themes/Default/icons/delete.gif" />
                                    <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this patient from the schedule?"
                                        TargetControlID="imgBtnDelete">
                                    </cc1:ConfirmButtonExtender>
                                </ItemTemplate>
                                <ItemStyle CssClass="tdActionsManagePatientLog" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:Button ID="btnDeletePatientLog" runat="server" Text="Delete Schedule" CausesValidation="false"
                        OnClick="btnDeletePatientLog_Click" Style="display: none;" />
                    <cc1:ConfirmButtonExtender ID="cbebtnDeletePatientLog" runat="server" ConfirmText="Are you sure you want to delete this patient schedule?"
                        TargetControlID="btnDeletePatientLog">
                    </cc1:ConfirmButtonExtender>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnAddPatient" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ddlServiceDate" EventName="SelectedIndexChanged" />
                <asp:PostBackTrigger ControlID="btnPrintPDF" />
                <asp:PostBackTrigger ControlID="btnPrintExcel" />
                <asp:PostBackTrigger ControlID="btnPrintPreview" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
