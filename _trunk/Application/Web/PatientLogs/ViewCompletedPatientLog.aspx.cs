﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;

using Microsoft.Reporting.WebForms;



namespace CEI.Web.PatientLogs
{
    public partial class ViewCompletedPatientLog : BasePage
    {

        #region GridView columns


        private const int GV_PATIENTS_TIME = 0;
        private const int GV_PATIENTS_GENDER = 1;
        private const int GV_PATIENTS_FIRST_NAME = 2;
        private const int GV_PATIENTS_LAST_NAME = 3;
        private const int GV_PATIENTS_DATE_OF_BIRTH = 4;
        private const int GV_PATIENTS_GLAUCOMA = 5;
        private const int GV_PATIENTS_RETINA = 6;
        private const int GV_PATIENTS_MANIFEST_REFRACTION = 7;
        private const int GV_PATIENTS_PACHYMETRY = 8;
        private const int GV_PATIENTS_INSURANCE = 9;
        private const int GV_PATIENTS_HISTORY = 10;
     
    
        

        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {

           
            
            if (!Page.IsPostBack)
            {
                if (this.ValidateRequestParams())
                {

                        lblPracticeNameData.Text = Request.QueryString["prName"];
                        lblServiceDateData.Text = Request.QueryString["servDate"];
                        List<PatientLogPatient> patientsList = new PatientLogBR().GetPatientLogPatientsByPatientLogId((int)ViewState["patientLogId"]);

                        ViewState["PatientsList"] = patientsList;
                        this.BingGridViewPatients(patientsList);
                   
                      
                }
                else
                {
                    //TODO: show error message
                    //Response.Redirect("~/Default.aspx");
                }
            }
        }




        #region gridview

        protected void gvPatients_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                PatientLogPatient patientLogPatient = e.Row.DataItem as PatientLogPatient;
                if (patientLogPatient.Glaucoma)
                    //    e.Row.Cells[GV_PATIENTS_GLAUCOMA].Text = "G";
                    e.Row.Cells[GV_PATIENTS_GLAUCOMA].Text = patientLogPatient.HRT_Glaucoma.ToString();

                if (patientLogPatient.Retina)
                    //    e.Row.Cells[GV_PATIENTS_RETINA].Text = "R";
                    e.Row.Cells[GV_PATIENTS_RETINA].Text = patientLogPatient.HRT_Retina.ToString();

                if (patientLogPatient.Gender.Equals("Male"))
                    e.Row.Cells[GV_PATIENTS_GENDER].Text = "M";
                else
                    if (patientLogPatient.Gender.Equals("Female"))
                        e.Row.Cells[GV_PATIENTS_GENDER].Text = "F";
                    else
                        e.Row.Cells[GV_PATIENTS_GENDER].Text = "N/A";




                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOD_Sph") as Label).Text = patientLogPatient.OD_Sphere;
                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOD_Cyl") as Label).Text = patientLogPatient.OD_Cylinder;
                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOD_AxisOfAstigmatism") as Label).Text = patientLogPatient.OD_AxisOfAstigmatism;

                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOS_Sph") as Label).Text = patientLogPatient.OS_Sphere;
                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOS_Cyl") as Label).Text = patientLogPatient.OS_Cylinder;
                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOS_AxisOfAstigmatism") as Label).Text = patientLogPatient.OS_AxisOfAstigmatism;





                if (patientLogPatient.OD_Pachymetry.HasValue)
                    (e.Row.Cells[GV_PATIENTS_PACHYMETRY].FindControl("lblOD_Pachymetry") as Label).Text = patientLogPatient.OD_Pachymetry.ToString();
                if (patientLogPatient.OS_Pachymetry.HasValue)
                    (e.Row.Cells[GV_PATIENTS_PACHYMETRY].FindControl("lblOS_Pachymetry") as Label).Text = patientLogPatient.OS_Pachymetry.ToString();

                if (patientLogPatient.PatientHistory.Equals(PatientHistoryEnum.No__Show))
                    e.Row.Cells[GV_PATIENTS_HISTORY].Text = "No Show";
                else if (patientLogPatient.PatientHistory.Equals(PatientHistoryEnum.Follow__Up))
                    e.Row.Cells[GV_PATIENTS_HISTORY].Text = "Follow Up";
                else if (patientLogPatient.PatientHistory.Equals(PatientHistoryEnum.New__Patient))
                    e.Row.Cells[GV_PATIENTS_HISTORY].Text = "New Patient";


                e.Row.Cells[GV_PATIENTS_TIME].Text = DateTime.ParseExact(patientLogPatient.AppointmentTime, "HH:mm:ss", null).ToString("h:mm tt");
            }
        }

       



        #endregion gridview


       


       

        protected void btnDownloadRpt_Click(object sender, EventArgs e)
        {
            switch (ddlPrintType.SelectedValue)
            {
                case "1":
                    PrintReport(ExportType.Printer);
                    break;
                case "2":
                    PrintReport(ExportType.Pdf);
                    break;
                case "3":
                    PrintReport(ExportType.Excel);
                    break;
                default:
                    break;
            }
        }

        protected void btnPrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Pdf);
        }

        protected void btnPrintExcel_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Excel);
        }

        protected void btnPrintPreview_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Printer);
        }

        #endregion //Event handlers

        #region Private methods

        private bool ValidateRequestParams()
        {
            bool isValid = false;
            int patientLogId;
                if (
                        
                        Request.QueryString["prName"] != null &&
                        Request.QueryString["servDate"] != null &&
                        Request.QueryString["logId"] != null &&
                        int.TryParse(Request.QueryString["logId"],out patientLogId) && 
                        this.ValidateServiceDate(Request.QueryString["servDate"])
                    )
                    {
                        isValid = true;
                        ViewState["patientLogId"] = patientLogId;
                    }




            return isValid;


        }



        private bool ValidateServiceDate(string serviceDate)
        {
            try
            {
                DateTime.ParseExact(serviceDate, "MM/dd/yyyy", null);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }

        }


        private void BingGridViewPatients(List<PatientLogPatient> patientsList)
        {

            gvPatients.DataSource = patientsList;
            gvPatients.DataBind();
            
        }


        

       

        #endregion //Private methods


        #region MICROSOFT REPORTING

        private void PrintReport(ExportType exportType)
        {

            TotalVisionReport _totalVisionReport = new TotalVisionReport();

            List<PatientLogPatient> patientsList = ViewState["PatientsList"] as List<PatientLogPatient>;

            

            LocalReport localReport = new LocalReport();

            localReport.ReportPath = Server.MapPath("~/Reports/rptPatientLog.rdlc");

            ReportDataSource reportDataSource = new ReportDataSource("PatientLogPatient", patientsList);
            localReport.DataSources.Add(reportDataSource);

            ReportParameter[] param = new ReportParameter[2];
            param[0] = new ReportParameter("rptParamPracticeName", lblPracticeNameData.Text);
            param[1] = new ReportParameter("rptParamDate", lblServiceDateData.Text);

            localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            if (exportType == ExportType.Printer)
            {
                _totalVisionReport.ListItems = patientsList;
                _totalVisionReport.Pameters.Add("rptParamPracticeName", lblPracticeNameData.Text);
                _totalVisionReport.Pameters.Add("rptParamDate", lblServiceDateData.Text);
                Session["PrintingParams"] = _totalVisionReport;

                Response.Redirect("~/Reports/ReportPreview.aspx?ReportName=PatientLog");
            }
            else if (exportType == ExportType.Pdf)
            {
                DownloadPDF(localReport);

            }
            else if (exportType == ExportType.Excel)
            {
                DownloadExcel(localReport);

            }
        }

        private void DownloadPDF(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PDF</OutputFormat>" +
            //"  <PageWidth>11in</PageWidth>" +
            //"  <PageHeight>8.5in</PageHeight>" +
            //"  <MarginTop>0.2in</MarginTop>" +
            //"  <MarginLeft>0.1in</MarginLeft>" +
            //"  <MarginRight>0.1in</MarginRight>" +
            //"  <MarginBottom>0.2in</MarginBottom>" +
            //"</DeviceInfo>";
            string deviceInfo =
    "<DeviceInfo>" +
    "  <OutputFormat>PDF</OutputFormat>" +
    "  <PageWidth>11in</PageWidth>" +
    "  <PageHeight>8.5in</PageHeight>" +
    "  <MarginTop>0.2in</MarginTop>" +
    "  <MarginLeft>0.6in</MarginLeft>" +
    "  <MarginRight>0.1in</MarginRight>" +
    "  <MarginBottom>0.2in</MarginBottom>" +
    "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=PatientSchedule." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DownloadExcel(LocalReport localReport)
        {
            string reportType = "Excel";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
            "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=PatientSchedule." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }


        #endregion

        

       










    }
}
