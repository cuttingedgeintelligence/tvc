﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.BR;
using CEI.Framework;
using CEI.Common;

namespace CEI.Web.PatientLogs
{
    public partial class PatientLogs : BasePage
    {

        #region GridView columns


        private const int GV_PATIENT_LOGS_PRACTICE_NAME = 0;
        private const int GV_PATIENT_LOGS_DATE = 1;
       
        private const int GV_PATIENT_LOGS_EDIT = 2;
        private const int GV_PATIENT_LOGS_DELETE = 3;

        #endregion //GridView columns


        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataTable dtPractices = new UserBR().GetAllPracticesNameAndId();
                BindListControl<DataTable>.Bind(ddlPractice, dtPractices, "name", "id");
                if (Roles.IsUserInRole(RoleEnum.Practice.ToString()))
                {
                    lblPractice.Style.Add("display", "none");
                    ddlPractice.Style.Add("display", "none");
                    ddlPractice.SelectedValue = User.Identity.Name;
                    ViewState["practiceId"] = new Guid(User.Identity.Name);
                }
                else
                {
                    ViewState["practiceId"] = Guid.Empty;
                }

                


                

                this.BindGridViewWithSearchResults();
            }
        }

        protected void btnCreatePatientLog_Click(object sender, EventArgs e)
        {
            //check if there are opened Final notes before letting the user to redirect.
            DataTable dtPendingFinalNotes = new FinalNotesWorksheetBR().GetFinalNotesWorksheetsSearch(Guid.Empty, new Guid(ddlPractice.SelectedValue), PatientLogStatusEnum.Worksheet, 10, 0);

            if (dtPendingFinalNotes.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(PatientLogs), "stillHavingPendingLogs", "alert('We noticed that there are one or more Pending Final Notes documents in the system for this practice. In order to create a new Patient Schedule you need to first Save & Complete the remaining Pending Final Notes for this practice.');", true);
                return;
            }
           
            Response.Redirect("~/PatientLogs/ManagePatientLog.aspx?action=create&prId="+ ddlPractice.SelectedValue + "&prName="+ ddlPractice.SelectedItem.Text,true);
        }

        protected void gvPatientLogs_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvPatientLogs.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvPatientLogs_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvPatientLogs.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvPatientLogs.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvPatientLogs.PageIndex == (gvPatientLogs.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }
        protected void gvPatientLogs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPatientLogs.PageIndex = e.NewPageIndex;
            gvPatientLogs.DataSource = odsPatientLogs;
            gvPatientLogs.DataBind();

        }

        protected void odsPatientLogs_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["practiceId"] = ViewState["practiceId"];

            e.InputParameters["status"] = PatientLogStatusEnum.Patient__log;
        }


        protected void gvPatientLogs_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            if (new PatientLogBR().DeletePatientLog((int)gvPatientLogs.DataKeys[e.RowIndex].Values["Id"]))
            {
                this.BindGridViewWithSearchResults();
            }

        }

        protected void gvPatientLogs_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int patientLogId = (int)gvPatientLogs.DataKeys[e.NewEditIndex].Values["Id"];
            Response.Redirect("~/PatientLogs/ManagePatientLog.aspx?action=update&prId=" + gvPatientLogs.DataKeys[e.NewEditIndex].Values["PracticeId"].ToString() + "&prName=" + gvPatientLogs.DataKeys[e.NewEditIndex].Values["PracticeName"].ToString() + "&servDate=" + gvPatientLogs.Rows[e.NewEditIndex].Cells[GV_PATIENT_LOGS_DATE].Text + "&logId=" + patientLogId.ToString(), true);
            
        }

     

     

        #endregion //Event handlers

        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvPatientLogs.PageIndex = 0;
            gvPatientLogs.DataSource = odsPatientLogs;
            gvPatientLogs.DataBind();
        }

        #endregion //Private methods

       

      

        

       
    }
}
