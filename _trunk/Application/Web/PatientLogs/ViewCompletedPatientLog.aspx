﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="True"
    CodeBehind="ViewCompletedPatientLog.aspx.cs" Inherits="CEI.Web.PatientLogs.ViewCompletedPatientLog"
    Title="View Patient Schedule" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControls/DayMonthYear.ascx" TagName="DayMonthYear" TagPrefix="uc1" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="View Patient Schedule" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <table width="300px">
        <tr>
            <td style="width:80px">
                <asp:Label ID="lblPracticeName" runat="server" Text="Practice Name:"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="lblPracticeNameData" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblServiceDate" runat="server" Text="Service Date:"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="lblServiceDateData" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <div>
        <table style="padding: 10px 0;">
            <tr>
                <td>
                    <asp:ImageButton ID="btnPrintPDF" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png"
                        ToolTip="Download PDF" OnClick="btnPrintPDF_Click" Style="padding-right: 3px;" />
                </td>
                <td>
                    <asp:ImageButton ID="btnPrintExcel" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png"
                        ToolTip="Download Excel" OnClick="btnPrintExcel_Click" Style="padding-left: 5px;" />
                </td>
                <td>
                    <asp:ImageButton ID="btnPrintPreview" runat="server" ImageUrl="~/Images/Mockups/printIcon.png"
                        ToolTip="View and Print" OnClick="btnPrintPreview_Click" Style="display: none;
                        padding-left: 5px;" />
                </td>
            </tr>
        </table>
        <div style="display: none;">
            <asp:DropDownList ID="ddlPrintType" runat="server">
                <asp:ListItem Text="To Printer" Value="1"></asp:ListItem>
                <asp:ListItem Text="To PDF" Value="2"></asp:ListItem>
                <asp:ListItem Text="To Excel" Value="3"></asp:ListItem>
            </asp:DropDownList>
            &nbsp;
            <asp:Button ID="btnDownloadRpt" runat="server" Text="Print Patient Schedule" CausesValidation="false"
                OnClick="btnDownloadRpt_Click" />
        </div>
    </div>
    <asp:UpdatePanel ID="upPatients" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvPatients"  runat="server"  AutoGenerateColumns="False" DataKeyNames="Id"
                SkinID="gridViewManagePatientLog" Width="920px" Style="clear: both;" OnRowDataBound="gvPatients_RowDataBound"
                EmptyDataText="There are no patients in the final notes worksheet">
                <Columns>
                    <asp:TemplateField HeaderText="Appt Time">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle Width="55px" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="F or M">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:HyperLinkField DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}"
                        DataTextField="firstName" HeaderText="First Name">
                        <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:HyperLinkField DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}"
                        DataTextField="lastName" HeaderText="Last Name">
                        <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:BoundField DataField="dateOfBirth" DataFormatString="{0:MM/dd/yyyy}" HeaderText="DOB">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="G">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="R">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Manifest Refraction">
                        <ItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="15px">
                                        <asp:Label ID="lblOD_SphTitle" runat="server" Text="OD:"></asp:Label>&nbsp;
                                    </td>
                                    <td width="20px" align="right">
                                        <asp:Label ID="Label1" runat="server" Text="sph" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td width="40px" align="left" style="padding-left: 5px;">
                                        <asp:Label ID="lblOD_Sph" runat="server"></asp:Label>
                                    </td>
                                    <td width="20px" style="padding-left: 14px;">
                                        <asp:Label ID="lblOD_CylTitle" runat="server" Text="cyl" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td width="40px" align="left" style="padding-left: 5px;">
                                        <asp:Label ID="lblOD_Cyl" runat="server"></asp:Label>
                                    </td>
                                    <td width="10px" style="padding-left: 14px;">
                                        <asp:Label ID="lblOD_AxisOfAstigmatismTitle" Font-Bold="True" runat="server" Text="x"></asp:Label>
                                    </td>
                                    <td width="25px" align="left" style="padding-left: 5px;">
                                        <asp:Label ID="lblOD_AxisOfAstigmatism" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblOS_SphTitle" runat="server" Text="OS:"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Label2" runat="server" Text="sph" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left" style="padding-left: 5px;">
                                        <asp:Label ID="lblOS_Sph" runat="server"></asp:Label>
                                    </td>
                                    <td style="padding-left: 14px;">
                                        <asp:Label ID="lblOS_CylTitle" runat="server" Text="cyl" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="left" style="padding-left: 5px;">
                                        <asp:Label ID="lblOS_Cyl" runat="server"></asp:Label>
                                    </td>
                                    <td style="padding-left: 14px;">
                                        <asp:Label ID="lblOS_AxisOfAstigmatismTitle" Font-Bold="True" runat="server" Text="x"></asp:Label>
                                    </td>
                                    <td align="left" style="padding-left: 5px;">
                                        <asp:Label ID="lblOS_AxisOfAstigmatism" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pachy">
                        <ItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblOD_PachymetryTitle" runat="server" Text="OD:"></asp:Label>&nbsp;
                                    </td>
                                    <td>
                                        <asp:Label ID="lblOD_Pachymetry" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblOS_PachymetryTitle" runat="server" Text="OS:"></asp:Label>&nbsp;
                                    </td>
                                    <td>
                                        <asp:Label ID="lblOS_Pachymetry" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="insuranceCompanyName" HeaderText="Patient Insurance">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                </asp:GridView>
            <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="false" PostBackUrl="~/PatientLogs/ViewCompletedPatientLogs.aspx" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divReport" visible="false" runat="server" style="width: 100%">
        <rsweb:ReportViewer ID="rptViewPatientLogs" runat="server" Width="100%">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
