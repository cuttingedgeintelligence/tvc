﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ViewCompletedPatientLogs.aspx.cs" Inherits="CEI.Web.PatientLogs.ViewCompletedPatientLogs"
    Title="Past Patient Schedule" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Submitted Schedule" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <div style="vertical-align: bottom; margin-bottom: 10px; overflow: hidden;">
        <fieldset class="fieldsetLeft" runat="server" id="searchPanel">
            <legend>
                <asp:Label ID="lblSearchHeader" runat="server" Text="Search Past Patient Schedules"></asp:Label></legend>
            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <table class="style1">
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPracticeSearch" runat="server" Text="Practice:"></asp:Label>&nbsp;
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPracticesSearch" runat="server" Style="margin: 5px 0;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 11px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Search"
                                OnClientClick="__defaultFired = false;" UseSubmitBehavior="False" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </fieldset>
    </div>
    <asp:UpdatePanel ID="upPatientLogs" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:GridView ID="gvPatientLogs" runat="server" OnRowCreated="gvPatientLogs_RowCreated" OnDataBound="gvPatientLogs_DataBound" AllowPaging="True" AutoGenerateColumns="False"
                DataKeyNames="Id,PracticeId,PracticeName" OnPageIndexChanging="gvPatientLogs_PageIndexChanging"
                SkinID="gridViewAdmin" Width="920px" Style="clear: both;" EmptyDataText="There are no past patient schedule in the system "
                OnRowCommand="gvPatientLogs_RowCommand">
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="PracticeId"  HeaderStyle-Width="440px" DataNavigateUrlFormatString="~/ViewPracticeProfile/ViewPracticeProfile.aspx?id={0}"
                        DataTextField="practiceName" HeaderText="Practice Name">
                        <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:BoundField DataField="serviceDate" HeaderText="Service Date" DataFormatString="{0:MM/dd/yyyy}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:ButtonField ButtonType="Button" CommandName="view" HeaderText="View" ShowHeader="True"
                        Text="View" CausesValidation="false">
                        <ControlStyle CssClass="btnClass" />
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                </Columns>
                <PagerTemplate>
                <div style="padding: 5px;">
                        <div>
                            <asp:Button CommandArgument="Prev" CommandName="Page" Text="Previous" ID="btnPrevious"
                                runat="server" meta:resourcekey="ibPrevResource1" />&nbsp;
                            <asp:Button CommandArgument="Next" CommandName="Page" Text="Next" ID="btnNext" runat="server"
                                meta:resourcekey="ibNextResource1" />
                        </div>
                        <span id="spannumeric" runat="server" class="spannumeric"></span>
                    </div>
  </PagerTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsPatientLogs" runat="server" EnablePaging="True" MaximumRowsParameterName="pageSize"
                OnSelecting="odsPatientLogs_Selecting" SelectCountMethod="GetCompletedPatientLogsSearchTotalCount"
                SelectMethod="GetCompletedPatientLogsSearch" TypeName="CEI.BR.PatientLogBR">
                <SelectParameters>
                    <asp:Parameter Name="practiceId" />
                    <asp:Parameter Name="status" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
