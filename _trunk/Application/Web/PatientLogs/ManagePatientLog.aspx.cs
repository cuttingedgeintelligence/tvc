﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;
using CEI.Common.RecurringAppointment;

using System.IO;
using Microsoft.Reporting.WebForms;



namespace CEI.Web.PatientLogs
{
    public partial class ManagePatientLog : BasePage
    {

        private const int NUMBER_OF_NEXT_APPOINTMENT_DATES = 10;


        #region GridView columns

        private const int GV_PATIENTS_TIME = 0;
        private const int GV_PATIENTS_GENDER = 1;
        private const int GV_PATIENTS_FIRST_NAME = 2;
        private const int GV_PATIENTS_LAST_NAME = 3;
        private const int GV_PATIENTS_DATE_OF_BIRTH = 4;
        private const int GV_PATIENTS_GLAUCOMA = 5;
        private const int GV_PATIENTS_RETINA = 6;
        private const int GV_PATIENTS_MANIFEST_REFRACTION = 7;
        private const int GV_PATIENTS_PACHYMETRY = 8;
        private const int GV_PATIENTS_INSURANCE = 9;
        private const int GV_PATIENTS_HISTORY = 10;
        private const int GV_PATIENTS_EDIT = 11;
        private const int GV_PATIENTS_DELETE = 12;

        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                if (this.ValidateRequestParams())
                {
                    ViewState["patientHistory"] = PatientHistoryEnum.New__Patient;
                    lblPracticeNameData.Text = ViewState["prName"].ToString();
                    if (ViewState["patientLogId"] != null) //update patient log
                    {
                        //TODO: check user roles only admin and office manager can update patient log
                        
                      

                        lblServiceDate.Text = "Appointment Date:";
                        ddlServiceDate.Style.Add("display", "none");
                        
                         //DateTime StartingTime = DateTime.Parse(StartDate);
                         //string startingDay = DateTime.Parse(StartDate).ToString("D", CultureInfo.CurrentCulture).ToString();


                        DateTime serviceDate = DateTime.ParseExact(Request.QueryString["servDate"], "MM/dd/yyyy", null);

                     //  lblNoteDate.Text = lblServiceDateData.Text = serviceDate.ToString("dd MMMM yyyy dddd");
                        lblNoteDate.Text = lblServiceDateData.Text = serviceDate.ToString("D");
                        this.FillFollowUpPatients(new Guid(ViewState["practiceId"].ToString()), serviceDate);

                        List<PatientLogPatient> patientsList = new PatientLogBR().GetPatientLogPatientsByPatientLogId((int)ViewState["patientLogId"]);

                        
                        this.BingGridViewPatients(patientsList);

                        ViewState["PatientsList"] = patientsList;

                        DataView dvPatients = new DataView(new PatientBR().GetPatientsByPracticeId(new Guid(ViewState["practiceId"].ToString())));
                        dvPatients.RowFilter = "IsActive = 'True'";

                        BindListControl<DataTable>.BindAndAddListItem(ddlPatient, 
                            dvPatients.ToTable(), 
                            "fullName", "id", "Select Patient", "-1");
                        BindListControl<DataTable>.BindAndAddListItem(ddlInsurance, new NameValueBR().GetAllInsuranceCompanies(), "name", "id", "Select Insurance","-1");

                        //request from manage patients page : create new patient functionality 
                        if(Request.QueryString["patientId"] != null)
                        {
                            ddlPatient.SelectedValue = Request.QueryString["patientId"];
                            this.FillPatientForm(int.Parse(Request.QueryString["patientId"]));
                            
                            
                        }
                    }
                    else //insert  new patient log
                    {
                        this.FillNextAppointmentDates(new Guid(ViewState["practiceId"].ToString()));
                        List<PatientLogPatient> patientLogPatients = new List<PatientLogPatient>();
                        bool bindPatients = true;
                        if(ddlServiceDate.Items.Count > 0)
                        {
                            //request from manage patients page : create new patient functionality 
                            if (Request.QueryString["patientId"] != null)
                                ddlServiceDate.SelectedValue = DateTime.ParseExact(Request.QueryString["servDate"], "MM/dd/yyyy", null).ToString("dd MMMM yyyy dddd");




                            PatientLog patientLog = new PatientLogBR().GetPatientLogByPracticeIdAndServiceDate(DateTime.ParseExact(DateTime.Parse(ddlServiceDate.SelectedValue).ToString("dd MMMM yyyy dddd"), "dd MMMM yyyy dddd", null), new Guid(ViewState["practiceId"].ToString()));

                            if (patientLog != null)
                            {
                                if (patientLog.Status.Equals(PatientLogStatusEnum.Patient__log)) //not submitted log
                                {
                                    patientLogPatients = patientLog.PatientLogPatients;
                                    ViewState["patientLogId"] = patientLog.Id;
                                    ViewState["PatientsList"] = patientLogPatients;
                                   // this.FillFollowUpPatients(new Guid(ViewState["practiceId"].ToString()), DateTime.ParseExact(ddlServiceDate.SelectedValue, "dd MMMM yyyy dddd", null));
                                    this.FillFollowUpPatients(new Guid(ViewState["practiceId"].ToString()), DateTime.ParseExact(DateTime.Parse(ddlServiceDate.SelectedValue).ToString("dd MMMM yyyy dddd"), "dd MMMM yyyy dddd", null));
                                    lblNoteDate.Text = ddlServiceDate.SelectedValue;

                                }
                                else  //submitted log
                                {
                                    bindPatients = false;
                                    pnlAddPatientAndFollowUp.Style.Add("display", "none");
                                    pnlPatients.Style.Add("display", "none");
                                   //patient log is already created and sent to the office manager
                                    lblPatientLogOnTheSameDateIsAlreadyCreated.Text = "Patient Schedule for this date is already created and sent to the office manager. You can find and view this schedule through the Past Patient Schedule page.";
                                }
                            }
                            else 
                            {
                                this.FillFollowUpPatients(new Guid(ViewState["practiceId"].ToString()), DateTime.ParseExact(DateTime.Parse(ddlServiceDate.SelectedValue).ToString("dd MMMM yyyy dddd"), "dd MMMM yyyy dddd", null));
                                lblNoteDate.Text = ddlServiceDate.SelectedValue;
                            }


                                                       
                            
                            
                        }

                        if (bindPatients)
                        {
                            BindListControl<DataTable>.BindAndAddListItem(ddlPatient, new PatientBR().GetPatientsByPracticeId(new Guid(ViewState["practiceId"].ToString())), "fullName", "id", "Select Patient", "-1");
                            BindListControl<DataTable>.BindAndAddListItem(ddlInsurance, new NameValueBR().GetAllInsuranceCompanies(), "name", "id","Select Insurance","-1");
                            this.BingGridViewPatients(patientLogPatients);

                            //request from manage patients page : create new patient functionality 
                            if (Request.QueryString["patientId"] != null)
                            {
                               
                                ddlPatient.SelectedValue = Request.QueryString["patientId"];
                                this.FillPatientForm(int.Parse(Request.QueryString["patientId"]));
                            }
                        }
                    }
                    
                }
                else
                {
                    pnlManagePatientLog.Style.Add("display", "none");
                    lblMainErrorMessage.Text = "Invalid parameters";
                }
            }
        }


        protected void ddlPatient_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlPatient.SelectedValue != "-1")
            {
                //TODO: IMI - 2011-10-28
                //check if patient is in ddlNoShowPatient, if yes set the ViewState["patientHistory"] = PatientHistoryEnum.No__Show;
                //else check if patient is in ddlFollowUpPatient, if yes set the ViewState["patientHistory"] = PatientHistoryEnum.Follow__Up;

                
                bool itemFound = false;

                //first check if patient is NoShow
                foreach (ListItem item in ddlNoShowPatient.Items)
                {
                    if (item.Value == ddlPatient.SelectedValue)
                    {
                        itemFound = true;
                        ViewState["patientHistory"] = PatientHistoryEnum.No__Show;
                        break;
                    }
                }

                //if patient is not NoShow then check if patient is FollowUp
                if (!itemFound)
                {
                    foreach (ListItem item in ddlFollowUpPatient.Items)
                    {
                        if (item.Value == ddlPatient.SelectedValue)
                        {
                            itemFound = true;
                            ViewState["patientHistory"] = PatientHistoryEnum.Follow__Up;
                            break;
                        }
                    }
                }               
                

                this.FillPatientForm(int.Parse(ddlPatient.SelectedValue));
            }
            else
            {
                //clear form
                this.ClearAddPatientForm();
            }

            lblErrorMessage.Style.Add("display","none");
        }

        protected void btnAddPatient_Click(object sender, EventArgs e)
        {
            if (ddlPatient.SelectedValue != "-1") //server side validation
            {

                bool isChacked = false;
                bool letsGo = false;
                if ((!rdbManifestRefraction.Items[0].Selected) && (!rdbManifestRefraction.Items[1].Selected))
                    ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePatientLog), "existsUserWithCountyId", "alert('You must select Yes or No for Manifest Refraction:');", true);
                else
                {
                    if (rdbManifestRefraction.Items[0].Selected)
                    {
                        if ((txtOD_Sph.Text != "") || (txtOD_Cyl.Text != "") || (txtOD_Axis_Of_Astigmatism.Text != "") || (txtOS_Sph.Text != "") || (txtOS_Cyl.Text != "") || (txtOS_Axis_Of_Astigmatism.Text != ""))
                        {
                            isChacked = true;

                        }
                        if (!isChacked)
                            ScriptManager.RegisterClientScriptBlock(this, typeof(ManagePatientLog), "existsUserWithCountyId", "alert('You must enter at least one value for Manifest Refraction:');", true);
                        else letsGo = true;
                    }
                    else
                    {
                        if (rdbManifestRefraction.Items[1].Selected)
                            letsGo = true;
                    }

                }

                if (letsGo)
                {

                    List<PatientLogPatient> patientsList = ViewState["PatientsList"] != null ? ViewState["PatientsList"] as List<PatientLogPatient> : new List<PatientLogPatient>();
                    int patientId;

                    if (ViewState["patientId"] != null) //update patient
                    {
                        patientId = (int)ViewState["patientId"];

                        PatientLogPatient patientLogPatientForEdit = patientsList.Find(delegate(PatientLogPatient patient) { return patient.PatientId == patientId; });


                        this.FillObject(patientLogPatientForEdit);

                        if (this.IsAppointmentTimeIntervalFree(patientLogPatientForEdit.AppointmentTime, patientsList, patientLogPatientForEdit.PatientId))
                        {

                            if (new PatientLogBR().UpdatePatientLogPatient(patientLogPatientForEdit))
                            {
                                new PatientBR().UpdatePatientInsurance(patientId, int.Parse(ddlInsurance.SelectedValue));
                                patientsList.Sort(new PatientLogPatientAppointmentTimeComparer());
                                ViewState["PatientsList"] = patientsList;

                                this.BingGridViewPatients(patientsList);
                                this.ClearAddPatientForm();

								ScriptManager.RegisterStartupScript(this, typeof(ManagePatientLog), "patientLogSuccess", "alert('Patient information has been successfully updated');", true);
                            }
                        }
                        else
                        {
                            lblErrorMessage.Text = "This appointment time is already reserved for other patient in the schedule. Please select different appointment time.";
                            lblErrorMessage.Style.Add("display", "");
                        }



                    }
                    else //add patient
                    {




                        PatientLogPatient patientLogPatient = new PatientLogPatient();
                        patientId = int.Parse(ddlPatient.SelectedValue);

                        DataRow patientRow = new PatientBR().GetPatientById(patientId);

                        if (patientRow["doctorId"] != DBNull.Value)
                            patientLogPatient.DoctorId = (int)patientRow["doctorId"];

                        if (patientsList.Exists(delegate(PatientLogPatient patient) { return patient.PatientId == patientId; }))
                        {



                            lblErrorMessage.Text = "This patient is already added in the patient schedule";
                            lblErrorMessage.Style.Add("display", "");
                        }
                        else
                        {
                            if (ViewState["patientLogId"] == null) //create patient log and insert first patient in the log
                            {
                                if (ddlServiceDate.Items.Count > 0)
                                {
                                    PatientLog patientLog = new PatientLog();
                                    patientLog.PracticeId = new Guid(ViewState["practiceId"].ToString());
                                    patientLog.CreatedBy = new Guid(User.Identity.Name);
                                    patientLog.ServiceDate = DateTime.ParseExact(DateTime.Parse(ddlServiceDate.SelectedValue).ToString("dd MMMM yyyy dddd"), "dd MMMM yyyy dddd", null);
                                    patientLog.Status = PatientLogStatusEnum.Patient__log;


                                    patientLogPatient.PatientId = patientId;
                                    patientLogPatient.PatientHistory = (PatientHistoryEnum)ViewState["patientHistory"];
                                    this.FillObject(patientLogPatient);

                                    patientLog.PatientLogPatients.Add(patientLogPatient);

                                    patientLog.NumOfPatients = (short)1;
                                    if (new PatientLogBR().InsertPatientLog(patientLog))
                                    {
                                       // new PatientBR().UpdatePatientInsurance(patientId, int.Parse(ddlInsurance.SelectedValue));
                                        patientsList.Add(patientLogPatient);
                                        ViewState["patientLogId"] = patientLog.Id;



                                        ViewState["PatientsList"] = patientsList;
                                        this.BingGridViewPatients(patientsList);
                                        this.ClearAddPatientForm();

										ScriptManager.RegisterStartupScript(this, typeof(ManagePatientLog), "patientLogSuccessCreateAndInsert", "alert('The patient has been added to the schedule');", true);
                                    }
                                }
                                else
                                {
                                    rfvServiceDate.IsValid = false;
                                    upPracticeAndServiceDate.Update();
                                }

                            }
                            else //insert only patient 
                            {

                                patientLogPatient.PatientId = patientId;
                                patientLogPatient.PatientHistory = (PatientHistoryEnum)ViewState["patientHistory"];
                                patientLogPatient.PatientLogId = (int)ViewState["patientLogId"];

                                this.FillObject(patientLogPatient);

                                if (this.IsAppointmentTimeIntervalFree(patientLogPatient.AppointmentTime, patientsList, -1))
                                {

                                    PatientLog patientLogForUpdate = new PatientLog();
                                    patientLogForUpdate.Id = (int)ViewState["patientLogId"];
                                    patientLogForUpdate.NumOfPatients = (short)(gvPatients.Rows.Count + 1);
                                    patientLogForUpdate.PatientLogPatients.Add(patientLogPatient);

                                    if (new PatientLogBR().UpdatePatientLog(patientLogForUpdate)) //update patient log num of patients and insert new patient
                                    {
                                      //  new PatientBR().UpdatePatientInsurance(patientId, int.Parse(ddlInsurance.SelectedValue));
                                        patientsList.Add(patientLogPatient);
                                        patientsList.Sort(new PatientLogPatientAppointmentTimeComparer());
                                        ViewState["PatientsList"] = patientsList;
                                        this.BingGridViewPatients(patientsList);
                                        this.ClearAddPatientForm();

										ScriptManager.RegisterStartupScript(this, typeof(ManagePatientLog), "patientLogSuccessInsert", "alert('The patient has been added to the schedule');", true);
                                    }
                                }
                                else
                                {
                                    lblErrorMessage.Text = "This appointment time is already reserved for other patient in the schedule. Please select different appointment time.";
                                    lblErrorMessage.Style.Add("display", "");
                                }
                            }


                        }

                    }
                }

            }
            else
            {
                cvPatient.IsValid = false;
            }
        }





        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ddlPatient.SelectedIndex = -1;
            this.ClearAddPatientForm();
            lblErrorMessage.Style.Add("display","none");
            ddlFollowUpPatient.SelectedIndex = -1;
            ddlNoShowPatient.SelectedIndex = -1;
        }


        protected void gvPatients_RowEditing(object sender, GridViewEditEventArgs e)
        {

            List<PatientLogPatient> patientsList = ViewState["PatientsList"] as List<PatientLogPatient>;

            btnAddPatient.Text = "Save";
            int patientId = (int)gvPatients.DataKeys[e.NewEditIndex].Values["PatientId"];
            PatientLogPatient patientLogPatientForEdit = patientsList.Find(delegate(PatientLogPatient patient) { return patient.PatientId == patientId; });

            int patientLogPatientId = (int)gvPatients.DataKeys[e.NewEditIndex].Values["Id"];

            ViewState["patientId"] = patientId;
            lblSelectedPatient.Text = "Update patient: " + patientLogPatientForEdit.FullName;

            ddlPatient.SelectedValue = patientLogPatientForEdit.PatientId.ToString();
            ddlPatient.Enabled = false;

            // appointmentTime == HH:mm:ss convert into h:mm AM(PM)
            string[] appointmentTime =  DateTime.ParseExact(patientLogPatientForEdit.AppointmentTime, "HH:mm:ss", null).ToString("h:mm tt").Split(' ');

           
            ddlAMPM.SelectedValue = appointmentTime[1];

            string[] hourAndMinutes = appointmentTime[0].Split(':');

            ddlHour.SelectedValue = hourAndMinutes[0];
            ddlMinutes.SelectedValue = hourAndMinutes[1];

            cbGlaucoma.Checked = patientLogPatientForEdit.Glaucoma;
            cbRetina.Checked = patientLogPatientForEdit.Retina;

            txtOS_Sph.Text = patientLogPatientForEdit.OS_Sphere;
            txtOD_Sph.Text = patientLogPatientForEdit.OD_Sphere;
            txtOS_Axis_Of_Astigmatism.Text = patientLogPatientForEdit.OS_AxisOfAstigmatism;
            txtOD_Axis_Of_Astigmatism.Text = patientLogPatientForEdit.OD_AxisOfAstigmatism;

            txtOS_Cyl.Text = patientLogPatientForEdit.OS_Cylinder;
            txtOD_Cyl.Text = patientLogPatientForEdit.OD_Cylinder;

            txtOD_Pachymetry.Text = patientLogPatientForEdit.OD_Pachymetry.HasValue ? patientLogPatientForEdit.OD_Pachymetry.ToString() : null;
            txtOS_Pachymetry.Text = patientLogPatientForEdit.OS_Pachymetry.HasValue ? patientLogPatientForEdit.OS_Pachymetry.ToString() : null;

            ddlInsurance.SelectedValue = patientLogPatientForEdit.InsuranceCompanyId.ToString();

            lblErrorMessage.Style.Add("display", "none");


          
           
            upAddPatient.Update();

            //scroll page to top
			ScriptManager.RegisterStartupScript(Page, typeof(ManagePatientLog),
		 "ScrollToTop", @"
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
                function pageLoaded(sender, args)
                {
                     scroll(0,0);
                     Sys.WebForms.PageRequestManager.getInstance().remove_pageLoaded(pageLoaded);
                }", true);


            
        }


        protected void gvPatients_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            List<PatientLogPatient> patientsList =  ViewState["PatientsList"] as List<PatientLogPatient>;
           
            int patientId = (int)gvPatients.DataKeys[e.RowIndex].Values["PatientId"];
            PatientLogPatient patientLogPatientForDelete = patientsList.Find(delegate(PatientLogPatient patient) { return patient.PatientId == patientId; });

            PatientLog patientLog = new PatientLog();
            patientLog.Id = (int)ViewState["patientLogId"];
            patientLog.NumOfPatients = (short)(gvPatients.Rows.Count - 1);
            patientLog.PatientLogPatients.Add(patientLogPatientForDelete);

            if (new PatientLogBR().DeletePatientLogPatient(patientLog))
            {

                patientsList.Remove(patientLogPatientForDelete);
                ViewState["PatientsList"] = patientsList;

                if (ViewState["patientId"] != null && patientId == (int)ViewState["patientId"])
                {
                    this.ClearAddPatientForm();
                    upAddPatient.Update();
                }

                this.BingGridViewPatients(patientsList);
            }

        }

      


       



        protected void gvPatients_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                PatientLogPatient patientLogPatient = e.Row.DataItem as PatientLogPatient;
                if (patientLogPatient.Glaucoma)
                //    e.Row.Cells[GV_PATIENTS_GLAUCOMA].Text ="G";
                e.Row.Cells[GV_PATIENTS_GLAUCOMA].Text = patientLogPatient.HRT_Glaucoma.ToString();

                if (patientLogPatient.Retina)
                //    e.Row.Cells[GV_PATIENTS_RETINA].Text = "R";
                e.Row.Cells[GV_PATIENTS_RETINA].Text = patientLogPatient.HRT_Retina.ToString();

                if(patientLogPatient.Gender.Equals("Male"))
                    e.Row.Cells[GV_PATIENTS_GENDER].Text = "M";
                else
                    if (patientLogPatient.Gender.Equals("Female"))
                    e.Row.Cells[GV_PATIENTS_GENDER].Text = "F";
                    else
                        e.Row.Cells[GV_PATIENTS_GENDER].Text = "N/A";


               
                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOD_Sph") as Label).Text = patientLogPatient.OD_Sphere;
                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOD_Cyl") as Label).Text = patientLogPatient.OD_Cylinder;
                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOD_AxisOfAstigmatism") as Label).Text = patientLogPatient.OD_AxisOfAstigmatism;

                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOS_Sph") as Label).Text = patientLogPatient.OS_Sphere;
                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOS_Cyl") as Label).Text = patientLogPatient.OS_Cylinder;
                (e.Row.Cells[GV_PATIENTS_MANIFEST_REFRACTION].FindControl("lblOS_AxisOfAstigmatism") as Label).Text = patientLogPatient.OS_AxisOfAstigmatism;
                
                

            

                 if(patientLogPatient.OD_Pachymetry.HasValue)
                    (e.Row.Cells[GV_PATIENTS_PACHYMETRY].FindControl("lblOD_Pachymetry") as Label).Text = patientLogPatient.OD_Pachymetry.ToString();
                 if (patientLogPatient.OS_Pachymetry.HasValue)
                     (e.Row.Cells[GV_PATIENTS_PACHYMETRY].FindControl("lblOS_Pachymetry") as Label).Text = patientLogPatient.OS_Pachymetry.ToString();

                if(patientLogPatient.PatientHistory.Equals(PatientHistoryEnum.No__Show))
                    e.Row.Cells[GV_PATIENTS_HISTORY].Text = "No Show";
                else if(patientLogPatient.PatientHistory.Equals(PatientHistoryEnum.Follow__Up))
                    e.Row.Cells[GV_PATIENTS_HISTORY].Text = "Follow Up";
                else if (patientLogPatient.PatientHistory.Equals(PatientHistoryEnum.New__Patient))
                    e.Row.Cells[GV_PATIENTS_HISTORY].Text = "New Patient";

                e.Row.Cells[GV_PATIENTS_TIME].Text = DateTime.ParseExact(patientLogPatient.AppointmentTime, "HH:mm:ss", null).ToString("h:mm tt");
            }
        }


      

        //protected void btnSubmitPatientLog_Click(object sender, EventArgs e)
        //{
        //    PatientLog patientLog = new PatientLog();
        //    //patientLog.PatientLogPatients = ViewState["PatientsList"] as List<PatientLogPatient>;
        //    patientLog.Status = PatientLogStatusEnum.Pending__patient__log;
            

            
        //    patientLog.Id = (int)ViewState["patientLogId"];
        //    //patientLog.ServiceDate = DateTime.ParseExact(lblServiceDateData.Text, "dd MMMM yyyy dddd", null);
        //    patientLog.ServiceDate = ViewState["action"].ToString() == "create" ? DateTime.ParseExact(ddlServiceDate.SelectedValue, "dd MMMM yyyy dddd", null) : DateTime.ParseExact(lblServiceDateData.Text, "dd MMMM yyyy dddd", null);

        //    if (new PatientLogBR().PatientLogSubmit(patientLog))
        //    {
        //        new PatientLogBR().SendMailPatientLogSubmit(Session["loggedUserFullName"] != null ? Session["loggedUserFullName"].ToString() : string.Empty, Session["loggedUserEmail"] != null ? Session["loggedUserEmail"].ToString() : string.Empty, "New patient log is created", patientLog.ServiceDate.ToString("MM/dd/yyyy"), lblPracticeNameData.Text);

        //        if (Roles.IsUserInRole(RoleEnum.Practice.ToString())) //TODO:temp solution
        //            Response.Redirect("~/PatientLogs/ManagePatientLog.aspx", true);
        //        else

        //        Response.Redirect("~/PatientLogs/PatientLogs.aspx", true);
        //    }
            
           


        //}

        protected void btnDeletePatientLog_Click(object sender, EventArgs e)
        {
            if (new PatientLogBR().DeletePatientLog((int)ViewState["patientLogId"]))
            {
                if(Roles.IsUserInRole(RoleEnum.Practice.ToString())) //TODO:temp solution
                    Response.Redirect("~/PatientLogs/ManagePatientLog.aspx", true);
                else
                Response.Redirect("~/PatientLogs/PatientLogs.aspx", true);
            }
        }

        protected void ddlServiceDate_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            // get patientLog by service date and practiceId
            PatientLog patientLog = new PatientLogBR().GetPatientLogByPracticeIdAndServiceDate(DateTime.ParseExact(DateTime.Parse(ddlServiceDate.SelectedValue).ToString("dd MMMM yyyy dddd"), "dd MMMM yyyy dddd", null), new Guid(ViewState["practiceId"].ToString()));

            if (patientLog != null)
            {

                if (patientLog.Status.Equals(PatientLogStatusEnum.Patient__log)) //not submitted log
                {
                    pnlAddPatientAndFollowUp.Style.Add("display", "");
                    pnlPatients.Style.Add("display", "");
                    lblPatientLogOnTheSameDateIsAlreadyCreated.Text = string.Empty;
                    ViewState["patientLogId"] = patientLog.Id;
                    ViewState["PatientsList"] = patientLog.PatientLogPatients;
                    this.FillFollowUpPatients(new Guid(ViewState["practiceId"].ToString()), DateTime.ParseExact(DateTime.Parse(ddlServiceDate.SelectedValue).ToString("dd MMMM yyyy dddd"), "dd MMMM yyyy dddd", null));
                    lblNoteDate.Text = ddlServiceDate.SelectedValue;

                    if (ddlPatient.Items.Count == 0) //bind if they are not previously binded 
                    {
                        BindListControl<DataTable>.BindAndAddListItem(ddlPatient, new PatientBR().GetPatientsByPracticeId(new Guid(ViewState["practiceId"].ToString())), "fullName", "id", "Select Patient", "-1");
                        BindListControl<DataTable>.BindAndAddListItem(ddlInsurance, new NameValueBR().GetAllInsuranceCompanies(), "name", "id","Select Insurance","-1");

                    }

                    this.BingGridViewPatients(patientLog.PatientLogPatients);
                    this.ClearAddPatientForm();
                    

                }
                else  //submitted log
                {
                    pnlAddPatientAndFollowUp.Style.Add("display", "none");
                    pnlPatients.Style.Add("display", "none");
                    //patient log is already created and sent to the office manager
                    lblPatientLogOnTheSameDateIsAlreadyCreated.Text = "Patient Schedule for this date is already created and sent to the office manager. You can find and view this schedule through the Past Patient Schedule page.";
                }
            }
            else
            {
                pnlAddPatientAndFollowUp.Style.Add("display", "");
                pnlPatients.Style.Add("display", "");
                lblPatientLogOnTheSameDateIsAlreadyCreated.Text = string.Empty;
                this.FillFollowUpPatients(new Guid(ViewState["practiceId"].ToString()), DateTime.ParseExact(DateTime.Parse(ddlServiceDate.SelectedValue).ToString("dd MMMM yyyy dddd"), "dd MMMM yyyy dddd", null));
                lblNoteDate.Text = ddlServiceDate.SelectedValue;

                if (ddlPatient.Items.Count == 0) //bind if they are not previously binded 
                {
                    BindListControl<DataTable>.BindAndAddListItem(ddlPatient, new PatientBR().GetPatientsByPracticeId(new Guid(ViewState["practiceId"].ToString())), "fullName", "id", "Select Patient", "-1");
                    BindListControl<DataTable>.BindAndAddListItem(ddlInsurance, new NameValueBR().GetAllInsuranceCompanies(), "name", "id","Select Insurance","-1");
                    
                }

                this.BingGridViewPatients(new List<PatientLogPatient>());
                ViewState["patientLogId"] = null;
                ViewState["PatientsList"] = null;
                this.ClearAddPatientForm();
               

            }

           
        }

        protected void ddlNoShowPatient_SelectedIndexChanged(object sender, EventArgs e)
        {
            //select follow up patient
            if (ddlNoShowPatient.SelectedValue != "-1")
            {
                ddlPatient.SelectedValue = ddlNoShowPatient.SelectedValue;
                ddlPatient.Enabled = false;
                ddlFollowUpPatient.SelectedIndex = -1;
                lblSelectedPatient.Text = "Add no show patient to schedule";
                this.FillPatientForm(int.Parse(ddlNoShowPatient.SelectedValue));
                ViewState["patientHistory"] = PatientHistoryEnum.No__Show;
            }
        }

        protected void ddlFollowUpPatient_SelectedIndexChanged(object sender, EventArgs e)
        {
            //select follow up patient
            if (ddlFollowUpPatient.SelectedValue != "-1")
            {
                ddlPatient.SelectedValue = ddlFollowUpPatient.SelectedValue;
                ddlPatient.Enabled = false;
                ddlNoShowPatient.SelectedIndex = -1;
                lblSelectedPatient.Text = "Add follow up patient to schedule";
                this.FillPatientForm(int.Parse(ddlFollowUpPatient.SelectedValue));
                ViewState["patientHistory"] = PatientHistoryEnum.Follow__Up;
            }
        }


        protected void btnCreateNewPatient_Click(object sender, EventArgs e)
        {
            if (ViewState["action"].ToString() == "create" && ddlServiceDate.Items.Count == 0)
            {
                rfvServiceDate.IsValid = false;
                upPracticeAndServiceDate.Update();
            }
            else
            {
               // DateTime serviceDate = ViewState["action"].ToString() == "create" ? DateTime.ParseExact(ddlServiceDate.SelectedValue, "dd MMMM yyyy dddd", null) : DateTime.ParseExact(lblServiceDateData.Text, "dd MMMM yyyy dddd", null);
                DateTime serviceDate = ViewState["action"].ToString() == "create" ? DateTime.ParseExact(ddlServiceDate.SelectedValue, "D", null) : DateTime.ParseExact(lblServiceDateData.Text, "D", null);
               
                if (ViewState["action"].ToString() == "create")
                {
                    Response.Redirect("~/ManagePatients/ManagePatients.aspx?action=create&prId=" + ViewState["practiceId"].ToString() + "&serviceDate=" + serviceDate.ToString("MM/dd/yyyy"), true);
                }
                else
                {
                    Response.Redirect("~/ManagePatients/ManagePatients.aspx?action=update&prId=" + ViewState["practiceId"].ToString() + "&serviceDate=" + serviceDate.ToString("MM/dd/yyyy") + "&logId=" + ViewState["patientLogId"].ToString(), true);
                }
            }




        }


        #region Old PDF Export
        //protected void btnDownload_Click(object sender, EventArgs e)
        //{

            
        //    Document doc = new Document(PageSize.A4.Rotate(),36,36,91,36);
            
        //    MemoryStream ms = new MemoryStream();
            
            
        //    PdfWriter writer =  PdfWriter.GetInstance(doc, ms);
        //    writer.PageEvent = new PDFPageEvent("http://" + Request.Url.Host + ResolveUrl("~/App_Themes/Default/images_new/logoPDF.png"), lblPracticeNameData.Text,lblServiceDateData.Text);
        //    doc.Open();
            

            
        //    PdfPTable tblPatients = new PdfPTable(11);
        //    tblPatients.HeaderRows = 1;



        //    Rectangle r = new Rectangle(PageSize.A4.Right, PageSize.A4.Top);   
        //    float[] widths = { 45f, 15f, 85f, 85f, 55f, 15f, 15f, 105f, 60f, 72f ,50f};
        //    tblPatients.SetWidthPercentage(widths, r);
        //    //tblPatients.SpacingBefore = 50f;


        //    Font fTableHeader = new Font(Font.HELVETICA, 11, Font.BOLD);
        //    //ADD TABLE HEADER
        //    PdfPCell cAppointmentTimeHeader = new PdfPCell(new Phrase("APPT TIME", fTableHeader));
        //    cAppointmentTimeHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cAppointmentTimeHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cAppointmentTimeHeader);

        //    PdfPCell cGenderHeader = new PdfPCell(new Phrase("F or M", fTableHeader));
        //    cGenderHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cGenderHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cGenderHeader);

        //    PdfPCell cFirstName = new PdfPCell(new Phrase("FIRST NAME", fTableHeader));
        //    cFirstName.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cFirstName.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cFirstName);

        //    PdfPCell cLastName = new PdfPCell(new Phrase("LAST NAME", fTableHeader));
        //    cLastName.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cLastName.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cLastName);

        //    PdfPCell cDateOfBirthHeader = new PdfPCell(new Phrase("DATE OF BIRTH", fTableHeader));
        //    cDateOfBirthHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cDateOfBirthHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cDateOfBirthHeader);

        //    PdfPCell cGlaucomaHeader = new PdfPCell(new Phrase("G", fTableHeader));
        //    cGlaucomaHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cGlaucomaHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cGlaucomaHeader);

        //    PdfPCell cRetinaHeader = new PdfPCell(new Phrase("R", fTableHeader));
        //    cRetinaHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cRetinaHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cRetinaHeader);

        //    PdfPCell cManifestRefractionHeader = new PdfPCell(new Phrase("MANIFEST REFRACTION", fTableHeader));
        //    cManifestRefractionHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cManifestRefractionHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cManifestRefractionHeader);

        //    Font fTableHeaderPachymetry = new Font(Font.HELVETICA, 10, Font.BOLDITALIC);
        //    Font fTableHeaderPachymetryColor = new Font(Font.HELVETICA, 10, Font.BOLDITALIC,Color.RED);
        //    Chunk chPachymetry1 = new Chunk("If available Pachymetry Reading for ",fTableHeaderPachymetry);
        //    Chunk chPachymetry2 = new Chunk("Glaucoma ",fTableHeaderPachymetryColor);
        //    Chunk chPachymetry3 = new Chunk("Patients",fTableHeaderPachymetry);
        //    Phrase pPachymetry = new Phrase();
        //    pPachymetry.Add(chPachymetry1);
        //    pPachymetry.Add(chPachymetry2);
        //    pPachymetry.Add(chPachymetry3);
        //    PdfPCell cPachymetryHeader = new PdfPCell(pPachymetry);
        //    cPachymetryHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cPachymetryHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cPachymetryHeader);

        //    PdfPCell cInsuranceHeader = new PdfPCell(new Phrase("PATIENT INSURANCE", fTableHeader));
        //    cInsuranceHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cInsuranceHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cInsuranceHeader);

        //    PdfPCell cHistoryHeader = new PdfPCell(new Phrase("PATIENT HISTORY", fTableHeader));
        //    cHistoryHeader.HorizontalAlignment = Element.ALIGN_CENTER;
        //    cHistoryHeader.VerticalAlignment = Element.ALIGN_MIDDLE;
        //    tblPatients.AddCell(cHistoryHeader);

          
           

           


        //    //TODO: get patients list from view state

        //    List<PatientLogPatient> patientsList = new PatientLogBR().GetPatientLogPatientsByPatientLogId((int)ViewState["patientLogId"]);

        //    Font fManifestRefractionAndPachymetry = new Font(Font.HELVETICA, 9, Font.NORMAL);

        //    foreach (PatientLogPatient patientLogPatient in patientsList)
        //    {
        //        PdfPCell cAppointmentTime = new PdfPCell(new Phrase(DateTime.ParseExact(patientLogPatient.AppointmentTime, "HH:mm:ss", null).ToString("h:mm tt")));
        //        cAppointmentTime.HorizontalAlignment = Element.ALIGN_CENTER;
        //        tblPatients.AddCell(cAppointmentTime);

        //        PdfPCell cGender = new PdfPCell(new Phrase( patientLogPatient.Gender.Equals("Male") ? "M" : "F" ));
        //        cGender.HorizontalAlignment = Element.ALIGN_CENTER;
        //        tblPatients.AddCell(cGender);

        //        tblPatients.AddCell(patientLogPatient.FirstName);
        //        tblPatients.AddCell(patientLogPatient.LastName);

        //        PdfPCell cDateOfBirth = new PdfPCell(new Phrase(patientLogPatient.DateOfBirth.ToString("MM/dd/yyyy")));
        //        cDateOfBirth.HorizontalAlignment = Element.ALIGN_CENTER;
        //        tblPatients.AddCell(cDateOfBirth);


        //        PdfPCell cGlaucoma = new PdfPCell(new Phrase(patientLogPatient.Glaucoma ? "G" : string.Empty));
        //        cGlaucoma.HorizontalAlignment = Element.ALIGN_CENTER;
        //        tblPatients.AddCell(cGlaucoma);

        //        PdfPCell cRetina = new PdfPCell(new Phrase(patientLogPatient.Retina ? "R" : string.Empty));
        //        cRetina.HorizontalAlignment = Element.ALIGN_CENTER;
        //        tblPatients.AddCell(cRetina);

                
                
                
        //        tblPatients.AddCell(new Phrase(string.Format("OD: sph {0} cyl {1} x {2}\nOS: sph {3} cyl {4} x {5}", patientLogPatient.OD_Sphere.PadLeft(6, ' '), patientLogPatient.OD_Cylinder.PadLeft(6, ' '), patientLogPatient.OD_AxisOfAstigmatism.PadLeft(3, ' '), patientLogPatient.OS_Sphere.PadLeft(6, ' '), patientLogPatient.OS_Cylinder.PadLeft(6, ' '), patientLogPatient.OS_AxisOfAstigmatism.PadLeft(3, ' ')), fManifestRefractionAndPachymetry));
               
                
               

        //        tblPatients.AddCell(new Phrase(string.Format(" OD: {0}\n OS: {1}", patientLogPatient.OD_Pachymetry.HasValue ? patientLogPatient.OD_Pachymetry.ToString().PadLeft(3, ' ') : "   ", patientLogPatient.OS_Pachymetry.HasValue ? patientLogPatient.OS_Pachymetry.ToString().PadLeft(3, ' ') : "   "), fManifestRefractionAndPachymetry));

        //        tblPatients.AddCell(patientLogPatient.InsuranceCompanyName);

        //        PdfPCell cHistory;
        //        if (patientLogPatient.PatientHistory.Equals(PatientHistoryEnum.No__Show))
        //            cHistory = new PdfPCell(new Phrase("no show"));
        //        else if (patientLogPatient.PatientHistory.Equals(PatientHistoryEnum.Follow__Up))
        //            cHistory = new PdfPCell(new Phrase("up"));
        //        else //PatientHistoryEnum.New__Patient
        //            cHistory = new PdfPCell(new Phrase(" "));

        //        cHistory.HorizontalAlignment = Element.ALIGN_CENTER;
        //        tblPatients.AddCell(cHistory);


               

                
              
        //    }

        //    doc.Add(tblPatients);

           

        //    Response.Clear();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment; filename=PatientLog.pdf");

        //    doc.Close();
           
        //    byte[] getContent = ms.ToArray();
        //    ms.Close();
        //    Response.BinaryWrite(getContent);

        //}

        #endregion //Old PDF Export

        protected void btnPrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Pdf);
        }

        protected void btnPrintExcel_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Excel);
        }

        protected void btnPrintPreview_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Printer);
        }


        #endregion //Event handlers

        #region Private methods
        private bool ValidateRequestParams()
        {
            bool isValid = false;
            if (Request.QueryString["action"] == null && Roles.IsUserInRole(RoleEnum.Practice.ToString()))
            {
                isValid = true;
                ViewState["action"] = "create";
                ViewState["practiceId"] = User.Identity.Name;
                ViewState["prName"] = Session["loggedUserFullName"];
            }
            else if (Request.QueryString["action"] != null && Request.QueryString["action"] == "create")
            {
                if (
                    Request.QueryString["prId"] != null &&
                    Request.QueryString["prName"] != null &&
                    this.IsGuid(Request.QueryString["prId"]) &&
                    this.PracticeIdValidation(Request.QueryString["prId"]) 
                    
                    )
                {
                    isValid = true;
                    ViewState["action"] = "create";
                    ViewState["practiceId"] = Request.QueryString["prId"];
                    ViewState["prName"] = Request.QueryString["prName"];
                }

            }
            else if (Request.QueryString["action"] != null && Request.QueryString["action"] == "update")
            {
                int patientLogId;
                if (
                    Request.QueryString["prId"] != null &&
                    Request.QueryString["prName"] != null &&
                    Request.QueryString["servDate"] != null &&
                    Request.QueryString["logId"] != null &&
                    this.IsGuid(Request.QueryString["prId"]) &&
                    this.PracticeIdValidation(Request.QueryString["prId"]) &&  //practice can update only his patient logs
                    int.TryParse(Request.QueryString["logId"],out patientLogId) && 
                    this.ValidateServiceDate(Request.QueryString["servDate"])
                    )
                {
                    isValid = true;
                    ViewState["action"] = "update";
                    ViewState["patientLogId"] = patientLogId;
                    ViewState["practiceId"] = Request.QueryString["prId"];
                    ViewState["prName"] = Request.QueryString["prName"];
                }


            }


            return isValid;


        }


        private bool IsGuid(string s)
        {
            try
            {
                new Guid(s);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }
        }


        private bool PracticeIdValidation(string practiceId)
        {
            if (Roles.IsUserInRole(RoleEnum.Practice.ToString()))
            {
                if (User.Identity.Name == practiceId)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
                return true;
        }

        private bool ValidateServiceDate(string serviceDate)
        {
            try
            {
                DateTime.ParseExact(serviceDate, "MM/dd/yyyy", null);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }

        }

        private void ClearAddPatientForm()
        {
            ViewState["patientId"] = null;
            ViewState["patientHistory"] = PatientHistoryEnum.New__Patient;
            lblSelectedPatient.Text = "Add patient to Schedule";
            btnAddPatient.Text = "Add";

            lblErrorMessage.Style.Add("display","none");

            cbGlaucoma.Checked = false;
            cbRetina.Checked = false;
            ddlHour.SelectedIndex = -1;
            ddlMinutes.SelectedIndex = -1;
            ddlAMPM.SelectedIndex = -1;

            txtOS_Sph.Text = string.Empty;
            txtOD_Sph.Text = string.Empty;
            txtOS_Axis_Of_Astigmatism.Text = string.Empty;
            txtOD_Axis_Of_Astigmatism.Text = string.Empty;
            txtOS_Cyl.Text = string.Empty;
            txtOD_Cyl.Text = string.Empty;
           
            txtOS_Pachymetry.Text = string.Empty;
            txtOD_Pachymetry.Text = string.Empty;
            ddlInsurance.SelectedIndex = -1;

            ddlPatient.SelectedIndex = -1;
            ddlPatient.Enabled = true;

            ddlFollowUpPatient.SelectedIndex = -1;
            ddlNoShowPatient.SelectedIndex = -1;
        }


        private void BingGridViewPatients(List<PatientLogPatient> patientsList)
        {
            gvPatients.DataSource = patientsList;
            gvPatients.DataBind();
            if (gvPatients.Rows.Count > 0)
            {
                //btnSubmitPatientLog.Style.Add("display", "");
                btnDeletePatientLog.Style.Add("display", "");
                btnPrintPDF.Style.Add("display", "");
                btnPrintExcel.Style.Add("display", "");
               // btnPrintPreview.Style.Add("display", "");

            }
            else
            {
                //btnSubmitPatientLog.Style.Add("display", "none");
                btnDeletePatientLog.Style.Add("display", "none");
                btnPrintPDF.Style.Add("display", "none");
                btnPrintExcel.Style.Add("display", "none");
                //btnPrintPreview.Style.Add("display", "none");
            }
        }


        private void FillObject(PatientLogPatient patientLogPatient)
        {

            if (ViewState["patientId"] == null)
            {

                patientLogPatient.FullName = ddlPatient.SelectedItem.Text;
                patientLogPatient.FirstName = ViewState["patientFirstName"].ToString();
                patientLogPatient.LastName = ViewState["patientLastName"].ToString();
                patientLogPatient.DateOfBirth = (DateTime)ViewState["patientDateOfBirth"];
                patientLogPatient.Gender = ViewState["patientGender"].ToString();
            }

            patientLogPatient.Glaucoma = cbGlaucoma.Checked;
            if (patientLogPatient.Glaucoma)
            {
                patientLogPatient.BFA = 1.0M;
                patientLogPatient.HRT_Glaucoma = 1.0M;
            }
            else
            {
                patientLogPatient.BFA = 0.0M;
                patientLogPatient.HRT_Glaucoma = 0.0M;
            }

            patientLogPatient.Retina = cbRetina.Checked;

            if (patientLogPatient.Retina)
            {
                patientLogPatient.HRT_Retina = 1.0M;
            }
            else
            { 
                patientLogPatient.HRT_Retina = 0.0M;
            }

            patientLogPatient.AppointmentTime = DateTime.ParseExact(ddlHour.SelectedValue + ":" + ddlMinutes.SelectedValue + " " + ddlAMPM.SelectedValue,"h:mm tt",null).TimeOfDay.ToString();
            
            

            patientLogPatient.OS_Sphere = txtOS_Sph.Text;
            patientLogPatient.OD_Sphere = txtOD_Sph.Text;
            patientLogPatient.OD_AxisOfAstigmatism = txtOD_Axis_Of_Astigmatism.Text;
            patientLogPatient.OS_AxisOfAstigmatism = txtOS_Axis_Of_Astigmatism.Text;
            patientLogPatient.OS_Cylinder = txtOS_Cyl.Text;
            patientLogPatient.OD_Cylinder = txtOD_Cyl.Text;
            if (txtOD_Pachymetry.Text.Trim() != string.Empty)
                patientLogPatient.OD_Pachymetry = short.Parse(txtOD_Pachymetry.Text);
            if (txtOS_Pachymetry.Text.Trim() != string.Empty)
                patientLogPatient.OS_Pachymetry = short.Parse(txtOS_Pachymetry.Text);

            patientLogPatient.InsuranceCompanyId = int.Parse(ddlInsurance.SelectedValue);
            patientLogPatient.InsuranceCompanyName = ddlInsurance.SelectedItem.Text;

           
        }



        private void FillPatientForm(int patientId)
        {
            DataRow drPatient = new PatientBR().GetPatientById(patientId);

            cbGlaucoma.Checked = false;
            cbRetina.Checked = false;
            ddlHour.SelectedIndex = -1;
            ddlMinutes.SelectedIndex = -1;
            ddlAMPM.SelectedIndex = -1;

            txtOS_Sph.Text = drPatient["OS_Sphere"].ToString();
            txtOD_Sph.Text = drPatient["OD_Sphere"].ToString();
            txtOS_Axis_Of_Astigmatism.Text = drPatient["OS_AxisOfAstigmatism"].ToString();
            txtOD_Axis_Of_Astigmatism.Text = drPatient["OD_AxisOfAstigmatism"].ToString();
            txtOS_Cyl.Text = drPatient["OS_Cylinder"].ToString();
            txtOD_Cyl.Text = drPatient["OD_Cylinder"].ToString();

            txtOD_Pachymetry.Text = drPatient["OD_Pachymetry"].ToString();
            txtOS_Pachymetry.Text = drPatient["OS_Pachymetry"].ToString();
            ddlInsurance.SelectedValue = drPatient["insuranceCompanyId"].ToString();

            ViewState["patientFirstName"] = drPatient["firstName"].ToString();
            ViewState["patientLastName"] = drPatient["lastName"].ToString();
            ViewState["patientDateOfBirth"] = (DateTime)drPatient["dateOfBirth"];
            ViewState["patientGender"] = drPatient["gender"].ToString();


            btnAddPatient.Focus();
           
        }

        private void FillNextAppointmentDates(Guid practiceId)
        {

           DataTable dtOneTimeAppointments = new PracticeOneTimeAppointmentBR().GetOneTimeAppointmentsByPracticeId(practiceId, DateTime.Today.AddDays(int.Parse(ConfigurationManager.AppSettings["numberOfDaysToSubmitPatientLogBeforeServiceDate"])), NUMBER_OF_NEXT_APPOINTMENT_DATES);
            List<DateTime> lsOneTimeAppointments = new List<DateTime>();
            foreach (DataRow drOneTimeAppointments in dtOneTimeAppointments.Rows)
                lsOneTimeAppointments.Add((DateTime)drOneTimeAppointments["appointmentDate"]);

            //bind next appointment dates
            this.BindNextAppointmentDates(this.GetNextRecurringAppointmentDates(practiceId),lsOneTimeAppointments ,NUMBER_OF_NEXT_APPOINTMENT_DATES);
        }


        private List<DateTime> GetNextRecurringAppointmentDates(Guid practiceId)
        {
            List<DateTime> nextAppointmentDates = new List<DateTime>();

            List<DateTime> newNextAppointmentDates = new List<DateTime>();


            DataRow drPracticeAppointmentsSettings = new PracticeAppointmentsSettingsBR().GetPracticeAppointmentSettingsByPracticeId(practiceId);
            if (drPracticeAppointmentsSettings != null)
            {
                // Get reccurrence info object 
                RecurrenceInfo info = RecurrenceHelper.GetFriendlySeriesInfo(drPracticeAppointmentsSettings["recurrencePattern"].ToString());

                if (info.RecurrenceType.Equals(RecurrenceType.Monthly))
                {
                    MonthlyRecurrenceSettings mo;

                    if (info.EndDateType.Equals(EndDateType.NumberOfOccurrences))
                    {
                        mo = new MonthlyRecurrenceSettings(info.StartDate, info.NumberOfOccurrences);
                        nextAppointmentDates = mo.GetValues(info.MonthlySpecificDatePartOne, info.MonthlySpecificDatePartTwo, info.MonthlyRegenEveryXMonths).Values;

                        // begin --Marina--to have only appoitment dates begining from today and ending 12 months later
                        for (int i = 0; i < nextAppointmentDates.Count; i++)
                        {
                            if (nextAppointmentDates[i].Date <=  DateTime.Now.AddMonths(12))
                                newNextAppointmentDates.Add(nextAppointmentDates[i].Date);
                        }
                        nextAppointmentDates = newNextAppointmentDates;
                        //end -- Marina
                    }
                    else if ((info.EndDateType.Equals(EndDateType.SpecificDate) && DateTime.Today < info.EndDate) ||
                               info.EndDateType.Equals(EndDateType.NoEndDate)
                             )
                    {

                        //set interval end date 
                        DateTime intervalEndDate;

                        //add info.MonthlyRegenEveryXMonths * 10 months on the start date(or today) to set interval  end date 
                        if (info.StartDate > DateTime.Today)
                        //    intervalEndDate = info.StartDate.AddMonths(info.MonthlyRegenEveryXMonths * 10);
                        //else
                        //    intervalEndDate = DateTime.Today.AddMonths(info.MonthlyRegenEveryXMonths * 10);

                            // begin --Marina--to have only appoitment dates begining from today and ending 12 months later
                            intervalEndDate = DateTime.Now.AddMonths(12);
                        else
                            intervalEndDate = DateTime.Now.AddMonths(12);
                        //end --Marina




                        if (info.EndDateType.Equals(EndDateType.SpecificDate) && intervalEndDate > info.EndDate)
                            intervalEndDate = info.EndDate.Value;


                        mo = new MonthlyRecurrenceSettings(info.StartDate, intervalEndDate);
                        nextAppointmentDates = mo.GetValues(info.MonthlySpecificDatePartOne, info.MonthlySpecificDatePartTwo, info.MonthlyRegenEveryXMonths).Values;


                    }

                }
                else if (info.RecurrenceType.Equals(RecurrenceType.Weekly))
                {

                    WeeklyRecurrenceSettings we;

                    if (info.EndDateType.Equals(EndDateType.NumberOfOccurrences))
                    {
                        we = new WeeklyRecurrenceSettings(info.StartDate, info.NumberOfOccurrences);
                        nextAppointmentDates = we.GetValues(info.WeeklyRegenEveryXWeeks, info.WeeklySelectedDays).Values;
                        // begin --Marina--to have only appoitment dates begining from today and ending 12 months later
                        for (int i = 0; i < nextAppointmentDates.Count; i++)
                        {
                            if (nextAppointmentDates[i].Date <= DateTime.Now.AddMonths(12))
                                newNextAppointmentDates.Add(nextAppointmentDates[i].Date);
                        }
                        nextAppointmentDates = newNextAppointmentDates;
                        //end -- Marina

                    }
                    else if ((info.EndDateType.Equals(EndDateType.SpecificDate) && DateTime.Today < info.EndDate) ||
                                info.EndDateType.Equals(EndDateType.NoEndDate)
                             )
                    {

                        //set interval end date 
                        DateTime intervalEndDate;


                        //add (info.WeeklyRegenEveryXWeeks < 4) ? 10 : info.WeeklyRegenEveryXWeeks / 4 * 10  months on the interval start date(or today) to set interval  end date ; 
                        if (info.StartDate > DateTime.Today)
                        //    intervalEndDate = info.StartDate.AddMonths((info.WeeklyRegenEveryXWeeks < 4) ? 10 : info.WeeklyRegenEveryXWeeks / 4 * 10);
                        //else
                        //    intervalEndDate = DateTime.Today.AddMonths((info.WeeklyRegenEveryXWeeks < 4) ? 10 : info.WeeklyRegenEveryXWeeks / 4 * 10);

                            // begin --Marina--to have only appoitment dates begining from today and ending 12 months later
                            intervalEndDate = DateTime.Now.AddMonths(12);
                        else
                            intervalEndDate = DateTime.Now.AddMonths(12);
                        //end --- Marina



                        if (info.EndDateType.Equals(EndDateType.SpecificDate) && intervalEndDate > info.EndDate)
                            intervalEndDate = info.EndDate.Value;

                        we = new WeeklyRecurrenceSettings(info.StartDate, intervalEndDate);
                        nextAppointmentDates = we.GetValues(info.WeeklyRegenEveryXWeeks, info.WeeklySelectedDays).Values;


                    }



                }
            }


            return nextAppointmentDates;
        }

        private void BindNextAppointmentDates(List<DateTime> appointmentsList, List<DateTime> lsOneTimeAppointments, int numOfAppointments)
        {

            //append oneTimeAppointments to a recurring appointment dates
            appointmentsList.AddRange(lsOneTimeAppointments);
            appointmentsList.Sort();

            ////remove duplicates
            IEnumerable<DateTime> appointmentListWithoutDuplicates = appointmentsList.Distinct();

            List<string> nextAppointmentDates = new List<string>();
            int counter = 0;
            foreach (DateTime appointmentsDate in appointmentListWithoutDuplicates)
            {
                if (appointmentsDate >= DateTime.Today.AddDays(int.Parse(ConfigurationManager.AppSettings["numberOfDaysToSubmitPatientLogBeforeServiceDate"])))
                {
                   // nextAppointmentDates.Add(appointmentsDate.ToString("dd MMMM yyyy dddd"));
                    nextAppointmentDates.Add(appointmentsDate.ToString("D"));
                   // DateTime.ParseExact(ddlServiceDate.SelectedValue, "D", null)
                    counter++;
                }
                // begin --Marina--to have only appoitment dates begining from today and ending 12 months later
               // if (counter == numOfAppointments)
                //    break;
                //end -- Marina
            }

            if (nextAppointmentDates.Count > 0)
            {
                ddlServiceDate.DataSource = nextAppointmentDates;
                ddlServiceDate.DataBind();
            }

            
        }

        private void FillFollowUpPatients(Guid practiceId,DateTime nextAppointmentDate)
        {
            DataRowCollection systemSetupValues = SystemSetupBR.GetAllSystemSetupValues().Rows;
            int numOfMonthsBetweenEachGlaucomaExam = int.Parse(systemSetupValues.Find(SystemSetupEnum.GlaucomaExamsRecurrence)["setupValue"].ToString());
            int numOfMonthsBetweenEachRetinaExam = int.Parse(systemSetupValues.Find(SystemSetupEnum.RetinaExamsRecurrence)["setupValue"].ToString());
            DateTime glaucomaExamDate = nextAppointmentDate.AddMonths(-numOfMonthsBetweenEachGlaucomaExam);
            DateTime retinaExamDate = nextAppointmentDate.AddMonths(-numOfMonthsBetweenEachRetinaExam);
            DataTable dtFollowUpPatients = new FinalNotesWorksheetBR().GetFollowUpPatientsForNextAppointmentDate(practiceId, retinaExamDate, glaucomaExamDate);

            List<NameValue> followUpPatients = new List<NameValue>();
            List<NameValue> noShowPatients = new List<NameValue>();
            foreach (DataRow drPatient in dtFollowUpPatients.Rows)
            {
                NameValue patient = new NameValue();
                patient.Id = (int)drPatient["patientId"];
                string exams = " [] ";
                if( (bool)drPatient["glaucoma"] && (bool)drPatient["retina"])
                    exams = " [G R] ";
                else if((bool)drPatient["glaucoma"])
                    exams = " [G] ";
                else if((bool)drPatient["retina"]) 
                    exams = " [R] ";

                patient.Name = drPatient["patientName"].ToString() + exams + " " + ((DateTime)drPatient["serviceDate"]).ToString("MM/dd/yyyy ");
                if ((short)drPatient["status"] == (short)PatientLogPatientStatusEnum.Examined)
                {
                    
                     followUpPatients.Add(patient);
                }
                else  //PatientLogPatientStatusEnum.Canceled || PatientLogPatientStatusEnum.No__Show
                {
                    noShowPatients.Add(patient);
                }
                
            }

            BindListControl<NameValue>.BindAndAddListItem(ddlFollowUpPatient, followUpPatients, "name", "id", "Select Patient", "-1");
            BindListControl<NameValue>.BindAndAddListItem(ddlNoShowPatient, noShowPatients, "name", "id", "Select Patient", "-1");

            

        }

        
        /// <summary>
        /// check if appointment is scheduled in 20 minutes unreserved interval
        /// </summary>
        /// <param name="appointmentTime" example="16:40:00"></param>
        /// <param name="patientsList"></param>
        /// <returns></returns>
        private bool IsAppointmentTimeIntervalFree(string appointmentTime,List<PatientLogPatient> patientsList,int patientIdForEdit)
        {
            bool isFree = true;
            List<PatientLogPatient> undeletedPatientLogPatients;

            if (patientIdForEdit == -1)
            {
                //filter deleted patients
                undeletedPatientLogPatients = patientsList;
            }
            else
            {
                //filter deleted patients and edited patient
                undeletedPatientLogPatients = patientsList.FindAll(delegate(PatientLogPatient patient) { return !patient.PatientId.Equals(patientIdForEdit); });
            }


            int nextPatientIndex = undeletedPatientLogPatients.FindIndex(delegate(PatientLogPatient patient) { return patient.AppointmentTime.CompareTo(appointmentTime) > 0; });
           
            DateTime newPatientAppointmentTime = DateTime.ParseExact(appointmentTime, "HH:mm:ss", null);
            DateTime previousPatientAppointmentTime, nextPatientAppointmentTime;
            if (nextPatientIndex != -1)
            {
                if (nextPatientIndex == 0) // newPatient -> nextPatient -> patient -> patient -> .... 
                {
                    nextPatientAppointmentTime = DateTime.ParseExact(undeletedPatientLogPatients[nextPatientIndex].AppointmentTime, "HH:mm:ss", null);
                    if (newPatientAppointmentTime.AddMinutes(20) > nextPatientAppointmentTime)
                        isFree = false;
                }
                else  //  ... -> patient -> previousPatient -> newPatient -> nextPatient -> patient ->  .... 
                {
                    nextPatientAppointmentTime = DateTime.ParseExact(undeletedPatientLogPatients[nextPatientIndex].AppointmentTime, "HH:mm:ss", null);
                    previousPatientAppointmentTime = DateTime.ParseExact(undeletedPatientLogPatients[nextPatientIndex - 1].AppointmentTime, "HH:mm:ss", null);
                    if (previousPatientAppointmentTime.AddMinutes(20) > newPatientAppointmentTime || newPatientAppointmentTime.AddMinutes(20) > nextPatientAppointmentTime)
                        isFree = false;
                }
            }
            else if (undeletedPatientLogPatients.Count > 0) //  .... -> patient -> patient -> previousPatient -> newPatient  
            {
                previousPatientAppointmentTime = DateTime.ParseExact(undeletedPatientLogPatients[undeletedPatientLogPatients.Count-1].AppointmentTime, "HH:mm:ss", null);
                if (previousPatientAppointmentTime.AddMinutes(20) > newPatientAppointmentTime)
                    isFree = false;
            }
            

            return isFree;
        }




        #endregion //Private methods

        #region MICROSOFT REPORTING

        private void PrintReport(ExportType exportType)
        {

            TotalVisionReport _totalVisionReport = new TotalVisionReport();

            List<PatientLogPatient> patientsList = ViewState["PatientsList"] as List<PatientLogPatient>;

         //   DateTime serviceDate = ViewState["action"].ToString() == "create" ? DateTime.ParseExact(ddlServiceDate.SelectedValue, "dd MMMM yyyy dddd", null) : DateTime.ParseExact(lblServiceDateData.Text, "dd MMMM yyyy dddd", null);
            DateTime serviceDate = ViewState["action"].ToString() == "create" ? DateTime.ParseExact(ddlServiceDate.SelectedValue, "D", null) : DateTime.ParseExact(lblServiceDateData.Text, "D", null);


            LocalReport localReport = new LocalReport();

            localReport.ReportPath = Server.MapPath("~/Reports/rptPatientLog.rdlc");

            ReportDataSource reportDataSource = new ReportDataSource("PatientLogPatient", patientsList);
            localReport.DataSources.Add(reportDataSource);

            ReportParameter[] param = new ReportParameter[2];
            param[0] = new ReportParameter("rptParamPracticeName", lblPracticeNameData.Text);
            param[1] = new ReportParameter("rptParamDate", serviceDate.Date.ToString("MM/dd/yyyy"));

            localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            if (exportType == ExportType.Printer)
            {
                _totalVisionReport.ListItems = patientsList;
                _totalVisionReport.Pameters.Add("rptParamPracticeName", lblPracticeNameData.Text);
                _totalVisionReport.Pameters.Add("rptParamDate", serviceDate.Date.ToString("MM/dd/yyyy"));
                Session["PrintingParams"] = _totalVisionReport;

                Response.Redirect("~/Reports/ReportPreview.aspx?ReportName=PatientLog");
            }
            else if (exportType == ExportType.Pdf)
            {
                DownloadPDF(localReport);

            }
            else if (exportType == ExportType.Excel)
            {
                DownloadExcel(localReport);

            }
        }

        private void DownloadPDF(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PDF</OutputFormat>" +
            //"  <PageWidth>11in</PageWidth>" +
            //"  <PageHeight>8.5in</PageHeight>" +
            //"  <MarginTop>0.2in</MarginTop>" +
            //"  <MarginLeft>0.1in</MarginLeft>" +
            //"  <MarginRight>0.1in</MarginRight>" +
            //"  <MarginBottom>0.2in</MarginBottom>" +
            //"</DeviceInfo>";

            string deviceInfo =
    "<DeviceInfo>" +
    "  <OutputFormat>PDF</OutputFormat>" +
    "  <PageWidth>11in</PageWidth>" +
    "  <PageHeight>8.5in</PageHeight>" +
    "  <MarginTop>0.2in</MarginTop>" +
    "  <MarginLeft>0.6in</MarginLeft>" +
    "  <MarginRight>0.1in</MarginRight>" +
    "  <MarginBottom>0.2in</MarginBottom>" +
    "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=PatientSchedule." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DownloadExcel(LocalReport localReport)
        {
            string reportType = "Excel";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
            "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=PatientSchedule." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }


        #endregion

        
       
       

      

      

       

       

      

      

       
      
       


    }
}



#region Old PDF Export

//public class PDFPageEvent : PdfPageEventHelper
//{
//    private string _imgLogoPath;
//    private string _practiceName;
//    private string _serviceDate;


//    public PDFPageEvent()
//    {
        
//    }


//    public PDFPageEvent(string imgLogoPath,string practiceName,string serviceDate)
//    {
//        this._imgLogoPath = imgLogoPath;
//        this._practiceName = practiceName;
//        this._serviceDate = serviceDate;
//    }

    
//    #region IPdfPageEvent Members

//    //void IPdfPageEvent.OnChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title)
//    //{
//    //    throw new System.NotImplementedException();
//    //}

//    //void IPdfPageEvent.OnChapterEnd(PdfWriter writer, Document document, float paragraphPosition)
//    //{
//    //    throw new System.NotImplementedException();
//    //}

//    //void IPdfPageEvent.OnCloseDocument(PdfWriter writer, Document document)
//    //{
//    //    throw new System.NotImplementedException();
//    //}

//    //void IPdfPageEvent.OnEndPage(PdfWriter writer, Document document)
//    //{
//    //    setHeader(writer, document,this._imgLogoPath);
//    //    //setFooter(writer, document);

//    //}

//    //void IPdfPageEvent.OnGenericTag(PdfWriter writer, Document document, Rectangle rect, string text)
//    //{
//    //    throw new System.NotImplementedException();
//    //}

//    //void IPdfPageEvent.OnOpenDocument(PdfWriter writer, Document document)
//    //{
//    //    throw new System.NotImplementedException();
//    //}

//    //void IPdfPageEvent.OnParagraph(PdfWriter writer, Document document, float paragraphPosition)
//    //{
//    //    throw new System.NotImplementedException();
//    //}

//    //void IPdfPageEvent.OnParagraphEnd(PdfWriter writer, Document document, float paragraphPosition)
//    //{
//    //    throw new System.NotImplementedException();
//    //}

//    //void IPdfPageEvent.OnSection(PdfWriter writer, Document document, float paragraphPosition, int depth, Paragraph title)
//    //{
//    //    throw new System.NotImplementedException();
//    //}

//    //void IPdfPageEvent.OnSectionEnd(PdfWriter writer, Document document, float paragraphPosition)
//    //{
//    //    throw new System.NotImplementedException();
//    //}

//    //void IPdfPageEvent.OnStartPage(PdfWriter writer, Document document)
//    //{
//    //    throw new System.NotImplementedException();
//    //}



//    #endregion

   

//    public override void OnEndPage(PdfWriter writer, Document document)
//    {
       
//        setHeader(writer, document, this._imgLogoPath,this._practiceName,this._serviceDate);
//        setFooter(writer, document);

//        base.OnEndPage(writer, document);
//    }

    



//    private void setHeader(PdfWriter writer, Document document,string imgLogoPath,string practiceName,string serviceDate)
//    {
        



//         PdfPTable tblHeader = new PdfPTable(3);
//         tblHeader.DefaultCell.Border = 0;

//         Rectangle r = new Rectangle(PageSize.A4.Right, PageSize.A4.Top);
//         float[] widths = { 500f, 250f, 120f};
//         tblHeader.SetWidthPercentage(widths, r);

//         Font fFaxMessage = new Font(Font.HELVETICA, 11, Font.BOLDITALIC);
//         fFaxMessage.Color = Color.RED;
//         PdfPTable tblNested1 = new PdfPTable(1);
         
//         PdfPCell cFaxMessage = new PdfPCell(new Phrase("Patient Schedule to be faxed(4) days prior to designated day to (310) 868-0440",fFaxMessage));
//         cFaxMessage.Border = 0;
//         tblNested1.AddCell(cFaxMessage);


//         Font fBold = new Font(Font.HELVETICA, 12, Font.BOLD);
//         Phrase phPractice = new Phrase();

//         Chunk chPracticeName = new Chunk("PRACTICE NAME ", fBold);
//         phPractice.Add(chPracticeName);

//         Chunk chPracticeNameData = new Chunk(practiceName);
//         //chPracticeNameData.SetUnderline(0.8f, -2f);
//         phPractice.Add(chPracticeNameData);

//         PdfPCell cPracticeInfo = new PdfPCell(phPractice);
//         cPracticeInfo.VerticalAlignment = Element.ALIGN_BOTTOM;
//         cPracticeInfo.Border = 0;
//         tblNested1.AddCell(cPracticeInfo);
//         tblHeader.AddCell(tblNested1);

//         PdfPTable tblNested2 = new PdfPTable(1);

//         Font fNote = new Font(Font.HELVETICA, 11, Font.BOLDITALIC);
//         fNote.Color = Color.BLUE;
//         PdfPCell cNote = new PdfPCell(new Phrase("Note: 20 min = 1 scan; 30 min = 2 scans", fNote));
//         cNote.Border = 0;
//         tblNested2.AddCell(cNote);

//         Phrase phDate = new Phrase();
//         Chunk chServiceDate = new Chunk("DATE ", fBold);
//         phDate.Add(chServiceDate);

//         Chunk chServiceDateData = new Chunk(serviceDate);
//         //chServiceDateData.SetUnderline(0.8f, -2f);
//         phDate.Add(chServiceDateData);

//         PdfPCell cServiceDate = new PdfPCell(phDate);
//         cServiceDate.VerticalAlignment = Element.ALIGN_BOTTOM;
//         cServiceDate.Border = 0;
//         tblNested2.AddCell(cServiceDate);
//         tblHeader.AddCell(tblNested2); 
        

//         iTextSharp.text.Image imgLogo = iTextSharp.text.Image.GetInstance(imgLogoPath);
//         ////imgLogo.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
//         PdfPCell cLogo = new PdfPCell(imgLogo);
//         cLogo.HorizontalAlignment = Element.ALIGN_RIGHT;
//         cLogo.Border = 0;
//         tblHeader.AddCell(cLogo);


//         tblHeader.TotalWidth = document.PageSize.Width - 72;
//        tblHeader.WriteSelectedRows(0, -1, document.Left, document.Top + 55, writer.DirectContent);
        
//    }

//    private void setFooter(PdfWriter writer, Document document)
//    {
//        Font fFooter = new Font(Font.HELVETICA, 10, Font.ITALIC);
//        Font fFooterBlue = new Font(Font.HELVETICA, 10, Font.ITALIC, Color.BLUE);
//        Font fFooterRed = new Font(Font.HELVETICA, 10, Font.ITALIC, Color.RED);

//        Phrase phFooter = new Phrase("M: Male F: Female G: Glaucoma Patient R: Retina Patient M: Medicare MC: Medi-Cal P/(Specify PPO name) C: Cach V: VSP Primary Care (Glaucoma Only))", fFooter);
//        ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_LEFT, phFooter, document.Left , document.Bottom - 10, 0);
//    }
//}

#endregion //Old PDF Export

