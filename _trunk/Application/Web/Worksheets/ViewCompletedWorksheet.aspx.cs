﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;
using Microsoft.Reporting.WebForms;
using System.Text;



namespace CEI.Web.Worksheets
{
    public partial class ViewCompletedWorksheet : BasePage
    {

        #region GridView columns


        private const int GV_PATIENTS_NAME = 0;
       
        private const int GV_PATIENTS_BFA = 1;
        private const int GV_PATIENTS_HRT_GLAUCOMA = 2;
        private const int GV_PATIENTS_HRT_RETINA = 3;
        private const int GV_PATIENTS_CPT_CODES = 4;
        private const int GV_PATIENTS_INSURANCE_COMPANY_NAME = 5;
       
    
        

        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {

           
            
            if (!Page.IsPostBack)
            {
                if (this.ValidateRequestParams())
                {

                        lblPracticeNameData.Text = Request.QueryString["prName"];
                        lblServiceDateData.Text = Request.QueryString["servDate"];
                        List<WorksheetPatient> patientsList = new WorksheetBR().GetWorksheetPatientsByPatientLogId((int)ViewState["patientLogId"]);
                        ViewState["WorkSheetPatients"] = patientsList;
                        this.BingGridViewPatients(patientsList);
                   
                      
                }
                else
                {
                    //TODO: show error message
                }
            }
        }




      

        protected void gvPatients_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                WorksheetPatient worksheetPatient = e.Row.DataItem as WorksheetPatient;
                if (worksheetPatient.Glaucoma)
                {
                    e.Row.Cells[GV_PATIENTS_BFA].Text = worksheetPatient.BFA.ToString(); //"BFA";
                    e.Row.Cells[GV_PATIENTS_HRT_GLAUCOMA].Text = worksheetPatient.HRT_Glaucoma.ToString();
                  //  e.Row.Cells[GV_PATIENTS_HRT_GLAUCOMA].Text = "HRT-G";
                }

                if (worksheetPatient.Retina)
                   e.Row.Cells[GV_PATIENTS_HRT_RETINA].Text = worksheetPatient.HRT_Retina.ToString();
                   // e.Row.Cells[GV_PATIENTS_HRT_RETINA].Text = "HRT-R";



             

              

             

                Label lblGlaucomaCodes = e.Row.Cells[GV_PATIENTS_CPT_CODES].FindControl("lblGlaucomaCodes") as Label;
                Label lblRetinaCodes = e.Row.Cells[GV_PATIENTS_CPT_CODES].FindControl("lblRetinaCodes") as Label;

                int i = 0;
                int j = 0;
                foreach (CPTCode CPT_Code in worksheetPatient.CPT_CodesList)
                {
                    //if (CPT_Code.CodeType.Equals(CPTCodeTypeEnum.Glaucoma))
                    //    lblGlaucomaCodes.Text += CPT_Code.Code + "<BR/>";
                    //else
                    //    lblRetinaCodes.Text += CPT_Code.Code + "<BR/>";
                    if (CPT_Code.CodeType.Equals(CPTCodeTypeEnum.Glaucoma))
                    {
                        // lblGlaucomaCodes.Text += CPT_Code.Code + "<BR/>";
                        lblGlaucomaCodes.Text += CPT_Code.Code + "&nbsp;&nbsp;&nbsp;&nbsp;";
                        i++;
                    }
                    else
                    {
                        //  lblRetinaCodes.Text += CPT_Code.Code + "<BR/>";
                        lblRetinaCodes.Text += CPT_Code.Code + "&nbsp;&nbsp;&nbsp;&nbsp;";
                        j++;
                    }

                    if (i == 2)
                    {
                        lblGlaucomaCodes.Text = lblGlaucomaCodes.Text.Substring(0, lblGlaucomaCodes.Text.Length - 24);
                        lblGlaucomaCodes.Text += "<BR/>";
                        i = 0;
                    }
                    if (j == 2)
                    {
                        lblRetinaCodes.Text = lblRetinaCodes.Text.Substring(0, lblRetinaCodes.Text.Length - 24);
                        lblRetinaCodes.Text += "<BR/>";
                        j = 0;
                    }
                }
                if (i == 1)
                    lblGlaucomaCodes.Text = lblGlaucomaCodes.Text.Substring(0, lblGlaucomaCodes.Text.Length - 24);
                if (j == 1)
                {
                    lblRetinaCodes.Text = lblRetinaCodes.Text.Substring(0, lblRetinaCodes.Text.Length - 24);
                }
               
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                 Label lblBFA = (Label)e.Row.FindControl("Label1");
                 Label lblHRTG = (Label)e.Row.FindControl("lblHRTG");
                 Label lblHRTR = (Label)e.Row.FindControl("Label11");
                 Label lblPatientCount = (Label)e.Row.FindControl("lblPatientCount1");

                 int totalCount = int.Parse(lblBFA.Text) + int.Parse(lblHRTG.Text) + int.Parse(lblHRTR.Text);
               //  lblPatientCount.Text =totalCount.ToString();
                // lblHRTG.Text = countBFA.ToString();
                //  lblBFA.Text = countBFA.ToString();
                //  lblHRTR.Text = countHRTR.ToString();
                //   lblPatientCount.Text =lblPatientCount.Text + ": "+ countPatient.ToString();

            }
        }


        protected void btnPrintPreview_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Printer);
        }

        protected void btnPrintExcel_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Excel);
        }

        protected void btnPrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Pdf);
        }

       



        #endregion //Event handlers

        #region Private methods

        private bool ValidateRequestParams()
        {
            bool isValid = false;
            int patientLogId;
                if (
                        
                        Request.QueryString["prName"] != null &&
                        Request.QueryString["servDate"] != null &&
                        Request.QueryString["logId"] != null &&
                        int.TryParse(Request.QueryString["logId"],out patientLogId) && 
                        this.ValidateServiceDate(Request.QueryString["servDate"])
                    )
                    {
                        isValid = true;
                        ViewState["patientLogId"] = patientLogId;
                    }




            return isValid;


        }



        private bool ValidateServiceDate(string serviceDate)
        {
            try
            {
                DateTime.ParseExact(serviceDate, "M/d/yyyy", null);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }

        }


        private void BingGridViewPatients(List<WorksheetPatient> patientsList)
        {

            gvPatients.DataSource = patientsList;
            gvPatients.DataBind();
            
        }


        

       

        #endregion //Private methods

        



        #region MICROSOFT REPORTING

        private void PrintReport(ExportType exportType)
        {

            TotalVisionReport _totalVisionReport = new TotalVisionReport();

            List<WorksheetPatient> patientsList = ViewState["WorkSheetPatients"] as List<WorksheetPatient>;



            LocalReport localReport = new LocalReport();

            localReport.ReportPath = Server.MapPath("~/Reports/CompletedWorksheet.rdlc");

            ReportDataSource reportDataSource = new ReportDataSource("WorksheetPatient", patientsList);
            localReport.DataSources.Add(reportDataSource);

            ReportParameter[] param = new ReportParameter[2];
            param[0] = new ReportParameter("rptParamPracticeName", lblPracticeNameData.Text);
            param[1] = new ReportParameter("rptParamDate", lblServiceDateData.Text);

            localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            if (exportType == ExportType.Printer)
            {
                _totalVisionReport.ListItems = patientsList;
                _totalVisionReport.Pameters.Add("rptParamPracticeName", lblPracticeNameData.Text);
                _totalVisionReport.Pameters.Add("rptParamDate", lblServiceDateData.Text);
                Session["PrintingParams"] = _totalVisionReport;

                Response.Redirect("~/Reports/ReportPreview.aspx?ReportName=CompletedWorksheet");
            }
            else if (exportType == ExportType.Pdf)
            {
                DownloadPDF(localReport);

            }
            else if (exportType == ExportType.Excel)
            {
                DownloadExcel(localReport);

            }
        }

        private void DownloadPDF(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PDF</OutputFormat>" +
            //"  <PageWidth>11in</PageWidth>" +
            //"  <PageHeight>8.5in</PageHeight>" +
            //"  <MarginTop>0.2in</MarginTop>" +
            //"  <MarginLeft>0.5in</MarginLeft>" +
            //"  <MarginRight>0.5in</MarginRight>" +
            //"  <MarginBottom>0.2in</MarginBottom>" +
            //"</DeviceInfo>";

            string deviceInfo =
           "<DeviceInfo>" +
           "  <OutputFormat>PDF</OutputFormat>" +
           "  <PageWidth>11in</PageWidth>" +
           "  <PageHeight>8.5in</PageHeight>" +
           "  <MarginTop>0.2in</MarginTop>" +
           "  <MarginLeft>0.6in</MarginLeft>" +
           "  <MarginRight>0.1in</MarginRight>" +
           "  <MarginBottom>0.2in</MarginBottom>" +
           "</DeviceInfo>";

          


            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=CompletedWorksheet." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DownloadExcel(LocalReport localReport)
        {
            string reportType = "Excel";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
            "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=CompletedWorksheet." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }


        #endregion

        protected void gvPatients_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
               
//                GridView gv = sender as GridView;
//                int index = gv.Rows.Count;
//                //Table table = e.Row.Parent as Table;
//                GridViewRow newRow = new GridViewRow(0, 0, DataControlRowType.Footer, DataControlRowState.Normal);
//                TableCell newCell = new TableCell();
//                newRow.Attributes.Add("class", "gridViewFooterManagePatientLog");
//                newCell.Text = "Total patients: ";
//                newRow.Cells.Add(newCell);
//                newCell = new TableCell();
//                newCell.Attributes.Add("style","colspan=5");
                
//                newCell.Text = "Ovde vrednosta";
//                newRow.Cells.Add(newCell);
//                //prv nacin
//                gv.Controls.Add(newRow);
                

////vtor nacin nacin
//                Table t = new Table();

//                t.Rows.Add(newRow);

//                System.Web.UI.Control newControl = new System.Web.UI.Control();
//                newControl.Controls.Add(t);
//                //moze ili va
//                e.Row.Cells[0].Controls.Add(t);
//                //ili ova
//                gv.Controls.Add(newControl);
            
            }
           // base.OnRowCreated(e);

        }

        //private void InitializeRow(GridViewRow row, DataControlField[] fields, TableRowCollection newRows)
        //{
        //    GridViewRowEventArgs e = new GridViewRowEventArgs(row);
        //    InitializeRow(row, fields);
        //    OnRowCreated(e);

        //    newRows.Add(row);

        //    row.DataBind();
        //    OnRowDataBound(e);
        //    row.DataItem = null;
        //}

       

      

      

       
      
       


    }
}
