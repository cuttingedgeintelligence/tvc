﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="True"
    CodeBehind="ManageWorksheet.aspx.cs" Inherits="CEI.Web.Worksheets.ManageWorksheet"
    Title="Manage Worksheet" Async="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControls/DayMonthYear.ascx" TagName="DayMonthYear" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>

    <script runat="server">
        int TotalRows = 0;
        string GetTotalRows(string Price)
        {
            TotalRows += 1;
            return Price;
        }
        int GetTotal()
        {
            return TotalRows;
        }

        int TotalBFA = 0;
        string GetTotalBFA(string BFA)
        {
            if (BFA != "False")
            {
                TotalBFA += 1;
            }
            return BFA;
        }
        int GetBFA()
        {
            return TotalBFA;
        }

        int TotalHRTR = 0;
        string GetTotalHRTR(string HRTR)
        {
            if (HRTR != "False")
            {
                TotalHRTR += 1;
            }
            return HRTR;
        }
        int GetHRTR()
        {
            return TotalHRTR;
        }
    
        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="javascript" type="text/javascript">

        function bindClickForCPTCodeCheckboxes(cblCPTCodesRetinaId, cblCPTCodesGlaucomaId, trCPTColorId) {


            jQuery('#' + cblCPTCodesRetinaId + ' input:checkbox').each(function() {

                jQuery(this).click(function() { showHideCPTMenu(cblCPTCodesRetinaId, cblCPTCodesGlaucomaId, trCPTColorId); });

            });


            jQuery('#' + cblCPTCodesGlaucomaId + ' input:checkbox').each(function() {

                jQuery(this).click(function() { showHideCPTMenu(cblCPTCodesRetinaId, cblCPTCodesGlaucomaId, trCPTColorId); });

            });

        }

        function showHideCPTMenu(cblCPTCodesRetinaId, cblCPTCodesGlaucomaId, trCPTColorId) {

            var numOfRetinaChecked = jQuery("#" + cblCPTCodesRetinaId + " input:checkbox[checked]");
            var numOfGlaucomaChecked = jQuery("#" + cblCPTCodesGlaucomaId + " input:checkbox[checked]");
            if (numOfRetinaChecked.length > 0 & numOfGlaucomaChecked.length > 0)
                jQuery("#" + trCPTColorId).css('display', '');
            else
                jQuery("#" + trCPTColorId).css('display', 'none');
        }
            

           
        
    </script>

    <asp:Label ID="lblHeaderText" runat="server" Text="Manage Worksheet" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <table width="400px">
        <tr>
            <td width="100px">
                <asp:Label ID="lblPracticeName" runat="server" Text="Practice Name:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblPracticeNameData" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100px">
                <asp:Label ID="lblServiceDate" runat="server" Text="Service Date:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblServiceDateData" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="padding: 10px 0; display:none;">
        <tr>
            <td>
                <asp:ImageButton ID="btnPrintPDF" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png"
                    ToolTip="Download PDF" OnClick="btnPrintPDF_Click" Style="padding-right: 3px;" />
            </td>
            <td>
                <asp:ImageButton ID="btnPrintExcel" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png"
                    ToolTip="Download Excel" OnClick="btnPrintExcel_Click" Style="padding-left: 5px;" />
            </td>
            <td>
                <asp:ImageButton ID="btnPrintPreview" runat="server" ImageUrl="~/Images/Mockups/printIcon.png"
                    ToolTip="View and Print" OnClick="btnPrintPreview_Click" Style="display: none;" />
            </td>
        </tr>
    </table>
    <div class="panelHolder" style="display: none;">
        <asp:UpdatePanel ID="upSelectCPTCodes" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="200" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="lblSelectedPatient" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="lblSelectCPTCodes" runat="server" Text="Select CPT codes:"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px 0;">
                            <table width="200" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table width="100" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblGlaucoma" runat="server" Text="Glaucoma"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBoxList ID="cblCPTCodesGlaucoma" runat="server">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="border-left: 1px solid #fff; padding-left: 10px;">
                                        <table width="99" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblRetina" runat="server" Text="Retina"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBoxList ID="cblCPTCodesRetina" runat="server">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px 0;">
                            <table id="tbpCPTCode" style="display: none;" runat="server" width="200" border="0"
                                cellspacing="0" cellpadding="0">
                                <tr id="trCTPColor" runat="server">
                                    <td align="right">
                                        <asp:Label ID="lblCTPColorLabel" runat="server" Text="Select Value: "></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:DropDownList ID="ddlCTPColorValues" runat="server">
                                            <asp:ListItem Text="Value 1" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Value 2" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Value 3" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Value 4" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="text-align: center;">
                        <td colspan="3">
                            <asp:Button ID="btnSaveCPTCodes" runat="server" CausesValidation="false" OnClick="btnSaveCPTCodes_Click"
                                Text="Save" />
                            <asp:Button ID="btnCancelCPTCodes" runat="server" CausesValidation="false" OnClientClick=" $('.panelHolder ').slideUp('slow');"
                                Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upPatients" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvPatients"  runat="server" AutoGenerateColumns="False"
                DataKeyNames="Id,PatientId,FullName" OnRowCommand="gvPatients_RowCommand" SkinID="gridViewAdmin"
                Width="920px" Style="clear: both;" OnRowDataBound="gvPatients_RowDataBound" EmptyDataText="There are no patients in the final notes worksheet" >
                <Columns>
                    <%--<asp:HyperLinkField DataNavigateUrlFields="PatientId" DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}"
                        DataTextField="fullName" HeaderText="Patient Name">
                        <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                        <ItemStyle HorizontalAlign="Left" />
                        <FooterStyle HorizontalAlign="Center" />
                    </asp:HyperLinkField>--%>
                    <asp:TemplateField HeaderText="Patient Name">
                        <ItemTemplate>
                            <asp:HyperLink ID="hpPatinetName" runat="server" Text='<%# GetTotalRows(Eval("fullName").ToString()) %>'
                                NavigateUrl='<%# "~/ViewPatientProfile/ViewPatientProfile.aspx?id="+ Eval("PatientId") %>'></asp:HyperLink>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblPatientCount" runat="server" Text=' <%# "TOTAL EXAMS: " + GetTotal() %>'></asp:Label>
                        </FooterTemplate>
                        <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                        <ItemStyle HorizontalAlign="Left" />
                        <FooterStyle HorizontalAlign="Left" Font-Bold="true" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="BFA">
                        <ItemTemplate>
                            <asp:Label ID="lblBFA" Visible="false" runat="server" Text='<%# GetTotalBFA( Eval("Glaucoma").ToString())%>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblCountBFA" Visible="false" runat="server" Text=""></asp:Label>
                            <asp:Label ID="Label1" runat="server" Text='<%# GetBFA() %>'></asp:Label>
                        </FooterTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center"  CssClass="moveCenter" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRT-G">
                        <ItemTemplate>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblCountHRTG" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblHRTG" runat="server" Text='<%# GetBFA() %>'></asp:Label>
                        </FooterTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center"  CssClass="moveCenter"/>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRT-R">
                        <ItemTemplate>
                            <asp:Label ID="lblhrtr" Visible="false" runat="server" Text='<%# GetTotalHRTR( Eval("Retina").ToString())%>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblCountHRTR" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="Label11" runat="server" Text='<%# GetHRTR() %>'></asp:Label>
                        </FooterTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center"  CssClass="moveCenter"/>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CPT Codes">
                        <ItemTemplate>
                            <table style="width: 315px;">
                                <tr>
                                    <td align="center" style="vertical-align: top; text-align: center;">
                                        <table style="padding-right: 5px; width: 150px;">
                                            <tr>
                                                <td style="text-align: center; font-weight: bold">
                                                    <asp:Label ID="lblGlaucoma" Style="display: block" runat="server" Text="Glaucoma"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblGlaucomaCodes" Style="display: block" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="center" style="vertical-align: top; text-align: center; border-left: 1px solid #c2c4d3;">
                                        <table style="padding-left: 5px; width: 150px;">
                                            <tr>
                                                <td style="text-align: center; width: 150px; font-weight: bold">
                                                    <asp:Label ID="lblRetina" Style="display: block" runat="server" Text="Retina"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblRetinaCodes" Style="display: block" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="315px" />
                    </asp:TemplateField>
                    <asp:ButtonField ButtonType="Button" CommandName="assigncpt" HeaderText="Assign CPT"
                        ShowHeader="True" Text="Assign CPT">
                        <ControlStyle CssClass="btnClass" />
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                    <asp:BoundField DataField="insuranceCompanyName" HeaderText="Insurance">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <asp:Button ID="btnSaveWorksheet" runat="server" CausesValidation="False" OnClick="btnSaveWorksheet_Click"
                Text="Save" Style="display: none;" />
            <asp:Button ID="btnCancelPatientLog" runat="server" Text="Cancel" CausesValidation="false"
                PostBackUrl="~/Worksheets/ViewPendingPatientLogs.aspx" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
