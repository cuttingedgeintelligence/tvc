﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;
using Microsoft.Reporting.WebForms;


using System.Text;

namespace CEI.Web.Worksheets
{
    public partial class ManageWorksheet : BasePage
    {

        #region GridView columns

        int countBFA =0;
        int countHRTR = 0;
        int countPatient = 0;
        private const int GV_PATIENTS_NAME = 0;
        private const int GV_PATIENTS_BFA = 1;
        private const int GV_PATIENTS_HRT_GLAUCOMA = 2;
        private const int GV_PATIENTS_HRT_RETINA = 3;
        private const int GV_PATIENTS_CPT_CODES = 4;
        private const int GV_PATIENTS_ASSIGN_CPT = 5;
        private const int GV_PATIENTS_INSURANCE_COMPANY_NAME = 6;

        //int TotalUnitPrice=0;
        //int GetUnitPrice(int TotalUnitPrice)
        //{
        //    TotalUnitPrice += 1;
        //    return TotalUnitPrice;
        //}
        //int GetTotal()
        //{
        //    return TotalUnitPrice;
        //}
    
        

        #endregion //GridView columns

        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {

           
            
            if (!Page.IsPostBack)
            {
                if (this.ValidateRequestParams())
                {

                        lblPracticeNameData.Text = Request.QueryString["prName"];
                        lblServiceDateData.Text = Request.QueryString["servDate"];
                        List<WorksheetPatient> patientsList = new WorksheetBR().GetWorksheetPatientsByPatientLogId((int)ViewState["patientLogId"]);
                        this.BingGridViewPatients(patientsList);
                        ViewState["PatientsList"] = patientsList;
                        
                        DataTable dtAllCPTCodes = new CPTCodeBR().GetAllCPTCodes();
                        DataTable dtCodesRetina = dtAllCPTCodes.Clone();
                        DataTable dtCodesGlaucoma = dtAllCPTCodes.Clone();
                    
                        foreach(DataRow drCPTCode in dtAllCPTCodes.Rows)
                        {
                            if(    ((short)drCPTCode["codeType"]).Equals((short)CPTCodeTypeEnum.Glaucoma))
                                dtCodesGlaucoma.ImportRow(drCPTCode);
                            else
                                dtCodesRetina.ImportRow(drCPTCode);
                        }

                        BindListControl<DataTable>.Bind(cblCPTCodesRetina, dtCodesRetina, "code", "id");
                        BindListControl<DataTable>.Bind(cblCPTCodesGlaucoma, dtCodesGlaucoma, "code", "id");
                       
                       
                       
                }
                else
                {
                    //TODO: show error message
                   // Response.Redirect("~/Default.aspx");
                }
            }
        }




        protected void btnSaveWorksheet_Click(object sender, EventArgs e)
        {

            List<WorksheetPatient> worksheetPatients = ViewState["PatientsList"] as List<WorksheetPatient>;
            if (new WorksheetBR().UpdateWorksheetPatients(worksheetPatients))
            {
                Response.Redirect("~/Worksheets/ViewPendingPatientLogs.aspx", true);   
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManageWorksheet), "SaveWorksheetErrorScript", "alert('Worksheet changes are not saved because some error occurs.Please try again');", true);
            }
         

            
        }

        protected void gvPatients_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                countPatient += 1;
                WorksheetPatient worksheetPatient = e.Row.DataItem as WorksheetPatient;
                
                if (worksheetPatient.Glaucoma)
                {

                    e.Row.Cells[GV_PATIENTS_BFA].Text = worksheetPatient.BFA.ToString(); //"BFA";
                  //  e.Row.Cells[GV_PATIENTS_HRT_GLAUCOMA].Text = "HRT-G";
                    e.Row.Cells[GV_PATIENTS_HRT_GLAUCOMA].Text = worksheetPatient.HRT_Glaucoma.ToString();
                    countBFA += 1;
                }

                if (worksheetPatient.Retina)
                {
                  //  e.Row.Cells[GV_PATIENTS_HRT_RETINA].Text ="HRT-R";
                   e.Row.Cells[GV_PATIENTS_HRT_RETINA].Text = worksheetPatient.HRT_Retina.ToString();
                    countHRTR += 1;
                }


               

                Label lblGlaucomaCodes = e.Row.Cells[GV_PATIENTS_CPT_CODES].FindControl("lblGlaucomaCodes") as Label;
                Label lblRetinaCodes = e.Row.Cells[GV_PATIENTS_CPT_CODES].FindControl("lblRetinaCodes") as Label;
                int i = 0;
                int j = 0;
                foreach (CPTCode CPT_Code in worksheetPatient.CPT_CodesList)
                {

                    if (CPT_Code.CodeType.Equals(CPTCodeTypeEnum.Glaucoma))
                    {
                        // lblGlaucomaCodes.Text += CPT_Code.Code + "<BR/>";
                        lblGlaucomaCodes.Text += CPT_Code.Code + "&nbsp;&nbsp;&nbsp;&nbsp;";
                        i++;
                    }
                    else
                    {
                        //  lblRetinaCodes.Text += CPT_Code.Code + "<BR/>";
                        lblRetinaCodes.Text += CPT_Code.Code + "&nbsp;&nbsp;&nbsp;&nbsp;";
                        j++;
                    }
                    
                    if (i == 2)
                    {
                     lblGlaucomaCodes.Text = lblGlaucomaCodes.Text.Substring(0, lblGlaucomaCodes.Text.Length -24);
                     lblGlaucomaCodes.Text += "<BR/>";
                     i = 0;
                    }
                    if (j == 2)
                    {
                     lblRetinaCodes.Text = lblRetinaCodes.Text.Substring(0, lblRetinaCodes.Text.Length -24);
                     lblRetinaCodes.Text += "<BR/>";
                     j = 0;
                    }
                }

                if (i == 1)
                    lblGlaucomaCodes.Text = lblGlaucomaCodes.Text.Substring(0, lblGlaucomaCodes.Text.Length - 24);
                if (j == 1)
                {
                    lblRetinaCodes.Text = lblRetinaCodes.Text.Substring(0, lblRetinaCodes.Text.Length - 24);
                }

                //e.Row.Cells[GV_PATIENTS_CPT_CODES].Text = selectedCPTCodesNames;
               
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
               // Label lblBFA = (Label)e.Row.FindControl("lblCountBFA");
                //Label lblHRTG = (Label)e.Row.FindControl("lblCountHRTG");
                //Label lblHRTR = (Label)e.Row.FindControl("lblCountHRTR");
             //   Label lblPatientCount = (Label)e.Row.FindControl("lblPatientCount");

                
               // lblHRTG.Text = countBFA.ToString();
              //  lblBFA.Text = countBFA.ToString();
              //  lblHRTR.Text = countHRTR.ToString();
             //   lblPatientCount.Text =lblPatientCount.Text + ": "+ countPatient.ToString();
                
            }
        }


        protected void gvPatients_RowCommand(object sender, GridViewCommandEventArgs e)
        {


            if (e.CommandName.Equals("assigncpt"))
            {
                ViewState["selectedPatientLogPatientIndex"] = int.Parse(e.CommandArgument.ToString());
                cblCPTCodesGlaucoma.ClearSelection();
                cblCPTCodesRetina.ClearSelection();
                this.SelectCPTCodes();
                lblSelectedPatient.Text = "Patient: " + gvPatients.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["FullName"].ToString();
                    

                upSelectCPTCodes.Update();
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManageWorksheet), "jQueryCollapseScript", "$('.panelHolder ').slideDown('slow');", true);
                ScriptManager.RegisterStartupScript(this, typeof(ManageWorksheet), "AddOnClickAttributeScript", "bindClickForCPTCodeCheckboxes('" + cblCPTCodesRetina.ClientID + "','" + cblCPTCodesGlaucoma.ClientID + "','" + trCTPColor.ClientID + "');", true);
            }

           
        }

        protected void btnSaveCPTCodes_Click(object sender, EventArgs e)
        {
            bool hasGlaucomaSelected = false;
            bool hasRetinaSelected = false;
            List<WorksheetPatient> worksheetPatients =  ViewState["PatientsList"] as List<WorksheetPatient>;
            int selectedPatientLogPatientId = (int)gvPatients.DataKeys[(int)ViewState["selectedPatientLogPatientIndex"]].Values["Id"];
            WorksheetPatient worksheetPatientForEdit = worksheetPatients.Find(delegate(WorksheetPatient worksheetPatientParam) { return worksheetPatientParam.Id == selectedPatientLogPatientId; });


            List<CPTCode> selectedCPTCodes = new List<CPTCode>();
            string selectedGlaucomaCPTCodesNames = string.Empty;
            string selectedRetinaCPTCodesNames = string.Empty;

            CPTCode CPT_Code_InList;
            //Get Glaucoma Codes
            int i = 0;
            int j = 0;
            foreach (ListItem itemCPTCodeGlaucoma in cblCPTCodesGlaucoma.Items)
            {

                CPT_Code_InList = worksheetPatientForEdit.CPT_CodesList.Find(delegate(CPTCode CPT_CodeParam) { return CPT_CodeParam.Id == int.Parse(itemCPTCodeGlaucoma.Value); });
                if (CPT_Code_InList != null)
                {
                    if (CPT_Code_InList.PatientLogPatientCPTCodeId != -1)
                    {

                       
                        if (itemCPTCodeGlaucoma.Selected)
                        {
                            hasGlaucomaSelected = true;
                            selectedGlaucomaCPTCodesNames += itemCPTCodeGlaucoma.Text + "&nbsp;&nbsp;&nbsp;&nbsp;";
                            i++;
                        }
                        else
                            CPT_Code_InList.ActionType = ActionTypeEnum.Delete;
                      


                        selectedCPTCodes.Add(CPT_Code_InList);
                    }
                    else
                    {
                        if (itemCPTCodeGlaucoma.Selected)
                        {
                            hasGlaucomaSelected = true;
                            CPTCode CPT_Code = new CPTCode();
                            CPT_Code.Id = int.Parse(itemCPTCodeGlaucoma.Value);
                            CPT_Code.Code = itemCPTCodeGlaucoma.Text;
                            CPT_Code.PatientLogPatientCPTCodeId = -1;
                            selectedCPTCodes.Add(CPT_Code);
                            selectedGlaucomaCPTCodesNames += itemCPTCodeGlaucoma.Text + "&nbsp;&nbsp;&nbsp;&nbsp;";
                            i++;
                        }

                    }
                   

                }
                else
                {
                    if (itemCPTCodeGlaucoma.Selected)
                    {
                        hasGlaucomaSelected = true;
                        CPTCode CPT_Code = new CPTCode();
                        CPT_Code.Id = int.Parse(itemCPTCodeGlaucoma.Value);
                        CPT_Code.Code = itemCPTCodeGlaucoma.Text;
                        CPT_Code.PatientLogPatientCPTCodeId = -1;
                        selectedCPTCodes.Add(CPT_Code);
                        selectedGlaucomaCPTCodesNames += itemCPTCodeGlaucoma.Text + "&nbsp;&nbsp;&nbsp;&nbsp;";
                        i++;
                    }
                }
                if (i == 2)
                {
                    selectedGlaucomaCPTCodesNames = selectedGlaucomaCPTCodesNames.Substring(0, selectedGlaucomaCPTCodesNames.Length -24);
                    selectedGlaucomaCPTCodesNames += "<BR/>";
                    i = 0;
                }


            }

            if (i == 1)
                selectedGlaucomaCPTCodesNames = selectedGlaucomaCPTCodesNames.Substring(0, selectedGlaucomaCPTCodesNames.Length - 24);
           


            //Get REtina Codes
            foreach (ListItem itemCPTCodeRetina in cblCPTCodesRetina.Items)
            {

                CPT_Code_InList = worksheetPatientForEdit.CPT_CodesList.Find(delegate(CPTCode CPT_CodeParam) { return CPT_CodeParam.Id == int.Parse(itemCPTCodeRetina.Value); });
                if (CPT_Code_InList != null)
                {
                    if (CPT_Code_InList.PatientLogPatientCPTCodeId != -1)
                    {

                        if (itemCPTCodeRetina.Selected)
                        {
                            hasRetinaSelected = true;
                            selectedRetinaCPTCodesNames += itemCPTCodeRetina.Text + "&nbsp;&nbsp;&nbsp;&nbsp;";
                            j++;
                        }
                        else
                            CPT_Code_InList.ActionType = ActionTypeEnum.Delete;

                        selectedCPTCodes.Add(CPT_Code_InList);
                    }
                    else
                    {
                        if (itemCPTCodeRetina.Selected)
                        {
                            hasRetinaSelected = true;
                            CPTCode CPT_Code = new CPTCode();
                            CPT_Code.Id = int.Parse(itemCPTCodeRetina.Value);
                            CPT_Code.Code = itemCPTCodeRetina.Text;
                            CPT_Code.PatientLogPatientCPTCodeId = -1;
                            selectedCPTCodes.Add(CPT_Code);
                            selectedRetinaCPTCodesNames += itemCPTCodeRetina.Text + "&nbsp;&nbsp;&nbsp;&nbsp;";
                            j++;
                        }

                    }
                }
                else
                {
                    if (itemCPTCodeRetina.Selected)
                    {
                        hasRetinaSelected = true;
                        CPTCode CPT_Code = new CPTCode();
                        CPT_Code.Id = int.Parse(itemCPTCodeRetina.Value);
                        CPT_Code.Code = itemCPTCodeRetina.Text;
                        CPT_Code.PatientLogPatientCPTCodeId = -1;
                        selectedCPTCodes.Add(CPT_Code);
                        selectedRetinaCPTCodesNames += itemCPTCodeRetina.Text + "&nbsp;&nbsp;&nbsp;&nbsp;";
                        j++;
                    }
                }
                if (j == 2)
                {
                    selectedRetinaCPTCodesNames = selectedRetinaCPTCodesNames.Substring(0, selectedRetinaCPTCodesNames.Length -24);
                    selectedRetinaCPTCodesNames += "<BR/>";
                    j = 0;
                }


            }

            if (j == 1)
            {
                selectedRetinaCPTCodesNames = selectedRetinaCPTCodesNames.Substring(0, selectedRetinaCPTCodesNames.Length - 24);
            }
            
            worksheetPatientForEdit.CPT_CodesList = selectedCPTCodes;
            if(hasGlaucomaSelected && hasRetinaSelected)
                worksheetPatientForEdit.ColorCodeId = ddlCTPColorValues.SelectedValue;
            ViewState["PatientsList"] = worksheetPatients;

           


            (gvPatients.Rows[(int)ViewState["selectedPatientLogPatientIndex"]].Cells[GV_PATIENTS_CPT_CODES].FindControl("lblGlaucomaCodes") as Label).Text = selectedGlaucomaCPTCodesNames;
            (gvPatients.Rows[(int)ViewState["selectedPatientLogPatientIndex"]].Cells[GV_PATIENTS_CPT_CODES].FindControl("lblRetinaCodes") as Label).Text = selectedRetinaCPTCodesNames;

            
            upPatients.Update();

            //gvPatients.DataBind();
            ScriptManager.RegisterClientScriptBlock(this, typeof(ManageWorksheet), "jQueryCollapseScript", "$('.panelHolder ').slideUp('slow');", true);

        }

        protected void btnPrintPreview_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Printer);
        }

        protected void btnPrintExcel_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Excel);
        }

        protected void btnPrintPDF_Click(object sender, ImageClickEventArgs e)
        {
            PrintReport(ExportType.Pdf);
        }


        #endregion //Event handlers

        #region Private methods

        private bool ValidateRequestParams()
        {
            bool isValid = false;
            int patientLogId;
                if (
                        
                        Request.QueryString["prName"] != null &&
                        Request.QueryString["servDate"] != null &&
                        Request.QueryString["logId"] != null &&
                        int.TryParse(Request.QueryString["logId"],out patientLogId) && 
                        this.ValidateServiceDate(Request.QueryString["servDate"])
                    )
                    {
                        isValid = true;
                        ViewState["patientLogId"] = patientLogId;
                    }




            return isValid;


        }



        private bool ValidateServiceDate(string serviceDate)
        {
            try
            {
                DateTime.ParseExact(serviceDate, "M/d/yyyy", null);
                return true;
            }
            catch (FormatException)
            {

                return false;
            }

        }


        private void BingGridViewPatients(List<WorksheetPatient> patientsList)
        {

            gvPatients.DataSource = patientsList;
            gvPatients.DataBind();
            if (gvPatients.Rows.Count > 0)
            {
                btnSaveWorksheet.Style.Add("display", "");
            }
            else
            {
                btnSaveWorksheet.Style.Add("display", "none");
            }
        }


        private void SelectCPTCodes()
        {

            List<WorksheetPatient> worksheetPatients = ViewState["PatientsList"] as List<WorksheetPatient>;
            int selectedPatientLogPatientId = (int)gvPatients.DataKeys[(int)ViewState["selectedPatientLogPatientIndex"]].Values["Id"];
            WorksheetPatient worksheetPatient = worksheetPatients.Find(delegate(WorksheetPatient worksheetPatientParam) { return worksheetPatientParam.Id == selectedPatientLogPatientId; });

            ListItem liCPTGlaucoma, liCPTRetina;
            int numOfGlaucomaSelected = 0;
            int numOfRetinaSelected = 0;
            foreach (CPTCode CPT_Code in worksheetPatient.CPT_CodesList)
            {
                if (!CPT_Code.ActionType.Equals(ActionTypeEnum.Delete))
                {
                    liCPTGlaucoma = cblCPTCodesGlaucoma.Items.FindByValue(CPT_Code.Id.ToString());
                    if (liCPTGlaucoma != null)
                    {
                        liCPTGlaucoma.Selected = true;
                        numOfGlaucomaSelected++;
                    }

                    liCPTRetina = cblCPTCodesRetina.Items.FindByValue(CPT_Code.Id.ToString());
                    if (liCPTRetina != null)
                    {
                        liCPTRetina.Selected = true;
                        numOfRetinaSelected++;

                    }
                }
               
            }

            if (numOfGlaucomaSelected > 0 && numOfRetinaSelected > 0)
                //trCTPColor.Style.Add("display", "");
                tbpCPTCode.Style.Add("display", "none");
            else
                tbpCPTCode.Style.Add("display", "none");
        }

       

        #endregion //Private methods

        #region MICROSOFT REPORTING

        private void PrintReport(ExportType exportType)
        {

            TotalVisionReport _totalVisionReport = new TotalVisionReport();

            List<WorksheetPatient> patientsList = ViewState["PatientsList"] as List<WorksheetPatient>;



            LocalReport localReport = new LocalReport();

            localReport.ReportPath = Server.MapPath("~/Reports/CompletedWorksheet.rdlc");

            ReportDataSource reportDataSource = new ReportDataSource("WorksheetPatient", patientsList);
            localReport.DataSources.Add(reportDataSource);

            ReportParameter[] param = new ReportParameter[2];
            param[0] = new ReportParameter("rptParamPracticeName", lblPracticeNameData.Text);
            param[1] = new ReportParameter("rptParamDate", lblServiceDateData.Text);

            localReport.SetParameters(param.AsEnumerable<ReportParameter>());

            if (exportType == ExportType.Printer)
            {
                _totalVisionReport.ListItems = patientsList;
                _totalVisionReport.Pameters.Add("rptParamPracticeName", lblPracticeNameData.Text);
                _totalVisionReport.Pameters.Add("rptParamDate", lblServiceDateData.Text);
                Session["PrintingParams"] = _totalVisionReport;

                Response.Redirect("~/Reports/ReportPreview.aspx?ReportName=CompletedWorksheet");
            }
            else if (exportType == ExportType.Pdf)
            {
                DownloadPDF(localReport);

            }
            else if (exportType == ExportType.Excel)
            {
                DownloadExcel(localReport);

            }
        }

        private void DownloadPDF(LocalReport localReport)
        {
            string reportType = "PDF";

            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PDF</OutputFormat>" +
            //"  <PageWidth>11in</PageWidth>" +
            //"  <PageHeight>8.5in</PageHeight>" +
            //"  <MarginTop>1in</MarginTop>" +
            //"  <MarginLeft>0.5in</MarginLeft>" +
            //"  <MarginRight>0.5in</MarginRight>" +
            //"  <MarginBottom>1in</MarginBottom>" +
            //"</DeviceInfo>";

            string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>PDF</OutputFormat>" +
        "  <PageWidth>11in</PageWidth>" +
        "  <PageHeight>8.5in</PageHeight>" +
        "  <MarginTop>0.2in</MarginTop>" +
        "  <MarginLeft>0.6in</MarginLeft>" +
        "  <MarginRight>0.1in</MarginRight>" +
        "  <MarginBottom>0.2in</MarginBottom>" +
        "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=ManageWorksheet." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }

        private void DownloadExcel(LocalReport localReport)
        {
            string reportType = "Excel";

            string mimeType;
            string encoding;
            string fileNameExtension;

            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>Excel</OutputFormat>" +
            "  <PageWidth>11in</PageWidth>" +
            "  <PageHeight>8.5in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.1in</MarginLeft>" +
            "  <MarginRight>0.1in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";



            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;



            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            //Clear the response stream and write the bytes to the outputstream
            //Set content-disposition to "attachment" so that user is prompted to take an action
            //on the file (open or save)
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=ManageWorksheet." + fileNameExtension);
            Response.BinaryWrite(renderedBytes);
            Response.End();
        }


        #endregion

       

       

      

      

       
      
       


    }
}
