﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework;

namespace CEI.Web.Worksheets
{
    public partial class ViewPendingPatientLogs : BasePage
    {

        #region GridView columns


        private const int GV_WORKSHEETS_PRACTICE_NAME = 0;
        private const int GV_WORKSHEETS_DATE = 1;
        private const int GV_WORKSHEETS_ASSIGN_CPT = 2;
        private const int GV_WORKSHEETS_ASSIGN_TECHNICIAN = 3;
        private const int GV_WORKSHEETS_EDIT = 4;
        private const int GV_WORKSHEETS_SUBMIT = 5;
      

        #endregion //GridView columns


        private DataTable _allActiveTechnicians;

        public DataTable AllActiveTechnicians
        {
            get { return _allActiveTechnicians; }
           
        }


        #region Event handlers


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {



                BindListControl<DataTable>.BindAndAddListItem(ddlPracticesSearch, new UserBR().GetAllPracticesNameAndId(), "name", "id", "All", Guid.Empty.ToString());


                ViewState["practiceId"] = ddlPracticesSearch.SelectedValue;

                if (Roles.IsUserInRole(RoleEnum.Technician.ToString()))
                    ViewState["technicianId"] = new Guid(User.Identity.Name);
                else
                    ViewState["technicianId"] = Guid.Empty;


                this.BindGridViewWithSearchResults();
            }
        }


        protected void gvWorksheets_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvWorksheets.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvWorksheets_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvWorksheets.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvWorksheets.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvWorksheets.PageIndex == (gvWorksheets.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }

        protected void gvWorksheets_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            

            if (e.CommandName.Equals("assigncpt"))
            {
                Response.Redirect("~/Worksheets/ManageWorksheet.aspx?logId=" + gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["Id"].ToString() + "&prName=" + gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["PracticeName"].ToString() + "&servDate=" + gvWorksheets.Rows[int.Parse(e.CommandArgument.ToString())].Cells[GV_WORKSHEETS_DATE].Text, true);


            }

            if (e.CommandName.Equals("submit"))
            {
                int patientLogId = (int)gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["Id"];
                short numOfPatients = (short)gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["NumOfPatients"];
                if (numOfPatients == new WorksheetBR().GetCountOfPatientsWithAssignedCPTCodesByPatientLogId(patientLogId))
                {
                    Worksheet worksheet = new Worksheet();
                    worksheet.Id = patientLogId;
                    worksheet.Status = PatientLogStatusEnum.Worksheet;
                    ListItem selectedTechnician = (gvWorksheets.Rows[int.Parse(e.CommandArgument.ToString())].Cells[GV_WORKSHEETS_ASSIGN_TECHNICIAN].FindControl("ddlTechnicians") as DropDownList).SelectedItem;
                    //selectedTechnician.Value == id#email
                    string[] idAndEmail = selectedTechnician.Value.Split('#');
                    worksheet.TechnicianId = new Guid(idAndEmail[0]);

                    if (new WorksheetBR().UpdateWorksheet(worksheet))
                    {
                        //send mail
                        string practiceName = gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["PracticeName"].ToString();
                        string serviceDate = gvWorksheets.Rows[int.Parse(e.CommandArgument.ToString())].Cells[GV_WORKSHEETS_DATE].Text;
                        new WorksheetBR().SendMailWorksheetSubmit(Session["loggedUserFullName"] != null ? Session["loggedUserFullName"].ToString() : string.Empty, Session["loggedUserEmail"] != null ? Session["loggedUserEmail"].ToString() : string.Empty, "New worksheet is created", serviceDate, practiceName, selectedTechnician.Text, idAndEmail[1], ResolveUrl("~/Worksheets/ViewCompletedWorksheet.aspx"),patientLogId.ToString());
                        this.BindGridViewWithSearchResults();
                        ScriptManager.RegisterClientScriptBlock(this, typeof(ViewPendingPatientLogs), "SubmitWorksheetScript", "alert('Worksheet is successfully submitted');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(ViewPendingPatientLogs), "SubmitWorksheetErrorScript", "alert('Worksheet is not submitted some error occurs.Please try again');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(ViewPendingPatientLogs), "NotAllPatientsHaveCPTCodesScript", "alert('You cannot submit this worksheet because there are patients without CPT codes in it');", true);
                }
            
            }

            if (e.CommandName.Equals("edit"))
            {
                int patientLogId = (int)gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["Id"];
                Response.Redirect("~/PatientLogs/ManagePatientLog.aspx?action=update&prId=" + gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["PracticeId"].ToString() + "&prName=" + gvWorksheets.DataKeys[int.Parse(e.CommandArgument.ToString())].Values["PracticeName"].ToString() + "&servDate=" + gvWorksheets.Rows[int.Parse(e.CommandArgument.ToString())].Cells[GV_WORKSHEETS_DATE].Text + "&logId=" + patientLogId.ToString(), true);
            }
        }


        protected void gvWorksheets_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvWorksheets.PageIndex = e.NewPageIndex;
            gvWorksheets.DataSource = odsWorksheets;
            gvWorksheets.DataBind();

        }

        protected void odsWorksheets_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["technicianId"] = ViewState["technicianId"];
            e.InputParameters["practiceId"] = ViewState["practiceId"];
            e.InputParameters["status"] = PatientLogStatusEnum.Pending__patient__log;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

           
            ViewState["practiceId"] = ddlPracticesSearch.SelectedValue;

            this.BindGridViewWithSearchResults();
        }

        protected void gvWorksheets_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if(e.Row.RowIndex == 0) //fill AllActiveTechnicians property
                {
                    this._allActiveTechnicians = new UserBR().GetAllTechnicians();
                }
                DropDownList ddlTechnicians = e.Row.Cells[GV_WORKSHEETS_ASSIGN_TECHNICIAN].FindControl("ddlTechnicians") as DropDownList;
                BindListControl<DataTable>.Bind(ddlTechnicians, this.AllActiveTechnicians, "fullName", "idAndEmail");  //idAndEmail id and email separated with #
              
            }
        }



        #endregion //Event handlers

        #region Private methods

        private void BindGridViewWithSearchResults()
        {
            gvWorksheets.PageIndex = 0;
            gvWorksheets.DataSource = odsWorksheets;
            gvWorksheets.DataBind();
        }

        #endregion //Private methods

   
    }
}
