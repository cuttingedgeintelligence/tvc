﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ViewPendingPatientLogs.aspx.cs" Inherits="CEI.Web.Worksheets.ViewPendingPatientLogs"
    Title="Pending Worksheets" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Pending Worksheets" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <div style="vertical-align: bottom; margin-bottom: 10px; overflow: hidden;">
        <fieldset class="fieldsetLeft">
            <legend>
                <asp:Label ID="lblSearchHeader" runat="server" Text="Search Pending Worksheets"></asp:Label></legend>
            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <table class="style1">
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPracticeSearch" runat="server" Text="Practice: "></asp:Label>
                        </td>
                        <td style="padding-left: 5px">
                            <asp:DropDownList ID="ddlPracticesSearch" runat="server" Style="margin: 5px 0;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td style="padding-top: 10px; padding-left: 5px">
                            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Search"
                                OnClientClick="__defaultFired = false;" UseSubmitBehavior="False" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </fieldset>
    </div>
    <asp:UpdatePanel ID="upWorksheets" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:GridView ID="gvWorksheets" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                DataKeyNames="Id,PracticeId,NumOfPatients,PracticeName" OnPageIndexChanging="gvWorksheets_PageIndexChanging"
                SkinID="gridViewAdmin" Width="920px" Style="clear: both;" EmptyDataText="There are no pending worksheets in the system "
                OnRowCommand="gvWorksheets_RowCommand" 
                OnRowDataBound="gvWorksheets_RowDataBound" ondatabound="gvWorksheets_DataBound" 
                onrowcreated="gvWorksheets_RowCreated">
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="PracticeId" HeaderStyle-Width="220px"
                        DataNavigateUrlFormatString="~/ViewPracticeProfile/ViewPracticeProfile.aspx?id={0}"
                        DataTextField="practiceName" HeaderText="Practice Name">
                        <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:HyperLinkField>
                    <asp:BoundField DataField="serviceDate" HeaderText="Service Date" HeaderStyle-Width="100px"
                        DataFormatString="{0:MM/dd/yyyy}">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:ButtonField ButtonType="Button" CommandName="assigncpt" HeaderText="Assign CPT"
                        HeaderStyle-Width="100px" ShowHeader="True" Text="Assign CPT">
                        <ControlStyle CssClass="btnClass" />
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                    <asp:TemplateField HeaderText="Assign to Technician">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlTechnicians" runat="server">
                            </asp:DropDownList>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:ButtonField ButtonType="Button" CommandName="edit" HeaderText="Edit" ShowHeader="True"
                        Text="Edit">
                        <ControlStyle CssClass="btnClass" />
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                    <asp:ButtonField ButtonType="Button" CommandName="submit" HeaderText="Submit" ShowHeader="True"
                        Text="Submit">
                        <ControlStyle CssClass="btnClass" />
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                </Columns>
                     <PagerTemplate>
                    <div style="padding: 5px;">
                        <div>
                            <asp:Button CommandArgument="Prev" CommandName="Page" Text="Previous" ID="btnPrevious"
                                runat="server" meta:resourcekey="ibPrevResource1" />&nbsp;
                            <asp:Button CommandArgument="Next" CommandName="Page" Text="Next" ID="btnNext" runat="server"
                                meta:resourcekey="ibNextResource1" />
                        </div>
                        <span id="spannumeric" runat="server" class="spannumeric"></span>
                    </div>
                </PagerTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsWorksheets" runat="server" EnablePaging="True" MaximumRowsParameterName="pageSize"
                OnSelecting="odsWorksheets_Selecting" SelectCountMethod="GetWorksheetsSearchTotalCount"
                SelectMethod="GetWorksheetsSearch" TypeName="CEI.BR.WorksheetBR">
                <SelectParameters>
                    <asp:Parameter Name="technicianId" />
                    <asp:Parameter Name="practiceId" />
                    <asp:Parameter Name="status" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
