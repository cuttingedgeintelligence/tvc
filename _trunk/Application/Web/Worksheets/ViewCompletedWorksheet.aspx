﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="True"
    CodeBehind="ViewCompletedWorksheet.aspx.cs" Inherits="CEI.Web.Worksheets.ViewCompletedWorksheet"
    Title="View Worksheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControls/DayMonthYear.ascx" TagName="DayMonthYear" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script runat="server">
        int TotalRows = 0;
        string GetTotalRows(string Price)
        {
            TotalRows += 1;
            return Price;
        }
        int GetTotal()
        {
            return TotalRows;
        }

        int TotalBFA = 0;
        string GetTotalBFA(string BFA)
        {
            if (BFA != "False")
            {
                TotalBFA += 1;
            }
            return BFA;
        }
        int GetBFA()
        {
            return TotalBFA;
        }

        int TotalHRTR = 0;
        string GetTotalHRTR(string HRTR)
        {
            if (HRTR != "False")
            {
                TotalHRTR += 1;
            }
            return HRTR;
        }
        int GetHRTR()
        {
            return TotalHRTR;
        }
    
        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="View Worksheet" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <table width="400px">
        <tr>
            <td width="100px">
                <asp:Label ID="lblPracticeName" runat="server" Text="Practice Name:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblPracticeNameData" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100px">
                <asp:Label ID="lblServiceDate" runat="server" Text="Service Date:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblServiceDateData" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="padding: 10px 0;">
        <tr>
            <td style="padding-top: 10px">
                <asp:ImageButton ID="btnPrintPDF" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png"
                    ToolTip="Download PDF" OnClick="btnPrintPDF_Click" Style="padding-right: 3px;" />
            </td>
            <td style="padding-top: 10px; padding-left: 5px">
                <asp:ImageButton ID="btnPrintExcel" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png"
                    ToolTip="Download Excel" OnClick="btnPrintExcel_Click" />
            </td>
            <td style="padding-top: 10px; padding-left: 5px">
                <asp:ImageButton ID="btnPrintPreview" runat="server" ImageUrl="~/Images/Mockups/printIcon.png"
                    ToolTip="View and Print" OnClick="btnPrintPreview_Click" Style="display: none;" />
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upPatients" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvPatients" ShowFooter="true" runat="server" AutoGenerateColumns="False"
                DataKeyNames="Id,PatientId" SkinID="gridViewAdmin" Width="920px" Style="clear: both;"
                OnRowDataBound="gvPatients_RowDataBound" EmptyDataText="There are no patients in the final notes worksheet"
                OnRowCreated="gvPatients_RowCreated">
                <Columns>
                    <%-- <asp:HyperLinkField DataNavigateUrlFields="PatientId" 
                                    DataNavigateUrlFormatString="~/ViewPatientProfile/ViewPatientProfile.aspx?id={0}" 
                                    DataTextField="fullName" HeaderText="Patient Name" >
                                    <HeaderStyle HorizontalAlign="Left" CssClass="moveLeft" />
                                    <ItemStyle HorizontalAlign="Left"  />
                                </asp:HyperLinkField>--%>
                    <asp:TemplateField HeaderText="Patient Name">
                        <ItemTemplate>
                            <asp:HyperLink ID="hpPatinetName" runat="server" Text='<%# GetTotalRows(Eval("fullName").ToString()) %>'
                                NavigateUrl='<%# "~/ViewPatientProfile/ViewPatientProfile.aspx?id="+ Eval("PatientId") %>'></asp:HyperLink>
                        </ItemTemplate>
                        <FooterTemplate>
                            <table style="padding-left: 2px;" width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 125px;" class="footerRow">
                                        <asp:Label ID="lblPatientCount" runat="server" Text="TOTAL EXAMS: "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPatientCount1" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                <td colspan="2" style="border-bottom:1px solid #C2C4D3;"></td>
                                </tr>
                                <tr>
                                    <td class="footerRow">
                                        <asp:Label ID="Label2" runat="server" Text="TOTAL PATIENTS: "></asp:Label>
                                    </td>
                                    <td>
                                        <%# GetTotal() %>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                        <HeaderStyle HorizontalAlign="Center"/>
                        <ItemStyle HorizontalAlign="Left" />
                        <FooterStyle HorizontalAlign="Left" Font-Bold="true" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="BFA">
                        <ItemTemplate>
                            <asp:Label ID="lblBFA" Visible="false" runat="server" Text='<%# GetTotalBFA( Eval("Glaucoma").ToString())%>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCountBFA" Visible="false" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="Label1" runat="server" Text='<%# GetBFA() %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRT-G">
                        <ItemTemplate>
                        </ItemTemplate>
                        <FooterTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCountHRTG" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="lblHRTG" runat="server" Text='<%# GetBFA() %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HRT-R">
                        <ItemTemplate>
                            <asp:Label ID="lblhrtr" Visible="false" runat="server" Text='<%# GetTotalHRTR( Eval("Retina").ToString())%>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCountHRTR" runat="server" Text="" Visible="false"></asp:Label>
                                        <asp:Label ID="Label11" runat="server" Text='<%# GetHRTR() %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CPT Codes">
                        <ItemTemplate>
                            <table style="width: 315px;">
                                <tr>
                                    <td align="center" style="vertical-align: top; text-align: center;">
                                        <table style="padding-right: 5px; width: 150px;">
                                            <tr>
                                                <td style="text-align: center; width: 90px; font-weight: bold;">
                                                    <asp:Label ID="lblGlaucoma" runat="server" Text="Glaucoma"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblGlaucomaCodes" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="center" style="vertical-align: top; text-align: center; border-left: 1px solid #c2c4d3;">
                                        <table style="padding-left: 5px; width: 150px;">
                                            <tr>
                                                <td style="text-align: center; width: 90px; font-weight: bold;">
                                                    <asp:Label ID="lblRetina" runat="server" Text="Retina"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblRetinaCodes" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Width="315px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="insuranceCompanyName" HeaderText="Insurance">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <%-- <asp:BoundField HeaderText="Practice Notes">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>--%>
                </Columns>
            </asp:GridView>
            <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="false" PostBackUrl="~/Worksheets/ViewCompletedWorksheets.aspx" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
