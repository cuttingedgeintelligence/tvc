﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true"
    CodeBehind="StatsSheets.aspx.cs" Inherits="CEI.Web.MockUps.StatsSheets" Title="Stats Sheets" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>
       
        <table class="gridView" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_gvUsers"
            style="width: 920px; border-collapse: collapse; clear: both;">
            <tr class="gridViewHeader">
                <th scope="col">
                    Practice
                    Name
                </th>
                <th align="center" scope="col">
                    Date
                </th>
                <th scope="col">
                    Stats
                    Sheet
                </th>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Dr Josh
                </td>
                <td>
                    19-Feb
                </td>
                <td align="center">
                     <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png" 
                        ToolTip="Export to PDF" />
                    <asp:Image ID="Image10" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png" 
                        ToolTip="Export to Excel" />
                    <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/Mockups/printIcon.png" 
                        ToolTip="Print" />
                    <asp:Image ID="Image12" runat="server" ImageUrl="~/Images/Mockups/mailIcon.png" 
                        ToolTip="Send Email" /></td>
            </tr>
            <tr class="gridViewAlternatingRow">
                <td>
                    Dr. Smith
                </td>
                <td>
                    19-Feb</td>
                <td align="center">
                       <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png" 
                        ToolTip="Export to PDF" />
                    <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png" 
                        ToolTip="Export to Excel" />
                    <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Mockups/printIcon.png" 
                        ToolTip="Print" />
                    <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/Mockups/mailIcon.png" 
                        ToolTip="Send Email" /></td>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Dr. Bosh
                </td>
                <td>
                    19-Feb</td>
                <td align="center">
                     <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Mockups/pdfIcon.png" 
                        ToolTip="Export to PDF" />
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Mockups/exelIcon.png" 
                        ToolTip="Export to Excel" />
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Mockups/printIcon.png" 
                        ToolTip="Print" />
                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Mockups/mailIcon.png" 
                        ToolTip="Send Email" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
