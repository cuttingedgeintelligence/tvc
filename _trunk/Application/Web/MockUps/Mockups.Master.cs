﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace CEI.Web.MockUps
{
    public partial class Mockups : System.Web.UI.MasterPage
    {

        public string MenuScript
        {
            get
            {
                return "http://" + Request.Url.Host + ResolveUrl("~/MenuJquery/ddsmoothmenu.js");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            string initMenuScript = @"ddsmoothmenu.init({
            	mainmenuid: '" + menuAdmin.ClientID + @"' , 
            	
            	contentsource: 'markup' 
            });";



            ScriptManager.RegisterStartupScript(this.Page, typeof(string), "InitMenuScript", initMenuScript, true);
            
        }

        public string JqueryString
        {
            get
            {
                return "http://" + Request.Url.Host + ResolveUrl("~/js/jquery-1.3.2.min.js");
            }
        }
    }
}
