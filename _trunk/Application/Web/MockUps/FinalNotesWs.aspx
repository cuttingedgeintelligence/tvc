﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true" CodeBehind="FinalNotesWs.aspx.cs" Inherits="CEI.Web.MockUps.FinalNotesWs" Title="Final Notes Worksheets" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <b style="display:block; margin-bottom:10px;">
        Search Worksheets</b>

    <table border="“3”" style="border: 1px solid #CDCFCF; padding: 10px; margin-top: -11px;">
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Doctor: "></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownList6" runat="server">
                    <asp:ListItem Text="Dr. Pantevski"></asp:ListItem>
                    <asp:ListItem Text="Dr. Johnson"></asp:ListItem>
                    <asp:ListItem Text="Dr. Jules"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:Button ID="Button10" runat="server" Text="Search" />
            </td>
        </tr>
    
        </td></tr>
    </table> <div>
        <table class="gridView" cellspacing="0" border="0" id="Table1" style="width: 920px;
            border-collapse: collapse; clear: both;">
            <tr class="gridViewHeader">
                <th scope="col">
                    Practice Name
                </th>
                <th align="center" scope="col">
                    Date
                </th>
           
                <th scope="col">
                    Notes
                </th>
                <th align="center" scope="col">
                    Submit
                </th>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Dr. Marina
                </td>
                <td>
                    01/02/2009
                </td>
        
                <td>
                    <asp:Button ID="Button13" runat="server" Text="Insert"  PostBackUrl="AddNote.aspx"/>
                </td>
                <td class="tdActions" align="center">
                   <asp:Button ID="Button6" runat="server" Text="Submit" />
                </td>
            
            </tr>
            <tr class="gridViewAlternatingRow">
                <td>
                    Dr. Mirko
                </td>
                <td>
                    01/02/2009
                </td>
         
                <td>
                    <asp:Button ID="Button3" runat="server" Text="Insert"  PostBackUrl="AddNote.aspx"/>
                </td>
                <td class="tdActions" align="center">
               <asp:Button ID="Button7" runat="server" Text="Submit" />
                </td>
                
            </tr>
            <tr class="gridViewRow">
                <td>
                    Dr. Arne
                </td>
                <td>
                    01/02/2009
                </td>
          
                <td>
                    <asp:Button ID="Button4" runat="server" Text="Insert"  PostBackUrl="AddNote.aspx"/>
                </td>
                <td class="tdActions" align="center">
               <asp:Button ID="Button8" runat="server" Text="Submit" />
                </td>
              
            </tr>
            <tr class="gridViewAlternatingRow">
                <td>
                    Dr. Zharko
                </td>
                <td>
                    01/02/2009
                </td>
           
                <td>
                    <asp:Button ID="Button5" runat="server" Text="Insert"  PostBackUrl="AddNote.aspx"/>
                </td>
                <td class="tdActions" align="center">
                <asp:Button ID="Button1" runat="server" Text="Submit" />
                </td>
           
            </tr>
            <tr class="gridViewRow">
                <td>
                    Dr. New
                </td>
                <td>
                    01/02/2009
                </td>
          
                <td>
                    <asp:Button ID="Button26" runat="server" Text="Insert"  PostBackUrl="AddNote.aspx"/>
                </td>
                <td class="tdActions" align="center">
            <asp:Button ID="Button9" runat="server" Text="Submit" />
                </td>
             
            </tr>
        </table>
    </div>

</asp:Content>
