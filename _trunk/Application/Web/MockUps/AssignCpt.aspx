﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true"
    CodeBehind="AssignCpt.aspx.cs" Inherits="CEI.Web.MockUps.AssignCpt" Title="Assign CPT Codes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panelHolder" style="display: none;">
        <table>
            <tr>
                <td align="left">
                </td>
                <td valign="top">
                    <asp:Label ID="Label1" runat="server" Text="Select CPT Code:"></asp:Label><br />
                    <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                        <asp:ListItem>91240</asp:ListItem>
                        <asp:ListItem>9548</asp:ListItem>
                        <asp:ListItem>5487</asp:ListItem>
                        <asp:ListItem>87564</asp:ListItem>
                        <asp:ListItem>57684</asp:ListItem>
                        <asp:ListItem>35461</asp:ListItem>
                        <asp:ListItem>53464</asp:ListItem>
                        <asp:ListItem>5986</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/App_Themes/Default/icons/save.png"
                        ValidationGroup="vgRole" OnClientClick=" $('.panelHolder').slideUp('slow');return false;"
                        ToolTip="save" />&nbsp;
                    <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" ImageUrl="~/App_Themes/Default/icons/back.png"
                        OnClientClick=" $('.panelHolder').slideUp('slow');return false;" ToolTip="cancel"
                        BackColor="" />
                </td>
            </tr>
        </table>
    </div>
    
    <asp:Label ID="Label2" runat="server" Text="Practice name:"></asp:Label> <asp:Label ID="Label3"
        runat="server" Font-Bold=true Text="Dr. Pantevski"></asp:Label> <asp:Label ID="Label4" runat="server"
            Text="Date:"> </asp:Label> <asp:Label ID="Label5" runat="server" Text="19-Feb-2009" Font-Bold=true></asp:Label>
    <table class="gridView" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_gvUsers"
        style="width: 920px; border-collapse: collapse; clear: both;">
        <tr class="gridViewHeader">
            <th scope="col">
                Patient Name
            </th>
            <th scope="col">
                Manifest refraction</th>
            <th scope="col">
                Pachymetry</th>
            <th scope="col">
                BFA
            </th>
            <th scope="col">
                HRT Glaucoma
            </th>
            <th scope="col">
                HRT Retina
            </th>
            <th align="center" scope="col">
                CPT
            </th>
            <th scope="col">
                Assign CPT
            </th>
        </tr>
        <tr class="gridViewRow">
            <td>
                Marina
            </td>
            <td>
                OD: sph -2.0<br />
                OS: sph -2.5</td>
            <td>
                &nbsp;</td>
            <td>
                <span id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_lblRoles">
                    <input type="image" id="Image1" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
                </span>
            </td>
            <td>
                <input type="image" id="Image2" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
                <input type="image" id="Image13" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td class="tdActions" align="center">
                32450; 87654; 654898
            </td>
            <td class="tdActions">
                <asp:Button ID="Button3" runat="server" Text="Assign CPT" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
        </tr>
        <tr class="gridViewAlternatingRow">
            <td>
                Mirko
            </td>
            <td>
                OD: cyl -3.0 x 120<br />
                OS: cyl -3.5 x 120</td>
            <td>
                OD: -3.0<br />
                OS:&nbsp; -3.5</td>
            <td>
            </td>
            <td>
                <input type="image" id="Image4" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
            </td>
            <td class="tdActions" align="center">
                92140
            </td>
            <td class="tdActions">
                <asp:Button ID="Button1" runat="server" Text="Assign CPT" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
        </tr>
        <tr class="gridViewRow">
            <td>
                Arne
            </td>
            <td>
                OD: sph -2.0<br />
                OS: sph -2.5</td>
            <td>
                &nbsp;</td>
            <td>
                <span id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_lblRoles">
                    <input type="image" id="Image5" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" /></span>
            </td>
            <td>
                <input type="image" id="Image6" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
                <input type="image" id="Image3" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td class="tdActions" align="center">
                93458; 87654;
            </td>
            <td class="tdActions">
                <asp:Button ID="Button2" runat="server" Text="Assign CPT" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
        </tr>
        <tr class="gridViewAlternatingRow">
            <td>
                Zharko
            </td>
            <td>
                OD: cyl -3.0 x 120<br />
                OS: cyl -3.5 x 120</td>
            <td>
                &nbsp;</td>
            <td>
            </td>
            <td>
                <input type="image" id="Image7" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
            </td>
            <td class="tdActions" align="center">
            </td>
            <td class="tdActions">
                <asp:Button ID="Button4" runat="server" Text="Assign CPT" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
        </tr>
        <tr class="gridViewRow">
            <td>
                New
            </td>
            <td>
                OD: sph -2.0<br />
                OS: sph -2.5</td>
            <td>
                &nbsp;</td>
            <td>
            </td>
            <td>
            </td>
            <td>
                <input type="image" id="Image8" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td class="tdActions" align="center">
            </td>
            <td class="tdActions">
                <asp:Button ID="Button5" runat="server" Text="Assign CPT" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
        </tr>
    </table>
    <div style="float: right; padding: 10px; margin-left: 10px;">
        <input type="image" id="Image11" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
        <input type="image" id="Image12" src="../App_Themes/Default/icons/back.png" style="border-width: 0px;" />
    </div>
</asp:Content>
