﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true" CodeBehind="InsCo.aspx.cs" Inherits="CEI.Web.MockUps.InsCo" Title="Manage Insurance Companies" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<div><asp:Button ID="Button10" runat="server" Text="Create Insurance Company" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
     <div class="panelHolder" style="display:none;">
       
                <table>
                    
                    <tr>
                        <td valign="top">
                            <asp:Label ID="Label1" runat="server" Text="Insurance Company:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtNote" runat="server" MaxLength="50"></asp:TextBox>
                           
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2"><br />
                            <asp:ImageButton ID="btnSave" runat="server"  ImageUrl="~/App_Themes/Default/icons/save.png"
                                ValidationGroup="vgRole"  OnClientClick=" $('.panelHolder').slideUp('slow');return false;"
                                ToolTip="save" />&nbsp;
                            <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" ImageUrl="~/App_Themes/Default/icons/back.png"
                                 OnClientClick=" $('.panelHolder').slideUp('slow');return false;"
                                ToolTip="cancel" BackColor="" />
                               
                        </td>
                    </tr>
                </table>
            
    </div>
	<table class="gridView" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_gvUsers" style="width:920px;border-collapse:collapse;clear:both;">
			<tr class="gridViewHeader">
				<th scope="col">Insurance Company</th><th align="center" scope="col">Edit</th><th scope="col">Delete</th>

			</tr><tr class="gridViewRow">
				<td>BC PPO</td>
						<td class="tdActions" align="center">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgbtnEdit" src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');" style="border-width:0px;" />
						</td><td class="tdActions">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgBtnDelete" src="../App_Themes/Default/icons/delete.gif" style="border-width:0px;" />
							
						</td>

			</tr><tr class="gridViewAlternatingRow">
				<td>VSP/PEC</td>
						<td class="tdActions" align="center">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgbtnEdit" src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');" style="border-width:0px;" />
						</td><td class="tdActions">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgBtnDelete" src="../App_Themes/Default/icons/delete.gif" style="border-width:0px;" />
							
						</td>

			</tr><tr class="gridViewRow">
				<td>BC PPO</td>
						<td class="tdActions" align="center">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgbtnEdit" src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');" style="border-width:0px;" />
						</td><td class="tdActions">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgBtnDelete" src="../App_Themes/Default/icons/delete.gif" style="border-width:0px;" />
							
						</td>

			</tr><tr class="gridViewAlternatingRow">
				<td>BC PPO</td>
						<td class="tdActions" align="center">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgbtnEdit" src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');" style="border-width:0px;" />
						</td><td class="tdActions">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgBtnDelete" src="../App_Themes/Default/icons/delete.gif" style="border-width:0px;" />
							
						</td>

			</tr><tr class="gridViewRow">
				<td>VSP/PEC</td>
						<td class="tdActions" align="center">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgbtnEdit" src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');" style="border-width:0px;" />
						</td><td class="tdActions">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgBtnDelete" src="../App_Themes/Default/icons/delete.gif" style="border-width:0px;" />
							
						</td>

			</tr>
		</table>
	</div>


</asp:Content>
