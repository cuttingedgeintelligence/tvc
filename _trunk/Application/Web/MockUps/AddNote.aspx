﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true"
    CodeBehind="AddNote.aspx.cs" Inherits="CEI.Web.MockUps.AddNote" Title="Insert Final Notes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panelHolder" style="display: none;">
        <table>
            <tr>
                <td align="left">
                </td>
                <td valign="top">
                    <asp:Label ID="Label1" runat="server" Text="Select Predifined Note:"></asp:Label><br />
                    <asp:DropDownList ID="DropDownList5" runat="server">
                        <asp:ListItem Text="Predifined Note 1"></asp:ListItem>
                        <asp:ListItem Text="Predifined Note 2"></asp:ListItem>
                        <asp:ListItem Text="Predifined Note 3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left">
                </td>
                <td valign="top">
                    <asp:Label ID="Label2" runat="server" Text="Write Custom Note:"></asp:Label><br />
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/App_Themes/Default/icons/save.png"
                        ValidationGroup="vgRole" OnClientClick=" $('.panelHolder').slideUp('slow');return false;"
                        ToolTip="save" />&nbsp;
                    <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" ImageUrl="~/App_Themes/Default/icons/back.png"
                        OnClientClick=" $('.panelHolder').slideUp('slow');return false;" ToolTip="cancel"
                        BackColor="" />
                </td>
            </tr>
        </table>
    </div>
       <asp:Label ID="Label3" runat="server" Text="Practice name:"></asp:Label> <asp:Label ID="Label4"
        runat="server" Font-Bold=true Text="Dr. Pantevski"></asp:Label> <asp:Label ID="Label5" runat="server"
            Text="Date:"> </asp:Label> <asp:Label ID="Label6" runat="server" Text="19-Feb-2009" Font-Bold=true></asp:Label>
    <table class="gridView" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_gvUsers"
        style="width: 920px; border-collapse: collapse; clear: both;">
        <tr class="gridViewHeader">
            <th scope="col">
                Patient Name
            </th>
            <th scope="col">
                BFA
            </th>
            <th scope="col">
                HRT Glaucoma
            </th>
            <th scope="col">
                HRT Retina
            </th>
            <th align="center" scope="col">
                CPT
            </th>
            <th align="center" scope="col">
                Status
            </th>
            <th align="center" scope="col">
                Internal<br />
                Notes
            </th>
            <th scope="col">
                Doctor<br />
                Notes
            </th>
            <th scope="col">
                OD: 
                <br />
                IOP/BFA
            </th>
            <th scope="col">
                OS: 
                <br />
                IOP/BFA
            </th>
        </tr>
        <tr class="gridViewRow">
            <td>
                Marina
            </td>
            <td>
                <span id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_lblRoles">
                    <input type="image" id="Image1" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
                </span>
            </td>
            <td align="center">
                <input type="image" id="Image2" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td align="center">
                <input type="image" id="Image13" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td class="tdActions" align="center">
                32450; 87654; 654898
            </td>
            <td class="tdActions" align="center">
                <asp:DropDownList ID="DropDownList6" runat="server">
                    <asp:ListItem Text="Examined"></asp:ListItem>
                    <asp:ListItem Text="No Show"></asp:ListItem>
                    <asp:ListItem Text="Canceled"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Here 
                <br />
                                comes 
                <br />
                                internal 
                <br />
                notes
                <br />
                <asp:Button ID="Button3" runat="server" Text="Insert" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
            <td class="tdActions">
                Here comes doctor notes<br />
                <asp:Button ID="Button6" runat="server" Text="Insert" 
                    OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
            <td align="center">
                <asp:TextBox ID="TextBox2" runat="server"  Width="35px"></asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="TextBox3" runat="server" Width="35px"></asp:TextBox>
            </td>
        </tr>
        <tr class="gridViewAlternatingRow">
            <td>
                Mirko
            </td>
            <td>
            </td>
            <td align="center">
                <input type="image" id="Image4" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
            </td>
            <td class="tdActions" align="center">
                92140
            </td>
            <td class="tdActions" align="center">
                <asp:DropDownList ID="DropDownList1" runat="server">
                    <asp:ListItem Text="Examined"></asp:ListItem>
                    <asp:ListItem Text="No Show"></asp:ListItem>
                    <asp:ListItem Text="Canceled"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
               Here 
                <br />
                                comes 
                <br />
                                internal 
                <br />
                notes 
                <br />
                <asp:Button ID="Button1" runat="server" Text="Insert" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
            <td class="tdActions">
                Here comes doctor notes<br />
                <asp:Button ID="Button7" runat="server" Text="Insert" 
                    OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
            <td align="center">
                <asp:TextBox ID="TextBox4" runat="server"  Width="35px"></asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="TextBox5" runat="server" Width="30px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="gridViewRow">
            <td>
                Arne
            </td>
            <td align="center">
                <span id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_lblRoles">
                    <input type="image" id="Image5" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" /></span>
            </td>
            <td align="center">
                <input type="image" id="Image6" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td align="center">
                <input type="image" id="Image3" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td class="tdActions" align="center">
                93458; 87654;
            </td>
            <td class="tdActions" align="center">
                <asp:DropDownList ID="DropDownList2" runat="server">
                    <asp:ListItem Text="Examined"></asp:ListItem>
                    <asp:ListItem Text="No Show"></asp:ListItem>
                    <asp:ListItem Text="Canceled"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
               Here 
                <br />
                                comes 
                <br />
                                internal 
                <br />
                notes 
                <br />
                <asp:Button ID="Button2" runat="server" Text="Insert" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
            <td class="tdActions">
                Here comes doctor notes<br />
                <asp:Button ID="Button8" runat="server" Text="Insert" 
                    OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
            <td align="center">
                <asp:TextBox ID="TextBox6" runat="server" Width="35px" ></asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="TextBox7" runat="server" Width="35px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="gridViewAlternatingRow">
            <td>
                Zharko
            </td>
            <td>
            </td>
            <td align="center">
                <input type="image" id="Image7" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
            </td>
            <td class="tdActions" align="center">
            </td>
            <td class="tdActions" align="center">
                <asp:DropDownList ID="DropDownList3" runat="server">
                    <asp:ListItem Text="Examined"></asp:ListItem>
                    <asp:ListItem Text="No Show"></asp:ListItem>
                    <asp:ListItem Text="Canceled"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Here 
                <br />
                                comes 
                <br />
                                internal 
                <br />
                notes 
                <br />
                <asp:Button ID="Button4" runat="server" Text="Insert" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
            <td class="tdActions">
                Here comes doctor notes<br />
                <asp:Button ID="Button9" runat="server" Text="Insert" 
                    OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
            <td align="center">
                <asp:TextBox ID="TextBox8" runat="server" Width="35px" ></asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="TextBox9" runat="server" Width="35px" ></asp:TextBox>
            </td>
        </tr>
        <tr class="gridViewRow">
            <td>
                New
            </td>
            <td>
            </td>
            <td>
            </td>
            <td align="center">
                <input type="image" id="Image8" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td class="tdActions" align="center">
            </td>
            <td class="tdActions" align="center">
                <asp:DropDownList ID="DropDownList4" runat="server">
                    <asp:ListItem Text="Examined"></asp:ListItem>
                    <asp:ListItem Text="No Show"></asp:ListItem>
                    <asp:ListItem Text="Canceled"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
               Here 
                <br />
                                comes<br />
                internal 
                <br />
                notes    <br />
                <asp:Button ID="Button5" runat="server" Text="Insert" OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
            <td class="tdActions">
                Here comes doctor notes<br />
                <asp:Button ID="Button10" runat="server" Text="Insert" 
                    OnClientClick=" $('.panelHolder').slideDown('slow');return false;" />
            </td>
            <td align="center">
                <asp:TextBox ID="TextBox10" runat="server" Width="35px" ></asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="TextBox11" runat="server" Width="35px" ></asp:TextBox>
            </td>
        </tr>
    </table>
    <div style="float: right; padding: 10px; margin-left: 10px;">
        <input type="image" id="Image11" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
        <input type="image" id="Image12" src="../App_Themes/Default/icons/back.png" style="border-width: 0px;" />
    </div>
</asp:Content>
