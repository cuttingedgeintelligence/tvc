﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true"
    CodeBehind="Logs.aspx.cs" Inherits="CEI.Web.MockUps.Logs" Title="Untitled Page" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>
       
        <table class="gridView" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_gvUsers"
            style="width: 920px; border-collapse: collapse; clear: both;">
            <tr class="gridViewHeader">
                <th scope="col">
                    Practice
                    Name
                </th>
                <th align="center" scope="col">
                    Date
                </th>
                <th scope="col">
                    Master Stats
                    File
                </th>
                <th scope="col">
                    Stats
                    Sheet
                </th>
                <th align="center" scope="col">
                    Report
                    Log
                </th>
                <th align="center" scope="col">
                Follow up</th>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Dr Josh
                </td>
                <td>
                    19-Feb
                </td>
                <td>
                   
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                   <asp:DropDownList ID="DropDownList6" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td  align="center">
                   <asp:DropDownList ID="DropDownList5" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td  align="center">
                   <asp:DropDownList ID="DropDownList4" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="gridViewAlternatingRow">
                <td>
                    Dr. Smith
                </td>
                <td>
                    19-Feb</td>
                <td>
                  <asp:DropDownList ID="DropDownList2" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                     <asp:DropDownList ID="DropDownList7" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td  align="center">
                    <asp:DropDownList ID="DropDownList8" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="center">
                     <asp:DropDownList ID="DropDownList9" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Dr. Bosh
                </td>
                <td>
                    19-Feb</td>
                <td>
                <asp:DropDownList ID="DropDownList3" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList10" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td  align="center">
                    <asp:DropDownList ID="DropDownList11" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td  align="center">
                     <asp:DropDownList ID="DropDownList12" runat="server">
                        <asp:ListItem>Select</asp:ListItem>
                        <asp:ListItem>PDF</asp:ListItem>
                        <asp:ListItem>Excel</asp:ListItem>
                        <asp:ListItem>Send Mail</asp:ListItem>
                        <asp:ListItem>Print</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
