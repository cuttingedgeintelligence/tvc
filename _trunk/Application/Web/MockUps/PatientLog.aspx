﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true"
    CodeBehind="PatientLog.aspx.cs" Inherits="CEI.Web.MockUps.PatientLog" Title="Patient's Logs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button ID="Button1" runat="server" Text="Create Schedule" PostBackUrl="CreateLog.aspx" />
    <div>
        <table class="gridView" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_gvUsers"
            style="width: 920px; border-collapse: collapse; clear: both;">
            <tr class="gridViewHeader">
                <th scope="col">
                    Practice Name
                </th>
                <th align="center" scope="col">
                    Date
                </th>
                
                <th scope="col">
                    Submit
                </th>
                <th align="center" scope="col">
                    Edit
                </th>
                <th scope="col">
                    Delete
                </th>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Dr. Marina
                </td>
                <td>
                    01/02/2009
                </td>
               
                <td>
                    <asp:Button ID="Button8" runat="server" Text="Submit" />
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewAlternatingRow">
                <td>
                    Dr. Mirko
                </td>
                <td>
                    01/02/2009
                </td>
              
                <td>
                    <asp:Button ID="Button9" runat="server" Text="Submit" />
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Dr. Arne
                </td>
                <td>
                    01/02/2009
                </td>
               
                <td>
                    <asp:Button ID="Button10" runat="server" Text="Submit" />
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewAlternatingRow">
                <td>
                    Dr. Zharko
                </td>
                <td>
                    01/02/2009
                </td>
               
                <td>
                    <asp:Button ID="Button11" runat="server" Text="Submit" />
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Dr. New
                </td>
                <td>
                    01/02/2009
                </td>
               
                <td>
                    <asp:Button ID="Button12" runat="server" Text="Submit" />
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
