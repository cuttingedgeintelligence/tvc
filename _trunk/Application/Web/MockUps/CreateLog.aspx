﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true"
    CodeBehind="CreateLog.aspx.cs" Inherits="CEI.Web.MockUps.CreateLog" Title="Create Patient's Schedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="border: 1px solid #000; float: left; padding: 10px;">
        <asp:Label Style="margin-bottom: 10px;">Practice Name:<asp:DropDownList ID="DropDownList1"
            runat="server">
            <asp:ListItem Text="Dr. Pantevski"></asp:ListItem>
            <asp:ListItem Text="Dr. Johnson"></asp:ListItem>
            <asp:ListItem Text="Dr. Jules"></asp:ListItem>
        </asp:DropDownList>
        </asp:Label><br />
        <asp:Label>Date:<input type="image" src="../App_Themes/Default/icons/calendar.png" /> </asp:Label>
    </div>
    <div style="border: 1px solid #000; float: left; padding: 10px; margin-left: 10px;">
        <asp:Label Style="display: block;">Patient:<asp:DropDownList ID="DropDownList2" runat="server">
        <asp:ListItem Text="John Locke"></asp:ListItem>
            <asp:ListItem Text="Jack Shephard"></asp:ListItem>
            <asp:ListItem Text="Kate Austen"></asp:ListItem>
        </asp:DropDownList>
        </asp:Label><br />
        <asp:Label Style="display: block;">Time:&nbsp;&nbsp;&nbsp;<input type="text" maxlength="2"
            style="width: 20px" />:
            <input type="text" style="width: 20px" maxlength="2" />&nbsp;<asp:DropDownList ID="DropDownList3"
                runat="server">
                <asp:ListItem Text="AM"></asp:ListItem>
                <asp:ListItem Text="PM"></asp:ListItem>
            </asp:DropDownList>
        </asp:Label>
        <br />
        <asp:Label Style="display: block;">Glaucoma:<input type="checkbox" style="" />Retina:<input type="checkbox" style=""  />&nbsp;</asp:Label><br />
        <asp:Label Style="display: block;">Manifest Refraction:<input type="text"  /> </asp:Label><br />
        <asp:Label Style="display: block;">Pachymetry:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text"  /> </asp:Label><br />
        <asp:Label>Insurance:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:DropDownList ID="DropDownList4" runat="server">
                <asp:ListItem Text="Insurance1"></asp:ListItem>
                <asp:ListItem Text="Insurance2"></asp:ListItem>
                <asp:ListItem Text="Insurance2"></asp:ListItem>
            </asp:DropDownList>
        </asp:Label><br />
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Add" /><asp:Button Style="margin-left: 5px;"
            ID="Button2" runat="server" Text="Cancel" />
    </div><asp:Button style="margin-left:5px;" ID="Button4" runat="server" Text="Quick create patient" PostBackUrl="~/MockUps/patients.aspx" />
    <table class="gridView" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_gvUsers"
        style="width: 920px; border-collapse: collapse; clear: both;">
        <tr class="gridViewHeader">
            <th scope="col">
                Patient Name
            </th>
            <th align="center" scope="col">
                Time
            </th>
            <th scope="col">
                Glaucoma
            </th>
            <th scope="col">
                Retina
            </th>
            <th scope="col">
                Insurance
            </th>
            <th align="center" scope="col">
                Edit
            </th>
            <th scope="col">
                Delete
            </th>
        </tr>
        <tr class="gridViewRow">
            <td>
                Marina
            </td>
            <td>
                12:00AM
            </td>
            <td>
                <span id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_lblRoles">
                    <input type="image" id="Image1" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
                </span>
            </td>
            <td>
                <input type="image" id="Image2" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
                Insurance1
            </td>
            <td class="tdActions" align="center">
                <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgbtnEdit"
                    src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                    style="border-width: 0px;" />
            </td>
            <td class="tdActions">
                <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgBtnDelete"
                    src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
            </td>
        </tr>
        <tr class="gridViewAlternatingRow">
            <td>
                Mirko
            </td>
            <td>
                12:00AM
            </td>
            <td>
                <span id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_lblRoles">
                    <input type="image" id="Image3" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" /></span>
            </td>
            <td>
                <input type="image" id="Image4" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
                Insurance1
            </td>
            <td class="tdActions" align="center">
                <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgbtnEdit"
                    src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                    style="border-width: 0px;" />
            </td>
            <td class="tdActions">
                <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgBtnDelete"
                    src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
            </td>
        </tr>
        <tr class="gridViewRow">
            <td>
                Arne
            </td>
            <td>
                12:00AM
            </td>
            <td>
                <span id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_lblRoles">
                    <input type="image" id="Image5" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" /></span>
            </td>
            <td>
                <input type="image" id="Image6" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
                Insurance1
            </td>
            <td class="tdActions" align="center">
                <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgbtnEdit"
                    src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                    style="border-width: 0px;" />
            </td>
            <td class="tdActions">
                <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgBtnDelete"
                    src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
            </td>
        </tr>
        <tr class="gridViewAlternatingRow">
            <td>
                Zharko
            </td>
            <td>
                12:00AM
            </td>
            <td>
                <span id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_lblRoles">
                    <input type="image" id="Image8" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" /></span>
            </td>
            <td>
                <input type="image" id="Image7" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
                Insurance1
            </td>
            <td class="tdActions" align="center">
                <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgbtnEdit"
                    src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                    style="border-width: 0px;" />
            </td>
            <td class="tdActions">
                <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgBtnDelete"
                    src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
            </td>
        </tr>
        <tr class="gridViewRow">
            <td>
                New
            </td>
            <td>
                12:00AM
            </td>
            <td>
                <span id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_lblRoles">
                    <input type="image" id="Image10" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" /></span>
            </td>
            <td>
                <input type="image" id="Image9" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
            </td>
            <td>
                Insurance1
            </td>
            <td class="tdActions" align="center">
                <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgbtnEdit"
                    src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                    style="border-width: 0px;" />
            </td>
            <td class="tdActions">
                <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgBtnDelete"
                    src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
            </td>
        </tr>
    </table>
    <div style="float: right; padding: 10px; margin-left: 10px;">
        <input type="image" id="Image11" src="../App_Themes/Default/icons/save.png" style="border-width: 0px;" />
        <input type="image" id="Image12" src="../App_Themes/Default/icons/back.png" style="border-width: 0px;" />
        <asp:Button ID="Button3" runat="server" Text="Submit Schedule" />
    </div>
</asp:Content>
