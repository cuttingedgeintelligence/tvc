﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true" CodeBehind="patients.aspx.cs" Inherits="CEI.Web.MockUps.patients" Title="Manage Patients" %>
<%@ Register src="../UserControls/DayMonthYear.ascx" tagname="DayMonthYear" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .style1
    {
        height: 29px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table border=“0” >
<tr><td> <b style="display:block; margin-bottom:0px;">Create New Patient Account</b> 

 <table border=“3” style="border:1px solid #CDCFCF; padding:10px;">
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td><asp:Label ID="Label10" runat="server" Text="First Name: "></asp:Label></td><td> <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox> </td></tr>
<tr><td><asp:Label ID="Label2" runat="server" Text="Middle Initial: "></asp:Label></td><td> <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox> </td></tr>
<tr><td><asp:Label ID="Label3" runat="server" Text="Last Name: "></asp:Label></td><td> <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox> </td></tr>
<tr><td><asp:Label ID="Label4" runat="server" Text="Gender: "></asp:Label></td><td>
    <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal">
        <asp:ListItem>Male</asp:ListItem>
        <asp:ListItem Selected="True">Female</asp:ListItem>
    </asp:RadioButtonList>
                                              </td></tr>
<tr><td><asp:Label ID="Label9" runat="server" Text="Date Of Birth: "></asp:Label></td><td> 
    <uc1:DayMonthYear ID="DayMonthYear1" runat="server" />
    </td></tr>

<tr><td><asp:Label ID="Label11" runat="server" Text="Manifest Refraction: "></asp:Label></td><td> <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox> </td></tr>
<tr><td><asp:Label ID="Label12" runat="server" Text="Pachymetry: "></asp:Label></td><td> <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox> </td></tr>
<tr><td><asp:Label ID="Label13" runat="server" Text="Insurance Type: "></asp:Label></td><td> <asp:DropDownList ID="DropDownList4" runat="server"><asp:ListItem Text="Insurance1" ></asp:ListItem><asp:ListItem Text="Insurance2" ></asp:ListItem><asp:ListItem Text="Insurance2" ></asp:ListItem> </asp:DropDownList> </td></tr>

<tr><td><asp:Label ID="Label14" runat="server" Text="Assign to Practice: "></asp:Label></td><td> <asp:DropDownList ID="DropDownList1" runat="server"><asp:ListItem Text="Dr. Pantevski" ></asp:ListItem><asp:ListItem Text="Dr. Johnson" ></asp:ListItem><asp:ListItem Text="Dr. Jules" ></asp:ListItem> </asp:DropDownList> </td></tr>
<tr><td><asp:Button ID="Button2" runat="server" Text="Create" /></td><td> &nbsp;</td></tr>
</table>




    </td>
    
    <td valign="top" align="right" style="vertical-align:top; ">    <b style="display:block; margin-left:11px;">Search Patient Accounts</b>
 <table border=“3” style="border:1px solid #CDCFCF; padding:10px;margin-left:10px;">
 <tr><td></td><td> &nbsp; </td></tr>
<tr><td><asp:Label ID="Label6" runat="server" Text="First Name: "></asp:Label></td><td> <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox> </td></tr>
<tr><td> <asp:Label ID="Label7" runat="server" Text="Last Name: "></asp:Label></td><td> <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox> </td></tr> 
<tr><td><asp:Label ID="Label5" runat="server" Text="Practice: "></asp:Label></td><td><asp:DropDownList ID="DropDownList2" runat="server"><asp:ListItem Text="Dr. Smith" ></asp:ListItem><asp:ListItem Text="Dr. Johnny" ></asp:ListItem><asp:ListItem Text="Dr. Pantevski" ></asp:ListItem> </asp:DropDownList></td></tr>

<tr><td>&nbsp;</td><td style="padding:5px;"> 
    <asp:Button ID="Button1" runat="server" Text="Search" />
    </td></tr>

    </td></tr>
    </table>
    </td>
    <td>
  
</table>
   	<div>
		<table class="gridView" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_gvUsers" style="width:920px;border-collapse:collapse;clear:both;">
			<tr class="gridViewHeader">
				<th scope="col" class="style1">First Name</th><th align="center" scope="col" 
                    class="style1">Last Name</th><th align="center" scope="col" class="style1">Date 
                    Of Birth</th><th scope="col" class="style1">Insurance Type</th>
                <th align="center" scope="col" class="style1">Edit</th><th scope="col" 
                    class="style1">Delete</th>

			</tr><tr class="gridViewRow">
				<td>Marina</td><td>Tekic</td>
						<td>12/02/1972</td>
						<td>Insurance1</td>
						<td class="tdActions" align="center">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgbtnEdit" src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');" style="border-width:0px;" />
						</td><td class="tdActions">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgBtnDelete" src="../App_Themes/Default/icons/delete.gif" style="border-width:0px;" />
							
						</td>

			</tr><tr class="gridViewAlternatingRow">
				<td>Mirko</td><td>Popov</td>
						<td>07/27/1977</td>
						<td>Insurance1</td>
						<td class="tdActions" align="center">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgbtnEdit" src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');" style="border-width:0px;" />
						</td><td class="tdActions">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgBtnDelete" src="../App_Themes/Default/icons/delete.gif" style="border-width:0px;" />
							
						</td>

			</tr><tr class="gridViewRow">
				<td>Arne</td><td>Dullaart</td>
						<td>11/30/1965</td>
						<td>Insurance1</td>
						<td class="tdActions" align="center">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgbtnEdit" src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');" style="border-width:0px;" />
						</td><td class="tdActions">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgBtnDelete" src="../App_Themes/Default/icons/delete.gif" style="border-width:0px;" />
							
						</td>

			</tr><tr class="gridViewAlternatingRow">
				<td>Zharko</td><td>Atkovski</td>
						<td>03/21/1978</td>
						<td>Insurance1</td>
						<td class="tdActions" align="center">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgbtnEdit" src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');" style="border-width:0px;" />
						</td><td class="tdActions">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgBtnDelete" src="../App_Themes/Default/icons/delete.gif" style="border-width:0px;" />
							
						</td>

			</tr><tr class="gridViewRow">
				<td>New</td><td>Stuff</td>
						<td>02/04/1948</td>
						<td>Insurance1</td>
						<td class="tdActions" align="center">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgbtnEdit" src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');" style="border-width:0px;" />
						</td><td class="tdActions">
							<input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgBtnDelete" src="../App_Themes/Default/icons/delete.gif" style="border-width:0px;" />
							
						</td>

			</tr>
		</table>
	</div>



</asp:Content>
