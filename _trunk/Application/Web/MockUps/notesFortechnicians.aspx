﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true"
    CodeBehind="notesFortechnicians.aspx.cs" Inherits="CEI.Web.MockUps.notesFortechnicians"
    Title="Manage Notes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Button ID="Button7" runat="server" Text="Create New Note" OnClientClick=" $('.panelHolderNote').slideDown('slow');return false;" />  
    &nbsp;<asp:Button ID="Button8" runat="server" Text="Create Category" 
            OnClientClick=" $('.panelHolderCategory').slideDown('slow');return false;" />
      <div class="panelHolder panelHolderNote" style="display:none;">
       
                <table>
                    
                    <tr>
                        <td valign="top">
                            <asp:Label ID="Label3" runat="server" Text="Category:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="DropDownList5" runat="server">
                                    <asp:ListItem Text="Category 1"></asp:ListItem>
                                    <asp:ListItem Text="Category 2"></asp:ListItem>
                                </asp:DropDownList>
                           
                        </td>
                    </tr>
                    
                    <tr>
                        <td valign="top">
                            <asp:Label ID="Label1" runat="server" Text="Note:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtNote" runat="server" MaxLength="50"></asp:TextBox>
                           
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2"><br />
                            <asp:ImageButton ID="btnSave" runat="server"  ImageUrl="~/App_Themes/Default/icons/save.png"
                                ValidationGroup="vgRole"  OnClientClick=" $('.panelHolderNote').slideUp('slow');return false;"
                                ToolTip="save" />&nbsp;
                            <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" ImageUrl="~/App_Themes/Default/icons/back.png"
                                 OnClientClick=" $('.panelHolderNote').slideUp('slow');return false;"
                                ToolTip="cancel" BackColor="" />
                               
                        </td>
                    </tr>
                </table>
            
    </div>
    
    
    <div class="panelHolder panelHolderCategory" style="display:none;">
       
                <table>
                    
                    <tr>
                        <td valign="top">
                            <asp:Label ID="Label2" runat="server" Text="Category:"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="TextBox1" runat="server" MaxLength="50"></asp:TextBox>
                           
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2"><br />
                            <asp:ImageButton ID="ImageButton1" runat="server"  ImageUrl="~/App_Themes/Default/icons/save.png"
                                ValidationGroup="vgRole"  OnClientClick=" $('.panelHolderCategory').slideUp('slow');return false;"
                                ToolTip="save" />&nbsp;
                            <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" ImageUrl="~/App_Themes/Default/icons/back.png"
                                 OnClientClick=" $('.panelHolderCategory').slideUp('slow');return false;"
                                ToolTip="cancel" BackColor="" />
                               
                        </td>
                    </tr>
                </table>
            
    </div>
    
    <div>
  
        <table class="gridView" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_gvUsers"
            style="width: 920px; border-collapse: collapse; clear: both;">
            <tr class="gridViewHeader">
                <th scope="col">
                    Notes
                </th>
                <th align="center" scope="col">
                    Edit
                </th>
                <th scope="col">
                    Delete
                </th>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Category 1
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolderCategory ').slideDown('slow');return false;"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewAlternatingRow">
                <td>
                    — Note 1
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolderNote ').slideDown('slow');return false;"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewRow">
                <td>
                     — Note 2
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolderNote ').slideDown('slow');return false;"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewAlternatingRow">
                <td>
                    Category 2
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolderCategory ').slideDown('slow');return false;"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewRow">
                <td>
                     — Note 3
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolderNote ').slideDown('slow');return false;"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
             <tr class="gridViewRow">
                <td>
                     — Note 4
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgbtnEdit" id="Image1"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolderNote ').slideDown('slow');return false;"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgBtnDelete" id="Image2"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
