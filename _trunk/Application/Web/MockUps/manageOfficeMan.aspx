﻿<%@ Page Language="C#" MasterPageFile="~/MockUps/Mockups.Master" AutoEventWireup="true"
    CodeBehind="manageOfficeMan.aspx.cs" Inherits="CEI.Web.MockUps.manageOfficeMan"
    Title="Manage Office Managers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table border="“0”">
        <tr>
            <td>
                <b style="display: block; margin-bottom: 0px;">Create New Office Manager Account</b>
                <table border="“3”" style="border: 1px solid #CDCFCF; padding: 10px;">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Title: "></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="TextBox1" runat="server">
                                <asp:ListItem Text="Mr."></asp:ListItem>
                                <asp:ListItem Text="Ms."></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="First Name: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Last Name: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label11" runat="server" Text="Username (e-mail): "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label14" runat="server" Text="Password: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox14" runat="server" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label19" runat="server" Text="Fax: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label20" runat="server" Text="Phone: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="Button2" runat="server" Text="Create" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;
            </td>
            <td valign="top" style="vertical-align: top;">
                <b style="display: block; margin-bottom: 0px;">Search Office Manager Accounts</b>
                <table border="“3”" style="border: 1px solid #CDCFCF; padding: 10px; margin-top: 0px;">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Title: "></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList1" runat="server">
                                <asp:ListItem Text="Mr."></asp:ListItem>
                                <asp:ListItem Text="Ms."></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label5" runat="server" Text="First Name: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text="Last Name: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Button ID="Button1" runat="server" Text="Search" />
                        </td>
                    </tr>
            </td>
        </tr>
    </table>
    </table>
    <div>
        <table class="gridView" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_gvUsers"
            style="width: 920px; border-collapse: collapse; clear: both;">
            <tr class="gridViewHeader">
                <th scope="col">
                    First Name
                </th>
                <th align="center" scope="col">
                    Last Name
                </th>
                <th align="center" scope="col">
                    Edit
                </th>
                <th scope="col">
                    Delete
                </th>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Marina
                </td>
                <td>
                    Tekic
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl02$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl02_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewAlternatingRow">
                <td>
                    Mirko
                </td>
                <td>
                    Popov
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl03$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl03_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewRow">
                <td>
                    Arne
                </td>
                <td>
                    Dullaart
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl04$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl04_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewAlternatingRow">
                <td>
                    Zharko
                </td>
                <td>
                    Atkovski
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl05$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl05_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
            <tr class="gridViewRow">
                <td>
                    New
                </td>
                <td>
                    Stuff
                </td>
                <td class="tdActions" align="center">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgbtnEdit" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgbtnEdit"
                        src="../App_Themes/Default/icons/edit.gif" onclick=" $('.panelHolder ').slideDown('slow');"
                        style="border-width: 0px;" />
                </td>
                <td class="tdActions">
                    <input type="image" name="ctl00$ContentPlaceHolder1$gvUsers$ctl06$imgBtnDelete" id="ctl00_ContentPlaceHolder1_gvUsers_ctl06_imgBtnDelete"
                        src="../App_Themes/Default/icons/delete.gif" style="border-width: 0px;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
