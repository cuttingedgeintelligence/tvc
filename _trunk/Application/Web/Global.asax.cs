﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Threading;
using System.Net;
using System.Diagnostics;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.Mail;
using System.Web.Caching;
using System.Web.SessionState;
using System.IO;
//using System.Messaging;
using System.Xml.Linq;

using CEI.Framework;
using CEI.Framework.Mailer;
using CEI.Common;
using CEI.BR;
using System.Configuration;

namespace CEI.Web
{
	public class Global : System.Web.HttpApplication
	{

		
         

		protected void Application_Start(Object sender, EventArgs e)
		{
			
		}

		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
			
		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{
			
		}

		protected void Session_End(Object sender, EventArgs e)
		{
            if (Session["UserActionLog"] != null)
            {
                UserAction _userAction = Session["UserActionLog"] as UserAction;
                _userAction.LogoutDateTime = DateTime.Now;
                new UserActionBR().UpdateUserLogout(_userAction);
            }
		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
	}
}