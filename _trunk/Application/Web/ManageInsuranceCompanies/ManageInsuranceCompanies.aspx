﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ManageInsuranceCompanies.aspx.cs" Inherits="CEI.Web.ManageInsuranceCompanies.ManageInsuranceCompanies"
    Title="Manage Insurance Companies" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="Manage Insurance Companies" CssClass="lblTitle"
        Style="float: none"></asp:Label>
    <div style="vertical-align: bottom; margin-bottom: 10px; overflow: hidden;">
        <asp:UpdatePanel ID="upInsertUpdate" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                <fieldset class="fieldsetLeft">
                    <legend>
                        <asp:Label ID="lblSelectedCompany" runat="server" Text="Create New Insurance Company"></asp:Label>
                    </legend>
                    <asp:Panel ID="pnlSaveInsuranceCompany" runat="server" DefaultButton="btnSave">
                        <table border="0">
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblName" runat="server" Text="Insurance Company:"></asp:Label>&nbsp;
                                </td>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                        Display="Dynamic" ErrorMessage="Please Enter Insurance Company" ValidationGroup="vgManageInsuranceCompany"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height:11px;">
                                <td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" OnClientClick="__defaultFired = false;"
                                        Text="Create" UseSubmitBehavior="False" ValidationGroup="vgManageInsuranceCompany" />
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                                        OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upInsuranceCompanies" runat="server" UpdateMode="Conditional"
        ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:GridView ID="gvInsuranceCompanies" runat="server" AllowPaging="False" PageSize="10"
                AutoGenerateColumns="False" DataKeyNames="id" SkinID="gridViewAdmin" OnRowEditing="gvInsuranceCompanies_RowEditing"
                Width="920px" Style="clear: both;" OnRowDeleting="gvInsuranceCompanies_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="name" HeaderText="Insurance Company" >
                     <HeaderStyle CssClass="moveLeft" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                ImageUrl="~/App_Themes/Default/icons/edit.gif" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" CssClass="tdActions" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnDelete" runat="server" CausesValidation="false" ImageUrl="~/App_Themes/Default/icons/delete.gif"
                                CommandName="Delete" />
                            <cc1:ConfirmButtonExtender ID="cbeImgBtnDelete" runat="server" ConfirmText="Are you sure you want to delete this insurance company?"
                                TargetControlID="imgBtnDelete">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                        <ItemStyle CssClass="tdActions" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
