﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;

namespace CEI.Web.ManageInsuranceCompanies
{
    public partial class ManageInsuranceCompanies : BasePage
    {

        #region GridView columns


        private const int GV_INSURANCE_COMPANIES_NAME = 0;
        private const int GV_INSURANCE_COMPANIES_EDIT = 1;
        private const int GV_INSURANCE_COMPANIES_DELETE = 2;

        #endregion //GridView columns


        #region Event handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridView();
            }
        }


      


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (ViewState["companyId"] != null) //update insurance company
                {
                  

                    NameValue insuranceCompany = new NameValue();
                    insuranceCompany.Id = (int)ViewState["companyId"];
                    insuranceCompany.Name = txtName.Text;
                   

                    if (new NameValueBR().UpdateInsuranceCompany(insuranceCompany))
                    {
                        this.BindGridView();
                        this.ClearInsertUpdateForm();
                        
                    }
                 

                }
                else //insert insurance company
                {
                   

                    NameValue insuranceCompany = new NameValue();
                    insuranceCompany.Name = txtName.Text;

                    if (new NameValueBR().InsertInsuranceCompany(insuranceCompany))
                    {
                        this.BindGridView();
                        this.ClearInsertUpdateForm();
                    }
                  

                }
            }
        }


        protected void gvInsuranceCompanies_RowEditing(object sender, GridViewEditEventArgs e)
        {
            ViewState["companyId"]  = (int)gvInsuranceCompanies.DataKeys[e.NewEditIndex].Value;
            btnSave.Text = "Save";
            lblSelectedCompany.Text = "Update Insurance Company: " + gvInsuranceCompanies.Rows[e.NewEditIndex].Cells[GV_INSURANCE_COMPANIES_NAME].Text;
            txtName.Text = gvInsuranceCompanies.Rows[e.NewEditIndex].Cells[GV_INSURANCE_COMPANIES_NAME].Text;

            upInsertUpdate.Update();
            
        }


        protected void gvInsuranceCompanies_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            if (new PatientBR().ExistsPatientWithInsuranceCompanyId((int)gvInsuranceCompanies.DataKeys[e.RowIndex].Value))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManageInsuranceCompanies), "existsPatientWithInsuranceCompanyId", "alert('You cannot delete this insurance company because is already assigned to a patient');", true);
                return;
            }


            if (new PatientLogBR().ExistsPatientLogPatientWithInsuranceCompanyId((int)gvInsuranceCompanies.DataKeys[e.RowIndex].Value))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(ManageInsuranceCompanies), "existsPatientLogPatientWithInsuranceCompanyId", "alert('You cannot delete this insurance company because is already assigned to a patient in a patient schedule');", true);
                return;
            }


            if (new NameValueBR().DeleteInsuranceCompany((int)gvInsuranceCompanies.DataKeys[e.RowIndex].Value))
            {

                if (ViewState["companyId"] != null && (int)gvInsuranceCompanies.DataKeys[e.RowIndex].Value == (int)ViewState["companyId"])
                {
                    this.ClearInsertUpdateForm();
                    upInsertUpdate.Update();
                }

                this.BindGridView();

            }
            
            

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.ClearInsertUpdateForm();
        }

        #endregion //Event handlers


        #region Private methods

        private void BindGridView()
        {

            gvInsuranceCompanies.DataSource = new NameValueBR().GetAllInsuranceCompanies();
            gvInsuranceCompanies.DataBind();
        }

        private void ClearInsertUpdateForm()
        {
            ViewState["companyId"] = null;
            lblSelectedCompany.Text = "Create New Insurance Company";
            btnSave.Text = "Create";
            //clear form
            txtName.Text = string.Empty;
        }


        #endregion //Private methods

        

    }
}
