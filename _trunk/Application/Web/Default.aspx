﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CEI.Web._Default" Title="Total Vision Care" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 143px;
           
        }
        .style2
        {
            width: 267px;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
   
                        <div  style="margin-left: 25px;width:430px; margin:0 auto; padding-left:50px;">
                            <table border="0" cellpadding="5" cellspacing="0" id="loginTbl">
                                 <tr>
                                    <td valign="middle"  style="text-align: justify; padding:0 10px 20px 0; color:#3c668f; font-size:23px; font-weight: bold; display:block;">
                                        <asp:Label ID="lblHeaderMessage" runat="server" Text="Welcome to Total Vision Care"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle"  style="text-align: left; color:#101114; display:block; width:370px;">
                                        <asp:Label ID="lblDescription" runat="server" Text="Total Vision Care is providing a progressive approach to helping you expand the scope of your practice by delivering a broad range of advanced ocular diagnostic testing to your patients. The value-added services provide you greater control and continuity of care, while enhancing the convenience and satisfaction of your patients."></asp:Label>
                                        <br /><br />
                                        <asp:Label ID="lblDescription1" runat="server" Text="We appreciate the opportunity to serve your practice."></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="upLogin" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Panel ID="pnlLogin" runat="server" DefaultButton="loginControl$LoginButton">
                                                <asp:Login ID="loginControl" runat="server" OnAuthenticate="loginControl_Authenticate"
                                                    TextLayout="TextOnTop" Width="246px" Height="152px" 
                                                    style="margin-left: 33px" 
                                                    FailureText="Your login attempt was not successful.&lt;/BR&gt; Please try again.">
                                                    <LayoutTemplate>
                                                         <table>
                                                                <tr>
                                                                    <td colspan="2" style="color: Red; text-align:center ; height: 20px;" 
                                                                        valign="middle" >
                                                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" valign="middle" >
                                                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" 
                                                                            Text="Username:   " />
                                                                    </td>
                                                                    <td valign="middle" class="style2" style="padding-left:5px;">
                                                                        <asp:TextBox ID="UserName" runat="server" MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >
                                                                        &nbsp;
                                                                    </td>
                                                                    <td class="style2">
                                                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                                            ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="vgLogin"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"  >
                                                                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" Text="Password:   " />
                                                                        
                                                                    </td>
                                                                    <td valign="middle" class="style2" style="padding-left:5px;">
                                                                        <asp:TextBox ID="Password" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" >
                                                                        &nbsp;
                                                                    </td>
                                                                    <td valign="middle" class="style2">
                                                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                                            ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="vgLogin"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"  align="center" style="padding: 15px 0 0 75px;">
                                                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Login" ValidationGroup="vgLogin"
                                                                            Style="float:left; margin-right: 10px;" UseSubmitBehavior="false" OnClientClick="__defaultFired = false;" />
                                                                        <asp:HyperLink ID="hlForgotPassword" runat="server" NavigateUrl="~/ForgotPassword.aspx"
                                                                            CssClass="btnClass linkFix" Style="height: 23px;">Forgot Password</asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                        </table>
                                                    </LayoutTemplate>
                                                 </asp:Login>
                                                 </asp:Panel>
                                                </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                   
    
</asp:Content>
