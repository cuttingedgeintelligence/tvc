<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ViewAppointmentDates.aspx.cs" Inherits="CEI.Web.ViewAppointmentDates.ViewAppointmentDates"
    Title="View Appointment Dates" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Label ID="lblHeaderText" runat="server" Text="View Appointment Dates" CssClass="lblTitle"
            Style="float: none"></asp:Label>
    </div>
    <fieldset class="fieldsetLeft" style="margin-bottom: 10px; overflow: hidden; width: 880px;
        margin-left: 10px;">
        <legend>
            <asp:Label ID="lblLegendCalendar" runat="server" Text="Preview Of Appointment Dates"></asp:Label>
        </legend>
        <div style="float: left; width: 600px;">
            <div style="font-weight: bold;">
                <asp:Label ID="lblReccurencePatternMessage" runat="server" Text=""></asp:Label>
            </div>
            <div style="font-weight: bold; padding: 10px 0;">
                <asp:Label ID="lblNextAppointmentDatesHeader" runat="server" Text=""></asp:Label><br />
            </div>
            <div style="padding-left: 20px;">
                <asp:Label ID="lblNextAppointmentDates" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div style="float: left; padding-top: 30px;">
            <asp:Calendar ID="calApointments" runat="server" CellPadding="2" DayNameFormat="Shortest"
                SkinID="theCalendarStyle" OnDayRender="calApointments_DayRender"></asp:Calendar>
        </div>
        <div style="clear: both; float: none;">
        </div>
    </fieldset>
</asp:Content>
