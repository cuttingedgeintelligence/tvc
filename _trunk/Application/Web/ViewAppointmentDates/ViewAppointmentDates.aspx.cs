﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.BR;
using CEI.Common.RecurringAppointment;


namespace CEI.Web.ViewAppointmentDates
{
    public partial class ViewAppointmentDates : BasePage
    {
        private const int NUMBER_OF_NEXT_APPOINTMENT_DATES = 10;

        #region Event handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataTable dtOneTimeAppointments = new PracticeOneTimeAppointmentBR().GetOneTimeAppointmentsByPracticeId(new Guid(User.Identity.Name), DateTime.Today, NUMBER_OF_NEXT_APPOINTMENT_DATES);
                List<DateTime> lsOneTimeAppointments = new List<DateTime>();
                foreach(DataRow drOneTimeAppointments in dtOneTimeAppointments.Rows)
                    lsOneTimeAppointments.Add((DateTime)drOneTimeAppointments["appointmentDate"]);

                //get next appointment dates
                List<DateTime> nextAppointmentDates = this.GetNextAppointmentDates(this.GetNextRecurringAppointmentDates(), lsOneTimeAppointments, NUMBER_OF_NEXT_APPOINTMENT_DATES);

                this.SelectCalendarDates(nextAppointmentDates);
                
                this.FormatAndShowNextAppointmentDates(nextAppointmentDates);
               
                
              
            }
        }

        protected void calApointments_DayRender(object sender, DayRenderEventArgs e)
        {
            e.Day.IsSelectable = false;
        }

        #endregion //Event handlers

        #region Private methods

        

        private List<DateTime> GetNextRecurringAppointmentDates()
        {
            List<DateTime> nextAppointmentDates = new List<DateTime>();
            DataRow drPracticeAppointmentsSettings = new PracticeAppointmentsSettingsBR().GetPracticeAppointmentSettingsByPracticeId(new Guid(User.Identity.Name));
            if (drPracticeAppointmentsSettings != null)
            {
                // Get reccurrence info object 
                RecurrenceInfo info = RecurrenceHelper.GetFriendlySeriesInfo(drPracticeAppointmentsSettings["recurrencePattern"].ToString());
                string endDateMessage = string.Empty;
                switch (info.EndDateType)
                {
                    case EndDateType.NoEndDate:
                        endDateMessage = "No end date.";
                        break;
                    case EndDateType.NumberOfOccurrences:
                        endDateMessage = "End after: " + info.NumberOfOccurrences.ToString() + " occurences.";
                        break;
                    case EndDateType.SpecificDate:
                        endDateMessage = "End by: " + info.EndDate.Value.ToString("dd MMMM yyyy ddddd") + ".";
                        break;

                }


                if (info.RecurrenceType.Equals(RecurrenceType.Monthly))
                {

                    lblReccurencePatternMessage.Text = "The " + info.MonthlySpecificDatePartOne.ToString().ToLower() + " " +
                                                                info.MonthlySpecificDatePartTwo.ToString().ToLower() + " of every " +
                                                                info.MonthlyRegenEveryXMonths.ToString() + " month(s).<BR />Start: " +
                                                                info.StartDate.ToString("dd MMMM yyyy ddddd") +
                                                                ". " + endDateMessage;


                    MonthlyRecurrenceSettings mo;

                    if (info.EndDateType.Equals(EndDateType.NumberOfOccurrences))
                    {
                        mo = new MonthlyRecurrenceSettings(info.StartDate, info.NumberOfOccurrences);
                        nextAppointmentDates = mo.GetValues(info.MonthlySpecificDatePartOne, info.MonthlySpecificDatePartTwo, info.MonthlyRegenEveryXMonths).Values;


                       
                    }
                    else if ((info.EndDateType.Equals(EndDateType.SpecificDate) && DateTime.Today < info.EndDate) ||
                               info.EndDateType.Equals(EndDateType.NoEndDate)
                             )
                    {

                        //set interval end date 
                        DateTime intervalEndDate;

                        //add info.MonthlyRegenEveryXMonths * 10 months on the start date(or today) to set interval  end date 
                        if (info.StartDate > DateTime.Today)
                            intervalEndDate = info.StartDate.AddMonths(info.MonthlyRegenEveryXMonths * 10);
                        else
                            intervalEndDate = DateTime.Today.AddMonths(info.MonthlyRegenEveryXMonths * 10);



                        if (info.EndDateType.Equals(EndDateType.SpecificDate) && intervalEndDate > info.EndDate)
                            intervalEndDate = info.EndDate.Value;


                        mo = new MonthlyRecurrenceSettings(info.StartDate, intervalEndDate);
                        nextAppointmentDates = mo.GetValues(info.MonthlySpecificDatePartOne, info.MonthlySpecificDatePartTwo, info.MonthlyRegenEveryXMonths).Values;


                        


                    }

                }
                else if (info.RecurrenceType.Equals(RecurrenceType.Weekly))
                {

                    string weeklySelectedDays = string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Monday ? DayOfWeek.Monday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Tuesday ? DayOfWeek.Tuesday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Wednesday ? DayOfWeek.Wednesday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Thursday ? DayOfWeek.Thursday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Friday ? DayOfWeek.Friday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Saturday ? DayOfWeek.Saturday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays += info.WeeklySelectedDays.Sunday ? DayOfWeek.Sunday.ToString().ToLower() + "," : string.Empty;
                    weeklySelectedDays = weeklySelectedDays.Substring(0, weeklySelectedDays.Length - 1);
                    lblReccurencePatternMessage.Text = "Recur every " + info.WeeklyRegenEveryXWeeks.ToString() + " week(s) on " +
                                                                weeklySelectedDays
                                                                + ".<BR />Start: " +
                                                                info.StartDate.ToString("dd MMMM yyyy ddddd") +
                                                                ". " + endDateMessage;


                    WeeklyRecurrenceSettings we;

                    if (info.EndDateType.Equals(EndDateType.NumberOfOccurrences))
                    {
                        we = new WeeklyRecurrenceSettings(info.StartDate, info.NumberOfOccurrences);
                        nextAppointmentDates = we.GetValues(info.WeeklyRegenEveryXWeeks, info.WeeklySelectedDays).Values;


                        

                    }
                    else if ((info.EndDateType.Equals(EndDateType.SpecificDate) && DateTime.Today < info.EndDate) ||
                                info.EndDateType.Equals(EndDateType.NoEndDate)
                             )
                    {

                        //set interval end date 
                        DateTime intervalEndDate;


                        //add (info.WeeklyRegenEveryXWeeks < 4) ? 10 : info.WeeklyRegenEveryXWeeks / 4 * 10  months on the interval start date(or today) to set interval  end date ; 
                        if (info.StartDate > DateTime.Today)
                            intervalEndDate = info.StartDate.AddMonths((info.WeeklyRegenEveryXWeeks < 4) ? 10 : info.WeeklyRegenEveryXWeeks / 4 * 10);
                        else
                            intervalEndDate = DateTime.Today.AddMonths((info.WeeklyRegenEveryXWeeks < 4) ? 10 : info.WeeklyRegenEveryXWeeks / 4 * 10);




                        if (info.EndDateType.Equals(EndDateType.SpecificDate) && intervalEndDate > info.EndDate)
                            intervalEndDate = info.EndDate.Value;

                        we = new WeeklyRecurrenceSettings(info.StartDate, intervalEndDate);
                        nextAppointmentDates  = we.GetValues(info.WeeklyRegenEveryXWeeks, info.WeeklySelectedDays).Values;


                        
                    }



                }
            }


            return nextAppointmentDates;
        }

        private List<DateTime> GetNextAppointmentDates(List<DateTime> appointmentsList, List<DateTime> lsOneTimeAppointments ,int numOfAppointments)
        {
            //append oneTimeAppointments to a recurring appointment dates
            appointmentsList.AddRange(lsOneTimeAppointments);
            appointmentsList.Sort();

            //remove duplicates
            IEnumerable<DateTime> appointmentListWithoutDuplicates =  appointmentsList.Distinct();
            
            List<DateTime> nextAppointmentDates = new List<DateTime>();
            
            int counter = 0;
            foreach (DateTime appointmentsDate in appointmentListWithoutDuplicates.ToList())
            {
                if (appointmentsDate > DateTime.Today)
                {
                    nextAppointmentDates.Add(appointmentsDate);
                    counter++;
                }
                if (counter == numOfAppointments)
                    break;
            }

            return nextAppointmentDates;
        }

       
        private void FormatAndShowNextAppointmentDates(List<DateTime> nextAppointmentDates)
        {
          
            string nextDates = string.Empty;

            foreach (DateTime appointmentDate in nextAppointmentDates)
                nextDates += appointmentDate.ToString("dd MMMM yyyy dddd") + "<br />";

            

            if (nextAppointmentDates.Count > 0)
            {
                lblNextAppointmentDatesHeader.Text = "Next " + nextAppointmentDates.Count.ToString() + " appointment dates with TVC:";
                lblNextAppointmentDates.Text = nextDates;
            }
            else
                lblNextAppointmentDatesHeader.Text = "There are no appointment dates with TVC";
        }

        private void SelectCalendarDates(List<DateTime> dates)
        {
            foreach (DateTime dateTime in dates)
                calApointments.SelectedDates.Add(dateTime);
        }

        #endregion //Private methods

        

        
    }
}
