﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.Framework;
using CEI.BR;
using System.Collections.Generic;

namespace CEI.Web.ManageFTP
{
    public partial class ManageFTP : BasePage
    {
        #region Event handlers
        static string prevPage = String.Empty;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindListControl<DataTable>.BindAndAddListItem(ddlPractices, new UserBR().GetAllPracticesNameAndId(), "name", "id", "Select Practice", Guid.Empty.ToString());

                if (Request.QueryString["prId"] != null && Request.QueryString["logId"] != null)
                {
                    ddlPractices.SelectedValue = Request.QueryString["prId"];
                    ViewState["practiceId"]=ddlPractices.SelectedValue;
                    this.BindServiceDates();
                    foreach (ListItem liServiceDate in ddlServiceDates.Items)
                    {
                        if (liServiceDate.Value.StartsWith(Request.QueryString["logId"] + "^"))
                        {
                            liServiceDate.Selected = true;
                            break;
                        }
                    }

                    if (ddlServiceDates.SelectedValue != "-1")
                    {
                        ViewState["serviceDate"] = ddlServiceDates.SelectedItem.Text;
                        //ddlServiceDates.SelectedValue == patientLogId^desktopImportedDate^desktopImportedComment
                        string[] desktopImportedList = ddlServiceDates.SelectedValue.Split('^');
                        ViewState["patientLogId"] = desktopImportedList[0];
                        //if desktopImportedDate is not null set cbImagesImported to checked
                        cbImagesImported.Checked = desktopImportedList[1] != string.Empty;

                        txtComment.Text = desktopImportedList[2];

                        PatientsGrid( new Guid(ViewState["practiceId"].ToString()), ViewState["serviceDate"].ToString());
                    }
                    else 
                    {
                        lblImagesImported.Style.Add("display", "none");
                        cbImagesImported.Style.Add("display", "none");
                        lblComment.Style.Add("display", "none");
                        txtComment.Style.Add("display", "none");
                        btnSave.Style.Add("display", "none");
                    }
                }
                else
                {

                    lblSelectServiceDate.Style.Add("display", "none");
                    ddlServiceDates.Style.Add("display", "none");
                    lblImagesImported.Style.Add("display", "none");
                    cbImagesImported.Style.Add("display", "none");
                    lblComment.Style.Add("display", "none");
                    txtComment.Style.Add("display", "none");
                    btnSave.Style.Add("display", "none");

                }

                ViewState["practiceId"] = ddlPractices.SelectedValue;
                ViewState["serviceDate"] = ddlServiceDates.Text;

                prevPage = Request.UrlReferrer.ToString();
                
            }
        }

        private void PatientsGrid(Guid practiceId, string serviceDate)
        {

            DataTable dtPatients = new UserBR().GetPatientsForFTPByPracticeIdandServiceDate(practiceId, serviceDate);
            gvPatients.DataSource = dtPatients;
            gvPatients.DataBind();
            if(gvPatients.Rows.Count > 0)
                btnSave.Style.Add("display", "");
            else
                btnSave.Style.Add("display", "none");

        }

        protected void ddlPractices_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPractices.SelectedValue != Guid.Empty.ToString())
            {
                //ViewState["practiceId"] = ddlPractices.SelectedValue;


                //if (ddlServiceDates.SelectedItem == null)
                //    PatientsGrid(new Guid(ViewState["practiceId"].ToString()), "");
                //else
                //{
                //    ViewState["serviceDate"] = ddlServiceDates.SelectedItem.Text;
                //    PatientsGrid(new Guid(ViewState["practiceId"].ToString()), ViewState["serviceDate"].ToString());
                //}
                this.BindServiceDates();


                lblSelectServiceDate.Style.Add("display", "");
                ddlServiceDates.Style.Add("display", "");
            }
            else
            {
                lblSelectServiceDate.Style.Add("display", "none");
                ddlServiceDates.Style.Add("display", "none");
                lblImagesImported.Style.Add("display", "none");
                cbImagesImported.Style.Add("display", "none");
                lblComment.Style.Add("display", "none");
                txtComment.Style.Add("display", "none");
                btnSave.Style.Add("display", "none");
            }

            ViewState["practiceId"] = ddlPractices.SelectedValue;


            if (ddlServiceDates.SelectedItem == null)
                PatientsGrid(new Guid(ViewState["practiceId"].ToString()), "");
            else
            {
                ViewState["serviceDate"] = ddlServiceDates.SelectedItem.Text;
                PatientsGrid(new Guid(ViewState["practiceId"].ToString()), ViewState["serviceDate"].ToString());
            }
        }

        protected void ddlServiceDates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlServiceDates.SelectedValue != "-1")
            {
                //ViewState["serviceDate"] = ddlServiceDates.SelectedItem.Text;
                //PatientsGrid(new Guid(ViewState["practiceId"].ToString()), ViewState["serviceDate"].ToString());
              
                //ddlServiceDates.SelectedValue == patientLogId^desktopImportedDate^desktopImportedComment
                string[]  desktopImportedList =  ddlServiceDates.SelectedValue.Split('^');
                ViewState["patientLogId"] = desktopImportedList[0];
                //if desktopImportedDate is not null set cbImagesImported to checked
                cbImagesImported.Checked = desktopImportedList[1] != string.Empty;

                txtComment.Text = desktopImportedList[2];

                lblImagesImported.Style.Add("display", "");
                cbImagesImported.Style.Add("display", "");
                lblComment.Style.Add("display", "");
                txtComment.Style.Add("display", "");
                btnSave.Style.Add("display", "");
            }
            else
            {
                //ViewState["serviceDate"] = ddlServiceDates.SelectedItem.Text;
                //PatientsGrid(new Guid(ViewState["practiceId"].ToString()), ViewState["serviceDate"].ToString());
                lblImagesImported.Style.Add("display", "none");
                cbImagesImported.Style.Add("display", "none");
                lblComment.Style.Add("display", "none");
                txtComment.Style.Add("display", "none");
                btnSave.Style.Add("display", "none");
            }
            ViewState["serviceDate"] = ddlServiceDates.SelectedItem.Text;
            PatientsGrid(new Guid(ViewState["practiceId"].ToString()), ViewState["serviceDate"].ToString());
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {


            List<string> listOfChacked = new List<string>();
            char[] delimiters = new char[] { ';' };
            string[] parts = lblHidden.Text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < parts.Length; i++)
            {
                listOfChacked.Add(parts[i]);
            }


            bool isUpdated = new UserBR().UpdatePatientLogPatients(listOfChacked);
         //   foreach (string chackForPatientLog in listOfChacked)
         //   {
              // delimiters = new char[] { '_' };
               // parts = lblHidden.Text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                //TODO !!!! OVDE!!!!! ke treba update vo patientsLogPatients kade parts[0] e id  parts[1] e true ili false i se pravi update na poleto isComplited
                //for (int j = 0; j < parts.Length; j++)
                //{
                //    listOfChacked.Add(parts[i]);
                //}
          //  }  //this is in DA            

         int numberFalse=0;
            int numberTrue=0;
            for(int numberPatients=0; numberPatients<gvPatients.Rows.Count; numberPatients++)
            {
                CheckBox cb = (CheckBox)gvPatients.Rows[numberPatients].FindControl("cbIsComplited");
                if (cb.Checked)
                    numberTrue++;
                else numberFalse++; ;
            }

         //   if (cbImagesImported.Checked)
            if(numberFalse==0)
            {
                if (new PatientLogBR().UpdatePatientLogDesktopImportedData(int.Parse(ViewState["patientLogId"].ToString()), DateTime.Now, txtComment.Text))
                    ddlServiceDates.Items[ddlServiceDates.SelectedIndex].Value = ViewState["patientLogId"].ToString() + "^" + DateTime.Now.ToString() + "^" + txtComment.Text;
            }
            else 
            {
                if (new PatientLogBR().UpdatePatientLogDesktopImportedData(int.Parse(ViewState["patientLogId"].ToString()), DateTime.MinValue, txtComment.Text))
                    ddlServiceDates.Items[ddlServiceDates.SelectedIndex].Value = ViewState["patientLogId"].ToString() + "^" + string.Empty + "^" + txtComment.Text;
               
            }
        }


        #endregion //Event handlers

        #region Private methods

        private void BindServiceDates()
        {

            ddlServiceDates.DataSource = new PatientLogBR().GetPatientLogsServiceDatesByPracticeId(new Guid(ddlPractices.SelectedValue)).Rows.Cast<DataRow>()
                    .Select(r => new { serviceDate = r["serviceDate"], id = r["patientLogId"] + "^" + r["desktopImportedDate"] + "^" + r["desktopImportedComment"] }).ToList();
            ddlServiceDates.DataBind();

            ddlServiceDates.Items.Insert(0, new ListItem("Select service date", "-1"));
 
        }


        #endregion //Private methods

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        protected void gvPatients_RowDataBound(object sender, GridViewRowEventArgs e)
        {


            if ((ddlServiceDates.SelectedValue != "") & (ddlServiceDates.SelectedValue != "-1") & (ddlServiceDates.SelectedValue != null) & (ddlPractices.SelectedValue != "-1") & (ddlPractices.SelectedValue != null))
                (sender as GridView).Columns[0].Visible = true;
            else
                (sender as GridView).Columns[0].Visible = false;


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox cbIsComplited = (CheckBox)e.Row.FindControl("cbIsComplited");
                Label lblIsComplited = (Label)e.Row.FindControl("lblIsComplited");


                if (lblIsComplited.Text == "True")
                    cbIsComplited.Checked = true;
                else
                    cbIsComplited.Checked = false;
                cbIsComplited.InputAttributes.Add("idForUpdate", (sender as GridView).DataKeys[e.Row.RowIndex]["Id"].ToString());
            }
        }

        protected void cbIsComplited_CheckedChanged(object sender, EventArgs e)
        {
            List<string> listOfChacked = new List<string>();
            string idAndChacked=(sender as CheckBox).InputAttributes["idForUpdate"].ToString()+"_"+(sender as CheckBox).Checked+";";

            lblHidden.Text += idAndChacked;

            //List<string> listOfChacked = new List<string>();
            //char[] delimiters = new char[] { ';' };
            //string[] parts = lblHidden.Text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            //for (int i = 0; i < parts.Length; i++)
            //{
            //    listOfChacked.Add(parts[i]);
            //}

            //listOfChacked.Add(idAndChacked);
        }

       

    }
}
