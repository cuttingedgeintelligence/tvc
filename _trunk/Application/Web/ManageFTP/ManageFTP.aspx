﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Default.Master" AutoEventWireup="true"
    CodeBehind="ManageFTP.aspx.cs" Inherits="CEI.Web.ManageFTP.ManageFTP" Title="Manage FTP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblHeaderText" runat="server" Text="FTP" CssClass="lblTitle" Style="float: none"></asp:Label>
    <asp:UpdatePanel ID="upFTP" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
                <tr>
                    <td width="180px">
                        <asp:Label ID="lblSelectPractice" runat="server" Text="Select Practice:"></asp:Label>
                    </td>
                    <td width="300px">
                        <asp:DropDownList ID="ddlPractices" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPractices_SelectedIndexChanged"
                            Width="300px" Style="margin: 5px 0px;">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblSelectServiceDate" runat="server" Text="Select Service Date:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlServiceDates" runat="server" AutoPostBack="True" DataTextField="serviceDate"
                            DataValueField="id" DataTextFormatString="{0:D}" OnSelectedIndexChanged="ddlServiceDates_SelectedIndexChanged"
                            Width="300px" Style="margin: 5px 0px;">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>
                        <asp:Label ID="lblImagesImported" runat="server" Text="Images Imported to Desktop:"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox ID="cbImagesImported" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblComment" runat="server" Text="Comment:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtComment" runat="server" MaxLength="200"  Width="300px" ></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td colspan="2" style="padding-top: 11px;">
                        <asp:UpdatePanel ID="upPatients" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="gvPatients" runat="server" AutoGenerateColumns="False" DataKeyNames="Id"
                                    SkinID="gridViewManagePatientLog" Width="920px" Style="clear: both;" EmptyDataText="There are no patients."
                                    OnRowDataBound="gvPatients_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Images Imported to Desktop">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbIsComplited" AutoPostBack="True" runat="server" Checked="false"
                                                    OnCheckedChanged="cbIsComplited_CheckedChanged" /><asp:Label ID="lblIsComplited"
                                                        Visible="false" runat="server" Text='<%# Eval("isComplited") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Patient Name" DataField="fullName">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Practice" DataField="PracticeName">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="serviceDate" HeaderText="Service Date" DataFormatString="{0:MM/dd/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                            <%--   <Triggers>
         <asp:AsyncPostBackTrigger EventName="CheckedChanged" ControlID="cbIsComplited" />
         </Triggers>--%>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 11px;">
                        <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Save Changes"
                            OnClick="btnSave_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 11px;">
                        <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="false" OnClick="btnBack_Click" />
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblHidden" Visible="false" runat="server"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
