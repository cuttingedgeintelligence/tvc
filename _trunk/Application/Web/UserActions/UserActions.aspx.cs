﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CEI.BR;
using System.Web.UI.HtmlControls;

namespace CEI.Web.UserActions
{
    public partial class UserActions : System.Web.UI.Page
    {
        #region GRID VIEW COLUMNS

        private const int GV_LOGIN_TIME = 0;
        private const int GV_LOGOUT_TIME = 1;
        private const int GV_TIME_SPAN = 2;

        #endregion

        #region EVENTS

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string userId = Page.Request.QueryString["Tech"].ToString();

                DataRow thisTech = new UserBR().GetTechnicianById(new Guid(userId));

                lblTechnician.Text = thisTech["firstName"] + " " + thisTech["lastName"];
                  
                if (!String.IsNullOrEmpty(userId))
                {
                    ViewState["Tech"] = new Guid(userId);
                }

                ucDateFrom.SetYearFromYearTo(1999, 2020);
                ucDateTo.SetYearFromYearTo(1999, 2020);

                btnView_Click(null, null);
            }
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            gvUserActions.PageIndex = 0;
            gvUserActions.DataSource = odsUserActions;
            gvUserActions.DataBind();
            if (gvUserActions.Rows.Count == 0)
                lblMessageNoAcount.Style.Add("display", "");
            else
                lblMessageNoAcount.Style.Add("display", "none");
          
        }

        protected void gvUserActions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                gvUserActions.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                LinkButton myButton = new LinkButton();
                Label lblSpace = new Label();
                GridViewRow pagerRow = e.Row;
                HtmlGenericControl spanNumeric = (HtmlGenericControl)pagerRow.FindControl("spannumeric");

                for (int i = 1; i < ((GridView)sender).PageCount + 1; i++)
                {
                    myButton = new LinkButton();
                    // myButton.Text = String.Format("{0}&nbsp;&nbsp;", i);
                    myButton.Text = i.ToString();
                    myButton.Attributes.CssStyle.Add("margin-left", "5px");
                    myButton.CommandName = "Page";
                    myButton.CommandArgument = i.ToString();
                    myButton.ID = "Page" + i;
                    if ((sender as GridView).PageIndex == i - 1)
                        myButton.Enabled = false;
                    spanNumeric.Controls.Add(myButton);
                    lblSpace.Text = "&nbsp;&nbsp; ";
                    spanNumeric.Controls.Add(lblSpace);


                }

            }
        }

        protected void gvReportLogs_DataBound(object sender, EventArgs e)
        {
            GridViewRow gvrPager = gvUserActions.BottomPagerRow;
            if (gvrPager == null) return;
            Button prev = (Button)gvrPager.Cells[0].FindControl("btnPrevious");
            Button next = (Button)gvrPager.Cells[0].FindControl("btnNext");


            if (gvUserActions.PageIndex == 0)
            {
                prev.Visible = false;
                next.Visible = true;
            }
            else
                if (gvUserActions.PageIndex == (gvUserActions.PageCount - 1))
                {
                    prev.Visible = true;
                    next.Visible = false;
                }


        }

        protected void gvUserActions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime loginDate = Convert.ToDateTime(e.Row.Cells[GV_LOGIN_TIME].Text);
                DateTime logoutDate = Convert.ToDateTime(e.Row.Cells[GV_LOGOUT_TIME].Text);

                Label lblTSpan = e.Row.Cells[GV_TIME_SPAN].FindControl("lblTimeSpan") as Label;
                //lblTSpan.Text = logoutDate.Subtract(loginDate).TotalMinutes.ToString();
                DateTime dtDuration = new DateTime(logoutDate.Subtract(loginDate).Ticks);
                lblTSpan.Text = dtDuration.ToString("HH:mm:ss");
                
            }
        }

        protected void odsUserActions_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (!IsPostBack)
            {
              
                e.InputParameters["dateFrom"] = DateTime.Today.AddDays(-5); 
                e.InputParameters["dateTo"] = DateTime.Today;
            }
            else
            {
                e.InputParameters["dateFrom"] = ucDateFrom.Date;
                e.InputParameters["dateTo"] = ucDateTo.Date;
            }
        }

        protected void gvUserActions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUserActions.PageIndex = e.NewPageIndex;
            gvUserActions.DataSource = odsUserActions;
            gvUserActions.DataBind();
        }

        #endregion

        #region PRIVATE METHODS
        


        #endregion

    }
}
