<%@ Page Title="User Activity" Language="C#" MasterPageFile="~/MasterPages/Default.Master"
    AutoEventWireup="true" CodeBehind="UserActions.aspx.cs" Inherits="CEI.Web.UserActions.UserActions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControls/DayMonthYear.ascx" TagName="DayMonthYear" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input id="btnBack" type="button" class="btnClass" value="Back" onclick="history.go(-1)" />
    <asp:Label ID="lblSelectDates" runat="server" Text="View Activity" CssClass="lblTitle"
        Style="float: none;" />
    <table width="100%;" cellpadding="0" cellspacing="0" border="0">
        <tr style="border: 1px;">
            <td style="padding-left: 5px; padding-bottom: 10px;" colspan="2">
                <asp:Label ID="lblNameTec" runat="server" Text="Technician:"></asp:Label>&nbsp;<asp:Label
                    ID="lblTechnician" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr style="border: 1px;">
            <td>
                <asp:Panel ID="pnlFrom" runat="server" GroupingText="Select Date From">
                    <br />
                    <uc1:DayMonthYear ID="ucDateFrom" runat="server" />
                </asp:Panel>
            </td>
            <td>
                <asp:Panel ID="pnlTo" runat="server" GroupingText="Select Date To">
                    <br />
                    <uc1:DayMonthYear ID="ucDateTo" runat="server" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 15px;">
                <asp:Button ID="btnView" runat="server" Text="View" OnClick="btnView_Click" />
            </td>
            <td>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upnlDataGrid" runat="server">
        <ContentTemplate>
            <asp:GridView runat="server" ID="gvUserActions" SkinID="gridViewAdmin" AutoGenerateColumns="false"
                Width="920px" OnRowDataBound="gvUserActions_RowDataBound" AllowPaging="True"
                OnPageIndexChanging="gvUserActions_PageIndexChanging" 
                ondatabound="gvReportLogs_DataBound" onrowcreated="gvUserActions_RowCreated">
                <Columns>
                    <asp:BoundField DataField="LoginTime" HeaderText="Login">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LogoutTime" HeaderText="Logout">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Duration">
                        <ItemTemplate>
                            <asp:Label ID="lblTimeSpan" runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                     <PagerTemplate>
                    <div style="padding: 5px;">
                        <div>
                            <asp:Button CommandArgument="Prev" CommandName="Page" Text="Previous" ID="btnPrevious"
                                runat="server" meta:resourcekey="ibPrevResource1" />&nbsp;
                            <asp:Button CommandArgument="Next" CommandName="Page" Text="Next" ID="btnNext" runat="server"
                                meta:resourcekey="ibNextResource1" />
                        </div>
                        <span id="spannumeric" runat="server" class="spannumeric"></span>
                    </div>
                </PagerTemplate>
            </asp:GridView>
            <asp:ObjectDataSource ID="odsUserActions" runat="server" EnablePaging="True" MaximumRowsParameterName="pageSize"
                OnSelecting="odsUserActions_Selecting" SelectCountMethod="GeUserActionsForUserIDTotalCount"
                SelectMethod="GetUserActionsForUserIDSearch" TypeName="CEI.BR.UserActionBR">
                <SelectParameters>
                    <asp:QueryStringParameter Name="userId" QueryStringField="Tech" />
                    <asp:Parameter Name="dateFrom" />
                    <asp:Parameter Name="dateTo" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <table width="100%;" cellpadding="0" cellspacing="0" border="0">
                <tr style="border: 1px;">
                    <td align="center">
                        <asp:Label ID="lblMessageNoAcount" Style="display: none;" runat="server" Text="No activity for this time period."></asp:Label>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnView" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
