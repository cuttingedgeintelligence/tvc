﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CEI.Common;
using CEI.BR;
using CEI.Framework.Encryption;
using Elmah;


namespace CEI.Web
{
    public partial class _Default : System.Web.UI.Page
    {
        #region Event handlers

        protected void Page_Load(object sender, EventArgs e)
        {

            loginControl.Focus();
            
          
        }

        protected void loginControl_Authenticate(object sender, AuthenticateEventArgs e)
        {
            if (Page.IsValid)
            {

                DataRow drUser = new UserBR().GetUserByEmail(loginControl.UserName);
                if (drUser != null && drUser["password"].ToString() == new EncryptDecrypt().Encrypt(loginControl.Password, string.Empty) && (bool)drUser["accountIsEnabled"])
                {
                    FormsAuthentication.SetAuthCookie(drUser["id"].ToString(), false);
                   
                    Session["loggedUserFullName"] = drUser["firstName"].ToString() + (drUser["lastName"] != DBNull.Value ? " " + drUser["lastName"].ToString() : string.Empty);
                    Session["loggedUserEmail"] = drUser["email"].ToString();
                    
                   
                    if (Roles.IsUserInRole(drUser["id"].ToString(), RoleEnum.Technician.ToString()))
                    {
                        this.LogUserAction(drUser["id"].ToString());
                        if (Request.QueryString["ReturnUrl"] == null)
                           // Response.Redirect("~/FinalNotesWorksheets/ViewPendingWorksheets.aspx", true);
                           Response.Redirect("~/Worksheets/ViewPendingPatientLogs.aspx", true);
                        else
                            Response.Redirect(Request.QueryString["ReturnUrl"], true);
                    }
                    else if (Roles.IsUserInRole(drUser["id"].ToString(), RoleEnum.Practice.ToString()))
                    {
                        if (Request.QueryString["ReturnUrl"] == null)
                            Response.Redirect("~/PatientLogs/ManagePatientLog.aspx", true);
                        else
                            Response.Redirect(Request.QueryString["ReturnUrl"], true);
                       
                    }
                    else
                    {
                        if (Request.QueryString["ReturnUrl"] == null)
                            Response.Redirect("~/PatientLogs/PatientLogs.aspx", true);
                        else
                            Response.Redirect(Request.QueryString["ReturnUrl"], true);
                        
                    }

                }

            }

        }

        protected void LogUserAction(string id)
        {
            Guid userId = String.IsNullOrEmpty(id) ? Guid.Empty : new Guid(id);

            Session["UserActionLog"] = new UserActionBR().InsertUserLogin(userId, DateTime.Now);
        }

        #endregion //Event handlers


    }
}
