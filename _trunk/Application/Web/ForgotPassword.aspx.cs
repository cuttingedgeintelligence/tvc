﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CEI.BR;
using CEI.Framework.Encryption;


namespace CEI.Web
{
    public partial class ForgotPassword : BasePage
    {
        #region Event handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnGetPassword_Click(object sender, EventArgs e)
        {
            DataRow drUser = new UserBR().GetUserByEmail(txtEmail.Text.Trim());
            if (drUser != null && (bool)drUser["accountIsEnabled"])
            {
                string password = new EncryptDecrypt().Decrypt(drUser["password"].ToString(), string.Empty);
                
              
                string fullName =  drUser["firstName"].ToString() + (drUser["lastName"] != DBNull.Value ?   " "  + drUser["lastName"].ToString() : string.Empty ) ;
                MailerBR.SendMailForgotPassword(txtEmail.Text, fullName, ConfigurationManager.AppSettings["mailSenderNameForgotPassword"], ConfigurationManager.AppSettings["mailFromForgotPassword"], password, ConfigurationManager.AppSettings["mailSubjectForgotPassword"]);

                lblMessage.Text = "Your password is sent";
            }
            else
            {
                //error message
                lblMessage.Text = "User with this email address does not exist";
            }
        }


        #endregion //Event handlers
    }
}
