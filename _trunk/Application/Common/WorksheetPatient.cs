﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{
    [Serializable]
    public class WorksheetPatient : PatientLogPatient
    {
        /// <summary>
        /// store for the CPT_Codes property
        /// </summary>
        private string _CPT_Codes;

        private string _CPT_CodesReport_G;

        private string _CPT_CodesReport_R;

        private string _colorCodeId;

        /// <summary>
        /// Gets or sets the Color of the CTP codes
        /// </summary>
        public string ColorCodeId
        {
            get { return _colorCodeId; }
            set { _colorCodeId = value; }
        }

        /// <summary>
        /// CPT_Codes property
        /// </summary>
        /// <value> comma separated CPT codes assigned to the worksheet patient  </value>
        public string CPT_Codes
        {
            get { return _CPT_Codes; }
            set { _CPT_Codes = value; }
        }
    

        private List<CPTCode> _CPT_CodesList;

        public List<CPTCode> CPT_CodesList
        {
            get { return _CPT_CodesList; }
            set { _CPT_CodesList = value; }
        }

        /// <summary>
        /// Gets the string prepared for printing
        /// </summary>
        public string CPT_CodesReport_G
        {
            get 
            {
                _CPT_CodesReport_G = string.Empty;

                if (this._CPT_CodesList != null)
                {
                    foreach (CPTCode CPT_Code in this.CPT_CodesList)
                    {
                        if (CPT_Code.CodeType.Equals(CPTCodeTypeEnum.Glaucoma))
                            _CPT_CodesReport_G += CPT_Code.Code + "|";
                    }
                }
                else
                {
                    string[] cptCodeNamesAndTypes = this.CPT_Codes.Split(',');
                    foreach (string CPT_CodeNameAndType in cptCodeNamesAndTypes)
                    {
                        string[] nameAndType = CPT_CodeNameAndType.Split('^');
                        if (nameAndType[1].Equals(((int)CPTCodeTypeEnum.Glaucoma).ToString()))
                            _CPT_CodesReport_G += nameAndType[0] + "|";
                    }
                }

                return _CPT_CodesReport_G; 
            }
        }

        public string CPT_CodesReport_R
        {
            get 
            {
                _CPT_CodesReport_R = string.Empty;

                if (this._CPT_CodesList != null)
                {
                    foreach (CPTCode CPT_Code in this.CPT_CodesList)
                    {
                        if (CPT_Code.CodeType.Equals(CPTCodeTypeEnum.Retina))
                            _CPT_CodesReport_R += CPT_Code.Code + "|";
                    }
                }
                else
                {
                    string[] cptCodeNamesAndTypes = this.CPT_Codes.Split(',');
                    foreach (string CPT_CodeNameAndType in cptCodeNamesAndTypes)
                    {
                        string[] nameAndType = CPT_CodeNameAndType.Split('^');
                        if (nameAndType[1].Equals(((int)CPTCodeTypeEnum.Retina).ToString()))
                            _CPT_CodesReport_R += nameAndType[0] + "|";
                    }
                }

                return _CPT_CodesReport_R; 
            }
        }
        
    }
}
