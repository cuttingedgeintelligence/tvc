using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{
    public enum ActionTypeEnum
    {
        Insert,
        Update,
        Delete,
        DoNothing
    }

    [Serializable]
    public abstract class BaseCommon
    {

        #region Private members

        private int _id;
        private ActionTypeEnum _actionType;

        #endregion

        #region Constructors
        public BaseCommon()
        {
            this._actionType = ActionTypeEnum.Insert;
            this._id = -1;
        }
        #endregion //Constructors

        #region Public properties

        public int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }


        public ActionTypeEnum ActionType
        {
            get
            {
                return this._actionType;
            }
            set
            {
                this._actionType = value;
                if (_actionType.Equals(ActionTypeEnum.Insert))
                    this._id = -1;
            }
        }
       

        #endregion //Public properties

    }
}
