﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
    public enum CPTCodeTypeEnum
    {
         Glaucoma = 1
        ,Retina = 2
    }



    [Serializable]
    public class CPTCode : BaseCommon
    {
        private string _code;

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private CPTCodeTypeEnum _codeType;

        /// <summary>
        /// CPT code type Glaucoma or Retina
        /// </summary>
        public CPTCodeTypeEnum CodeType
        {
            get { return _codeType; }
            set { _codeType = value; }
        }

        /// <summary>
        /// store for the PatientLogPatientCPTCodeId property
        /// </summary>
        private int _patientLogPatientCPTCodeId;


        /// <summary>
        /// PatientLogPatientCPTCodeId property
        /// </summary>
        /// <value>  id from intersection table tbl_PatientLogsPatientsCPTCodes </value>
        public int PatientLogPatientCPTCodeId
        {
            get { return _patientLogPatientCPTCodeId; }
            set { _patientLogPatientCPTCodeId = value; }
        }
    }
}
