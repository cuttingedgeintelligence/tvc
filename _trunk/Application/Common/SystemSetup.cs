using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{
    public enum SystemSetupEnum
    {
         MailDomain = 1
        ,UseMailCredentials = 2
        ,MailUsername = 3
        ,MailPassword = 4
        ,SendMailsFromApplication = 5
        ,RetinaExamsRecurrence = 6
        ,GlaucomaExamsRecurrence = 7
        ,SmtpHostPort = 8
        ,SmtpEnableSsl = 9
       
    }

    public class SystemSetup : BaseCommon
    {
        private string _name;
        private string _setupValue;

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }
        

        public string SetupValue
        {
            get
            {
                return this._setupValue;
            }
            set
            {
                this._setupValue = value;
            }
        }

        public SystemSetup()
        {
        }

        public SystemSetup(int pId, string pSetupValue)
        {
            this.Id = pId;
            this.SetupValue = pSetupValue;
        }
    }
}
