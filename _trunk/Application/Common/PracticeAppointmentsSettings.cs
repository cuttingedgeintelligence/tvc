﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
    /// <summary>
    /// appointments settings of practice with the TVC   
    /// </summary>
   public  class PracticeAppointmentsSettings : BaseCommon
    {
       /// <summary>
        /// store for the PracticeId property
       /// </summary>
        private Guid _practiceId;

       /// <summary>
        /// PracticeId property
       /// </summary>
        public Guid PracticeId
        {
            get { return _practiceId; }
            set { _practiceId = value; }
        }



       /// <summary>
        /// store for the RecurrencePattern property
       /// </summary>
        private string _recurrencePattern;

       /// <summary>
        /// RecurrencePattern property
       /// </summary>
        /// <value> recurrence pattern of appointment dates of practice with the TVC </value>
        public string RecurrencePattern
        {
            get { return _recurrencePattern; }
            set { _recurrencePattern = value; }
        }


     


        private DateTime _dateCreated;

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }


        private Guid _createdBy;

        public Guid CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

   
   }
}
