﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
    public class FinalNotesWorksheet : Worksheet
    {
        private List<FinalNotesWorksheetPatient> _finalNotesWorksheetPatients;

        public List<FinalNotesWorksheetPatient> FinalNotesWorksheetPatients
        {
            get { return _finalNotesWorksheetPatients; }
            set { _finalNotesWorksheetPatients = value; }
        }

    }
}
