﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{
    public enum PatientLogPatientStatusEnum
    {
        No__Show = 1
        
        ,Examined = 2
        
       ,Canceled = 3
    }

    [Serializable]
    public class FinalNotesWorksheetPatient : WorksheetPatient
    {

        private string _internalNotes;

        public string InternalNotes
        {
            get { return _internalNotes; }
            set { _internalNotes = value; }
        }


        private string _practiceNotes;

        public string PracticeNotes
        {
            get { return _practiceNotes; }
            set { _practiceNotes = value; }
        }


        private string _insurance;
        public string Insurance
        {
            get { return _insurance; }
            set { _insurance = value; }
        }

        private string _insuranceCompanyName;
        public string InsuranceCompanyName
        {
            get { return this.Insurance; }
        }

        private PatientLogPatientStatusEnum _status;

        public PatientLogPatientStatusEnum Status
        {
            get { return _status; }
            set { _status = value; }
        }

        //private PatientLogPatientStatusEnum _practiceNotes;
        //public PatientLogPatientStatusEnum PracticeNotes
        //{
        //    get { return this.Status; }
        //}

        

        

        private string _OD_IOP;

        public string OD_IOP
        {
            get { return _OD_IOP; }
            set { _OD_IOP = value; }
        }
        private string _OS_IOP;

        public string OS_IOP
        {
            get { return _OS_IOP; }
            set { _OS_IOP = value; }
        }
        private short? _OD_BFA;

        public short? OD_BFA
        {
            get { return _OD_BFA; }
            set { _OD_BFA = value; }
        }
        private short? _OS_BFA;

        public short? OS_BFA
        {
            get { return _OS_BFA; }
            set { _OS_BFA = value; }
        }

        private string _internalCustomNotes;

        public string InternalCustomNotes
        {
            get { return _internalCustomNotes; }
            set { _internalCustomNotes = value; }
        }

        private string _practiceCustomNotes;

        public string PracticeCustomNotes
        {
            get { return _practiceCustomNotes; }
            set { _practiceCustomNotes = value; }
        }

        private short? _OD_Pachymetry;
        public short? OD_Pachymetry
        {
            get { return _OD_Pachymetry; }
            set { _OD_Pachymetry = value; }
        }

        private short? _OS_Pachymetry;

        public short? OS_Pachymetry
        {
            get { return _OS_Pachymetry; }
            set { _OS_Pachymetry = value; }
        }
      
    }
}
