using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{
	public enum RoleEnum
	{
		 Ultimate__admin = 1
		,Office__manager = 2
        ,Technician = 3
        ,Practice = 4

	}

    public class Role : BaseCommon
    {
        #region Private members

        //private int _userId;
        private string _name;
       
		
        
        #endregion

        #region Public property

       

        public string  Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

       


        #endregion
    }
}
