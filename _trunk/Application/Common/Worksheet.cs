﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{

   

    public class Worksheet : PatientLog
    {


        private Guid _technicianId;

        public Guid TechnicianId
        {
            get { return _technicianId; }
            set { _technicianId = value; }
        }

        private List<WorksheetPatient> _worksheetPatients;

        public List<WorksheetPatient> WorksheetPatients
        {
            get { return _worksheetPatients; }
            set { _worksheetPatients = value; }
        }


      
       
    }
}
