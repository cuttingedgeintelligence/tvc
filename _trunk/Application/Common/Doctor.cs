﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
    public class Doctor : BaseCommon
    {
        #region Properties

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Guid PracticeId { get; set; }

        #endregion

    }
}
