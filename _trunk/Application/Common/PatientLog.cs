﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{

    public enum PatientLogStatusEnum
    {
        Patient__log = 1
       ,Pending__patient__log = 2
       ,Worksheet = 3
       ,Final__notes__worksheet = 4

    }


    public class PatientLog : BaseCommon
    {

        public PatientLog()
        {
            _patientLogPatients = new List<PatientLogPatient>();
        }

        private Guid _practiceId;

        public Guid PracticeId
        {
            get { return _practiceId; }
            set { _practiceId = value; }
        }

        private Guid _technicianId;

        public Guid TechnicianId
        {
            get { return _technicianId; }
            set { _technicianId = value; }
        }
        
        private Guid _completedById;

        public Guid CompletedById
        {
            get { return _completedById; }
            set { _completedById = value; }
        }


        private DateTime _completedOn;
        public DateTime CompletedOn
        {
            get { return _completedOn; }
            set { _completedOn = value; }
        }


        private DateTime _desktopImportedDate;
        public DateTime DesktopImportedDate
        {
            get { return _desktopImportedDate; }
            set { _desktopImportedDate = value; }
        }

        private string _desktopImportedComment;
        public string DesktopImportedComment
        {
            get { return _desktopImportedComment; }
            set { _desktopImportedComment = value; }
        }


        private DateTime _serviceDate;

        public DateTime ServiceDate
        {
            get { return _serviceDate; }
            set { _serviceDate = value; }
        }

        
       
        private DateTime _dateCreated;

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }
        private Guid _createdBy;

        public Guid CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        private PatientLogStatusEnum _status;

        public PatientLogStatusEnum Status
        {
            get { return _status; }
            set { _status = value; }
        }

        /// <summary>
        /// store for the NumOfPatients property
        /// </summary>
        private short numOfPatients;

        /// <summary>
        /// NumOfPatients property
        /// </summary>
        /// <value> number of patients in the patient log</value>
        public short NumOfPatients
        {
            get { return numOfPatients; }
            set { numOfPatients = value; }
        }

        private List<PatientLogPatient> _patientLogPatients;

        public List<PatientLogPatient> PatientLogPatients
        {
            get { return _patientLogPatients; }
            set { _patientLogPatients = value; }
        }

        private List<FinalNotesWorksheetPatient> _finalNotesWorksheetPatients;

        public List<FinalNotesWorksheetPatient> FinalNotesWorksheetPatients
        {
            get { return _finalNotesWorksheetPatients; }
            set { _finalNotesWorksheetPatients = value; }
        }

       
    }
}
