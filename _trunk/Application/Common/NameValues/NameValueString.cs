﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
	public class NameValueString : BaseCommon
	{
		private string _name;
		private string _idString;


        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public string IdString
        {
			get
			{
				return this._idString;
			}
			set
			{
				this._idString = value;
			}
        }

		public NameValueString()
		{
		}

        public NameValueString(string pName, string pValue)
        {
            this.Name = pName;
            this.IdString = pValue;
        }
	}
}
