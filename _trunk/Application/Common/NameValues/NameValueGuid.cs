﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
	public class NameValueGuid : BaseCommon
	{
		private string _name;
		private Guid _idGuid;


        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public Guid IdGuid
        {
			get
			{
				return this._idGuid;
			}
			set
			{
				this._idGuid = value;
			}
        }

		public NameValueGuid()
		{
		}

		public NameValueGuid(string pName, Guid pValue)
        {
            this.Name = pName;
            this.IdGuid = pValue;
        }
	}
}
