using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{
   public class NameValue:BaseCommon
    {
        private string _name;


        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        public NameValue()
        {
        }

        public NameValue(string pName, int pValue)
        {
            this.Name = pName;
            this.Id = pValue;
        }
    }
}
