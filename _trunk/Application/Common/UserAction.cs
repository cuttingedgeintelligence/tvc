﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
    /// <summary>
    /// Class representing the user action. It's used for logging the login and logout DateTime of a user
    /// </summary>
    public class UserAction
    {
        #region PUBLIC PROPERTIES

        public int Id { get; set; }
        public Guid UserId { get; set; }
        public DateTime LoginDateTime { get; set; }
        public DateTime LogoutDateTime { get; set; }

        #endregion

        #region CONSTRUCTOR

        public UserAction()
        {

        }

        #endregion
    }
}
