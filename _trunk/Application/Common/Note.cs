﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
    [Serializable]
    public class Note : BaseCommon
    {
        private string _text;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        private int _categoryId;

        public int CategoryId
        {
            get { return _categoryId; }
            set { _categoryId = value; }
        }
    }
}
