﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
    public class NoteTypes : BaseCommon
    {
        private string _text;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }
    }
}
