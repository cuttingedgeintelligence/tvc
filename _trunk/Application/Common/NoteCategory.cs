﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
    public class NoteCategory : BaseCommon 
    {
        private string _category;

        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private int _noteTypeId;

        public int NoteTypeId
        {
            get { return _noteTypeId; }
            set { _noteTypeId = value; }
        }
       
    }

   
}
