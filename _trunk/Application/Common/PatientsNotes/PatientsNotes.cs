﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common.PatientsNotes
{
    /// <summary>
    /// Class representing the Patients notes supplied by the Doctor or by the Tech.
    /// </summary>
    [Serializable]
    public abstract class PatientsNotes
    {
        #region PROPERTIES

        /// <summary>
        /// Gets or sets the Patients Notes Table ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the PatientsLogPatientsID
        /// </summary>
        public int PatientLogPatientId { get; set; }

        /// <summary>
        /// Gets or sets the NoteID
        /// </summary>
        public int NoteId { get; set; }

        #endregion

        #region CONSTRUCTOR

        public PatientsNotes()
        {

        }

        #endregion
    }
}
