﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{
    [Serializable]
    public class Patient : BaseCommon
    {

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _middleInitial;

        public string MiddleInitial
        {
            get { return _middleInitial; }
            set { _middleInitial = value; }
        }
        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }


        private string _fullName;

        public string FullName
        {
            get { return _fullName; }
            set { _fullName = value; }
        }

        private string _gender;

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        private DateTime _dateOfBirth;

        public DateTime DateOfBirth
        {
            get { return _dateOfBirth; }
            set { _dateOfBirth = value; }
        }
        private int _insuranceCompanyId;

        public int InsuranceCompanyId
        {
            get { return _insuranceCompanyId; }
            set { _insuranceCompanyId = value; }
        }


        private string _insuranceCompanyName;

        public string InsuranceCompanyName
        {
            get { return _insuranceCompanyName; }
            set { _insuranceCompanyName = value; }
        }
        private string _OD_Sphere;

        public string OD_Sphere
        {
            get { return _OD_Sphere; }
            set { _OD_Sphere = value; }
        }
        private string _OS_Sphere;

        public string OS_Sphere
        {
            get { return _OS_Sphere; }
            set { _OS_Sphere = value; }
        }
        private string _OD_Cylinder;

        public string OD_Cylinder
        {
            get { return _OD_Cylinder; }
            set { _OD_Cylinder = value; }
        }
        private string _OS_Cylinder;

        public string OS_Cylinder
        {
            get { return _OS_Cylinder; }
            set { _OS_Cylinder = value; }
        }
        private string _OD_AxisOfAstigmatism;

        public string OD_AxisOfAstigmatism
        {
            get { return _OD_AxisOfAstigmatism; }
            set { _OD_AxisOfAstigmatism = value; }
        }
        private string _OS_AxisOfAstigmatism;

        public string OS_AxisOfAstigmatism
        {
            get { return _OS_AxisOfAstigmatism; }
            set { _OS_AxisOfAstigmatism = value; }
        }


        private short? _OD_Pachymetry;

        public short? OD_Pachymetry
        {
            get { return _OD_Pachymetry; }
            set { _OD_Pachymetry = value; }
        }

        private short? _OS_Pachymetry;

        public short? OS_Pachymetry
        {
            get { return _OS_Pachymetry; }
            set { _OS_Pachymetry = value; }
        }

        private string _TVC_idCode;

        public string TVC_IdCode
        {
            get { return _TVC_idCode; }
            set { _TVC_idCode = value; }
        }
        private DateTime _dateCreated;

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }
        private Guid _createdBy;

        public Guid CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        private Guid _practiceId;

        public Guid PracticeId
        {
            get { return _practiceId; }
            set { _practiceId = value; }
        }

        public int DoctorId
        {
            get;
            set;
        }

        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
    }
}
