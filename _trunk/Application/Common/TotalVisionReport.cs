﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CEI.Common
{
    public enum ExportType
    {
        Printer,
        Pdf,
        Excel
    }

    [Serializable]
    public class TotalVisionReport
    {
        private Dictionary<string, object> _pameters;

        public Dictionary<string, object> Pameters
        {
            get { return _pameters; }
            set { _pameters = value; }
        }

        private object _listItems;

        public object ListItems
        {
            get { return _listItems; }
            set { _listItems = value; }
        }

        public TotalVisionReport()
        {
            _pameters = new Dictionary<string, object>();
        }
    }
}
