using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{
    public enum FrequencyEnum
    {
        Quarterly = 1
       ,Monthly = 2
       ,BiMonthly = 3
    }

    public class User : BaseCommon
    {
        #region Properties

        private Guid _userId;

        public Guid UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        private string _middleName;

        public string MiddleName
        {
            get { return _middleName; }
            set { _middleName = value; }
        }
        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        private bool _accountIsEnabled;

        public bool AccountIsEnabled
        {
            get { return _accountIsEnabled; }
            set { _accountIsEnabled = value; }
        }


        private string _fax;

        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }
        private string _phone;

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        private string _city;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        private string _zipCode;

        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }
        private string _address1;

        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        private string _address2;

        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        private int _stateId;

        public int StateId
        {
            get { return _stateId; }
            set { _stateId = value; }
        }

        //only for Technician accounts
        private List<NameValue> _CountiesList;

        //only for Technician accounts
        public List<NameValue> CountiesList
        {
            get { return _CountiesList; }
            set { _CountiesList = value; }
        }


       
        private int _countyId;

        public int CountyId
        {
            get { return _countyId; }
            set { _countyId = value; }
        }
        private DateTime _dateCreated;

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }
        private Guid _createdBy;

        public Guid CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        private string _doctorsDegree;

        public string DoctorsDegree
        {
            get { return _doctorsDegree; }
            set { _doctorsDegree = value; }
        }
        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        private string _abbreviation;

        public string Abbreviation
        {
            get { return _abbreviation; }
            set { _abbreviation = value; }
        }
        private string _referredBy;

        public string ReferredBy
        {
            get { return _referredBy; }
            set { _referredBy = value; }
        }


        private DateTime _desktopImportedDate;

        public DateTime DesktopImportedDate
        {
            get { return _desktopImportedDate; }
            set { _desktopImportedDate = value; }
        }



        public bool SlitlampAndTableAreRequired
        {
            get;
            set;
        }

        public FrequencyEnum Frequency
        {
            get;
            set;
        }

      
        public string PreferredDay1
        {
            get;
            set;
        }

        public string PreferredDay2
        {
            get;
            set;
        }

        public string PreferredDay3
        {
            get;
            set;
        }

        public string AssignedTech
        {
            get;
            set;
        }



        #endregion //Properties






    }
}
