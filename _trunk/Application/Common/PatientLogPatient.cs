﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CEI.Common
{
    public enum PatientHistoryEnum
    {
        New__Patient = 1
       ,Follow__Up = 2
       , No__Show = 3
    }





    [Serializable]
    public class PatientLogPatient : Patient
    {

        private int _patientId;

        public int PatientId
        {
            get { return _patientId; }
            set { _patientId = value; }
        }


        private int _patientLogId;

        public int PatientLogId
        {
            get { return _patientLogId; }
            set { _patientLogId = value; }
        }


        private bool _glaucoma;

        public bool Glaucoma
        {
            get { return _glaucoma; }
            set { _glaucoma = value; }
        }

        private string _OD_IOP;
        public string OD_IOP
        {
            get { return _OD_IOP; }
            set { _OD_IOP = value; }
        }

         private string _OS_IOP;
         public string OS_IOP
         {
             get { return _OS_IOP; }
             set { _OS_IOP = value; }
         }
         private string _OD_BFA;
         public string OD_BFA
         {
             get { return _OD_BFA; }
             set { _OD_BFA = value; }
         }
         private string _OS_BFA;
         public string OS_BFA
         {
             get { return _OS_BFA; }
             set { _OS_BFA = value; }
         }

        private bool _retina;

        public bool Retina
        {
            get { return _retina; }
            set { _retina = value; }
        }

        private string _internalNotes;
        public string internalNotes
        {
            get { return _internalNotes; }
            set { _internalNotes = value; }
        }

        private string _practiceNotes;
        public string practiceNotes
        {
            get { return _practiceNotes; }
            set { _practiceNotes = value; }
        }

        private string _appointmentTime;

        public string AppointmentTime
        {
            get { return _appointmentTime; }
            set { _appointmentTime = value; }
        }

        private PatientHistoryEnum _patientHistory;

        public PatientHistoryEnum PatientHistory
        {
            get { return _patientHistory; }
            set { _patientHistory = value; }
        }

        public int DoctorId
        {
            get;
            set;
        }


        private decimal _BFA;

        public decimal BFA
        {
            get { return _BFA; }
            set { _BFA = value; }
        }

        private decimal _HRT_Glaucoma;

        public decimal HRT_Glaucoma
        {
            get { return _HRT_Glaucoma; }
            set { _HRT_Glaucoma = value; }
        }

        private decimal _HRT_Retina;

        public decimal HRT_Retina
        {
            get { return _HRT_Retina; }
            set { _HRT_Retina = value; }
        }

        //private short _Status;

        //public short Status
        //{
        //    get { return Status; }
        //    set { Status = value; }
        //}

      

       
    }



    public class PatientLogPatientAppointmentTimeComparer : IComparer<PatientLogPatient>
    {

        #region IComparer<PatientLogPatient> Members

        public int Compare(PatientLogPatient patient1, PatientLogPatient patient2)
        {
            //order patientLogPatients ascending by appointment time
            return patient1.AppointmentTime.CompareTo(patient2.AppointmentTime);
        }

        #endregion
    }

}
