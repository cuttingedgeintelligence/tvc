﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Data.SqlClient;
using System.Data;
using System.Configuration.Provider;

namespace CEI.Common
{
    public class SqlRoleProvider : RoleProvider
    {
        private string _connectionString;


        private const string CACHE_KEY_USER_ROLES = "USER_ROLES:";
        private const int USER_ROLES_CACHE_SLIDING_EXPIRATION_MINUTES = 60;

        #region stored procedure names
        private const string STP_ROLES_USERSROLES_GET_ROLES_BY_USER_ID = "dbo.stp_Roles_UsersRoles_GetRolesByUserId";
        #endregion //stored procedure names
        #region System.Configuration.Provider.ProviderBase.Initialize Method

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException("Configuration for SqlRoleProvider in web.config is missing");

            base.Initialize(name, config);

            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings[config["connectionStringName"]];

            if (connectionStringSettings == null || connectionStringSettings.ConnectionString.Trim() == "")
                throw new ProviderException("Connection string for SqlRoleProvider cannot be blank");


            this._connectionString = connectionStringSettings.ConnectionString;



        }

        #endregion //System.Configuration.Provider.ProviderBase.Initialize Method


        #region  System.Web.Security.RoleProvider properties

        private string _applicationName;

        public override string ApplicationName
        {
            get
            {
                return this._applicationName;
            }
            set
            {
                this._applicationName = value;
            }
        }

        #endregion  //System.Web.Security.RoleProvider properties


        #region System.Web.Security.RoleProvider methods



        /// <summary>
        /// RoleProvider.GetRolesForUser 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>string[] - list of user permissions(names) </returns>
        public override string[] GetRolesForUser(string userId)
        {
            if (userId == null)
                throw new ArgumentNullException("userId");

            if (userId.Equals(string.Empty))
                throw new ArgumentException("userId is empty");

            string cacheKey = CACHE_KEY_USER_ROLES + userId;
            string[] cachedUserRoles = HttpRuntime.Cache[cacheKey] as string[];

            if (cachedUserRoles == null)
            {
                string tmpPermissions = string.Empty;

                SqlConnection conn = new SqlConnection(_connectionString);
                SqlCommand command = new SqlCommand(STP_ROLES_USERSROLES_GET_ROLES_BY_USER_ID, conn);


                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@userId",  new Guid(userId)));


                //SqlCacheDependency cacheDependency = new SqlCacheDependency(command);


                SqlDataReader reader = null;

                try
                {
                    
                    conn.Open();
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        tmpPermissions += reader["name"].ToString() + ",";
                    }
                }
                catch (SqlException e)
                {
                    throw e;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();

                    conn.Close();
                }


                if (tmpPermissions.Length > 0)
                {
                    // Remove trailing comma.
                    tmpPermissions = tmpPermissions.Substring(0, tmpPermissions.Length - 1);
                    string[] tmpPermissionsList = tmpPermissions.Split(',');
                    // this.InsertInCache(cacheKey, tmpRoleNamesList,cacheDependency);
                    this.InsertInCache(cacheKey, tmpPermissionsList);
                    return tmpPermissionsList;
                }
            }
            else
                return cachedUserRoles;

            return new string[0];
        }


        /// <summary>
        /// RoleProvider.IsUserInRole 
        /// </summary>
        /// <param name="userId" ></param>
        /// <param name="permissionName"></param>
        /// <returns>bool - user has permission </returns>

        public override bool IsUserInRole(string userId, string permissionName)
        {
            bool userHasPermission = false;

            foreach (string permission in this.GetRolesForUser(userId))
            {
                if (permission.Equals(permissionName))
                {
                    userHasPermission = true;
                    break;
                }
            }



            return userHasPermission;
        }



        #endregion //System.Web.Security.RoleProvider methods

        #region Cache methods
       


        private void InsertInCache(string key, object item)
        {

            HttpRuntime.Cache.Insert(key, item, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, USER_ROLES_CACHE_SLIDING_EXPIRATION_MINUTES, 0), CacheItemPriority.Normal, null);

        }

        //private void InsertInCache(string key, object item,SqlCacheDependency cacheDependency)
        //{
        //   
        //    HttpRuntime.Cache.Insert(key, item, cacheDependency, DateTime.MaxValue, Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);

        //}


        #endregion //Cache methods

        #region STILL NOT IMPLEMENTED METHODS


        //
        // RoleProvider.CreateRole
        //

        public override void CreateRole(string rolename)
        {
            //if (rolename.IndexOf(',') > 0)
            //{
            //    throw new ArgumentException("Role names cannot contain commas.");
            //}

            //if (RoleExists(rolename))
            //{
            //    throw new ProviderException("Role name already exists.");
            //}

            //OdbcConnection conn = new OdbcConnection(connectionString);
            //OdbcCommand cmd = new OdbcCommand("INSERT INTO [" + rolesTable + "]" +
            //        " (Rolename, ApplicationName) " +
            //        " Values(?, ?)", conn);

            //cmd.Parameters.Add("@Rolename", OdbcType.VarChar, 255).Value = rolename;
            //cmd.Parameters.Add("@ApplicationName", OdbcType.VarChar, 255).Value = ApplicationName;

            //try
            //{
            //    conn.Open();

            //    cmd.ExecuteNonQuery();
            //}
            //catch (OdbcException e)
            //{
            //    if (WriteExceptionsToEventLog)
            //    {
            //        WriteToEventLog(e, "CreateRole");
            //    }
            //    else
            //    {
            //        throw e;
            //    }
            //}
            //finally
            //{
            //    conn.Close();
            //}
        }


        //
        // RoleProvider.DeleteRole
        //

        public override bool DeleteRole(string rolename, bool throwOnPopulatedRole)
        {
            //if (!RoleExists(rolename))
            //{
            //    throw new ProviderException("Role does not exist.");
            //}

            //if (throwOnPopulatedRole && GetUsersInRole(rolename).Length > 0)
            //{
            //    throw new ProviderException("Cannot delete a populated role.");
            //}

            //OdbcConnection conn = new OdbcConnection(connectionString);
            //OdbcCommand cmd = new OdbcCommand("DELETE FROM [" + rolesTable + "]" +
            //        " WHERE Rolename = ? AND ApplicationName = ?", conn);

            //cmd.Parameters.Add("@Rolename", OdbcType.VarChar, 255).Value = rolename;
            //cmd.Parameters.Add("@ApplicationName", OdbcType.VarChar, 255).Value = ApplicationName;


            //OdbcCommand cmd2 = new OdbcCommand("DELETE FROM [" + usersInRolesTable + "]" +
            //        " WHERE Rolename = ? AND ApplicationName = ?", conn);

            //cmd2.Parameters.Add("@Rolename", OdbcType.VarChar, 255).Value = rolename;
            //cmd2.Parameters.Add("@ApplicationName", OdbcType.VarChar, 255).Value = ApplicationName;

            //OdbcTransaction tran = null;

            //try
            //{
            //    conn.Open();
            //    tran = conn.BeginTransaction();
            //    cmd.Transaction = tran;
            //    cmd2.Transaction = tran;

            //    cmd2.ExecuteNonQuery();
            //    cmd.ExecuteNonQuery();

            //    tran.Commit();
            //}
            //catch (OdbcException e)
            //{
            //    try
            //    {
            //        tran.Rollback();
            //    }
            //    catch { }


            //    if (WriteExceptionsToEventLog)
            //    {
            //        WriteToEventLog(e, "DeleteRole");

            //        return false;
            //    }
            //    else
            //    {
            //        throw e;
            //    }
            //}
            //finally
            //{
            //    conn.Close();
            //}

            return false;
        }

        //
        // RoleProvider.GetAllRoles
        //

        public override string[] GetAllRoles()
        {
            //string tmpRoleNames = "";



            //OdbcConnection conn = new OdbcConnection(connectionString);
            //OdbcCommand cmd = new OdbcCommand("SELECT Rolename FROM [" + rolesTable + "]" +
            //          " WHERE ApplicationName = ?", conn);

            //cmd.Parameters.Add("@ApplicationName", OdbcType.VarChar, 255).Value = ApplicationName;

            //OdbcDataReader reader = null;

            //try
            //{
            //    conn.Open();

            //    reader = cmd.ExecuteReader();

            //    while (reader.Read())
            //    {
            //        tmpRoleNames += reader.GetString(0) + ",";
            //    }
            //}
            //catch (OdbcException e)
            //{
            //    if (WriteExceptionsToEventLog)
            //    {
            //        WriteToEventLog(e, "GetAllRoles");
            //    }
            //    else
            //    {
            //        throw e;
            //    }
            //}
            //finally
            //{
            //    if (reader != null) { reader.Close(); }
            //    conn.Close();
            //}

            //if (tmpRoleNames.Length > 0)
            //{
            //    // Remove trailing comma.
            //    tmpRoleNames = tmpRoleNames.Substring(0, tmpRoleNames.Length - 1);
            //    return tmpRoleNames.Split(',');
            //}

            return new string[0];
        }

        //
        // RoleProvider.RoleExists
        //

        public override bool RoleExists(string rolename)
        {
            bool exists = false;

            //OdbcConnection conn = new OdbcConnection(connectionString);
            //OdbcCommand cmd = new OdbcCommand("SELECT COUNT(*) FROM [" + rolesTable + "]" +
            //          " WHERE Rolename = ? AND ApplicationName = ?", conn);

            //cmd.Parameters.Add("@Rolename", OdbcType.VarChar, 255).Value = rolename;
            //cmd.Parameters.Add("@ApplicationName", OdbcType.VarChar, 255).Value = ApplicationName;

            //try
            //{
            //    conn.Open();

            //    int numRecs = (int)cmd.ExecuteScalar();

            //    if (numRecs > 0)
            //    {
            //        exists = true;
            //    }
            //}
            //catch (OdbcException e)
            //{
            //    if (WriteExceptionsToEventLog)
            //    {
            //        WriteToEventLog(e, "RoleExists");
            //    }
            //    else
            //    {
            //        throw e;
            //    }
            //}
            //finally
            //{
            //    conn.Close();
            //}

            return exists;
        }

        //
        // RoleProvider.FindUsersInRole
        //

        public override string[] FindUsersInRole(string rolename, string usernameToMatch)
        {
            //OdbcConnection conn = new OdbcConnection(connectionString);
            //OdbcCommand cmd = new OdbcCommand("SELECT Username FROM [" + usersInRolesTable + "] " +
            //          "WHERE Username LIKE ? AND RoleName = ? AND ApplicationName = ?", conn);
            //cmd.Parameters.Add("@UsernameSearch", OdbcType.VarChar, 255).Value = usernameToMatch;
            //cmd.Parameters.Add("@RoleName", OdbcType.VarChar, 255).Value = rolename;
            //cmd.Parameters.Add("@ApplicationName", OdbcType.VarChar, 255).Value = pApplicationName;

            //string tmpUserNames = "";
            //OdbcDataReader reader = null;

            //try
            //{
            //    conn.Open();

            //    reader = cmd.ExecuteReader();

            //    while (reader.Read())
            //    {
            //        tmpUserNames += reader.GetString(0) + ",";
            //    }
            //}
            //catch (OdbcException e)
            //{
            //    if (WriteExceptionsToEventLog)
            //    {
            //        WriteToEventLog(e, "FindUsersInRole");
            //    }
            //    else
            //    {
            //        throw e;
            //    }
            //}
            //finally
            //{
            //    if (reader != null) { reader.Close(); }

            //    conn.Close();
            //}

            //if (tmpUserNames.Length > 0)
            //{
            //    // Remove trailing comma.
            //    tmpUserNames = tmpUserNames.Substring(0, tmpUserNames.Length - 1);
            //    return tmpUserNames.Split(',');
            //}

            return new string[0];
        }

        public override void AddUsersToRoles(string[] usernames, string[] rolenames)
        {
            //foreach (string rolename in rolenames)
            //{
            //    if (!RoleExists(rolename))
            //    {
            //        throw new ProviderException("Role name not found.");
            //    }
            //}

            //foreach (string username in usernames)
            //{
            //    if (username.IndexOf(',') > 0)
            //    {
            //        throw new ArgumentException("User names cannot contain commas.");
            //    }

            //    foreach (string rolename in rolenames)
            //    {
            //        if (IsUserInRole(username, rolename))
            //        {
            //            throw new ProviderException("User is already in role.");
            //        }
            //    }
            //}


            //OdbcConnection conn = new OdbcConnection(connectionString);
            //OdbcCommand cmd = new OdbcCommand("INSERT INTO [" + usersInRolesTable + "]" +
            //        " (Username, Rolename, ApplicationName) " +
            //        " Values(?, ?, ?)", conn);

            //OdbcParameter userParm = cmd.Parameters.Add("@Username", OdbcType.VarChar, 255);
            //OdbcParameter roleParm = cmd.Parameters.Add("@Rolename", OdbcType.VarChar, 255);
            //cmd.Parameters.Add("@ApplicationName", OdbcType.VarChar, 255).Value = ApplicationName;

            //OdbcTransaction tran = null;

            //try
            //{
            //    conn.Open();
            //    tran = conn.BeginTransaction();
            //    cmd.Transaction = tran;

            //    foreach (string username in usernames)
            //    {
            //        foreach (string rolename in rolenames)
            //        {
            //            userParm.Value = username;
            //            roleParm.Value = rolename;
            //            cmd.ExecuteNonQuery();
            //        }
            //    }

            //    tran.Commit();
            //}
            //catch (OdbcException e)
            //{
            //    try
            //    {
            //        tran.Rollback();
            //    }
            //    catch { }


            //    if (WriteExceptionsToEventLog)
            //    {
            //        WriteToEventLog(e, "AddUsersToRoles");
            //    }
            //    else
            //    {
            //        throw e;
            //    }
            //}
            //finally
            //{
            //    conn.Close();
            //}
        }

        //
        // RoleProvider.RemoveUsersFromRoles
        //

        public override void RemoveUsersFromRoles(string[] usernames, string[] rolenames)
        {
            //foreach (string rolename in rolenames)
            //{
            //    if (!RoleExists(rolename))
            //    {
            //        throw new ProviderException("Role name not found.");
            //    }
            //}

            //foreach (string username in usernames)
            //{
            //    foreach (string rolename in rolenames)
            //    {
            //        if (!IsUserInRole(username, rolename))
            //        {
            //            throw new ProviderException("User is not in role.");
            //        }
            //    }
            //}


            //OdbcConnection conn = new OdbcConnection(connectionString);
            //OdbcCommand cmd = new OdbcCommand("DELETE FROM [" + usersInRolesTable + "]" +
            //        " WHERE Username = ? AND Rolename = ? AND ApplicationName = ?", conn);

            //OdbcParameter userParm = cmd.Parameters.Add("@Username", OdbcType.VarChar, 255);
            //OdbcParameter roleParm = cmd.Parameters.Add("@Rolename", OdbcType.VarChar, 255);
            //cmd.Parameters.Add("@ApplicationName", OdbcType.VarChar, 255).Value = ApplicationName;

            //OdbcTransaction tran = null;

            //try
            //{
            //    conn.Open();
            //    tran = conn.BeginTransaction();
            //    cmd.Transaction = tran;

            //    foreach (string username in usernames)
            //    {
            //        foreach (string rolename in rolenames)
            //        {
            //            userParm.Value = username;
            //            roleParm.Value = rolename;
            //            cmd.ExecuteNonQuery();
            //        }
            //    }

            //    tran.Commit();
            //}
            //catch (OdbcException e)
            //{
            //    try
            //    {
            //        tran.Rollback();
            //    }
            //    catch { }


            //    if (WriteExceptionsToEventLog)
            //    {
            //        WriteToEventLog(e, "RemoveUsersFromRoles");
            //    }
            //    else
            //    {
            //        throw e;
            //    }
            //}
            //finally
            //{
            //    conn.Close();
            //}
        }

        //
        // RoleProvider.GetUsersInRole
        //

        public override string[] GetUsersInRole(string rolename)
        {
            //string tmpUserNames = "";

            //OdbcConnection conn = new OdbcConnection(connectionString);
            //OdbcCommand cmd = new OdbcCommand("SELECT Username FROM [" + usersInRolesTable + "]" +
            //          " WHERE Rolename = ? AND ApplicationName = ?", conn);

            //cmd.Parameters.Add("@Rolename", OdbcType.VarChar, 255).Value = rolename;
            //cmd.Parameters.Add("@ApplicationName", OdbcType.VarChar, 255).Value = ApplicationName;

            //OdbcDataReader reader = null;

            //try
            //{
            //    conn.Open();

            //    reader = cmd.ExecuteReader();

            //    while (reader.Read())
            //    {
            //        tmpUserNames += reader.GetString(0) + ",";
            //    }
            //}
            //catch (OdbcException e)
            //{
            //    if (WriteExceptionsToEventLog)
            //    {
            //        WriteToEventLog(e, "GetUsersInRole");
            //    }
            //    else
            //    {
            //        throw e;
            //    }
            //}
            //finally
            //{
            //    if (reader != null) { reader.Close(); }
            //    conn.Close();
            //}

            //if (tmpUserNames.Length > 0)
            //{
            //    // Remove trailing comma.
            //    tmpUserNames = tmpUserNames.Substring(0, tmpUserNames.Length - 1);
            //    return tmpUserNames.Split(',');
            //}

            return new string[0];
        }

        #endregion //STILL NOT IMPLEMENTED METHODS
    }
}
