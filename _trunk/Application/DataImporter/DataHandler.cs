﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ADODB;

namespace CEI.DataImporter
{
    public class DataHandler
    {
        #region VARIABLE DECLARATIONS

        readonly string connString = System.Configuration.ConfigurationManager.ConnectionStrings["CEIDataImporterConnString"].ConnectionString;

        #endregion

        #region CONSTRUCTOR
        
        #endregion

        #region PUBLIC METHODS

        public void StartCollectingData()
        {
            using (OleDbConnection conn = Connection())
            {
                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.Connection = conn;
                    
                }
            }
        }

        #endregion

        #region PRIVATE METHODS

        private OleDbConnection Connection()
        {
            return new OleDbConnection(connString);
        }
        
        #endregion

    }
}
