﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class CPTCodeBR:BaseBR
    {
        #region DataTable

        public DataTable GetAllCPTCodes()
        {
            return new CPTCodeDA().GetAllCPTCodes();
        }

        #endregion //DataTable

        #region DB CUD methods

        public bool InsertCPTCode(CPTCode cptCode)
        {

            return new CPTCodeDA().InsertCPTCode(cptCode);

        }

        public bool UpdateCPTCode(CPTCode cptCode)
        {

            return new CPTCodeDA().UpdateCPTCode(cptCode);

        }

        public bool DeleteCPTCode(int id)
        {

            return new CPTCodeDA().DeleteCPTCode(id);

        }

        #endregion //DB CUD methods
    }
}
