using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class PracticeAppointmentsSettingsBR : BaseBR
    {
       
     
      


        #region Constants

        #endregion //Constants

       

        #region Get methods


     
       
        #endregion //Get methods

        #region Search methods


       

        #endregion //Search methods

        #region DataTable


        #endregion //DataTable

        #region DataRow

        public DataRow GetPracticeAppointmentSettingsByPracticeId(Guid practiceId)
        {
            DataTable dtPracticeAppointmentsSettings = new PracticeAppointmentsSettingsDA().GetPracticeAppointmentsSettingsByPracticeId(practiceId);
            if (dtPracticeAppointmentsSettings.Rows.Count == 1)
                return dtPracticeAppointmentsSettings.Rows[0];
            else return null;
        }

     

        #endregion //DataRow


        #region DB CUD methods


        public bool InsertPracticeAppointmentsSettings(PracticeAppointmentsSettings practiceAppointmentsSettings)
        {

            return new PracticeAppointmentsSettingsDA().InsertPracticeAppintmentsSettings(practiceAppointmentsSettings);
        }




        public bool UpdatePracticeAppointmentsSettings(PracticeAppointmentsSettings practiceAppointmentsSettings)
        {

            return new PracticeAppointmentsSettingsDA().UpdatePracticeAppintmentsSettings(practiceAppointmentsSettings);
        }



        public bool DeletePracticeAppointmentsSettings(Guid practiceId)
        {
            return new PracticeAppointmentsSettingsDA().DeletePracticeAppintmentsSettings(practiceId);
        }



      

        #endregion //DB CUD methods
        
       
       

        

       
    }
}
