﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class NoteTypesBR : BaseBR
    {
        #region DataTable

        
        public DataTable GetAllNoteTypes()
        {
            return new NoteTypesDA().GetAllNoteTypes();
        }

        
        #endregion //DataTable

        #region DB CUD methods
         

       

        #endregion //DB CUD methods
    }
}
