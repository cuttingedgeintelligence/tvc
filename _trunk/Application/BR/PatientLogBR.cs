using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class PatientLogBR : BaseBR
    {

       

        private int _totalCountPatientLogsSearch = 0;
        private int _totalCountCompletedPatientLogsSearch = 0;

        #region Get methods


        public bool ExistsPatientLogPatientWithInsuranceCompanyId(int insuranceCompanyId)
        {
            return new PatientLogPatientDA().ExistsPatientLogPatientWithInsuranceCompanyId(insuranceCompanyId);
        }

        public bool ExistsPatientLogPatientWithPatientId(int patientId)
        {
            return new PatientLogPatientDA().ExistsPatientLogPatientWithPatientId(patientId);
        }

        public List<PatientLogPatient> GetPatientLogPatientsByPatientLogId(int patientLogId)
        {
            return new PatientLogPatientDA().GetPatientLogPatientsByPatientLogId(patientLogId);
        }

        public DataTable GetPatientlodserviceDate_ByPracticeId(DateTime serviceDate, Guid practiceId, string appointmentTime)
        {
            return new PatientLogDA().GetPatientlodserviceDate_ByPracticeId(serviceDate, practiceId, appointmentTime);
        }
       
        #endregion //Get methods

        #region Search methods


        public DataTable GetPatientLogsSearch(Guid practiceId,PatientLogStatusEnum status, int pageSize, int startRowIndex)
        {
            return new PatientLogDA().GetPatientLogsSearch(practiceId,status, pageSize, startRowIndex, out _totalCountPatientLogsSearch);
        }



        public int GetPatientLogsSearchTotalCount(Guid practiceId,PatientLogStatusEnum status)
        {
            return _totalCountPatientLogsSearch;
        }


        public DataTable GetCompletedPatientLogsSearch(Guid practiceId, PatientLogStatusEnum status, int pageSize, int startRowIndex)
        {
            return new PatientLogDA().GetCompletedPatientLogsSearch(practiceId, status, pageSize, startRowIndex, out _totalCountCompletedPatientLogsSearch);
        }



        public int GetCompletedPatientLogsSearchTotalCount(Guid practiceId, PatientLogStatusEnum status)
        {
            return _totalCountCompletedPatientLogsSearch;
        }



        #endregion //Search methods

        #region DataTable

        public PatientLog GetPatientLogByPracticeIdAndServiceDate(DateTime serviceDate, Guid practiceId)
        {
          
           DataTable dtPatientLogWithPatients =  new PatientLogDA().GetPatientLogByPracticeIdAndServiceDate(serviceDate, practiceId);
           if (dtPatientLogWithPatients.Rows.Count > 0)
           {
               PatientLog patientLog = new PatientLog();
               //fill patientLog status
               patientLog.Id = (int)dtPatientLogWithPatients.Rows[0]["patientLogId"];
               patientLog.Status =  (PatientLogStatusEnum)Enum.Parse(typeof(PatientLogStatusEnum), dtPatientLogWithPatients.Rows[0]["patientLogStatus"].ToString());
               foreach (DataRow drPatient in dtPatientLogWithPatients.Rows)
               {
                   PatientLogPatient patientLogPatient = new PatientLogPatient();
                   patientLogPatient.Id = (int)drPatient["id"];
                   patientLogPatient.FullName = drPatient["fullName"].ToString();
                   patientLogPatient.FirstName = drPatient["firstName"].ToString();
                   patientLogPatient.LastName = drPatient["lastName"].ToString();
				   patientLogPatient.DateOfBirth = (DateTime)drPatient["dateOfBirth"];
                   patientLogPatient.Gender = drPatient["gender"].ToString();
                   patientLogPatient.PatientHistory = (PatientHistoryEnum)Enum.Parse(typeof(PatientHistoryEnum), drPatient["patientHistory"].ToString()); 
                   patientLogPatient.InsuranceCompanyId = (int)drPatient["insuranceCompanyId"];
                   patientLogPatient.InsuranceCompanyName = drPatient["insuranceCompanyName"].ToString();
                   patientLogPatient.PatientId = (int)drPatient["patientId"];
                   patientLogPatient.OD_Sphere = drPatient["OD_Sphere"].ToString();
                   patientLogPatient.OS_Sphere = drPatient["OS_Sphere"].ToString();
                   patientLogPatient.OD_Cylinder = drPatient["OD_Cylinder"].ToString();
                   patientLogPatient.OS_Cylinder = drPatient["OS_Cylinder"].ToString();
                   patientLogPatient.OD_AxisOfAstigmatism = drPatient["OD_AxisOfAstigmatism"].ToString();
                   patientLogPatient.OS_AxisOfAstigmatism = drPatient["OS_AxisOfAstigmatism"].ToString();
                   if(drPatient["OD_Pachymetry"] != DBNull.Value)
                       patientLogPatient.OD_Pachymetry = (short)drPatient["OD_Pachymetry"];
                   if (drPatient["OS_Pachymetry"] != DBNull.Value)
                       patientLogPatient.OS_Pachymetry = (short)drPatient["OS_Pachymetry"];
                   patientLogPatient.AppointmentTime = drPatient["appointmentTime"].ToString();
                   patientLogPatient.Glaucoma = (bool)drPatient["glaucoma"];
                   patientLogPatient.Retina = (bool)drPatient["retina"];
                   patientLogPatient.HRT_Glaucoma =(decimal) drPatient["HRT_Glaucoma"];
                   patientLogPatient.HRT_Retina = (decimal)drPatient["HRT_Retina"];

                  

                   patientLog.PatientLogPatients.Add(patientLogPatient);
               }
               return patientLog;
           }
           else
               return null;
            
           
           
        }
       

         /// <summary>
        /// Get patient logs service dates(ordered desc) by practice Id 
        /// Used in: FTP functionality
        /// </summary>
        /// <param name="practiceId"></param>
        /// <returns>data table : serviceDate , patientLogId , desktopImportedDate , desktopImportedComment</returns>
        public DataTable GetPatientLogsServiceDatesByPracticeId(Guid practiceId)
        {
            return new PatientLogDA().GetPatientLogsServiceDatesByPracticeId(practiceId);
        }


        public DataTable GetTablesToInsertFromExcel()
        {
            return new PatientLogDA().GetTablesToInsertFromExcel();
        }


        #endregion //DataTable

        #region DataRow

        

        #endregion //DataRow


        #region DB CUD methods



        public DataTable GetCPTCodeIdForExcelImport(string CPTCodeName)
        {
            return new PatientLogDA().GetCPTCodeIdForExcelImport(CPTCodeName);
        }


        public bool InsertPatientLog(PatientLog patientLog)
        {
            return new PatientLogDA().InsertPatientLog(patientLog);
        }



        public bool InsertPatientLogFromExcel(FinalNotesWorksheet patientLog, DateTime nextAppointmentDate, DateTime retinaExamDate, DateTime glaucomaExamDate,int CPTCodeId)
        {
            return new PatientLogDA().InsertPatientLogFromExcel(patientLog, nextAppointmentDate, retinaExamDate, glaucomaExamDate,CPTCodeId);
        }

        

        

        

        

        public bool UpdatePatientLog(PatientLog patientLog)
        {
            return new PatientLogDA().UpdatePatientLog(patientLog);
        }


        public bool UpdatePatientLogPatient(PatientLogPatient patientLogPatient)
        {
            return new PatientLogPatientDA().UpdatePatientLogPatient(patientLogPatient);
        }

        public bool PatientLogSubmit(PatientLog patientLog)
        {
            return new PatientLogDA().PatientLogSubmit(patientLog);
        }


        public bool DeletePatientLog(int id)
        {
            return new PatientLogDA().DeletePatientLog(id);
        }

        public bool DeletePatientLogPatient(PatientLog patientLog)
        {
            return new PatientLogDA().DeletePatientLogPatient(patientLog);
        }


        /// <summary>
        /// Update patient log desktop imported data(desktopImportedDate and desktopImportedComment) 
        /// </summary>
        /// <param name="patientLogId"></param>
        /// <param name="desktopImportedDate"></param>
        /// <param name="desktopImportedComment"></param>
        /// <returns></returns>
        public bool UpdatePatientLogDesktopImportedData(int patientLogId, DateTime desktopImportedDate, string desktopImportedComment)
        {
            return new PatientLogDA().UpdatePatientLogDesktopImportedData(patientLogId, desktopImportedDate, desktopImportedComment);
        }

        /// <summary>
        /// Scheduled method. Submit automatically patient logs  every day .
        /// </summary>
        /// <returns></returns>
        public bool PatientLogsSubmission()
        {
            bool returnValue = true;
            try
            {
                //get all patient logs for submission :  deadlineDate[today.AddDays(numberOfDaysToSubmitPatientLogBeforeServiceDate)]   >=  serviceDateand patientLogStatus == PatientLogStatusEnum.Patient__log
                DataTable dtPatientLogsForSumbmission = new PatientLogDA().GetPatientLogsForSubmission(DateTime.Today.AddDays(int.Parse(ConfigurationManager.AppSettings["numberOfDaysToSubmitPatientLogBeforeServiceDate"])), PatientLogStatusEnum.Patient__log);
                if (dtPatientLogsForSumbmission.Rows.Count > 0)
                {
                    foreach (DataRow drPatientLog in dtPatientLogsForSumbmission.Rows)
                    {
                        try
                        {
                            //change status to PatientLogStatusEnum.Pending__patient__log
                            PatientLog patientLog = new PatientLog();
                            patientLog.Id = (int)drPatientLog["id"];
                            patientLog.Status = PatientLogStatusEnum.Pending__patient__log;
                            if (new PatientLogDA().PatientLogSubmit(patientLog))
                            {

                                this.SendMailPatientLogSubmit(ConfigurationManager.AppSettings["mailSenderNamePatientLogSubmit"], ConfigurationManager.AppSettings["mailFromPatientLogSubmit"], ConfigurationManager.AppSettings["mailSubjectPatientLogSubmit"], ((DateTime)drPatientLog["serviceDate"]).ToString("MM/dd/yyyy"), drPatientLog["practiceName"].ToString(), "/PatientLogs/ViewCompletedPatientLog.aspx", patientLog.Id.ToString());

                            }
                        }
                        catch (Exception exc)
                        {
                            returnValue = false;
                        }
                    }

                }
            }
            catch
            {
                returnValue = false;
            }

            return returnValue;
               
        }
      

        #endregion //DB CUD methods


        #region Send mail

        public void SendMailPatientLogSubmit(string senderName,string senderEmail,string subject,string patientLogServiceDate,string patientLogPracticeName, string url, string logId)
        {
            DataTable dtAdmins = new UserDA().GetAllAdmins();
            DataTable dtOfficeManagers = new UserDA().GetAllOfficeManagers();

            string[] sendTo = new string[dtAdmins.Rows.Count + dtOfficeManagers.Rows.Count];
            int i = 0;
            foreach (DataRow drAdmin in dtAdmins.Rows)
            {
                sendTo[i] = drAdmin["email"].ToString();
                i++;
            }

            foreach (DataRow drOfficeManager in dtOfficeManagers.Rows)
            {
                sendTo[i] = drOfficeManager["email"].ToString();
                i++;
            }

            //string[] sendTo = new string[1];
            //sendTo[0] = "office@totalvisioncare.net";

            MailerBR.SendMailPatientLogSubmit(sendTo, senderName, senderEmail, subject, patientLogServiceDate, patientLogPracticeName, url, logId);

        }

        #endregion //Send mail




    }
}
