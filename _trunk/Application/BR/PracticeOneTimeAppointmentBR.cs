using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class PracticeOneTimeAppointmentBR : BaseBR
    {


        private int _totalCountPracticeOneTimeAppointmentsSearch = 0;


        #region Constants

        #endregion //Constants

       

        #region Get methods

        public bool ExistsOneTimeAppointmentWithPracticeIdAndAppointmentDate(Guid practiceId, DateTime appointmentDate)
        {
            return new PracticeOneTimeAppointmentDA().ExistsOneTimeAppointmentWithPracticeIdAndAppointmentDate(practiceId, appointmentDate);
        }
     
       
        #endregion //Get methods

        #region Search methods

        public DataTable GetPracticeOneTimeAppointmentsSearch(Guid practiceId, int pageSize, int startRowIndex)
        {
            return new PracticeOneTimeAppointmentDA().GetPracticeOneTimeAppointmentsSearch(practiceId, pageSize, startRowIndex, out _totalCountPracticeOneTimeAppointmentsSearch);
        }

        public int GetPracticeOneTimeAppointmentsSearchTotalCount(Guid practiceId)
        {
            return _totalCountPracticeOneTimeAppointmentsSearch;
        }

       

        #endregion //Search methods

        #region DataTable

        /// <summary>
        /// Get top numberOfAppointments by practiceId with appointmentDate greater than currentDate
        /// </summary>
        /// <param name="practiceId"></param>
        /// <param name="currentDate"></param>
        /// <param name="numberOfAppointments"></param>
        /// <returns></returns>
        public DataTable GetOneTimeAppointmentsByPracticeId(Guid practiceId, DateTime currentDate, int numberOfAppointments)
        {
            return new PracticeOneTimeAppointmentDA().GetOneTimeAppointmentsByPracticeId(practiceId, currentDate, numberOfAppointments);
        }

        #endregion //DataTable

        #region DataRow

      

     

        #endregion //DataRow


        #region DB CUD methods


        public bool InsertPracticeOneTimeAppointment(PracticeOneTimeAppointment practiceOneTimeAppointment)
        {
            return new PracticeOneTimeAppointmentDA().InsertPracticeOneTimeAppointment(practiceOneTimeAppointment);
        }



        public bool DeletePracticeOneTimeAppointment(int id)
        {
            return new PracticeOneTimeAppointmentDA().DeletePracticeOneTimeAppointment(id);
        }



      

        #endregion //DB CUD methods
        
       
       

        

       
    }
}
