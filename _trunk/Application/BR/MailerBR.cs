using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using System.Web;
using System.Data.SqlClient;

using CEI.Common;
using CEI.DA;
using CEI.Framework.Mailer;
using System.Data;





namespace CEI.BR
{
    public class MailerBR
    {


	    public static void SendMailForgotPassword(string sendTo,string receiverName,string senderName, string senderEmail,string password,string subject)
        {
            if (bool.Parse(SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.SendMailsFromApplication)["setupValue"].ToString()))
            {
                DataRowCollection  systemSetupValues = SystemSetupBR.GetAllSystemSetupValues().Rows;
                
                CeiMailer mailer = new CeiMailer(systemSetupValues.Find(SystemSetupEnum.MailDomain)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailUsername)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailPassword)["setupValue"].ToString(), bool.Parse(systemSetupValues.Find(SystemSetupEnum.UseMailCredentials)["setupValue"].ToString()), int.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpHostPort)["setupValue"].ToString()), bool.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpEnableSsl)["setupValue"].ToString()));

                mailer.From(senderEmail, senderName);
                mailer.To(sendTo);

                mailer.Subject(subject);
                string _body = ReadMailTemplateForgotPassword();

                _body = _body.Replace("#name#", receiverName);
                _body = _body.Replace("#password#", password);
                
                mailer.Body(_body);
                mailer.SendAsync();
            }
        }


        public static void SendMailPatientLogSubmit(string[] sendTo, string senderName, string senderEmail, string subject, string serviceDate, string practiceName, string url, string logId)
        {
          
            //if (bool.Parse(SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.SendMailsFromApplication)["setupValue"].ToString()))
            //{
            //    DataRowCollection systemSetupValues = SystemSetupBR.GetAllSystemSetupValues().Rows;
            //    CeiMailer mailer = new CeiMailer(systemSetupValues.Find(SystemSetupEnum.MailDomain)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailUsername)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailPassword)["setupValue"].ToString(), bool.Parse(systemSetupValues.Find(SystemSetupEnum.UseMailCredentials)["setupValue"].ToString()), int.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpHostPort)["setupValue"].ToString()), bool.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpEnableSsl)["setupValue"].ToString()));

            //    mailer.From(senderEmail, senderName);
            //    mailer.To(sendTo);

            //    mailer.Subject(subject);
         
            //    mailer.Body("<p>Patient log is submitted:</p><p>Practice name: " + practiceName + "</p><p>Service date: " + serviceDate + "</p>");

                
            //    mailer.SendSync();
                
            //}

            if (bool.Parse(SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.SendMailsFromApplication)["setupValue"].ToString()))
            {
                DataRowCollection systemSetupValues = SystemSetupBR.GetAllSystemSetupValues().Rows;
                CeiMailer mailer = new CeiMailer(systemSetupValues.Find(SystemSetupEnum.MailDomain)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailUsername)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailPassword)["setupValue"].ToString(), bool.Parse(systemSetupValues.Find(SystemSetupEnum.UseMailCredentials)["setupValue"].ToString()), int.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpHostPort)["setupValue"].ToString()), bool.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpEnableSsl)["setupValue"].ToString()));

               mailer.From(senderEmail, senderName);
               mailer.To(sendTo);

                //mailer.Subject(subject);

             //   mailer.Body("<p>Patient log is submitted:</p><p>Practice name: " + practiceName + "</p><p>Service date: " + serviceDate + "</p>");

               // mailer.From("office@totalvisioncare.net", "Admin TVC");
               //string link = "http://" + HttpContext.Current.Request.Url.Host + url;
               string link = "http://" + HttpContext.Current.Request.Url.Host + url + "?logId=" + logId + "&prName=" + practiceName + "&servDate=" + serviceDate + "";
               link = link.Replace(" ", "%20");
               // mailer.To("tekikmarina@gmail.com");
               mailer.Subject("New Patient Schedule Generated  for " + practiceName + " for " + serviceDate + "");
               mailer.Body("<p>Hello TVC Admin,</p><p>A new Patient Schedule has been generated  for " + practiceName + " for " + serviceDate + ".</p><p>&nbsp;</p><p>To view the Patient Schedule please click <a  href=" + link + " >here</a></p><p>Login may be needed.</p><p>&nbsp;</p><p>Your friend,</p><p>TVC Engine.</p>");




                mailer.SendSync();

            }
        }



        public static void SendMailWorksheetSubmit(string[] sendTo, string senderName, string senderEmail, string subject, string serviceDate, string practiceName,string technicianName, string url, string logId)
        {
            //TODO: uncomment this section in order to send mails
            //if (bool.Parse(SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.SendMailsFromApplication)["setupValue"].ToString()))
            //{
            //    DataRowCollection systemSetupValues = SystemSetupBR.GetAllSystemSetupValues().Rows;
            //    CeiMailer mailer = new CeiMailer(systemSetupValues.Find(SystemSetupEnum.MailDomain)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailUsername)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailPassword)["setupValue"].ToString(), bool.Parse(systemSetupValues.Find(SystemSetupEnum.UseMailCredentials)["setupValue"].ToString()), int.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpHostPort)["setupValue"].ToString()), bool.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpEnableSsl)["setupValue"].ToString()));

            //    mailer.From(senderEmail, senderName);
            //    mailer.To(sendTo);

            //    mailer.Subject(subject);
            //    string _body = ReadMailTemplateWorksheet();

            //    _body = _body.Replace("#practicename#", practiceName);
            //    _body = _body.Replace("#servicedate#", serviceDate);
            //    _body = _body.Replace("#technicianname#", technicianName);


            //    mailer.Body(_body);
            //    mailer.SendAsync();
            //}

           // TODO: uncomment this section in order to send mails
            if (bool.Parse(SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.SendMailsFromApplication)["setupValue"].ToString()))
            {
                DataRowCollection systemSetupValues = SystemSetupBR.GetAllSystemSetupValues().Rows;
                CeiMailer mailer = new CeiMailer(systemSetupValues.Find(SystemSetupEnum.MailDomain)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailUsername)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailPassword)["setupValue"].ToString(), bool.Parse(systemSetupValues.Find(SystemSetupEnum.UseMailCredentials)["setupValue"].ToString()), int.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpHostPort)["setupValue"].ToString()), bool.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpEnableSsl)["setupValue"].ToString()));

                 mailer.From("office@totalvisioncare.net", "Admin TVC");
                //mailer.From(senderEmail, senderName);
                // mailer.Subject(subject);
                 string link = "http://" + HttpContext.Current.Request.Url.Host + url + "?logId=" + logId + "&prName=" + practiceName + "&servDate=" + serviceDate + "";
                 link = link.Replace(" ", "%20");
                //mailer.To("tekikmarina@gmail.com");
                 mailer.To(sendTo);
                mailer.Subject("New Worksheet for " + practiceName + " for " + serviceDate + "");
                mailer.Body("<p>Hello,</p><p>A new Worksheet has been generated for " + practiceName + " for " + serviceDate + ".</p><p>&nbsp;</p><p>To view the Worksheet please click <a  href=" + link + " >here</a></p><p>Login may be needed.</p><p>&nbsp;</p><p>Your friend,</p><p>TVC Engine.</p>");
                mailer.SendSync();

            }
        }

        public static void SendMailFinalNotesWorksheetSubmit(string[] sendTo, string senderName, string senderEmail, string subject, string serviceDate, string practiceName, string logId, string url)
        {
            //TODO: uncomment this section in order to send mails
            //if (bool.Parse(SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.SendMailsFromApplication)["setupValue"].ToString()))
            //{
            //    DataRowCollection systemSetupValues = SystemSetupBR.GetAllSystemSetupValues().Rows;
            //    CeiMailer mailer = new CeiMailer(systemSetupValues.Find(SystemSetupEnum.MailDomain)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailUsername)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailPassword)["setupValue"].ToString(), bool.Parse(systemSetupValues.Find(SystemSetupEnum.UseMailCredentials)["setupValue"].ToString()), int.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpHostPort)["setupValue"].ToString()), bool.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpEnableSsl)["setupValue"].ToString()));

            //    mailer.From(senderEmail, senderName);
            //    mailer.To(sendTo);

            //    mailer.Subject(subject);
            //    string _body = ReadMailTemplateFinalNotesWorksheet();

            //    _body = _body.Replace("#practicename#", practiceName);
            //    _body = _body.Replace("#servicedate#", serviceDate);
            //    _body = _body.Replace("#technicianname#", senderName);


            //    mailer.Body(_body);
            //    mailer.SendAsync();
            //}

            if (bool.Parse(SystemSetupBR.GetAllSystemSetupValues().Rows.Find(SystemSetupEnum.SendMailsFromApplication)["setupValue"].ToString()))
            {
                DataRowCollection systemSetupValues = SystemSetupBR.GetAllSystemSetupValues().Rows;
                CeiMailer mailer = new CeiMailer(systemSetupValues.Find(SystemSetupEnum.MailDomain)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailUsername)["setupValue"].ToString(), systemSetupValues.Find(SystemSetupEnum.MailPassword)["setupValue"].ToString(), bool.Parse(systemSetupValues.Find(SystemSetupEnum.UseMailCredentials)["setupValue"].ToString()), int.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpHostPort)["setupValue"].ToString()), bool.Parse(systemSetupValues.Find(SystemSetupEnum.SmtpEnableSsl)["setupValue"].ToString()));
                //mailer.From(senderEmail, senderName);
                mailer.From("office@totalvisioncare.net", "Admin TVC");
                // mailer.Subject(subject);
                //mailer.To("tekikmarina@gmail.com");
                mailer.To(sendTo);
                // string link="~/FinalNotesWorksheets/ViewCompletedFinalNotesWorksheet.aspx?logId=" + logId + "&prName=" + practiceName + "&servDate=" + serviceDate + "";
                // lblPermalink.Text = "http://" + Request.Url.Host + ResolveUrl("~/Blog/" + blogPost.DateCreated.ToLocalTime().ToString(@"yyyy\/MM\/dd") + "/" + blogPost.TitleUrlRewrite);
                // string link = "http://" + HttpContext.Current.Request.Url.Host + "/FinalNotesWorksheets/ViewCompletedFinalNotesWorksheet.aspx?logId=" + logId + "&prName=" + practiceName + "&servDate=" + serviceDate+"";
                string link = "http://" + HttpContext.Current.Request.Url.Host + url + "?logId=" + logId + "&prName=" + practiceName + "&servDate=" + serviceDate + "";
                link=link.Replace(" ", "%20");

                mailer.Subject("New Final Notes for  " + practiceName + " for " + serviceDate + "");
                mailer.Body("<p>Hello,</p><p>A new Final Notes has been generated for " + practiceName + " for " + serviceDate + " by " + senderName + " .</p><p>&nbsp;</p><p>To view the Final Notes please click <a  href=" + link + " >here</a></p><p>Login may be needed.</p><p>&nbsp;</p><p>Your friend,</p><p>TVC Engine.</p>");
                mailer.SendSync();

            }
        }

        #region private methods

      
        private static string ReadMailTemplateForgotPassword()
        {
            StreamReader objReader = new StreamReader(HttpContext.Current.Request.PhysicalApplicationPath + "//MailTemplates//ForgotPassword.htm");
            string mailTemplate = objReader.ReadToEnd();
            objReader.Close();
            return mailTemplate;
        }


        //private static string ReadMailTemplatePatientLog()
        //{
        //    StreamReader objReader = new StreamReader(HttpContext.Current.Request.PhysicalApplicationPath + "//MailTemplates//PatientLog.htm");
        //    string mailTemplate = objReader.ReadToEnd();
        //    objReader.Close();
        //    return mailTemplate;
        //}

        private static string ReadMailTemplateWorksheet()
        {
            StreamReader objReader = new StreamReader(HttpContext.Current.Request.PhysicalApplicationPath + "//MailTemplates//Worksheet.htm");
            string mailTemplate = objReader.ReadToEnd();
            objReader.Close();
            return mailTemplate;
        }

        private static string ReadMailTemplateFinalNotesWorksheet()
        {
            StreamReader objReader = new StreamReader(HttpContext.Current.Request.PhysicalApplicationPath + "//MailTemplates//FinalNotesWorksheet.htm");
            string mailTemplate = objReader.ReadToEnd();
            objReader.Close();
            return mailTemplate;
        }


		

        #endregion //private methods


    }
}
