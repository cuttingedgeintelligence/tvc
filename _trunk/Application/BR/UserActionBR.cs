﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CEI.Common;
using CEI.DA;
using System.Data;

namespace CEI.BR
{
    public class UserActionBR : BaseBR
    {
        #region VARIABLE DECLARATIONS

        private int _totalCountManageAdminsSearch = 0;

        #endregion

        #region DELEGATES

        delegate void AsyncUpdateUserLogout(int id, Guid userId, DateTime logoutTime);

        #endregion

        #region CONSTRUCTOR

        public UserActionBR()
        {

        }

        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// Inserts the login of the user in the UserActions table
        /// </summary>
        /// <param name="userId">UserID that is logging in</param>
        /// <param name="loginDateTime">The date time of the logging</param>
        /// <returns>UserAction the complete login action of the user</returns>
        public UserAction InsertUserLogin(Guid userId, DateTime loginDateTime)
        {
            UserAction _retVal = new UserAction();
            _retVal.UserId = userId;
            _retVal.LoginDateTime = loginDateTime;
            _retVal.Id = new UserActionDA().InsertLoginUser(userId, loginDateTime);

            return _retVal;
        }

        /// <summary>
        /// Updates the logout of the user that was logged in.
        /// This methos calls the the DA with async delegate, without callback reslut. It is fire and forget action.
        /// </summary>
        /// <param name="id">The Id of the UserActions table returned in the login process</param>
        /// <param name="userId">UserID that is logging out.</param>
        /// <param name="logoutTime">DateTime of the logging out.</param>
        public void UpdateUserLogout(UserAction userAction)
        {
            AsyncUpdateUserLogout _delegate = new AsyncUpdateUserLogout(new UserActionDA().UpdateLogoutUser);

            _delegate.BeginInvoke(userAction.Id, userAction.UserId, userAction.LogoutDateTime, null, null);
        }

        public DataTable GetUserActionsForUserIDSearch(string userId, DateTime dateFrom, DateTime dateTo, int pageSize, int startRowIndex)
        {
            Guid _userID = new Guid(userId);
            return new UserActionDA().GetUserActionsForUserIDSearch(_userID, dateFrom, dateTo, pageSize, startRowIndex, out _totalCountManageAdminsSearch);
        }


        public int GeUserActionsForUserIDTotalCount(string userId, DateTime dateFrom, DateTime dateTo)
        {
            return _totalCountManageAdminsSearch;
        }


        #endregion
    }
}
