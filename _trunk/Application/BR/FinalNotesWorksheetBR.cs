using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class FinalNotesWorksheetBR : BaseBR
    {
        private int _totalCountFinalNotesWorksheetsSearch = 0;
        private int _totalCountFollowUpsSearch = 0;
        private int _totalCountCompletedFinalNotesWorksheetsSearch = 0;
        
        #region Get methods


        public List<FinalNotesWorksheetPatient> GetFinalNotesWorksheetPatientsByPatientLogId(int patientLogId)
        {
            return new FinalNotesWorksheetPatientDA().GetFinalNotesWorksheetPatientsByPatientLogId(patientLogId);
        }
       
        #endregion //Get methods

        #region Search methods


        public DataTable GetFinalNotesWorksheetsSearch(Guid technicianId,Guid practiceId,PatientLogStatusEnum status, int pageSize, int startRowIndex)
        {
            return new FinalNotesWorksheetDA().GetFinalNotesWorksheetsSearch(technicianId, practiceId, status, pageSize, startRowIndex, out _totalCountFinalNotesWorksheetsSearch);
        }


        public int GetFinalNotesWorksheetsSearchTotalCount(Guid technicianId,Guid practiceId, PatientLogStatusEnum status)
        {
            return _totalCountFinalNotesWorksheetsSearch;
        }





        public DataTable GetCompletedFinalNotesWorksheetsSearch(Guid technicianId, Guid practiceId, PatientLogStatusEnum status, int pageSize, int startRowIndex)
        {
            return new FinalNotesWorksheetDA().GetCompletedFinalNotesWorksheetsSearch(technicianId, practiceId, status, pageSize, startRowIndex, out _totalCountCompletedFinalNotesWorksheetsSearch);
        }


        public int GetCompletedFinalNotesWorksheetsSearchTotalCount(Guid technicianId, Guid practiceId, PatientLogStatusEnum status)
        {
            return _totalCountCompletedFinalNotesWorksheetsSearch;
        }

        public DataTable GetFollowUpsSearch(Guid practiceId, int pageSize, int startRowIndex)
        {
            return new FinalNotesWorksheetDA().GetFollowUpsSearch(practiceId, pageSize,startRowIndex, out _totalCountFollowUpsSearch);
        }

        public int GetFollowUpsSearchTotalCount(Guid practiceId)
        {
            return _totalCountFollowUpsSearch;
        }

        #endregion //Search methods

        #region DataTable

        public DataTable GetFollowUpPatientsByFollowUpId(int followUpId)
        {
            return new FinalNotesWorksheetDA().GetFollowUpPatientsByFollowUpId(followUpId);
        }

        /// <summary>
        ///  get follow up patients for the next appointment date
        /// </summary>
        /// <param name="practiceId"></param>
        /// <param name="retinaExamDate"></param>
        /// <param name="glaucomaExamDate"></param>
        /// <returns></returns>
        public DataTable GetFollowUpPatientsForNextAppointmentDate(Guid practiceId, DateTime retinaExamDate, DateTime glaucomaExamDate)
        {
            return new FinalNotesWorksheetDA().GetFollowUpPatientsForNextAppointmentDate(practiceId, retinaExamDate, glaucomaExamDate);
        }

        /// <summary>
        /// Get monthly report
        /// </summary>
        /// <param name="practiceId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable GetMonthlyReportByPracticeIdAndYear(Guid practiceId, DateTime year)
        {
            return new FinalNotesWorksheetDA().GetMonthlyReportByPracticeIdAndYear(practiceId, year);
        }

        /// <summary>
        /// Get year to year report
        /// </summary> 
        /// <param name="patientLogStatus"></param>
        /// <param name="patientLogPatientStatus"></param>
        /// <returns></returns>
        public DataTable GetYearToYearReport(PatientLogStatusEnum patientLogStatus, PatientLogPatientStatusEnum patientLogPatientStatus)
        {
            return new FinalNotesWorksheetDA().GetYearToYearReport(patientLogStatus, patientLogPatientStatus);
        }


        /// <summary>
        /// Get patients history by patient log id
        /// </summary>
        /// <param name="patientLogId"></param>
        /// <param name="patientLogStatus"></param>
        /// <returns>Data table of patients history data [ patient name, service date... ]</returns>
        public DataTable GetPatientsHistoryByPatientLogId(int patientLogId, PatientLogStatusEnum patientLogStatus)
        {
            return new FinalNotesWorksheetDA().GetPatientsHistoryByPatientLogId(patientLogId, patientLogStatus);
        }


        /// <summary>
        /// Get patient history by patient id
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="patientLogStatus"></param>
        /// <returns>Data table of patient history data [  service date,practice notes , exam results etc... ]</returns>
        public DataTable GetPatientHistoryByPatientId(int patientId, PatientLogStatusEnum patientLogStatus)
        {
            return new FinalNotesWorksheetDA().GetPatientHistoryByPatientId(patientId, patientLogStatus);
        }

        #endregion //DataTable


        /// <summary>
        /// Get per practice report
        /// </summary>
        /// <param name="practiceId"></param>
        /// <returns>data table </returns>
        public DataTable GetPerPracticeReport(Guid practiceId)
        {
            return new FinalNotesWorksheetDA().GetPerPracticeReport(practiceId);
        }

        #region DataRow

      

        #endregion //DataRow

        #region DB CUD methods


        public bool UpdateFinalNotesWorksheet(FinalNotesWorksheet worksheet)
        {
            return new FinalNotesWorksheetDA().UpdateFinalNotesWorksheet(worksheet);
        }

        public bool UpdateFinalNotesWorksheetStatus(int patientLogId, List<FinalNotesWorksheetPatient> patientsList, PatientLogStatusEnum status, bool createFollowUp, DateTime nextAppointmentDate, DateTime retinaExamDate, DateTime glaucomaExamDate, Guid practiceId, Guid createdBy, Guid completedById, DateTime completedOn)
        {
            return new FinalNotesWorksheetDA().UpdateFinalNotesWorksheetStatus(patientLogId, patientsList, status, createFollowUp, nextAppointmentDate, retinaExamDate, glaucomaExamDate, practiceId, createdBy, completedById, completedOn);
        }


        #endregion //DB CUD methods

        #region Send mail

        public void SendMailFinalNotesWorksheetSubmit(string senderName, string senderEmail, string subject, string patientLogServiceDate, string patientLogPracticeName, Guid practiceId, string logId, string url)
        {
            DataTable dtAdmins = new UserDA().GetAllAdmins();
            DataTable dtOfficeManagers = new UserDA().GetAllOfficeManagers();

            string[] sendTo = new string[dtAdmins.Rows.Count + dtOfficeManagers.Rows.Count + 1];
            int i = 0;
            foreach (DataRow drAdmin in dtAdmins.Rows)
            {
                sendTo[i] = drAdmin["email"].ToString();
                i++;
            }

            foreach (DataRow drOfficeManager in dtOfficeManagers.Rows)
            {
                sendTo[i] = drOfficeManager["email"].ToString();
                i++;
            }

            sendTo[i] = (new UserDA().GetPracticeById(practiceId)).Rows[0]["email"].ToString();

            //string[] sendTo = new string[1];
            //sendTo[0] = "office@totalvisioncare.net";

            MailerBR.SendMailFinalNotesWorksheetSubmit(sendTo, senderName, senderEmail, subject, patientLogServiceDate, patientLogPracticeName,logId, url);

        }

        #endregion //Send mail

        #region Excel Report

        //public string GetExcelReportContent(int patientLogId)
        //{
        //    List<FinalNotesWorksheetPatient> finalNotesWorksheetPatients = new FinalNotesWorksheetBR().GetFinalNotesWorksheetPatientsByPatientLogId(patientLogId);

        //    StringBuilder sb = new StringBuilder(null);
        //    sb.Append("Patient Name \t");
        //    sb.Append("BFA \t");
        //    sb.Append("HRT Glaucoma \t");
        //    sb.Append("HRT Retina \t");
        //    sb.Append("CPT \t");
        //    sb.Append("Status \t");
        //    if (!Roles.IsUserInRole(RoleEnum.Practice.ToString()))
        //    {
        //        sb.Append("Internal Notes \t");
        //    }
        //    sb.Append("Practice Notes \t");
        //    sb.Append("OD: IOP/BFA \t");
        //    sb.Append("OS: IOP/BFA \t\n");

        //    foreach (FinalNotesWorksheetPatient finalNotesWorksheetPatient in finalNotesWorksheetPatients)
        //    {
        //        sb.Append(finalNotesWorksheetPatient.FullName);
        //        sb.Append("\t");

        //        if (finalNotesWorksheetPatient.Glaucoma)
        //        {
        //            sb.Append("+\t");
        //            sb.Append("+\t");
        //        }
        //        else
        //        {
        //            sb.Append("-\t");
        //            sb.Append("-\t");
        //        }

        //        if (finalNotesWorksheetPatient.Retina)
        //            sb.Append("+\t");
        //        else
        //            sb.Append("-\t");

        //        sb.Append(finalNotesWorksheetPatient.CPT_Codes);
        //        sb.Append("\t");

        //        sb.Append(finalNotesWorksheetPatient.Status.ToString());
        //        sb.Append("\t");

        //        if (!Roles.IsUserInRole(RoleEnum.Practice.ToString()))
        //        {
        //            sb.Append(finalNotesWorksheetPatient.InternalNotes);
        //            sb.Append("\t");
        //        }

        //        sb.Append(finalNotesWorksheetPatient.PracticeNotes);
        //        sb.Append("\t");

        //        sb.Append(finalNotesWorksheetPatient.OD_IOP);
        //        sb.Append("/");
        //        sb.Append(finalNotesWorksheetPatient.OD_BFA);
        //        sb.Append("\t");

        //        sb.Append(finalNotesWorksheetPatient.OS_IOP);
        //        sb.Append("/");
        //        sb.Append(finalNotesWorksheetPatient.OS_BFA);
        //        sb.Append("\t");

        //        sb.Append("\n");
        //    }

        //    return sb.ToString();
        //}
        
        # endregion // Excel Report
    }
}
