﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class NoteBR : BaseBR  
    {
        private int _totalCountManageNotesSearch = 0;

        #region DB CUD methods

        public bool InsertNote(Note note, int noteTypeId)
        {

            return new NoteDA().InsertNote(note, noteTypeId);

        }

        public bool UpdateNote(Note note, int noteTypeId)
        {

            return new NoteDA().UpdateNote(note, noteTypeId);

        }

        public bool DeleteNote(int id,int noteTypeId)
        {

            return new NoteDA().DeleteNote(id, noteTypeId);

        }

        #endregion //DB CUD methods

        #region Search methods

        public DataTable GetNotesManageSearch(int categoryId, int pageSize, int startRowIndex, int noteTypeId)
        {
            return new NoteDA().GetNotesManageSearch(categoryId, pageSize, startRowIndex, noteTypeId,out _totalCountManageNotesSearch); 
        }

        public DataTable GetAllNotes(int noteTypeId)
        {
            return new NoteDA().GetAllNotes(noteTypeId); 
        }

        public int GetNotesManageSearchTotalCount(int categoryId, int noteTypeId)
        {
            return _totalCountManageNotesSearch;
        }

        #endregion // Search methods
    }
}
