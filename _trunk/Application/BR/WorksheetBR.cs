using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class WorksheetBR : BaseBR
    {

       

        private int _totalCountWorksheetsSearch = 0;
        private int _totalCountCompletedWorksheetsSearch = 0;
        

        #region Get methods


        public List<WorksheetPatient> GetWorksheetPatientsByPatientLogId(int patientLogId)
        {
            DataTable dtWorksheetPatients =  new WorksheetPatientDA().GetWorksheetPatientsByPatientLogId(patientLogId);
            List<WorksheetPatient> worksheetPatients = new List<WorksheetPatient>();
            int currentWorksheetPatientId = -1;
            foreach (DataRow drWorksheetPatient in dtWorksheetPatients.Rows)
            {

                if (currentWorksheetPatientId != (int)drWorksheetPatient["id"])
                {
                    WorksheetPatient worksheetPatient = new WorksheetPatient();
                    worksheetPatient.Id = (int)drWorksheetPatient["id"];
                    currentWorksheetPatientId = worksheetPatient.Id;

                    worksheetPatient.FullName = (string)drWorksheetPatient["fullName"];
                    worksheetPatient.PatientId = (int)drWorksheetPatient["patientId"];

                    worksheetPatient.Glaucoma = (bool)drWorksheetPatient["glaucoma"];
                    worksheetPatient.Retina = (bool)drWorksheetPatient["retina"];
                    worksheetPatient.InsuranceCompanyName = drWorksheetPatient["insuranceCompanyName"].ToString();

                    worksheetPatient.HRT_Glaucoma = (decimal) drWorksheetPatient["HRT_Glaucoma"];
                    worksheetPatient.HRT_Retina = (decimal)drWorksheetPatient["HRT_Retina"];
                    worksheetPatient.BFA = (decimal)drWorksheetPatient["BFA"];
                

                    worksheetPatient.CPT_CodesList = new List<CPTCode>();
                    if (drWorksheetPatient["CPT_CodeId"] != DBNull.Value)
                    {
                        CPTCode CPT_Code = new CPTCode();
                        CPT_Code.Id = (int)drWorksheetPatient["CPT_CodeId"];
                        CPT_Code.Code = (string)drWorksheetPatient["code"];
                        CPT_Code.PatientLogPatientCPTCodeId = (int)drWorksheetPatient["patientLogPatientCPTCodeId"];
                        CPT_Code.CodeType = (CPTCodeTypeEnum)Enum.Parse(typeof(CPTCodeTypeEnum), drWorksheetPatient["codeType"].ToString());
                        CPT_Code.ActionType = ActionTypeEnum.DoNothing;
                        worksheetPatient.CPT_CodesList.Add(CPT_Code);
                    }

                    worksheetPatients.Add(worksheetPatient);
                }

                else
                {
                    //find worksheet patient 
                    WorksheetPatient worksheetPatient = worksheetPatients.Find(delegate(WorksheetPatient worksheetPatientParam) { return worksheetPatientParam.Id == currentWorksheetPatientId; });
                    CPTCode CPT_Code = new CPTCode();
                    CPT_Code.Id = (int)drWorksheetPatient["CPT_CodeId"];
                    CPT_Code.Code = (string)drWorksheetPatient["code"];
                    CPT_Code.PatientLogPatientCPTCodeId = (int)drWorksheetPatient["patientLogPatientCPTCodeId"];
                    CPT_Code.CodeType = (CPTCodeTypeEnum)Enum.Parse(typeof(CPTCodeTypeEnum), drWorksheetPatient["codeType"].ToString());
                    CPT_Code.ActionType = ActionTypeEnum.DoNothing;
                    worksheetPatient.CPT_CodesList.Add(CPT_Code);


                }
                

          
            }

            return worksheetPatients;

        }

        /// <summary>
        /// Check if CPT Code is assigned to some patient in some worksheet document
        /// </summary>
        /// <param name="CPT_CodeId"></param>
        /// <returns></returns>
        public bool ExistsWorksheetPatientWithCPTCodeId(int CPT_CodeId)
        {
            return new WorksheetPatientDA().ExistsWorksheetPatientWithCPTCodeId(CPT_CodeId);
        }


        /// <summary>
        /// returns number of patients with assigned CPT codes(at least one) by patient log id
        /// </summary>
        /// <param name="patientLogId"></param>
        /// <returns></returns>
        public int GetCountOfPatientsWithAssignedCPTCodesByPatientLogId(int patientLogId)
        {
            return new WorksheetDA().GetCountOfPatientsWithAssignedCPTCodesByPatientLogId(patientLogId);
        }



        /// <summary>
        ///  Check if technicianId is assigned to some worksheet
        /// </summary>
        /// <param name="technicianId"></param>
        /// <returns></returns>
        public bool ExistsWorksheetWithTechnicianId(Guid technicianId)
        {
            return new WorksheetDA().ExistsWorksheetWithTechnicianId(technicianId);
        }


        #endregion //Get methods

        #region Search methods


        public DataTable GetWorksheetsSearch(Guid technicianId, Guid practiceId,PatientLogStatusEnum status, int pageSize, int startRowIndex)
        {
            return new WorksheetDA().GetWorksheetsSearch(technicianId,practiceId, status, pageSize, startRowIndex, out _totalCountWorksheetsSearch);
        }


        public int GetWorksheetsSearchTotalCount(Guid technicianId,Guid practiceId, PatientLogStatusEnum status)
        {
            return _totalCountWorksheetsSearch;
        }


        public DataTable GetCompletedWorksheetsSearch(Guid technicianId, Guid practiceId, PatientLogStatusEnum status, int pageSize, int startRowIndex)
        {
            return new WorksheetDA().GetCompletedWorksheetsSearch(technicianId,practiceId, status, pageSize, startRowIndex, out _totalCountCompletedWorksheetsSearch);
        }

        public int GetCompletedWorksheetsSearchTotalCount(Guid technicianId,Guid practiceId, PatientLogStatusEnum status)
        {
            return _totalCountCompletedWorksheetsSearch;
        }

        #endregion //Search methods

        #region DataTable


        #endregion //DataTable

        #region DataRow

      

        #endregion //DataRow


        #region DB CUD methods



        /// <summary>
        /// Set status of the pending patient log to "Worksheet" and assigns a technician to the log
        /// </summary>
        /// <param name="worksheet"></param>
        /// <returns></returns>
        public bool UpdateWorksheet(Worksheet worksheet)
        {
            return new WorksheetDA().UpdateWorksheet(worksheet);
        }


        public bool UpdateWorksheetPatients(List<WorksheetPatient> worksheetPatients)
        {
            return new WorksheetPatientDA().UpdateWorksheetPatients(worksheetPatients);
        }

      
      

        #endregion //DB CUD methods


        #region Send mail
        /// <summary>
        /// Sends email to all active admins and assigned technician on worksheet submit by the office manager
        /// </summary>
        /// <param name="senderName"></param>
        /// <param name="senderEmail"></param>
        /// <param name="subject"></param>
        /// <param name="patientLogServiceDate"></param>
        /// <param name="patientLogPracticeName"></param>
        /// <param name="technicianName"></param>
        /// <param name="technicianEmail"></param>
        public void SendMailWorksheetSubmit(string senderName, string senderEmail, string subject, string patientLogServiceDate, string patientLogPracticeName,string technicianName,string technicianEmail,string url, string logId)
        {
            DataTable dtAdmins = new UserDA().GetAllAdmins();

            string[] sendTo = new string[dtAdmins.Rows.Count + 1];
            int i = 0;
            foreach (DataRow drAdmin in dtAdmins.Rows)
            {
                sendTo[i] = drAdmin["email"].ToString();
                i++;
            }

            //add technician email to the list
            sendTo[i] = technicianEmail;

            //string[] sendTo = new string[2];
            //sendTo[0] = technicianEmail;
            //sendTo[1] = "office@totalvisioncare.net";

            MailerBR.SendMailWorksheetSubmit(sendTo, senderName, senderEmail, subject, patientLogServiceDate, patientLogPracticeName, technicianName, url, logId);

        }

        #endregion //Send mail

        

       
    }
}
