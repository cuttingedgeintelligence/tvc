﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CEI.DA;
using CEI.Common;
using System.Data;

namespace CEI.BR
{
    public class DoctorBR : BaseBR
    {
        private int _totalCountDoctors = 0;

        #region Constants

        #endregion //Constants

        #region Get methods

        public bool ExistsDoctorWithPracticeId(Guid practiceId)
        {
            return new DoctorDA().ExistsDoctorWithPracticeId(practiceId);
        }

        #endregion //Get methods



        #region DataTable

        public DataRow GetDoctorById(int id)
        {
            return new DoctorDA().GetDoctorById(id); 
        }

        public DataTable GetDoctorsByPracticeId(Guid practiceId, int pageSize, int startRowIndex)
        {
            return new DoctorDA().GetDoctorsByPracticeId(practiceId, pageSize, startRowIndex, out _totalCountDoctors);
        }

        public int GetDoctorsByPracticeIdTotalCount(Guid practiceId)
        {
            return _totalCountDoctors;
        }

        #endregion //DataTable

        #region DataRow
    
        #endregion //DataRow

        #region DB CUD methods

        public bool InsertDoctor(Doctor doctor)
        {
            return new DoctorDA().InsertDoctor(doctor);
        }

        public bool UpdateDoctor(Doctor doctor)
        {
            return new DoctorDA().UpdateDoctor(doctor);
        }

        public bool DeleteDoctor(int id)
        {
            return new DoctorDA().DeleteDoctor(id);
        }

        #endregion //DB CUD methods

    }
}
