using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class UserBR : BaseBR
    {
       
     
        private int _totalCountManageAdminsSearch = 0;
        private int _totalCountManageOfficeManagersSearch = 0;
        private int _totalCountManageTechniciansSearch = 0;
        private int _totalCountManagePracticesSearch = 0;
        


        #region Constants

        #endregion //Constants

       

        #region Get methods


        public bool ExistsUserWithEmail(string email)
        {
            return new UserDA().ExistsUserWithEmail(email);
        }

        public bool ExistsUserWithEmailOnUpdate(Guid id, string email)
        {
            return new UserDA().ExistsUserWithEmailOnUpdate(id, email);
        }

        public bool ExistsUserWithCountyId(int countyId)
        {
            return new UserDA().ExistsUserWithCountyId(countyId);
        }


          /// <summary>
        /// get all technicians with accountIsEnabled == true 
        /// </summary>
        /// <returns></returns>
        public DataTable GetAllTechnicians()
        {
            return new UserDA().GetAllTechnicians();
        }

        #endregion //Get methods

        #region Search methods


        public DataTable GetAdminsManageSearch(int roleId, string title, string firstName, string lastName, int pageSize, int startRowIndex)
        {
            return new UserDA().GetAdminsManageSearch(roleId, title, firstName, lastName, pageSize, startRowIndex, out _totalCountManageAdminsSearch);
        }

        public int GetAdminsManageSearchTotalCount(int roleId, string title, string firstName, string lastName)
        {
            return _totalCountManageAdminsSearch;
        }


        public DataTable GetOfficeManagersManageSearch(int roleId, string title, string firstName, string lastName, int pageSize, int startRowIndex)
        {
            return new UserDA().GetOfficeManagersManageSearch(roleId, title, firstName, lastName, pageSize, startRowIndex, out _totalCountManageOfficeManagersSearch);
        }

        public int GetOfficeManagersManageSearchTotalCount(int roleId, string title, string firstName, string lastName)
        {
            return _totalCountManageOfficeManagersSearch;
        }


        public DataTable GetTechniciansManageSearch(int roleId,int countyId, string title, string firstName, string lastName, int pageSize, int startRowIndex)
        {
            return new UserDA().GetTechniciansManageSearch(roleId, countyId, title, firstName, lastName, pageSize, startRowIndex, out _totalCountManageTechniciansSearch);
        }

        public int GetTechniciansManageSearchTotalCount(int roleId,int countyId, string title, string firstName, string lastName)
        {
            return _totalCountManageTechniciansSearch;
        }

        public DataTable GetPracticesManageSearch(int roleId, string name, string address, string zipCode, string city, int countyId, string sortExpression, int pageSize, int startRowIndex)
        {
            return new UserDA().GetPracticesManageSearch(roleId, name, address, zipCode, city, countyId, sortExpression, pageSize, startRowIndex, out _totalCountManagePracticesSearch);
        }

        public int GetPracticesManageSearchTotalCount(int roleId, string name, string address, string zipCode, string city, int countyId)
        {
            return _totalCountManagePracticesSearch;
        }

        public DataTable GetPatientsForFTPByPracticeIdandServiceDate(Guid practiceId, string dateServie)
        {
            return new UserDA().GetPatientsForFTPByPracticeIdandServiceDate(practiceId,dateServie);
     
        }

        #endregion //Search methods

        #region DataTable

        public DataTable GetAllPracticesNameAndId()
        {
            return new UserDA().GetAllPracticesNameAndId();
        }

        public DataTable GetAllUsers()
        {
            return new UserDA().GetAllUsers();
        }
        public DataTable GetAllFromExcelForImportPetient(DateTime serviceDate, string tableName)
        {
            return new UserDA().GetAllFromExcelForImportPetient(serviceDate, tableName);
        }

        public DataTable GetAllServiceDateFromExcelForImportPetient(string tableName)
        {
            return new UserDA().GetAllServiceDateFromExcelForImportPetient(tableName);
        }

        
      

        #endregion //DataTable


        #region DataSet

        /// <summary>
        /// Get practice with doctors 
        /// </summary>
        /// <param name="practiceId"></param>
        /// <returns>dataset table 0 - practice table 1 - doctors</returns>
        public DataSet GetPracticeByIdForProfilePage(Guid practiceId)
        {
            return new UserDA().GetPracticeByIdForProfilePage(practiceId);
        }

        #endregion //DataSet

        #region DataRow

        public DataRow GetUserByEmail(string email)
        {
            DataTable dtUser = new UserDA().GetUserByEmailAsDataTable(email);
            if (dtUser.Rows.Count == 1)
                return dtUser.Rows[0];
            else return null;
        }

        public DataRow GetAdminById(Guid userId)
        {
            
            DataTable dtAdmin = new UserDA().GetAdminById(userId);
            if (dtAdmin.Rows.Count == 1)
                return dtAdmin.Rows[0];
            else return null;
        }


        public DataRow GetOfficeManagerById(Guid userId)
        {
            DataTable dtOfficeManager = new UserDA().GetOfficeManagerById(userId);
            if (dtOfficeManager.Rows.Count == 1)
                return dtOfficeManager.Rows[0];
            else return null;
        }

        public DataRow GetTechnicianById(Guid userId)
        {
            DataTable dtTechnician = new UserDA().GetTechnicianById(userId);
            if (dtTechnician.Rows.Count == 1)
                return dtTechnician.Rows[0];
            else return null;
        }

        public DataRow GetPracticeById(Guid userId)
        {
            DataTable dtPractice = new UserDA().GetPracticeById(userId);
            if (dtPractice.Rows.Count == 1)
                return dtPractice.Rows[0];
            else return null;
        }



        #endregion //DataRow


        #region DB CUD methods


        public bool InsertPractice(User practice)
        {
            practice.Password = new EncryptDecrypt().Encrypt(practice.Password, string.Empty);
            return new UserDA().InsertPractice(practice);
        }

        public bool InsertPracticeWithDoctor(User practice, Doctor doctor)
        {
            practice.Password = new EncryptDecrypt().Encrypt(practice.Password, string.Empty);
            return new UserDA().InsertPracticeWithDoctor(practice, doctor);
        }

        public bool InsertAdmin(User admin)
        {
            admin.Password = new EncryptDecrypt().Encrypt(admin.Password, string.Empty);
            return new UserDA().InsertAdmin(admin);
        }

        public bool InsertOfficeManager(User officeManager)
        {
            officeManager.Password = new EncryptDecrypt().Encrypt(officeManager.Password, string.Empty);
            return new UserDA().InsertOfficeManager(officeManager);
        }

        public bool InsertTechnician(User technician)
        {
            technician.Password = new EncryptDecrypt().Encrypt(technician.Password, string.Empty);
            return new UserDA().InsertTechnician(technician);
        }


        public bool UpdateAdmin(User admin)
        {
            admin.Password = new EncryptDecrypt().Encrypt(admin.Password, string.Empty);
            return new UserDA().UpdateAdmin(admin);
        }

        public bool UpdatePatientLogPatients(List<string> listOfIdAndValue)
        {
            return new UserDA().UpdatePatientLogPatients(listOfIdAndValue);
        }

        public bool UpdateOfficeManager(User officeManager)
        {
            officeManager.Password = new EncryptDecrypt().Encrypt(officeManager.Password, string.Empty);
            return new UserDA().UpdateOfficeManager(officeManager);
        }


        public bool UpdateTechnician(User technician)
        {
            technician.Password = new EncryptDecrypt().Encrypt(technician.Password, string.Empty);
            return new UserDA().UpdateTechnician(technician);
        }

        public bool UpdatePractice(User practice)
        {
            practice.Password = new EncryptDecrypt().Encrypt(practice.Password, string.Empty);
            return new UserDA().UpdatePractice(practice);
        }

        public bool DeleteUser(Guid userId,RoleEnum role)
        {
            return new UserDA().DeleteUser(userId,role);
        }

        public bool DeleteTechnician(Guid userId)
        {
            return new UserDA().DeleteTechnician(userId);
        }

      

        #endregion //DB CUD methods
        
       
       

        

       
    }
}
