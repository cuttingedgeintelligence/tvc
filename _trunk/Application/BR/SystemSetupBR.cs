using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;

using CEI.Common;
using CEI.DA;

namespace CEI.BR
{
    public class SystemSetupBR : BaseBR 
    {
        

        public static DataTable GetAllSystemSetupValues()
        {
            return new SystemSetupDB().GetAllSystemSetupValues();
        }


        public bool UpdateSystemSetupValues(List<SystemSetup> systemSetupValues)
        {
            return new SystemSetupDB().UpdateSystemSetupValues(systemSetupValues);
        }
    }
}
