using System;
using System.Collections.Generic;
using System.Text;

using CEI.Common;
using CEI.DA;
using System.Data;

namespace CEI.BR
{
    public class NameValueBR: BaseBR
    {


        #region DataTable

        public DataTable GetAllStates()
		{
			return new NameValueDA().GetAllStates();
        }


        public DataTable GetAllCounties()
        {
            return new NameValueDA().GetAllCounties();
        }


        public DataTable GetAllCountries()
        {
            return new NameValueDA().GetAllCountries();
        }

        public DataTable GetAllInsuranceCompanies()
        {
            return new NameValueDA().GetAllInsuranceCompanies();
        }


        #endregion //DataTable


        #region CUD methods

        public bool InsertInsuranceCompany(NameValue insuranceCompany)
        {
            return new NameValueDA().InsertInsuranceCompany(insuranceCompany);
        }

        public bool UpdateInsuranceCompany(NameValue insuranceCompany)
        {
            return new NameValueDA().UpdateInsuranceCompany(insuranceCompany);
        }

        public bool DeleteInsuranceCompany(int insuranceCompanyId)
        {
            return new NameValueDA().DeleteInsuranceCompany(insuranceCompanyId);
        }


        public bool InsertCounty(NameValue county)
        {
            return new NameValueDA().InsertCounty(county);
        }

        public bool UpdateCounty(NameValue county)
        {
            return new NameValueDA().UpdateCounty(county);
        }

        public bool DeleteCounty(int countyId)
        {
            return new NameValueDA().DeleteCounty(countyId);
        }




        #endregion //CUD methods
     
    }
}
