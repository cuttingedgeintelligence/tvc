﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CEI.Common.PatientsNotes;
using CEI.DA;

namespace CEI.BR
{
    public class PatientsNotesBR<T> where T : PatientsNotes
    {

        #region CONSTRUCTOR

        public PatientsNotesBR()
        {
        }

        #endregion

        #region PUBLIC METHODS

        public List<T> GetPatientNotes(int id)
        {
            return new PatientsNotesDA<T>().GetPatientsNotes(id);
        }

        public bool InsertPatientsNotes(List<T> items, string customNote, string previewNote)
        {
            return new PatientsNotesDA<T>().InsertPatientsNotes(items, customNote, previewNote);
        }

        #endregion
    }
}
