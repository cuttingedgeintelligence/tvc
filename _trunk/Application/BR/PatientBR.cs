using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class PatientBR : BaseBR
    {

       

        private int _totalCountManagePatientsSearch = 0;
        

        #region Get methods

        public bool ExistsPatientWithInsuranceCompanyId(int insuranceCompanyId)
        {
            return new PatientDA().ExistsPatientWithInsuranceCompanyId(insuranceCompanyId);
        }

        public bool ExistsPatientWithPracticeId(Guid practiceId)
        {
            return new PatientDA().ExistsPatientWithPracticeId(practiceId);
        }

        public bool ExistsPatientWithDoctorId(int doctorId)
        {
            return new PatientDA().ExistsPatientWithDoctorId(doctorId);
        }

       
        #endregion //Get methods

        #region Search methods


        public DataTable GetPatientsManageSearch(Guid practiceId, string firstName, string lastName, string dateOfBirth, int pageSize, int startRowIndex)
        {
            return new PatientDA().GetPatientsManageSearch(practiceId,firstName,lastName, dateOfBirth, pageSize, startRowIndex, out _totalCountManagePatientsSearch);
        }

        public int GetPatientsManageSearchTotalCount(Guid practiceId, string firstName, string lastName, string dateOfBirth)
        {
            return _totalCountManagePatientsSearch;
        }


        #endregion //Search methods

        #region DataTable


        public DataTable GetPatientsByPracticeId(Guid practiceId)
        {
            return new PatientDA().GetPatientsByPracticeId(practiceId);
        }

        #endregion //DataTable

        #region DataRow

        public DataRow GetPatientById(int id)
        {
            DataTable dtPatient = new PatientDA().GetPatientById(id);
            if (dtPatient.Rows.Count == 1)
                return dtPatient.Rows[0];
            else return null;
        }


        public DataRow GetPatientByIdForProfilePage(int id)
        {
            DataTable dtPatient = new PatientDA().GetPatientByIdForProfilePage(id);
            if (dtPatient.Rows.Count == 1)
                return dtPatient.Rows[0];
            else return null;
        }

        #endregion //DataRow


        #region DB CUD methods


        public bool InsertPatient(Patient patient)
        {

            return new PatientDA().InsertPatient(patient);
        }

        public bool UpdatePatient(Patient patient)
        {

            return new PatientDA().UpdatePatient(patient);
        }
        public bool UpdatePatientInsurance(int patientId,int insuranceId)
        {

            return new PatientDA().UpdatePatientInsurance(patientId,insuranceId);
        }
        public bool UpdatePatient(int patientId, int doctorId)
        {

            return new PatientDA().UpdatePatientDoctor(patientId,doctorId);
        }
        public bool DeletePatient(int id)
        {
            return new PatientDA().DeletePatient(id);
        }



      

        #endregion //DB CUD methods
        
       
       

        

       
    }
}
