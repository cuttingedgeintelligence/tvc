﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Data;
using System.Web.Caching;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

using CEI.Framework.Encryption;
using CEI.DA;
using CEI.Common;
using CEI.Framework;

namespace CEI.BR
{
    public class NoteCategoryBR : BaseBR
    {
        #region DataTable

        public DataTable GetAllNoteCategories(int noteTypeId)
        {
            return new NoteCategoryDA().GetAllNoteCategories(noteTypeId);
        }
        public DataTable GetNoteCategoriesById(int categoryId)
        {
            return new NoteCategoryDA().GetNoteCategoriesById(categoryId);
        }


              
        #endregion //DataTable

        #region DB CUD methods

        public bool InsertNoteCategory(NoteCategory noteCategory, int noteTypeId)
        {

            return new NoteCategoryDA().InsertNoteCategory(noteCategory, noteTypeId);

        }

        public bool UpdateNoteCategory(NoteCategory noteCategory, int noteTypeId)
        {

            return new NoteCategoryDA().UpdateNoteCategory(noteCategory, noteTypeId);

        }

        public bool DeleteNoteCategory(int id)
        {

            return new NoteCategoryDA().DeleteNoteCategory(id);

        }

        #endregion //DB CUD methods
    }
}
